from enum import Enum


class MetaheuristicType(Enum):
    """
    This Enum class is there to represent the 7 different MetaheuristicType that can
    be used for optimising the factory.
    Including an 8th type which stand for that no MetaheuristicType is chosen, so far.
    """
    TABU_SEARCH = 0
    SIMULATED_ANNEALING = 1
    THRESHOLD_ACCEPTING = 2
    GENETIC_ALGORITHM = 4
    GA_TS = 5
    GA_SA = 6
    NOT_ACTIVE = 7
