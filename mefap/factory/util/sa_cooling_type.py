from enum import Enum


class SimulatedAnnealingCoolingType(Enum):
    """
    This Enum class is there to represent the 3 different SimulatedAnnealingCoolingTypes that can be
     used as opening in the simulated annealing algorithm.
    """
    LINEAR = 0
    LOGARITHM_FAST = 1
    LOGARITHM_SLOW = 2
