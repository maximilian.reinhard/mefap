from enum import Enum


class MutationType(Enum):
    """
    This Enum class is there to represent the 2 different MutationTypes that can be
     used in the genetic algorithm.
    """
    SHIFT_MUTATION = 0
    EXCHANGE_MUTATION = 1
