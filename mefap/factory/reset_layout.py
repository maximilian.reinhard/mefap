import copy

import numpy as np

from mefap.factory.database import Database
from mefap.factory.factory_model import FactoryModel
from mefap.optimization.deleteFacility import delete_all_facilities
from mefap.optimization.deletePath import delete_all_paths


def reset_layout_to_variant(factory: FactoryModel, database: Database, last_layout_index) -> None:
    """
    Reset layout, facility_list and path_list based on old variant.
    :param factory:
    :param database:
    :param last_layout_index:
    :return: None
    """
    # overwrite current layout
    factory.layout = np.copy(database.layouts[last_layout_index])

    # overwrite facility_data
    for reset_facility in database.layouts_facility_data[last_layout_index]:
        factory.facility_list[reset_facility.index_num].curr_position = copy.deepcopy(reset_facility.curr_position)
        factory.facility_list[reset_facility.index_num].curr_source_position = \
            copy.deepcopy(reset_facility.curr_source_position)
        factory.facility_list[reset_facility.index_num].curr_sink_position = \
            copy.deepcopy(reset_facility.curr_sink_position)
        factory.facility_list[reset_facility.index_num].curr_rotation = copy.deepcopy(reset_facility.curr_rotation)
        factory.facility_list[reset_facility.index_num].curr_mirror = copy.deepcopy(reset_facility.curr_mirror)

    # overwrite path_data
    factory.path_list = copy.deepcopy(database.layouts_path_data[last_layout_index])

    # overwrite evaluation data
    factory.quamfab = copy.deepcopy(database.layouts_evaluation_data[last_layout_index])


def reset_layout(factory: FactoryModel) -> None:
    """
    Delete all facilities and paths from layout, if not fixed!
    :param factory: includes layout where all facilities and paths will be removed, if not fixed.
    :return:
    """
    delete_all_paths(factory)
    delete_all_facilities(factory)

    # get fixed facilities
    fixed_facilities = []
    for facility_index in range(len(factory.facility_list)):
        if factory.facility_list[facility_index].fixed_position:
            fixed_facilities.append(facility_index)

    for y_index in range(0, factory.layout.shape[0], 1):
        for x_index in range(0, factory.layout.shape[1], 1):
            if factory.layout[y_index, x_index] != factory.parameter["fixed_road_value"] and \
                    factory.layout[y_index, x_index] != factory.parameter["hidden_cell_value"] and \
                    factory.layout[y_index, x_index] not in fixed_facilities:
                factory.layout[y_index, x_index] = factory.parameter["empty_cell_value"]
