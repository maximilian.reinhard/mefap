import copy
import numpy as np

from mefap.factory.factory_model import FactoryModel
from mefap.factory.facility_data import FacilityData


class Database:
    """
    Data structure for storing data generated from the optimisation algorithm.
    """

    def __init__(self) -> None:
        # contains database / information of past layouts
        self.layouts = []
        self.layouts_facility_data = []
        self.layouts_path_data = []
        self.layouts_evaluation_data = []

    def save_in_database(self, factory: FactoryModel) -> None:
        """
        This method is used to save current layout/facility data
        :param factory: The FactoryModel which will be saved
        :return: None
        """
        help_list = []
        for index in range(len(factory.facility_list)):
            help_list.append(FacilityData())
            help_list[-1].index_num = factory.facility_list[index].index_num
            help_list[-1].curr_position = factory.facility_list[index].curr_position
            help_list[-1].curr_source_position = factory.facility_list[index].curr_source_position
            help_list[-1].curr_sink_position = factory.facility_list[index].curr_sink_position
            help_list[-1].curr_rotation = factory.facility_list[index].curr_rotation
            help_list[-1].curr_mirror = factory.facility_list[index].curr_mirror
        # save facility data
        self.layouts_facility_data.append(help_list)
        # save path data
        self.layouts_path_data.append(copy.deepcopy(factory.path_list))
        # save layout
        self.layouts.append(np.copy(factory.layout))
        # save layout evaluation
        self.layouts_evaluation_data.append(copy.deepcopy(factory.quamfab))

    def save_variant_in_database(self, factory, variant) -> None:
        """
        Save variant from local_database (factory_model) in database
        :param factory: FactoryModel from where the variant gets stored.
        :param variant: Index of variant from FactoryModel
        :return: None
        """
        # save facility data
        self.layouts_facility_data.append(copy.deepcopy(factory.layouts_facility_data[variant]))
        # save path data
        self.layouts_path_data.append(copy.deepcopy(factory.layouts_path_data[variant]))
        # save layout
        self.layouts.append(np.copy(factory.layouts[variant]))
        # save layout evaluation
        self.layouts_evaluation_data.append(copy.deepcopy(factory.layouts_evaluation_data[variant]))

    def delete_variant_from_database(self, variant) -> None:
        """
        Deletes variant on given index.
        :param variant: index from which variant get deleted
        :return: None
        """
        self.layouts_facility_data.pop(variant)
        self.layouts_path_data.pop(variant)
        self.layouts.pop(variant)
        self.layouts_evaluation_data.pop(variant)

    def transfer_all_variants_between_databases(self, second_database) -> None:
        """
        Transfer complete database to a second one.
        :param second_database: Database where the data gets transferred to.
        :return: None
        """
        # save facility data
        self.layouts_facility_data = copy.deepcopy(second_database.layouts_facility_data)
        # save path data
        self.layouts_path_data = copy.deepcopy(second_database.layouts_path_data)
        # save layout
        self.layouts = copy.deepcopy(second_database.layouts)  # np.copy(second_database.layouts)
        # save layout evaluation
        self.layouts_evaluation_data = copy.deepcopy(second_database.layouts_evaluation_data)

    def transfer_variant_between_databases(self, second_database, variant) -> None:
        """
        Save variant from local_database (factory_model) in database
        :param second_database: Where the variant is in
        :param variant: Index of variant in current second_database
        :return: None
        """
        # save facility data
        self.layouts_facility_data.append(
            copy.deepcopy(second_database.layouts_facility_data[variant]))
        # save path data
        self.layouts_path_data.append(copy.deepcopy(second_database.layouts_path_data[variant]))
        # save layout
        self.layouts.append(np.copy(second_database.layouts[variant]))
        # save layout evaluation
        self.layouts_evaluation_data.append(
            copy.deepcopy(second_database.layouts_evaluation_data[variant]))

    def sort_by_weight_sum(self) -> None:
        """
        Sorting all variants in database depending on the weight_sum of the EvaluationDate.
        Ascending order.
        :return: None
        """
        # get best layout
        best_variants = []
        # iterate over database
        for index in range(len(self.layouts_evaluation_data)):
            best_variants.append((index, self.layouts_evaluation_data[index].weight_sum))
        # sort based in weight_sum
        best_variants.sort(key=lambda tup: tup[1], reverse=True)

        layouts_unsorted = self.layouts
        layouts_facility_data_unsorted = self.layouts_facility_data
        layouts_path_data_unsorted = self.layouts_path_data
        layouts_evaluation_data_unsorted = self.layouts_evaluation_data

        self.layouts = []
        self.layouts_facility_data = []
        self.layouts_path_data = []
        self.layouts_evaluation_data = []

        for variant in best_variants:
            self.layouts.append(layouts_unsorted[variant[0]])
            self.layouts_facility_data.append(layouts_facility_data_unsorted[variant[0]])
            self.layouts_path_data.append(layouts_path_data_unsorted[variant[0]])
            self.layouts_evaluation_data.append(layouts_evaluation_data_unsorted[variant[0]])

    def clear_database(self) -> None:
        """
        Delete everything from the database.
        :return: None
        """
        self.layouts_facility_data = []
        self.layouts_path_data = []
        self.layouts = []
        self.layouts_evaluation_data = []

    def duplicate_variant(self, variant) -> None:
        """
        Duplicate an entry/variant and store at end ist db
        :return: None
        """
        self.layouts_facility_data.append(copy.deepcopy(self.layouts_facility_data[variant]))
        self.layouts_path_data.append(copy.deepcopy(self.layouts_path_data[variant]))
        self.layouts.append(copy.deepcopy(self.layouts[variant]))
        self.layouts_evaluation_data.append(copy.deepcopy(self.layouts_evaluation_data[variant]))
