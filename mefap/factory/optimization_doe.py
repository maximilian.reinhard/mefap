from typing import List
from mefap.factory.util.mutation_type import MutationType


class Optimization_DOE:
    def __init__(self) -> None:
        super().__init__()

        # genetic algorithm
        self.population_size = []
        self.number_of_generations = []
        self.crossover_rate = []
        self.number_of_elite_parents = []
        self.mutation_rate = []
        self.mutation_max_shift_step = []
        self.mutation_max_exchange = []

        self.lrs_step_size = []
