class RestrictiveArea:
    """
    This class represents restrictive areas. Mainly to assign a color to each area.
    """

    def __init__(self, name, area_id, color=None):
        self.name = name
        self.id = area_id
        if color:
            self.color = color
        else:
            self.color = None
