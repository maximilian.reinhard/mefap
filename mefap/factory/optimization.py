from typing import List

from mefap.factory.util.metaheuristik_type import MetaheuristicType
from mefap.factory.util.mutation_type import MutationType
from mefap.factory.util.neighborhood_search_type import NeighborhoodSearchType
from mefap.factory.util.opening_type import OpeningType
from mefap.factory.util.sa_cooling_type import SimulatedAnnealingCoolingType


class Optimization:
    """
    The Optimization class is representing all attributes for optimization,
    which can be set inside OptimizeDialog (GUI)
    """

    def __init__(self) -> None:
        super().__init__()
        self.metaheuristic_type: MetaheuristicType = MetaheuristicType.TABU_SEARCH

        # tabu search
        self.tabu_length = 0
        self.tabu_iteration = 0

        # simulating annealing
        self.starting_temperature = 0
        self.cooling_rate = 0
        self.increment = 0
        self.cooling_type: SimulatedAnnealingCoolingType = SimulatedAnnealingCoolingType.LOGARITHM_FAST

        # genetic algorithm
        self.population_size = 0
        self.number_of_generations = 0
        self.crossover_rate = 0
        self.number_of_elite_parents = 0
        self.mutation_rate = 0
        self.mutation_max_shift_step = 0
        self.mutation_max_exchange = 0
        self.mutation_type: List[MutationType] = [MutationType.SHIFT_MUTATION,
                                                  MutationType.EXCHANGE_MUTATION]

        # GA_TS & GA_SA
        self.number_take_over_variants = 5

        # opening method
        self.opening: OpeningType = OpeningType.RANDOM

        # neighborhood search methods
        self.nhs_methods: List[NeighborhoodSearchType] = [
            NeighborhoodSearchType.LocalReallocationSearch, NeighborhoodSearchType.FreeAreaSearch,
            NeighborhoodSearchType.MultipleElementsExchangeSearch]
        self.lrs_step_size = 6
        self.lrs_distribution = 0
        self.mees_facility_range = (2, 3)
        self.mees_step_size = 2
        self.mees_area_difference = 0.5
        self.mees_distribution = 0
        self.fas_order = 1
        self.fas_distribution = 0

        # optional restrictions
        self.rotation = True
        self.mirror = True

    def is100(self) -> bool:
        """
        Checks if the distribution of the neighbourhood search is equal 100.
        :return: True if distribution sums up to exactly 100.
        """
        return self.lrs_distribution + self.fas_distribution + self.mees_distribution == 100

    def greater100(self) -> bool:
        """
        Checks if the distribution of the neighbourhood search is greater 100.
        :return: True if distribution sums up to above 100.
        """
        return self.lrs_distribution + self.fas_distribution + self.mees_distribution > 100

    def set_distribution_zero(self) -> None:
        """
        Sets all neighborhood distribution parameter to zero.
        :return: None
        """
        self.lrs_distribution = 0
        self.fas_distribution = 0
        self.mees_distribution = 0

    def set_distribution_tabu(self) -> None:
        """
        Sets the neighborhood distribution for tabu search. Since mees is not allowed within tabu search it is
        set to zero.
        :return: None
        """
        self.lrs_distribution = 50
        self.fas_distribution = 50
        self.mees_distribution = 0

    def set_distribution_simulated_annealing(self) -> None:
        """
        Sets the neighborhood distribution for simulated annealing. All neighborhood search types are allowed and
        weighted equally. The values of the distribution are type int, because of this lrs is greater then the other
        values. This is necessary to get a sum of 100%.
        :return: None
        """
        self.lrs_distribution = 34
        self.fas_distribution = 33
        self.mees_distribution = 33

    def set_default(self, num_facilities) -> None:
        """
        Sets default values for all optimisation parameter.
        Values depend on the number of facilities.
        :param num_facilities: Number of facilities in factory
        :return: None
        """
        # metaheuristic
        # self.metaheuristic_type: MetaheuristikType = MetaheuristikType.GA_SA

        # opening method
        self.opening = OpeningType.RANDOM_BY_SIZE  # 4?

        # tabu search
        self.tabu_iteration = 10 * round(num_facilities/ 2)
        self.tabu_length = round(num_facilities / 2)

        # simulating annealing
        self.starting_temperature = 30
        self.cooling_rate = 0.9
        self.increment = 100
        # factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_FAST

        # genetic algorithm
        self.population_size = round (2.5 * num_facilities)  # 100
        self.number_of_generations = 100 * round(num_facilities / 2)
        self.crossover_rate = 0.2
        self.number_of_elite_parents = round(num_facilities / 4)
        self.mutation_rate = 0.5
        self.mutation_max_shift_step = 2
        self.mutation_max_exchange = 3
        # self.mutation_type: List[MutationType] = [MutationType.SHIFT_MUTATION, MutationType.EXCHANGE_MUTATION]

        # GA_TS & GA_SA
        # self.number_take_over_variants = 5

        # neighborhood search methods
        # self.nhs_methods: List[NeighborhoodSearchType] = [
        #     NeighborhoodSearchType.LocalReallocationSearch, NeighborhoodSearchType.FreeAreaSearch,
        #     NeighborhoodSearchType.MultipleElementsExchangeSearch]

        # optional restrictions
        # self.rotation = True
        # self.mirror = True


def set_optimization_npp(factory):
    """
    set control parameter of metaheuristics, when post_npp (no_path_planning = True)
    :param factory:
    :return:
    """
    # opening method
    factory.optimization.opening = OpeningType.JUMP_OVER  # 4?

    # tabu search
    factory.optimization.tabu_length = round(len(factory.facility_list) / 3)  # 1/3 of number of facilities
    factory.optimization.tabu_iteration = 10 * round(len(factory.facility_list) / 2)  # 500

    # simulating annealing
    factory.optimization.starting_temperature = 3
    factory.optimization.cooling_rate = 0.75
    factory.optimization.increment = 10 * round(len(factory.facility_list)/2)  # 200
    # factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_FAST

    # genetic algorithm
    factory.optimization.population_size = len(factory.facility_list)  # 25
    factory.optimization.number_of_generations = 10 * round(len(factory.facility_list) / 2)  # 200
    # factory.optimization.crossover_rate = 0.05
    factory.optimization.number_of_elite_parents = round(len(factory.facility_list) / 4)  # 5
    # factory.optimization.mutation_rate = 0.05
    # factory.optimization.mutation_max_shift_step = 0
    # factory.optimization.mutation_max_exchange = 0
    # factory.optimization.mutation_type = [MutationType.SHIFT_MUTATION, MutationType.EXCHANGE_MUTATION]
