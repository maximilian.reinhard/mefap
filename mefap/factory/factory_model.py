import copy
import json
from typing import List, Tuple

import matplotlib
import numpy as np
from PySide2.QtGui import QColor

from mefap.factory.evaluation_data import EvaluationData
from mefap.factory.facility_attributs import FacilityAttributes
from mefap.factory.facility_characteristics import FacilityCharacteristics
from mefap.factory.facility_data import FacilityData
from mefap.factory.media_availability import MediaAvailability
from mefap.factory.media_requirements_import import MediaRequirementsImport
from mefap.factory.noise_area import NoiseArea
from mefap.evaluation.noise_propagation.noise_propagation_conditions import NoisePropagationMatrix
from mefap.factory.optimization import Optimization
from mefap.factory.path_elements import PathElements
from mefap.factory.restriction import Restriction
from mefap.factory.restrictive_area import RestrictiveArea
from mefap.factory.util.factory_encoder import FactoryEncoder, enum_decode, factory_decode
from mefap.factory.util.light_level import LightLevel
from mefap.factory.util.resize import resize_matrix, insert_old_values
from mefap.factory.weighting import Weighting
from mefap.gui.util import colors
from mefap.optimization.deleteFacility import delete_position_facility
from mefap.optimization.deletePath import delete_paths_from_layout, delete_all_paths
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition


class FactoryModel:
    """ This class contains all information about the factory """

    def __init__(self) -> None:
        super().__init__()
        self.rows = 100
        self.columns = 100
        self.cell_size = 5
        # for making display cells bigger
        self.cell_zoom = 20
        self.layout: np.ndarray = None  # contains the actual factory layout
        self.media_availability: List[MediaAvailability] = []
        self.media_requirements: List[MediaRequirementsImport] = []
        self.quamfab = EvaluationData()
        self.quamfab_min = EvaluationData()
        self.path_list: List[PathElements] = []
        self.restrictions: List[Restriction] = []
        self.restrictive_area: np.ndarray = None
        self.area_types: List[RestrictiveArea] = []
        self.facility_list: List[FacilityAttributes] = []
        self.facility_characteristics: List[FacilityCharacteristics] = []
        self.illuminance_matrix = None
        self.noise_matrix = None
        self.noise_wall_list = None
        self.noise_corner_list = None
        self.mf_matrix = None  # material flow matrix
        self.cf_matrix = None  # communication flow matrix
        self.parameter = {  # significant parameters for experiments
            'empty_cell_value': -1,
            'local_road_value': -2,
            'fixed_road_value': -3,
            'hidden_cell_value': -50,
            'empty_cell_costs': 3,
            'local_road_costs': 1.5,
            'fixed_road_costs': 1,
            'media_avail_factor': 0.5,
            'road_illuminance': 0,
            'vibration_factor': 0.125,
            'cleanliness_factor': 0.5,
            'temperature_factor': 0.5,
            'crossing_with_source': 0,
            'no_path_planning': False,
            'lrs_search_steps': 100,
            'number_saved_variants': 5,
            'multicore': 1,
            'termination_criterion': 30,
            'number_of_experiments': 50,
            'sound_pressure_reduction': 4,
            'sound_propagation_conditions': 8,
            'noise_semi_diffuse': False,
            'noise_flat_room': False,
            'noise_calc_wall_corner': False,
            'noise_encapsulation': 25,
            'noise_barrier_isolation': 15,
            'noise_factory_wall_isolation': 40,
            'noise_room_condition': {-1: 10000},  # ceiling without noise walls
            'noise_absorption_coefficient': 0.15,
            'noise_absorption_floor': 0.1,
            'noise_absorption_ceil': 0.3,
            'noise_absorption_wall': 0.3,
            'color_facilities_by_sound': False
        }
        # contains database / information of past layouts
        self.layouts = []
        self.layouts_facility_data = []
        self.layouts_path_data = []
        self.layouts_evaluation_data: List[EvaluationData] = []
        self.optimization: Optimization = None
        self.weighting: Weighting = None
        self.noise_area: NoiseArea = None
        self.noise_propagation_matrix: NoisePropagationMatrix = None

    def initialize_content(self) -> None:
        """
        This method initializes the factory. It is called when user creating a new factory.
        :return: None
        """
        self.layout = None
        self.make_size_displayable()
        self.rows //= self.cell_size
        self.columns //= self.cell_size
        self.layout = np.ones(shape=(self.columns, self.rows)) * self.parameter['empty_cell_value']
        self.mf_matrix = np.zeros(shape=(0, 0))
        self.cf_matrix = np.zeros(shape=(0, 0))
        self.restrictive_area = np.zeros(shape=(self.columns, self.rows))
        self.add_area_type("None", neutral=True)

        # initialize light
        self.illuminance_matrix = np.full((self.columns, self.rows), LightLevel.OFF)

        # initialize noise
        self.noise_matrix = np.zeros((self.columns, self.rows))
        self.noise_area = NoiseArea(self.columns, self.rows)
        self.noise_propagation_matrix = NoisePropagationMatrix(self.columns, self.rows)

        # initialize optimization.py
        self.optimization = Optimization()
        self.weighting = Weighting()

    def reset_rows_columns(self):
        """
        Only use from GUI. Used for step setsizedialog back to factorysizedialog.
        :return:
        """
        self.rows *= self.cell_size
        self.columns *= self.cell_size

    def make_size_displayable(self) -> None:
        """
        This method corrects the factory size if the rows
        and columns are not divideable by the cell size.
        :return: None
        """
        row_overhead = self.rows % self.cell_size
        if row_overhead != 0:
            self.rows = round(self.rows - row_overhead)

        column_overhead = self.columns % self.cell_size
        if column_overhead != 0:
            self.columns = round(self.columns - column_overhead)

    def save_factory(self, file) -> None:
        """
        This method will be called when the user is saving this factory.
        :param file: File/path where the factory will be saved in.
        :return: None
        """

        if self.parameter["noise_semi_diffuse"]:
            calc_noise_room_condition(self)
        # calc noise_propagation_matrix for all cells
        self.noise_propagation_matrix.calc_noise_propagation_conditions(self)

        with open(file, 'w') as f:
            json.dump(self.__dict__, f, cls=FactoryEncoder)

    def load_factory(self, file) -> None:
        """
        This method will be called when the user is loading an existing factory.
        :param file: the pickle file where the factory will be loaded from
        :return: the factory model loaded from pickle
        """
        with open(file, "r") as read_file:
            temp_dict = json.load(read_file, object_hook=enum_decode)
            self.__dict__.clear()
            factory_decode(self, temp_dict)

    def add_layout_data(self) -> None:
        """
        This method is used to save current layout/facility data
        :return: None
        """
        help_list = []
        for index in range(len(self.facility_list)):
            help_list.append(FacilityData())
            help_list[-1].index_num = self.facility_list[index].index_num
            help_list[-1].curr_position = self.facility_list[index].curr_position
            help_list[-1].curr_source_position = self.facility_list[index].curr_source_position
            help_list[-1].curr_sink_position = self.facility_list[index].curr_sink_position
            help_list[-1].curr_rotation = self.facility_list[index].curr_rotation
            help_list[-1].curr_mirror = self.facility_list[index].curr_mirror
        # save facility data
        self.layouts_facility_data.append(help_list)
        # save path data
        self.layouts_path_data.append(copy.deepcopy(self.path_list))
        # save layout
        self.layouts.append(np.copy(self.layout))
        # save layout evaluation
        self.layouts_evaluation_data.append(copy.deepcopy(self.quamfab))

    def delete_variant_from_layouts_data(self, variant) -> None:
        """
        Deletes a variant of the layouts from the factory
        :param variant: Index of variant to delete
        :return: None
        """
        self.layouts_facility_data.pop(variant)
        self.layouts_path_data.pop(variant)
        self.layouts.pop(variant)
        self.layouts_evaluation_data.pop(variant)

    def delete_all_from_layouts_data(self) -> None:
        """
        Deletes all data about layouts from the factory.
        :return: None
        """
        self.layouts_facility_data = []
        self.layouts_path_data = []
        self.layouts = []
        self.layouts_evaluation_data = []

    def cut_layout_data(self, number_save_variants) -> None:
        best_variants = []

        # iterate over database
        for index in range(len(self.layouts_evaluation_data)):
            best_variants.append((index, self.layouts_evaluation_data[index].weight_sum))

        # sort based in weight_sum
        best_variants.sort(key=lambda tup: tup[1], reverse=True)

        # if not enough solutions -> take all
        if len(best_variants) < number_save_variants:
            number_save_variants = len(best_variants)
        best_variants = best_variants[0:number_save_variants]

        # sort based on index
        best_variants.sort(key=lambda tup: tup[0], reverse=True)

        # cut tuples -> leaf index
        best_variants = [i[0] for i in best_variants]

        # delete other variants from db
        for index in range(len(self.layouts_evaluation_data) - 1, -1, -1):
            if index not in best_variants:
                self.layouts.pop(index)
                self.layouts_evaluation_data.pop(index)
                self.layouts_facility_data.pop(index)
                self.layouts_path_data.pop(index)

    def transfer_all_variants_to_factory(self, second_database) -> None:
        """
        Transfer complete database to the factory.
        :param second_database: Database where the data get transfered from.
        :return: None
        """
        # save facility data
        self.layouts_facility_data = copy.deepcopy(second_database.layouts_facility_data)
        # save path data
        self.layouts_path_data = copy.deepcopy(second_database.layouts_path_data)
        # save layout
        self.layouts = copy.deepcopy(second_database.layouts)  # np.copy(second_database.layouts)
        # save layout evaluation
        self.layouts_evaluation_data = copy.deepcopy(second_database.layouts_evaluation_data)

    def remove_from_layout(self, index: int) -> None:
        """
        Removes an index from the factory layout.
        :param index: Index
        :return: None
        """
        self.layout[self.layout == index] = -1

    def get_upper_left_of_placed_facilities(self) -> List[Tuple[Tuple[int, int], int]]:
        """
        Return the upper left corner of placed facilities in the layout.
        :return:  A List of tuple. Index 0 of this tuple is a tuple of the position
                    and Index 1 holds the id of the factory.
        """
        result: List[Tuple[Tuple[int, int], int]] = []
        found: List[int] = []
        for row_idx, row in enumerate(self.layout):
            for column_idx, column in enumerate(row):
                if column >= 0 and int(column) not in found:
                    result.append(((column_idx, row_idx), int(column)))
                    found.append(int(column))
        return result

    def set_restrictions_to_max(self):
        """
        This method should be called after loading the facilities into the factory.
        Each restriction is set to maximum value found in the self.facility_list.
        :return: None
        """
        max_height, max_floor, max_cover = self.get_max_restrictions()

        self.restrictions.append(Restriction("floor_capacity", self.rows, self.columns, max_floor))
        self.restrictions.append(Restriction("cover_capacity", self.rows, self.columns, max_cover))
        self.restrictions.append(Restriction("height_profile", self.rows, self.columns, max_height))

    def get_max_restrictions(self):
        """
        Returns the maximum for floor_capacity, cover_capacity and height_profile, which is needed, that all facilities,
        can be placed.
        :return: max_height, max_floor, max_cover
        """
        max_height, max_floor, max_cover = 0, 0, 0
        for facility in self.facility_list:
            if facility.floorload > max_floor:
                max_floor = facility.floorload
            if facility.ceilingload > max_floor:
                max_cover = facility.ceilingload
            if facility.height > max_height:
                max_height = facility.height
        return max_height, max_floor, max_cover

    def add_area_type(self, area: str, neutral=False) -> None:
        """
        This method adds a new restrictive area type to this facility
        :param area: Name of the area
        :param neutral: True is this added area type is type None.
        :return: None
        """
        if neutral:
            self.area_types.append(
                RestrictiveArea(area, len(self.area_types), color=colors.EMPTY_CELL))
        else:
            self.area_types.append(RestrictiveArea(area, len(self.area_types)))

    def area_exists(self, area: int) -> bool:
        return area in self.restrictive_area

    def get_facilitytype_by_name(self, facility_type_name) -> RestrictiveArea:
        """
        This returns a RestrictiveArea found by his name.
        :param facility_type_name: name of RestrictiveArea
        :return: RestrictiveArea
        """
        for area_tpye in self.area_types:
            if area_tpye.name == facility_type_name:
                return area_tpye

    def get_facilitytype_by_id(self, facility_type_id) -> RestrictiveArea:
        """
        This returns a RestrictiveArea found by his id.
        :param facility_type_id: id of RestrictiveArea
        :return: RestrictiveArea
        """
        for area_tpye in self.area_types:
            if area_tpye.id == facility_type_id:
                return area_tpye

    def generate_colors(self) -> None:
        """
        After loading the excel file into the factory this method will be calles from main_view.
        This method is generating colors for each area and facility. The facilities will be colored
        in similar colors then the area they belong to.
        :return: None
        """
        cmap = matplotlib.cm.get_cmap('nipy_spectral')
        step_size_area = 0.9 / len(self.area_types)
        pos = 0.1
        for area in self.area_types:
            if area.color != colors.EMPTY_CELL:
                color = cmap(pos, bytes=True)
                area.color = QColor(color[0], color[1], color[2], color[3])
                area_fac = [x for x in self.facility_list if x.facility_type == area.name]
                if len(area_fac) > 0:
                    fac_step = (step_size_area / 2) / len(area_fac)
                    for facility in area_fac:
                        color = cmap(pos, bytes=True)
                        facility.color = QColor(color[0], color[1], color[2], color[3])
                        pos += fac_step
                pos += step_size_area / 2

    def get_next_facility_index(self) -> int:
        """
        :return: Returns the index for the next possible facility which can be added to the factory
        """
        if self.facility_list:
            return max([facility.index_num for facility in self.facility_list]) + 1
        else:
            return 0

    def delete_facility_complete(self, name, not_init=False) -> None:
        """
        Deletes a facility completely from this factory.
        :param not_init: True if facility is not initialized.
        :param name: Name of the facility to delete.
        :return: None
        """
        index_layout = -1
        for facility in self.facility_list:
            if facility.name == name:
                index_layout = facility.index_num
        if index_layout == -1:
            return

        self.delete_facility_from_layouts(index_layout)

        delete_paths_from_layout(self, self.facility_list[index_layout])

        if not not_init:
            with np.nditer(self.layout, op_flags=['readwrite']) as it:
                for value in it:
                    if value == index_layout:
                        value[...] = self.parameter['empty_cell_value']
                    elif value > index_layout:
                        value -= 1

        for (facility, path) in zip(self.facility_list, self.path_list):
            if facility.index_num > index_layout:
                # updates index of other facilities
                facility.index_num -= 1
                path.facility_index -= 1

        del self.facility_list[index_layout]
        del self.facility_characteristics[index_layout]
        del self.path_list[index_layout]

        # delete in path of other facilities
        for path in self.path_list:
            del path.paths[index_layout]

        # delete requirements
        for req in self.media_requirements:
            del req.requirements[index_layout]

        # delete comunictions flow
        self.cf_matrix = np.delete(self.cf_matrix, index_layout, axis=0)
        self.cf_matrix = np.delete(self.cf_matrix, index_layout, axis=1)

        # delete materialflow
        self.mf_matrix = np.delete(self.mf_matrix, index_layout, axis=0)
        self.mf_matrix = np.delete(self.mf_matrix, index_layout, axis=1)

    def delete_facility_from_layouts(self, index) -> None:
        """
        Deletes a facility completely from the saved layouts.
        :param index: Index of the facility to delete
        :return: None
        """
        back_layout = copy.deepcopy(self.layout)
        back_facilites = copy.deepcopy(self.facility_list)
        back_path = copy.deepcopy(self.path_list)
        for (layout, facilities, path_data) in zip(self.layouts, self.layouts_facility_data, self.layouts_path_data):
            self.layout = layout
            self.path_list = path_data
            for facility in facilities:
                self.facility_list[
                    facility.index_num].curr_position = facility.curr_position
                self.facility_list[
                    facility.index_num].curr_source_position = facility.curr_source_position
                self.facility_list[
                    facility.index_num].curr_sink_position = facility.curr_sink_position
                self.facility_list[
                    facility.index_num].curr_rotation = facility.curr_rotation
                self.facility_list[facility.index_num].curr_mirror = facility.curr_mirror

            delete_paths_from_layout(self, self.facility_list[index])

            with np.nditer(self.layout, op_flags=['readwrite']) as it:
                for value in it:
                    if value == index:
                        value[...] = self.parameter['empty_cell_value']
                    elif value > index:
                        value -= 1

            for (facility, path) in zip(facilities, self.path_list):
                if facility.index_num > index:
                    # updates index of other facilities
                    facility.index_num -= 1
                    path.facility_index -= 1

            del facilities[index]
            del self.path_list[index]

            # delete in path of other facilities
            for path in self.path_list:
                del path.paths[index]

        self.layout = back_layout
        self.path_list = back_path
        self.facility_list = back_facilites

    def all_facilities_placed(self) -> bool:
        """
        Checks if all facilities of this factory are placed.
        :return: True if all facilities are placed.
        """
        for facility in self.facility_list:
            if facility.curr_position[0] is None or facility.curr_position[1] is None:
                return False
        return True

    def add_media(self, name) -> None:
        """
        Adds a new media to the factory.
        :param name: name of the new media
        :return: None
        """
        self.media_availability.append(MediaAvailability(name, self))
        media_req = []
        for _ in self.facility_list:
            media_req.append(False)
        self.media_requirements.append(MediaRequirementsImport(name, media_req))

    def remove_media(self, name) -> None:
        """
        removes media with given name from factory
        :param name: name of media
        :return: None
        """
        for media_avail in self.media_availability:
            if media_avail.name == name:
                self.media_availability.remove(media_avail)
                break
        for media_req in self.media_requirements:
            if media_req.name == name:
                self.media_requirements.remove(media_req)
                break

    def get_paths_mf(self, facility_id: int) -> (List[List[Tuple[int]]], List[int], List[List[Tuple[int]]], List[int]):
        """
        Returns all in and outgoing paths mf of the given facility and the ids of facilities which is connected to.
        :param facility_id: The id of the facility for whoch the return is generated.
        :return: List of ingoing pahts, list of facility id where the ingoing connections come from, list of outgoing
        connections, list of facility ids where the outgoing connections goes to.
        """
        ins = []
        out = []
        ins_index = []
        out_index = []
        for idx, facility in enumerate(self.facility_list):
            if facility.index_num == facility_id:
                facility_id = idx
                break
        path_list = self.path_list[facility_id]
        mf_col = self.mf_matrix[facility_id, :]
        mf_row = self.mf_matrix[:, facility_id]
        for index, element in enumerate(mf_col):
            if element > 0:
                out.append(path_list.paths[index])
                out_index.append(self.facility_list[index].index_num)

        for index, element in enumerate(mf_row):
            if element > 0:
                ins.append(self.path_list[index].paths[facility_id])
                ins_index.append(self.facility_list[index].index_num)
        return ins, ins_index, out, out_index

    def get_paths_cf(self, facility_id: int) -> (List[List[Tuple[int]]], List[int], List[List[Tuple[int]]], List[int]):
        """
        Returns all in and outgoing cf paths of the given facility and the ids of facilities which is connected to.
        :param facility_id: The id of the facility for whoch the return is generated.
        :return: List of ingoing pahts, list of facility id where the ingoing connections come from, list of outgoing
        connections, list of facility ids where the outgoing connections goes to.
        """
        ins = []
        out = []
        ins_index = []
        out_index = []
        for idx, facility in enumerate(self.facility_list):
            if facility.index_num == facility_id:
                facility_id = idx
                break
        path_list = self.path_list[facility_id]
        cf_col = self.cf_matrix[facility_id, :]
        cf_row = self.cf_matrix[:, facility_id]
        for index, element in enumerate(cf_col):
            if element > 0:
                out.append(path_list.paths[index])
                out_index.append(self.facility_list[index].index_num)

        for index, element in enumerate(cf_row):
            if element > 0:
                ins.append(self.path_list[index].paths[facility_id])
                ins_index.append(self.facility_list[index].index_num)

        return ins, ins_index, out, out_index

    def resize_factory(self, x: int, y: int) -> None:
        """
        Resizes the base size of the factory to the given number of cells.
        :param x: Width of the factory
        :param y: Height of the factory
        :return: None
        """
        delete_all_paths(self)

        for facility in self.facility_list:
            if facility.curr_position[0] is not None:
                if facility.curr_rotation % 2 == 0:
                    if x < (facility.curr_position[1] + facility.cell_width1) or y < (
                            facility.curr_position[0] + facility.cell_width2):
                        delete_position_facility(self, facility)
                else:
                    if x < (facility.curr_position[1] + facility.cell_width2) or y < (
                            facility.curr_position[0] + facility.cell_width1):
                        delete_position_facility(self, facility)

        self.rows = x
        self.columns = y

        self.layout = resize_matrix(self.layout, x, y, self.parameter['empty_cell_value'])
        self.restrictive_area = resize_matrix(self.restrictive_area, x, y, 0)
        for media in self.media_availability:
            media.availability_layout = resize_matrix(media.availability_layout, x, y, None, bools=True)

        old_illuminance = self.illuminance_matrix
        self.illuminance_matrix = np.full((y, x), LightLevel.OFF)
        insert_old_values(self.illuminance_matrix, old_illuminance)

        self.noise_matrix = resize_matrix(self.noise_matrix, x, y, 0)

        max_height, max_floor, max_cover = self.get_max_restrictions()
        for res in self.restrictions:
            if res.name == "floor_capacity":
                res.matrix = resize_matrix(res.matrix, x, y, max_floor)
            elif res.name == "cover_capacity":
                res.matrix = resize_matrix(res.matrix, x, y, max_cover)
            elif res.name == "height_profile":
                res.matrix = resize_matrix(res.matrix, x, y, max_height)
            else:
                res.matrix = resize_matrix(res.matrix, x, y, 1)

        try_path_planning_for_layout(self, True)

    def import_factory(self, import_factory) -> None:
        """
        Imports data from another factory into this factory. Mostly the facilities.
        :param import_factory: The factory where the data gets imported from.
        """
        media_names = [media.name for media in self.media_requirements]
        area_names = [area.name for area in self.area_types]
        names = [fac.name for fac in self.facility_list]
        facility_count = 0
        for faciliy, characteristics in zip(import_factory.facility_list, import_factory.facility_characteristics):
            if faciliy.name in names:
                continue
            # Checks if new media is required
            for media in import_factory.media_requirements:
                if media.name not in media_names:
                    self.add_media(media.name)
                    media_names.append(media.name)

            for area in import_factory.area_types:
                if area.name not in area_names:
                    self.add_area_type(area.name)
                    area_names.append(area.name)
            # create index num
            index = self.get_next_facility_index()
            faciliy.index_num = index

            # Copy raw data
            self.facility_list.append(faciliy)
            self.facility_characteristics.append(characteristics)

            # insert media requieremnt
            for req in self.media_requirements:
                found = False
                for med_req in import_factory.media_requirements:
                    if med_req.name == req.name:
                        req.requirements.append(med_req.requirements[facility_count])
                        found = True
                        break
                if not found:
                    req.requirements.append(False)

            # add rows for cf and mf matrix
            self.cf_matrix = np.column_stack((self.cf_matrix, np.zeros(len(self.facility_list) - 1)))
            self.cf_matrix = np.row_stack((self.cf_matrix, np.zeros(len(self.facility_list))))

            self.mf_matrix = np.column_stack((self.mf_matrix, np.zeros(len(self.facility_list) - 1)))
            self.mf_matrix = np.row_stack((self.mf_matrix, np.zeros(len(self.facility_list))))

            # add path
            self.path_list.append(PathElements(index))
            for facility_path in self.path_list:
                while len(facility_path.paths) < (len(self.facility_list)):
                    facility_path.paths.append([])

            facility_count += 1
        # Adding cf and mf entries
        for row_mf, row_cf, facility in zip(import_factory.mf_matrix, import_factory.cf_matrix,
                                            import_factory.facility_list):
            for row_mf_old, row_cf_old, old_facility in zip(self.mf_matrix, self.cf_matrix, self.facility_list):
                if facility.name == old_facility.name:
                    for i, facility_c in enumerate(import_factory.facility_list):
                        for j, old_facility_c in enumerate(self.facility_list):
                            if facility_c.name == old_facility_c.name:
                                row_mf_old[j] = row_mf[i]
                                row_cf_old[j] = row_cf[i]
                                break
                    break

    def __repr__(self) -> str:
        return (
            "Factorysize :\n{}x{}\nCellsize:\n{}\nLayout:\n{}\n Materialflow:\n{}"
            "\nControlflow:\n{}\nFacility:\n{}\nMedia:\n{}").format(
            self.rows, self.columns, self.cell_size, self.layout, self.mf_matrix,
            self.cf_matrix,
            self.facility_list, self.media_requirements)
