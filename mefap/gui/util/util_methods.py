from PySide2.QtWidgets import QSpinBox


def set_spinbox(spin1: QSpinBox, spin2: QSpinBox, val, limit=0):
    if spin1.value() != val:
        if spin1.value() > limit != 0:
            val = limit
            spin2.setValue(limit)
            spin1.setValue(limit)
        else:
            val = spin1.value()
            spin2.setValue(val)
    elif spin2.value() != val:
        if spin2.value() > limit != 0:
            val = limit
            spin1.setValue(limit)
            spin2.setValue(limit)
        else:
            val = spin2.value()
            spin1.setValue(val)
    return val


def set_spinbox_triple(spin1: QSpinBox, spin2: QSpinBox, spin3: QSpinBox, val):
    if spin1.value() != val:
        val = spin1.value()
        spin2.setValue(val)
        spin3.setValue(val)
    elif spin3.value() != val:
        val = spin3.value()
        spin1.setValue(val)
        spin2.setValue(val)
    else:
        val = spin2.value()
        spin3.setValue(val)
        spin1.setValue(val)
    return val
