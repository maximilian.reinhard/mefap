class WriteStream:
    """
    This class should replace the sys.stdout stream. All data gets written into a thread and process save queue.
    """

    def __init__(self, queue):
        self.queue = queue

    def write(self, text) -> None:
        """
        Adds text to our queue.
        :param text: Text which gets added.
        :return: None
        """
        self.queue.put(text)

    def flush(self):
        """
        Not implemented but needed for working Stream
        :return:
        """
        pass

    def fileno(self):
        """
        Not implemented but needed for working Stream
        :return:
        """
        return -1
