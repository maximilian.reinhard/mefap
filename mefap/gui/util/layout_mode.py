from enum import Enum


class LayoutMode(Enum):
    """
    The LayoutMode class is an enumeration object,
    which contains the different modes of FactoryLayoutScene.
    """
    SHOW = 0
    REMOVE = 1
    ADD_FACILITIES = 2
    MEDIA = 3
    LIGHT = 4
    RESTRICTION = 5
    AREA = 6
    INFO = 7
    NOISE = 8
    MF = 9
    CF = 10
