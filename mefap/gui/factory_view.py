import functools
from typing import List

import numpy as np
from PySide2 import QtGui
from PySide2.QtCore import QSize, QRect, QPoint, Qt
from PySide2.QtWidgets import QGraphicsView, QFrame, QRubberBand, QMenu, QAction, QInputDialog, \
    QGraphicsItem, QMessageBox

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.factory.media_availability import MediaAvailability
from mefap.factory.util.light_level import LightLevel
from mefap.gui.factory_cell import FactoryCell
from mefap.gui.factory_facility import FactoryFacility
from mefap.gui.util.layout_mode import LayoutMode
from mefap.optimization.deletePath import try_delete_fixed_road_cell
from mefap.optimization.positionFacility import position_facility_on_cell


class FactoryView(QGraphicsView):
    """
    View to display FactoryLayoutScene in the MainView.
    """

    def __init__(self, parent=None) -> None:
        super(FactoryView, self).__init__(parent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setFrameShape(QFrame.NoFrame)
        # for rubberband
        self.rubber_band = QRubberBand(QRubberBand.Rectangle, self)
        self.origin = QPoint()
        self.rightMousePressed = False

        self.curr_scale = 1

        self.setContextMenuPolicy(Qt.CustomContextMenu)

    def show_noise_wall_context_menu(self, pos: QPoint, cells: List[FactoryCell], facilities: List[FactoryFacility]) -> None:
        menu = QMenu("Noise wall", self)
        remove = False
        for cell in cells:
            if self.scene().factory.noise_area.noise_area_matrix[cell.y_cord][cell.x_cord] != -1:
                action = QAction('Remove sound isolation area', self)
                action.triggered.connect(functools.partial(self.remove_noise_wall, cells))
                menu.addAction(action)
                remove = True
                break
        if not remove:
            action = QAction('Add sound isolation area', self)
            action.triggered.connect(functools.partial(self.add_noise_wall, cells, facilities))
            menu.addAction(action)
        menu.exec_(self.mapToGlobal(pos))

    def remove_noise_wall(self, cells: List[FactoryCell]) -> None:
        for cell in cells:
            self.scene().factory.noise_area.remove_noise_area_by_position(cell.x_cord, cell.y_cord)

        self.scene().show_noise_matrix()

    def add_noise_wall(self, cells: List[FactoryCell], facilities: List[FactoryFacility]) -> None:
        indices = []
        for fac in facilities:
            fac_items = fac.collidingItems()
            fac_items = [x for x in fac_items if isinstance(x, FactoryCell)]
            for fac_item in fac_items:
                if fac_item not in cells:
                    errordialog = QMessageBox(self.scene().parent())
                    errordialog.setText("Sound isolation area cannot be place")
                    errordialog.setInformativeText(str("A facility must be completely contained in a sound area."))
                    errordialog.setIcon(QMessageBox.Warning)
                    errordialog.exec_()
                    return

        for cell in cells:
            indices.append((cell.x_cord, cell.y_cord))

        indices = np.array(indices)
        upper_left = indices.min(axis=0)
        lower_right = indices.max(axis=0)

        self.scene().factory.noise_area.add_noise_area(upper_left[0], upper_left[1], lower_right[0] - upper_left[0],
                                                       lower_right[1] - upper_left[1])

        self.scene().show_noise_matrix()

    def show_facility_context_menu(self, pos: QPoint) -> None:
        """
        Shows context menu to add facility to layout.
        :param pos: Position where the context menu and facility should appear.
        :return: None
        """
        menu = QMenu(self.tr("FacilityMenu"), self)
        for facility in self.scene().factory.facility_list:
            if facility not in [x.attributes for x in self.scene().facilities]:
                action = QAction(facility.name, self)
                action.triggered.connect(functools.partial(self.add_facility, facility, pos))
                menu.addAction(action)
        menu.exec_(self.mapToGlobal(pos))

    def show_media_context_menu(self, pos: QPoint, media: MediaAvailability, cells: List[FactoryCell]) -> None:
        """
        Shows context menu to change MediaAvailability on selected cells.
        :param pos: Position where the context menu should appear.
        :param media: Media to be changed
        :param cells: Selected cells.
        :return: None
        """
        menu = QMenu("MediaMenu", self)
        action_add = QAction(media.name + self.tr(" vorhanden"), self)
        action_remove = QAction(self.tr("Kein ") + media.name, self)
        action_add.triggered.connect(functools.partial(media.change_availability, cells, True))
        action_remove.triggered.connect(functools.partial(media.change_availability, cells, False))
        menu.addAction(action_add)
        menu.addAction(action_remove)
        menu.exec_(self.mapToGlobal(pos))

    def show_light_context_menu(self, pos: QPoint, cells: List[FactoryCell]):
        """
        Shows context menu to change lightning on selected cells.
        :param pos: Position where the context menu should appear.
        :param cells: Selected cells.
        :return: None
        """
        menu = QMenu(self.tr("LightMenu"), self)
        light_off = QAction(self.tr("Kein Licht [< 100lx]"), self)
        light_very_dark = QAction(self.tr("Sehr wenig Licht [100 - 200 lx]"), self)
        light_dark = QAction(self.tr("Wenig Licht [200 - 300lx]"), self)
        light_normal = QAction(self.tr("Normales Licht [300 - 500 lx]"), self)
        light_bright = QAction(self.tr("Helles Licht [500 - 750 lx]"), self)
        light_very_bright = QAction(self.tr("Sehr helles Licht [750 - 1000 lx]"), self)
        light_maximal = QAction(self.tr("maximales Licht [1000 - 1500 lx]"), self)

        light_off.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.OFF))
        light_very_dark.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.VERY_DARK))
        light_dark.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.DARK))
        light_normal.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.NORMAL))
        light_bright.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.BRIGHT))
        light_very_bright.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.VERY_BRIGHT))
        light_maximal.triggered.connect(
            functools.partial(self.scene().change_illuminance, cells, LightLevel.MAXIMAL))

        menu.addAction(light_off)
        menu.addAction(light_very_dark)
        menu.addAction(light_dark)
        menu.addAction(light_normal)
        menu.addAction(light_bright)
        menu.addAction(light_very_bright)
        menu.addAction(light_maximal)
        menu.exec_(self.mapToGlobal(pos))

    def show_area_context_menu(self, pos: QPoint, cells: List[FactoryCell]):
        """
        Shows context menu to change restrictive area of selected cells.
        :param pos: Position where the context menu should appear.
        :param cells: Selected cells.
        :return: None
        """
        menu = QMenu("Area's", self)
        for area in self.scene().factory.area_types:
            action = QAction(area.name, self)
            action.triggered.connect(
                functools.partial(self.scene().change_restrictive_area, cells, area))
            menu.addAction(action)
        menu.exec_(self.mapToGlobal(pos))

    def show_mf_context_menu(self, pos: QPoint):
        """
        Shows context menu to change between mf in or out.
        :param pos: Position where the context menu should appear.
        :return: None
        """
        menu = QMenu("MF", self)
        action = QAction(self.tr("Toggle In/Out"), self)
        action.triggered.connect(self.scene().toggle_mf)
        menu.addAction(action)
        menu.exec_(self.mapToGlobal(pos))

    def show_cf_context_menu(self, pos: QPoint):
        """
        Shows context menu to change between cf in or out.
        :param pos: Position where the context menu should appear.
        :return: None
        """
        menu = QMenu("CF", self)
        action = QAction(self.tr("Toggle In/Out"), self)
        action.triggered.connect(self.scene().toggle_cf)
        menu.addAction(action)
        menu.exec_(self.mapToGlobal(pos))

    def show_restriction_dialog(self, cells: List[FactoryCell]) -> None:
        """
        Shows input dialog to change value of restrictions of selected cells.
        :param cells: Selected cells
        :return: None
        """
        num, ok = QInputDialog.getInt(self, self.scene().actual_restriction.name,
                                      self.tr("Geben Sie einen Wert ein"))
        if ok:
            not_fit = False
            for cell in cells:
                if self.scene().factory.layout[cell.y_cord][cell.x_cord] >= 0:
                    for facility in self.scene().factory.facility_list:
                        if facility.index_num \
                                == self.scene().factory.layout[cell.y_cord][cell.x_cord]:
                            if self.scene().actual_restriction.name == 'floor_capacity':
                                restriction_value = facility.floorload
                            elif self.scene().actual_restriction.name == 'cover_capacity':
                                restriction_value = facility.ceilingload
                            else:
                                restriction_value = facility.height
                            if num < restriction_value:
                                not_fit = True
                            else:
                                self.scene().actual_restriction.matrix[cell.y_cord][
                                    cell.x_cord] = num
                else:
                    self.scene().actual_restriction.matrix[cell.y_cord][cell.x_cord] = num
            if not_fit:
                errordialog = QMessageBox(self)
                errordialog.setText(self.tr("Value error!"))
                errordialog.setInformativeText(self.tr(
                    "Not all cells could be updated because the objects placed on them need a larger {} value than the entered value.").format(
                    self.scene().actual_restriction.name))
                errordialog.icon(QMessageBox.Critical)
                errordialog.exec_()
            self.scene().show_restriction(self.scene().actual_restriction)

    def show_road_context_menu(self, pos: QPoint, cells: List[FactoryCell]) -> None:
        """
        Show context menu to set road status of selected cells
        :param pos: Positions where to show context menu
        :param cells: Cells which are selected
        :return: None
        """
        menu = QMenu("Roads", self)
        action = QAction(self.tr("Fixed Road"), self)
        action.triggered.connect(functools.partial(self.change_road, cells,
                                                   self.scene().factory.parameter[
                                                       'fixed_road_value']))
        menu.addAction(action)
        for cell in cells:
            if self.scene().factory.layout[cell.y_cord][cell.x_cord] == \
                    self.scene().factory.parameter['fixed_road_value']:
                remove = QAction(self.tr("Remove Fixed Road"), self)
                remove.triggered.connect(functools.partial(self.change_road, cells,
                                                           self.scene().factory.parameter[
                                                               'empty_cell_value']))
                menu.addAction(remove)
                break

        menu.exec_(self.mapToGlobal(pos))

    def change_road(self, cells: List[FactoryCell], type: int):
        """
        Changes selected cells to road type.
        :param cells: Cells which gets changed
        :param type: Roadtype
        :return:
        """
        for cell in cells:
            if type == self.scene().factory.parameter['fixed_road_value']:
                cell.set_fixed_road()
            if type == self.scene().factory.parameter['empty_cell_value']:
                if try_delete_fixed_road_cell(self.scene().factory, (cell.y_cord, cell.x_cord)):
                    cell.color_empty()
                else:
                    cell.color_road()

    def add_facility(self, facility: FacilityAttributes, pos: QPoint) -> None:
        """
        Adds new facility to scene.
        :param facility: Facility which gets added
        :param pos: Point where facility gets placed
        :return: None
        """
        item: QGraphicsItem = self.itemAt(pos)
        # muss kleiner sein als das vorherige grid, weil sont überlappt es zu viele grid items
        if self.scene().factory.parameter["color_facilities_by_sound"]:
            facilityitem = FactoryFacility(facility, item.boundingRect().topLeft().x() + 1.5,
                                           item.boundingRect().topLeft().y() + 1.5,
                                           facility.cell_width1 * item.boundingRect().width() - facility.cell_width1 - 2,
                                           facility.cell_width2 * item.boundingRect().height() - facility.cell_width2 - 2,
                                           user=True,
                                           color_sound=self.scene().factory.facility_characteristics[facility.index_num].noise_exposure)
        else:
            facilityitem = FactoryFacility(facility, item.boundingRect().topLeft().x() + 1.5,
                                           item.boundingRect().topLeft().y() + 1.5,
                                           facility.cell_width1 * item.boundingRect().width() - facility.cell_width1 - 2,
                                           facility.cell_width2 * item.boundingRect().height() - facility.cell_width2 - 2,
                                           user=True)
        self.scene().addItem(facilityitem)
        self.scene().facilities.append(facilityitem)

        top_left = facilityitem.top_left_cell()
        try:
            position_facility_on_cell(self.scene().factory, facility, top_left.x_cord,
                                      top_left.y_cord, facility.curr_rotation,
                                      facility.curr_mirror,
                                      True)  # or not facilityitem.check_position():
            facilityitem.make_tool_tip()
        except Exception as e:
            facilityitem.delete()
            errordialog = QMessageBox(self.scene().parent())
            errordialog.setText(self.tr("Facility can't be placed here!"))
            errordialog.setInformativeText(str(e))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        self.scene().update_path()
        self.parent().widget(0).set_facilities_table(self.scene().facilities)

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        """ This method is responsible for zooming in this scene, a little bit buggy"""
        self.update()
        # Zoom Factor
        zoom_in_factor = 1.15
        zoom_out_factor = 1 / zoom_in_factor

        # Zoom
        if event.angleDelta().y() > 0:
            self.curr_scale *= zoom_in_factor
            self.scale(zoom_in_factor, zoom_in_factor)
        elif self.curr_scale > 0.1:
            self.curr_scale *= zoom_out_factor
            self.scale(zoom_out_factor, zoom_out_factor)

        super().wheelEvent(event)

    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        """ This method created a rubberband an leftclick"""
        if event.button() == Qt.RightButton and (self.scene().mode == LayoutMode.REMOVE or
                                                 self.scene().mode == LayoutMode.MEDIA or
                                                 self.scene().mode == LayoutMode.LIGHT or
                                                 self.scene().mode == LayoutMode.RESTRICTION or
                                                 self.scene().mode == LayoutMode.AREA or
                                                 self.scene().mode == LayoutMode.NOISE or
                                                 (
                                                         self.scene().mode == LayoutMode.ADD_FACILITIES and len(
                                                     [x for x in self.scene().items(
                                                         self.mapToScene(event.pos())) if
                                                      isinstance(x, FactoryFacility)]) == 0)):
            self.origin = QPoint(int(event.pos().x()), int(event.pos().y()))
            self.rubber_band.setGeometry(QRect(self.origin, QSize()))
            self.rubber_band.show()

        super().mousePressEvent(event)

    def mouseMoveEvent(self, event: QtGui.QMouseEvent) -> None:
        """ This method will change the rubberband size if the mouse is moving"""
        if not self.origin.isNull():
            self.rubber_band.setGeometry(QRect(self.origin, event.pos()).normalized())

        super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event: QtGui.QMouseEvent) -> None:
        """
        If FactoryCell items are selected by the rubberband
        they will be removed from the scene
        """
        rubberband_sel = False
        point = self.mapToScene(event.pos())
        under = self.scene().items(point)
        selected_point = [x for x in under if isinstance(x, FactoryCell)]
        selected_facility = [x for x in under if isinstance(x, FactoryFacility)]
        if event.button() == Qt.RightButton:
            if self.rubber_band.isVisible():
                self.rubber_band.hide()
                rect = self.rubber_band.geometry()
                rect_scene = self.mapToScene(rect).boundingRect()
                selecteduno = self.scene().items(rect_scene)
                selected = [x for x in selecteduno if isinstance(x, FactoryCell)]
                selected_fac = [x for x in selecteduno if isinstance(x, FactoryFacility)]
                if selected:
                    rubberband_sel = True
                    if self.scene().mode == LayoutMode.REMOVE:
                        for item in selected:
                            if item.gray:
                                item.tone()
                            else:
                                item.graying()
                    if self.scene().mode == LayoutMode.MEDIA:
                        self.show_media_context_menu(event.pos(), self.scene().actual_media,
                                                     selected)
                    if self.scene().mode == LayoutMode.LIGHT:
                        self.show_light_context_menu(event.pos(), selected)
                    if self.scene().mode == LayoutMode.RESTRICTION:
                        self.show_restriction_dialog(selected)
                    if self.scene().mode == LayoutMode.AREA:
                        self.show_area_context_menu(event.pos(), selected)
                    if self.scene().mode == LayoutMode.ADD_FACILITIES:
                        self.show_road_context_menu(event.pos(), selected)
                    if self.scene().mode == LayoutMode.NOISE:
                        self.show_noise_wall_context_menu(event.pos(), selected, selected_fac)
            if not rubberband_sel:
                if self.scene().mode == LayoutMode.REMOVE:
                    for item in selected_point:
                        if item.gray:
                            item.tone()
                        else:
                            item.graying()
                elif self.scene().mode == LayoutMode.ADD_FACILITIES and \
                        not any(isinstance(x, FactoryFacility) for x in under) \
                        and any(isinstance(x, FactoryCell) for x in under):
                    self.show_facility_context_menu(event.pos())
                elif self.scene().mode == LayoutMode.MEDIA:
                    self.show_media_context_menu(event.pos(), self.scene().actual_media,
                                                 selected_point)
                elif self.scene().mode == LayoutMode.LIGHT:
                    self.show_light_context_menu(event.pos(), selected_point)
                elif self.scene().mode == LayoutMode.RESTRICTION:
                    self.show_restriction_dialog(selected_point)
                elif self.scene().mode == LayoutMode.AREA:
                    self.show_area_context_menu(event.pos(), selected_point)
                elif self.scene().mode == LayoutMode.MF:
                    self.show_mf_context_menu(event.pos())
                elif self.scene().mode == LayoutMode.CF:
                    self.show_cf_context_menu(event.pos())
        elif event.button() == Qt.MidButton:
            self.fitInView(0, 0,
                           self.scene().factory.layout.shape[1] * self.scene().factory.cell_zoom,
                           self.scene().factory.layout.shape[0] * self.scene().factory.cell_zoom,
                           Qt.KeepAspectRatio)
        elif event.button() == Qt.LeftButton:
            if self.scene().mode == LayoutMode.MF and selected_facility:
                self.scene().display_mf(selected_facility[0])
            elif self.scene().mode == LayoutMode.CF and selected_facility:
                self.scene().display_cf(selected_facility[0])

        super().mouseReleaseEvent(event)
