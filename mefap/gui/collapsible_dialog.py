import matplotlib
from PySide2 import QtCore
from PySide2.QtGui import QPixmap, QIcon, QColor, QPainter, QBrush, QPen
from PySide2.QtWidgets import QPushButton, QDialog, QTreeWidget, QVBoxLayout, QListWidget, \
    QTreeWidgetItem, QListWidgetItem

from mefap.factory.factory_model import FactoryModel
from mefap.gui.factory_compare_dialog import FactoryCompareDialog
from mefap.gui.util import colors


class SectionExpandButton(QPushButton):
    """
    A QPushbutton that can expand or collapse its section
    """

    def __init__(self, item, text="", parent=None) -> None:
        super().__init__(text, parent)
        self.section = item
        self.clicked.connect(self.on_clicked)

    def on_clicked(self) -> None:
        """
        Toggle expand/collapse of section by clicking
        :return: None
        """
        if self.section.isExpanded():
            self.section.setExpanded(False)
        else:
            self.section.setExpanded(True)


class CollapsibleDialog(QDialog):
    """
    A dialog to which collapsible sections can be added;
    subclass and reimplement define_sections() to define sections and
    add them as (title, widget) tuples to self.sections
    """

    def __init__(self, factory: FactoryModel) -> None:
        super().__init__()
        self.tree = QTreeWidget()
        self.tree.setHeaderHidden(True)
        layout = QVBoxLayout()
        layout.addWidget(self.tree)
        self.setLayout(layout)
        self.tree.setIndentation(0)

        self.factory = factory
        self.d_button = None
        self.d_section = None
        self.area_button = None
        self.area_section = None
        self.noise_legend_section = None
        self.noise_legend_button = None

        self.fac_table = QListWidget(parent=self)
        self.area_table = QListWidget(parent=self)
        self.legend_table = QListWidget(parent=self)

        item_movable = QListWidgetItem(self.tr("Movable facility"), self.legend_table)
        item_movable.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.IPH_RED)
        item_movable.setIcon(QIcon(pixmap))

        item_fixed = QListWidgetItem(self.tr("Fixed facility"), self.legend_table)
        item_fixed.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.WHITE)
        painter = QPainter(pixmap)
        brush = QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(colors.IPH_RED)
        painter.setBrush(brush)
        painter.setPen(QPen(brush, 5, QtCore.Qt.SolidLine, QtCore.Qt.SquareCap))
        painter.drawLine(0, 0, pixmap.width(), pixmap.height())
        painter.drawLine(pixmap.width(), 0, 0, pixmap.height())
        painter.drawLine(pixmap.width() / 2, 0, 0, pixmap.height() / 2)
        painter.drawLine(pixmap.width(), pixmap.height()/2, pixmap.width()/2, pixmap.height())
        painter.drawLine(pixmap.width()/2, 0, pixmap.width(), pixmap.height()/2)
        painter.drawLine(0, pixmap.height()/2, pixmap.width()/2, pixmap.height())
        painter.end()
        item_fixed.setIcon(QIcon(pixmap))

        path = QListWidgetItem(self.tr("Variable transport route"), self.legend_table)
        path.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.GREY_CELL)
        path.setIcon(QIcon(pixmap))

        path_fixed = QListWidgetItem(self.tr("Fixed transport route"), self.legend_table)
        path_fixed.setForeground(colors.BLACK)
        pixmap = QPixmap(100, 100)
        pixmap.fill(colors.DARK_GREY_CELL)
        path_fixed.setIcon(QIcon(pixmap))

        self.sections = []
        self.define_sections()
        self.add_sections()

    def add_sections(self) -> None:
        """
        Adds a collapsible sections for every
        (title, widget) tuple in self.sections
        :return: None
        """
        for (title, widget) in self.sections:
            button1 = self.add_button(title)
            section1 = self.add_widget(button1, widget)
            button1.addChild(section1)
            if title == self.tr("Diagram"):
                self.d_button = button1
                self.d_button.setToolTip(0, self.tr('A network diagram with all evaluation criteria is displayed here.'))
                self.d_section = section1
            elif title == self.tr("Restrictive area's"):
                self.area_button = button1
                self.area_button.setToolTip(0, self.tr('All restrictive areas of use and their associated color are displayed here.'))
                self.area_section = section1
            elif title == self.tr("Legend"):
                button1.setToolTip(0, self.tr('Here is a legend showing what which cell means.'))
            elif title == self.tr('Availible Facilities'):
                button1.setToolTip(0, self.tr('This shows which facilities have already been placed.'))

    def define_sections(self) -> None:
        """
        This adds sections to our collapsible Dialog
        :return: None
        """
        self.sections.append((self.tr("Legend"), self.legend_table))
        self.sections.append((self.tr("Availible Facilities"), self.fac_table))
        self.sections.append((self.tr("Restrictive area's"), self.area_table))
        self.sections.append(
            (self.tr("Diagram"), FactoryCompareDialog(
                self.factory.layouts_evaluation_data[:self.factory.parameter['number_saved_variants']])))

    def add_button(self, title) -> QTreeWidgetItem:
        """
        creates a QTreeWidgetItem containing a button
        to expand or collapse its section
        :param title: Title of our created section
        :return: None
        """
        item = QTreeWidgetItem()
        self.tree.addTopLevelItem(item)
        self.tree.setItemWidget(item, 0, SectionExpandButton(item, text=title))
        return item

    def add_widget(self, button, widget) -> QTreeWidgetItem:
        """
        creates a QWidgetItem containing the widget,
        as child of the button-QWidgetItem
        :param button: The button for expanding our Widget
        :param widget: Widget to display after expansion
        :return: None
        """
        section = QTreeWidgetItem(button)
        section.setDisabled(True)
        self.tree.setItemWidget(section, 0, widget)
        return section

    def clear_facilities_table(self) -> None:
        """
        Deletes all elements from the fac_table.
        :return: None
        """
        self.fac_table.clear()

    def set_facilities_table(self, scene_facilities) -> None:
        """
        Fills the facility table with all of our facilities in our factory and places a square with
        the corresponding color next to them.
        Already placed facilities are marked in a different text color.
        :param scene_facilities: List of our facilities which are already placed in our scene
        :return: None
        """
        self.fac_table.clear()
        for facility in self.factory.facility_list:
            if facility in [x.attributes for x in scene_facilities]:
                item = QListWidgetItem(facility.name, self.fac_table)
                item.setForeground(colors.FAC_ORANGE)
                pixmap = QPixmap(100, 100)
                pixmap.fill(facility.color)
                item.setIcon(QIcon(pixmap))
            else:
                QListWidgetItem(facility.name, self.fac_table)

    def set_area_table(self) -> None:
        """
        Fills out area_table with all all types of different restrictive areas.
        Also places a square with the corresponding color next to them.
        :return: None
        """
        self.area_table.clear()
        for area in self.factory.area_types:
            item = QListWidgetItem(area.name, self.area_table)
            pixmap = QPixmap(100, 100)
            pixmap.fill(QColor(area.color))
            item.setIcon(QIcon(pixmap))

    def clear_area_table(self) -> None:
        """
        Deletes all elements from the area_table.
        :return: None
        """
        self.area_table.clear()

    def update_sidebar(self) -> None:
        """
        After changes inside the factory some values needs to get updated.
        :return: None
        """
        self.sections.pop()
        self.tree.removeItemWidget(self.d_button, 0)
        self.tree.removeItemWidget(self.d_section, 0)
        self.tree.takeTopLevelItem(self.tree.topLevelItemCount() - 1)
        self.sections.append(
            (self.tr("Diagram"), FactoryCompareDialog(
                self.factory.layouts_evaluation_data[:self.factory.parameter['number_saved_variants']])))
        button1 = self.add_button(self.sections[-1][0])
        section1 = self.add_widget(button1, self.sections[-1][1])
        button1.addChild(section1)
        self.d_section = section1
        self.d_button = button1
        self.d_button.setToolTip(0, self.tr('A network diagram with all evaluation criteria is displayed here.'))

        self.set_area_table()

    def add_noise_legend(self) -> None:
        """
        Adds a legend section for the noise heatmap.
        :return: None
        """
        cmap = matplotlib.cm.get_cmap('RdYlGn')
        nois_leg = QListWidget(parent=self)
        db_highest = 85
        db_threshold = 40
        db_highest -= db_threshold

        lower_end = QListWidgetItem("≤ 40 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(255, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        lower_end.setIcon(QIcon(pixmap))

        fifty = QListWidgetItem("= 50 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(1 - (50 - db_threshold) / db_highest, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        fifty.setIcon(QIcon(pixmap))

        sixty = QListWidgetItem("= 60 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(1 - (60 - db_threshold) / db_highest, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        sixty.setIcon(QIcon(pixmap))

        seventy = QListWidgetItem("= 70 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(1 - (70 - db_threshold) / db_highest, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        seventy.setIcon(QIcon(pixmap))

        eighty = QListWidgetItem("= 80 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(1 - (80 - db_threshold) / db_highest, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        eighty.setIcon(QIcon(pixmap))

        upper_end = QListWidgetItem("≥ 85 dB", nois_leg)
        pixmap = QPixmap(100, 100)
        color = cmap(0, bytes=True)
        pixmap.fill(QColor(color[0], color[1], color[2], color[3]))
        upper_end.setIcon(QIcon(pixmap))

        nois_leg.addItem(lower_end)
        nois_leg.addItem(upper_end)

        self.noise_legend_button = self.add_button(self.tr("Noise Legend"))
        self.noise_legend_button.setToolTip(0, self.tr('A legend is shown here which indicates which color means which volume.'))
        self.noise_legend_section = self.add_widget(self.noise_legend_button, nois_leg)
        self.noise_legend_button.addChild(self.noise_legend_section)

    def remove_noise_legend(self) -> None:
        """
        Remove section of noise legend.
        :return: None
        """
        if self.noise_legend_section and self.noise_legend_button:
            self.tree.removeItemWidget(self.noise_legend_button, 0)
            self.tree.removeItemWidget(self.noise_legend_section, 0)
            self.tree.takeTopLevelItem(self.tree.topLevelItemCount() - 1)
            self.noise_legend_section = None
            self.noise_legend_button = None

    def translate_ui(self) -> None:
        """
        Updates this widget if the language of the gui changes.
        :return: None
        """
        self.d_button = None
        self.d_section = None
        self.tree.takeTopLevelItem(2)
        self.tree.takeTopLevelItem(1)
        self.tree.takeTopLevelItem(0)
        self.define_sections()
        self.add_sections()
