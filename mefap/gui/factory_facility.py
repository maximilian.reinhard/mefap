import logging
from typing import List, Tuple

import matplotlib
from PySide2.QtCore import Qt, QObject
from PySide2.QtGui import QBrush, QColor
from PySide2.QtWidgets import QGraphicsRectItem, QGraphicsItem, QMenu, QAction, \
    QGraphicsSceneMouseEvent, QGraphicsTextItem, QMessageBox
from numpy.linalg import norm

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.gui.factory_cell import FactoryCell
from mefap.gui.util import colors
from mefap.gui.util.layout_mode import LayoutMode
from mefap.gui.util.sinksource import SINK_SOURCE, SOURCE, SINK
from mefap.optimization.deletePath import delete_paths_from_layout
from mefap.optimization.positionFacility import re_position_facility_all_paths


class FactoryFacility(QGraphicsRectItem, QObject):
    """
    The FactoryFacility class is the representation of a Facility of the factory.
    It will be placed on the grid inside the FactoryLayoutScene.
    """

    def __init__(self, facility: FacilityAttributes, *args, user=False, color_sound=-1, **kwargs) -> None:
        QGraphicsRectItem.__init__(self, *args, **kwargs)
        self.attributes: FacilityAttributes = facility
        self.color_sound = color_sound
        if user:
            self.attributes.fixed_position = True
        #else:
        #    self.attributes.fixed_position = False
        self.make_coloring()
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QGraphicsItem.ItemIsMovable, True)

        # savinf old position if new position is invalid
        self.old_pos = self.pos()

        self.sink_source: List[QGraphicsTextItem] = []

        self.name = QGraphicsTextItem(facility.name, self)
        self.name.setDefaultTextColor(colors.BLACK)
        self.name.setPos(self.boundingRect().topLeft())
        self.name.adjustSize()
        scale = self.name.scale()
        if self.name.boundingRect().width() > self.boundingRect().width():
            scale = min(scale, self.boundingRect().width() / self.name.boundingRect().width())
        if self.name.boundingRect().height() > self.boundingRect().height():
            scale = min(scale, self.boundingRect().height() / self.name.boundingRect().height())
        self.name.setScale(scale)
        # self.name.setPos(self.boundingRect().left(), self.boundingRect().y() + (self.name.boundingRect().height()/2))
        self.name.setTextInteractionFlags(Qt.NoTextInteraction)
        if not facility.curr_mirror:
            self.attributes.curr_mirror = False
        if not facility.curr_rotation:
            self.attributes.curr_rotation = 0

        self.make_tool_tip()

        self.snap()

    def rotate_after_loading(self) -> None:
        """
        This method is rotating the facility to the right position depending on the current_rot.
        +1 is the border thickness of the cells. facility.
        :return: None
        """
        if self.attributes.cell_width2 == self.attributes.cell_width1:
            self.setTransformOriginPoint(self.boundingRect().center())
            self.setRotation(self.attributes.curr_rotation * 90)
        elif self.attributes.curr_rotation == 1:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(90)
            self.moveBy(self.attributes.cell_width2 *
                        self.scene().factory.cell_zoom - 1,
                        0)
        elif self.attributes.curr_rotation == 2:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(180)
            self.moveBy(
                self.attributes.cell_width1 * self.scene().factory.cell_zoom - 1,
                self.attributes.cell_width2 * self.scene().factory.cell_zoom - 1)
        elif self.attributes.curr_rotation == 3:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(270)
            self.moveBy(0,
                        self.attributes.cell_width1 *
                        self.scene().factory.cell_zoom - 1)
        self.old_pos = self.pos()
        self.make_tool_tip()

    def mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        """
        On right click open menu with edit options for the positioning of this facility
        :param event:
        :return: None
        """
        if self.scene().mode == LayoutMode.ADD_FACILITIES:
            if event.button() == Qt.RightButton:
                menu = QMenu(self.tr("Rotation"))
                rotate90 = QAction(self.tr("Rotate 90"))
                rotate90.triggered.connect(self.rotate_right_90)
                menu.addAction(rotate90)
                rotate180 = QAction(self.tr("Rotate 180"))
                rotate180.triggered.connect(self.rotate_right_180)
                menu.addAction(rotate180)
                delete = QAction(self.tr("Delete"))
                delete.triggered.connect(self.delete)
                menu.addAction(delete)
                mirror = QAction(self.tr("Mirror"))
                mirror.triggered.connect(self.mirror)
                menu.addAction(mirror)
                lock = QAction(self.tr("Un-/Lock position"))
                lock.triggered.connect(self.lock)
                menu.addAction(lock)
                menu.exec_(event.screenPos())
            else:
                self.check_position()
        if self.attributes.curr_rotation:
            self.make_tool_tip()
        super().mouseReleaseEvent(event)

    def snap(self) -> Tuple[FactoryCell, float]:
        """
        Snaps this object to right grid position.
        :return: tupel with nearest factory cell and distance
        """
        self_scene_pos = self.top_left_scene_pos()
        items = self.collidingItems()
        items = [x for x in items if isinstance(x, FactoryCell)]
        if items:
            item_scene_pos = items[0].mapToScene(items[0].boundingRect().topLeft())
            nearest = (
                items[0], norm([self_scene_pos.x() - item_scene_pos.x(),
                                self_scene_pos.y() - item_scene_pos.y()]))
            for item in items:
                item_scene_pos = item.mapToScene(item.boundingRect().topLeft())
                distance = norm([self_scene_pos.x() - item_scene_pos.x(),
                                 self_scene_pos.y() - item_scene_pos.y()])
                if distance < nearest[1]:
                    nearest = (item, distance)

            item_scene_pos = nearest[0].mapToScene(nearest[0].boundingRect().topLeft())
            self.moveBy(item_scene_pos.x() - self_scene_pos.x() + 1,
                        item_scene_pos.y() - self_scene_pos.y() + 1)
            return nearest

    def check_position(self, rot: float = -1) -> bool:
        """
        This method snaps this objects upper left corner to the nearest upper left corner
        of underlying grid rectangles.
        :return:
        """
        self.delete_old_sink_source()

        nearest = self.snap()

        if len(
                self.collidingItems()) != self.attributes.cell_width2 * self.attributes.cell_width1 + len(
            self.childItems()):
            self.outside_grid()
            if rot != -1:
                self.setRotation(rot)
                self.attributes.curr_rotation = rot // 90
            self.setPos(self.old_pos)
            self.generate_sink_source()
            logging.info("Not on grid")
            return False
        old_fixed = self.attributes.fixed_position
        self.attributes.fixed_position = False
        try:
            re_position_facility_all_paths(self.scene().factory, self.attributes,
                                           nearest[0].x_cord, nearest[0].y_cord,
                                           self.attributes.curr_rotation,
                                           self.attributes.curr_mirror, True)
            self.old_pos = self.pos()
            self.set_index_in_matrix()
        except Exception as e:
            if rot != -1:
                self.setRotation(rot)
                self.attributes.curr_rotation = rot // 90
            self.setPos(self.old_pos)
            self.generate_sink_source()
            logging.info("Not allowed by backend")
            errordialog = QMessageBox(self.scene().parent())
            errordialog.setText(self.tr("Placement error"))
            errordialog.setInformativeText(str(e))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
            self.attributes.fixed_position = old_fixed
            return False
        self.attributes.fixed_position = old_fixed
        self.scene().update_path()
        self.make_tool_tip()
        return True

    def rotate_right_90(self) -> None:
        """
        This method is responsible for rotating this object to the right by 90 degrees
        :return: None
        """
        old_fixed = self.attributes.fixed_position
        self.attributes.fixed_position = False
        try:
            if self.attributes.curr_rotation >= 3:
                if re_position_facility_all_paths(self.scene().factory, self.attributes,
                                                  self.attributes.curr_position[1],
                                                  self.attributes.curr_position[0], 0,
                                                  self.attributes.curr_mirror, True):
                    old = self.mapToScene(self.boundingRect().topRight())
                    self.setRotation(0)
                    new = self.mapToScene(self.boundingRect().topLeft())
                    self.moveBy(old.x() - new.x(), old.y() - new.y())
            else:
                if re_position_facility_all_paths(self.scene().factory, self.attributes,
                                                  self.attributes.curr_position[1],
                                                  self.attributes.curr_position[0],
                                                  self.attributes.curr_rotation + 1,
                                                  self.attributes.curr_mirror, True):
                    if self.attributes.curr_rotation == 1:
                        old = self.mapToScene(self.boundingRect().topLeft())
                        self.setRotation(90)
                        new = self.mapToScene(self.boundingRect().bottomLeft())
                        self.moveBy(old.x() - new.x(), old.y() - new.y())
                    elif self.attributes.curr_rotation == 2:
                        old = self.mapToScene(self.boundingRect().bottomLeft())
                        self.setRotation(180)
                        new = self.mapToScene(self.boundingRect().bottomRight())
                        self.moveBy(old.x() - new.x(), old.y() - new.y())
                    elif self.attributes.curr_rotation == 3:
                        old = self.mapToScene(self.boundingRect().bottomRight())
                        self.setRotation(270)
                        new = self.mapToScene(self.boundingRect().topRight())
                        self.moveBy(old.x() - new.x(), old.y() - new.y())
        except Exception as e:
            errordialog = QMessageBox(self.scene().parent())
            errordialog.setText(self.tr("Placement error"))
            errordialog.setInformativeText(str(e))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        self.attributes.fixed_position = old_fixed
        self.generate_sink_source()
        self.scene().update_path()
        self.old_pos = self.pos()
        self.make_tool_tip()

    def top_left_cell(self) -> FactoryCell:
        """
        Returns cell on the top left corner of this object.
        :return: FactoryCell
        """
        top_left = None
        items = self.collidingItems()
        items = [x for x in items if isinstance(x, FactoryCell)]
        for item in items:
            if not top_left:
                top_left = item
            else:
                if item.x_cord <= top_left.x_cord and item.y_cord <= top_left.y_cord:
                    top_left = item
        return top_left

    def top_left_scene_pos(self):
        """
        Returns position of the top left corner from this object.
        :return:
        """
        if self.rotation() == 0:
            self_scene_pos = self.mapToScene(self.boundingRect().topLeft())
        elif self.rotation() == 90:
            self_scene_pos = self.mapToScene(self.boundingRect().bottomLeft())
        elif self.rotation() == 180:
            self_scene_pos = self.mapToScene(self.boundingRect().bottomRight())
        else:
            self_scene_pos = self.mapToScene(self.boundingRect().topRight())
        return self_scene_pos

    def rotate_right_180(self) -> None:
        """
        This method is responsible for rotating this object to the right by 180 degrees
        :return: None
        """
        old_fixed = self.attributes.fixed_position
        self.attributes.fixed_position = False
        try:
            if self.attributes.curr_rotation == 3 and re_position_facility_all_paths(
                    self.scene().factory,
                    self.attributes,
                    self.attributes.curr_position[
                        1],
                    self.attributes.curr_position[
                        0],
                    1,
                    self.attributes.curr_mirror,
                    True):
                old = self.mapToScene(self.boundingRect().topRight())
                self.setRotation(90)
                new = self.mapToScene(self.boundingRect().bottomLeft())
                self.moveBy(old.x() - new.x(), old.y() - new.y())
            elif self.attributes.curr_rotation == 2 and re_position_facility_all_paths(
                    self.scene().factory,
                    self.attributes,
                    self.attributes.curr_position[
                        1],
                    self.attributes.curr_position[
                        0], 0,
                    self.attributes.curr_mirror,
                    True):
                old = self.mapToScene(self.boundingRect().bottomRight())
                self.setRotation(0)
                new = self.mapToScene(self.boundingRect().topLeft())
                self.moveBy(old.x() - new.x(), old.y() - new.y())
            elif self.attributes.curr_rotation == 1 and re_position_facility_all_paths(
                    self.scene().factory,
                    self.attributes,
                    self.attributes.curr_position[
                        1],
                    self.attributes.curr_position[
                        0], 3,
                    self.attributes.curr_mirror,
                    True):
                old = self.mapToScene(self.boundingRect().bottomLeft())
                self.setRotation(270)
                new = self.mapToScene(self.boundingRect().topRight())
                self.moveBy(old.x() - new.x(), old.y() - new.y())
            elif self.attributes.curr_rotation == 0 and re_position_facility_all_paths(
                    self.scene().factory,
                    self.attributes,
                    self.attributes.curr_position[1],
                    self.attributes.curr_position[0],
                    2, self.attributes.curr_mirror,
                    True):
                old = self.mapToScene(self.boundingRect().topLeft())
                self.setRotation(180)
                new = self.mapToScene(self.boundingRect().bottomRight())
                self.moveBy(old.x() - new.x(), old.y() - new.y())
        except Exception as e:
            errordialog = QMessageBox(self.scene().parent())
            errordialog.setText(self.tr("Placement error"))
            errordialog.setInformativeText(str(e))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        self.attributes.fixed_position = old_fixed
        self.generate_sink_source()
        self.scene().update_path()
        self.old_pos = self.pos()
        self.make_tool_tip()

    def outside_grid(self) -> bool:
        """
        Creates error dialog if this facility is placed outside the grid.
        :return:
        """
        errordialog = QMessageBox(self.scene().parent())
        errordialog.setText(self.tr("Placement error"))
        errordialog.setInformativeText(self.tr(
            "No Placing outside the grid allowed!"))
        errordialog.setIcon(QMessageBox.Warning)
        return errordialog.exec_()

    def is_allowed_area(self) -> bool:
        """
        Checks if this facility is allowed to be placed on this position. (restrictive area)
        :return:
        """
        facility_type_area = self.scene().factory.get_facilitytype_by_name(
            self.attributes.facility_type)
        if self.scene().factory.area_exists(facility_type_area.id):
            for cell in self.collidingItems():
                if isinstance(cell, FactoryCell) and facility_type_area.id != cell.get_area():
                    return False
        return True

    def delete(self, nopath=False, without_propagation=False) -> None:
        """
        This method will be called to delete this object
        :return:
        """
        self.remove_index_in_matrix()
        self.attributes.fixed_position = False
        self.attributes.curr_position = [None, None]
        self.attributes.curr_mirror = None
        self.attributes.curr_rotation = None
        self.attributes.curr_sink_position = [None, None]
        self.attributes.curr_source_position = [None, None]
        self.scene().facilities.remove(self)
        if not without_propagation:
            self.scene().propagate_up_facility_list()
        delete_paths_from_layout(self.scene().factory, self.attributes)
        if not nopath:
            self.scene().update_path()
        self.scene().removeItem(self)
        del self

    def set_index_in_matrix(self, quiet=False) -> None:
        """
        The method will be called after every moving of this object to ensure the right values
        inside the layout matrix of the factory object.
        :return:
        """
        if len(self.collidingItems()) != \
                self.attributes.cell_width2 * self.attributes.cell_width1 + len(self.childItems()):
            raise ValueError("Number of items bigger/smaller as expected")
        else:
            self.remove_index_in_matrix()
            min_cell: FactoryCell = None
            for x in self.collidingItems():
                if isinstance(x, FactoryCell):
                    if not min_cell:
                        min_cell = x
                    elif x.x_cord < min_cell.x_cord or x.y_cord < min_cell.y_cord:
                        min_cell = x
                    x.set_index_in_matrix(self.attributes.index_num)
            self.attributes.curr_position = [min_cell.y_cord, min_cell.x_cord]
            if not quiet:
                logging.info("{} was set to position {}".format(self.attributes.name,
                                                                self.attributes.curr_position))
            self.generate_sink_source()

    def remove_index_in_matrix(self) -> None:
        """
        This method will be called before moving this object for removing the position of this
        object inside the factory object layout matrix
        :return: None
        """
        self.scene().factory.remove_from_layout(self.attributes.index_num)

    def generate_sink_source(self) -> None:
        """
        This method generates the QGraphicsItems which reperesenting sink and source of this
        facility.
        :return:
        """
        self.delete_old_sink_source()
        if self.scene().mode == LayoutMode.INFO:
            cell_width = self.boundingRect().width() / self.attributes.cell_width1
            cell_height = self.boundingRect().height() / self.attributes.cell_width2
            if self.attributes.source_position == self.attributes.sink_position:
                sinksource = QGraphicsTextItem(SINK_SOURCE, self)
                sinksource.setDefaultTextColor(colors.BLACK)
                sinksource.setScale((self.boundingRect().height() / self.attributes.cell_width2)
                                    / sinksource.boundingRect().height())
                if self.attributes.curr_mirror:
                    # +1 because otherwise it will be placed right to the right position
                    x_cord = self.boundingRect().topRight().x() - (
                            self.attributes.sink_position[1] + 1) * cell_width
                else:
                    x_cord = self.boundingRect().topLeft().x() + self.attributes.sink_position[
                        1] * cell_width
                sinksource.setPos(x_cord,
                                  self.boundingRect().topLeft().y() + self.attributes.sink_position[
                                      0] * cell_height)
                self.sink_source.append(sinksource)
            else:
                source = QGraphicsTextItem(SOURCE, self)
                sink = QGraphicsTextItem(SINK, self)
                source.setDefaultTextColor(colors.BLACK)
                sink.setDefaultTextColor(colors.BLACK)
                sink.setScale(cell_height / sink.boundingRect().height())
                source.setScale(cell_height / source.boundingRect().height())
                if self.attributes.curr_mirror:
                    x_cord_sink = self.boundingRect().topRight().x() - (
                            self.attributes.sink_position[1] + 1) * cell_width
                    x_cord_source = self.boundingRect().topRight().x() - (
                            self.attributes.source_position[1] + 1) * cell_width
                else:
                    x_cord_sink = self.boundingRect().topLeft().x() + self.attributes.sink_position[
                        1] * cell_width
                    x_cord_source = self.boundingRect().topLeft().x() + \
                                    self.attributes.source_position[
                                        1] * cell_width
                sink.setPos(x_cord_sink,
                            self.boundingRect().topLeft().y() + self.attributes.sink_position[
                                0] * cell_height)
                source.setPos(x_cord_source,
                              self.boundingRect().topLeft().y() + self.attributes.source_position[
                                  0] * cell_height)
                self.sink_source.append(sink)
                self.sink_source.append(source)

    def delete_old_sink_source(self) -> None:
        """
        This method deletes all QGraphicItems which are representing sink and source.
        :return: None
        """
        for text in self.sink_source:
            self.scene().removeItem(text)
        self.sink_source.clear()

    def mirror(self) -> None:
        """
        This method is for mirroring the facility.
        The fixed position attribut need to false for enabling the backend to mirror sind and source.
        :return: None
        """
        old_fixed = self.attributes.fixed_position
        self.attributes.fixed_position = False
        try:
            if self.attributes.curr_mirror:
                re_position_facility_all_paths(self.scene().factory, self.attributes,
                                               self.attributes.curr_position[1],
                                               self.attributes.curr_position[0],
                                               self.attributes.curr_rotation,
                                               False, True)
            else:
                re_position_facility_all_paths(self.scene().factory, self.attributes,
                                               self.attributes.curr_position[1],
                                               self.attributes.curr_position[0],
                                               self.attributes.curr_rotation,
                                               True, True)
        except Exception as e:
            errordialog = QMessageBox(self.scene().parent())
            errordialog.setText(self.tr("Placement error"))
            errordialog.setInformativeText(str(e))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        self.attributes.fixed_position = old_fixed
        self.generate_sink_source()
        self.scene().update_path()
        self.make_tool_tip()

    def lock(self) -> None:
        """
        Locks position of this facility.
        :return: None
        """
        if self.attributes.fixed_position:
            self.attributes.fixed_position = False
        else:
            self.attributes.fixed_position = True
        self.make_coloring()
        self.make_tool_tip()

    def make_tool_tip(self) -> None:
        """
        Adding a tool tip to this facility.
        :return: None
        """
        self.setToolTip(
            self.tr("Name: {}\nPosition: {}\nRotation: {}\nSpiegelung: {}\nFixiert: {}".format(self.attributes.name,
                                                                                                self.attributes.curr_position,
                                                                                                self.attributes.curr_rotation * 90,
                                                                                                self.attributes.curr_mirror,
                                                                                                self.attributes.fixed_position)))

    def make_coloring(self) -> None:
        """
        The method is coloring the facility in the right color, including the fixed look.
        :return: None
        TODO: not called after every layut switch.
        """
        if self.color_sound >= 0:
            cmap = matplotlib.cm.get_cmap('RdYlGn')
            exposure = self.color_sound
            db_highest = 85
            db_threshold = 40
            db_highest -= db_threshold
            exposure -= db_threshold
            color = cmap(1 - exposure / db_highest, bytes=True)
            sound_color = QColor(color[0], color[1], color[2], color[3])

        if self.attributes.fixed_position:
            if self.color_sound >= 0:
                self.setBrush(QBrush(sound_color, bs=Qt.Dense5Pattern))
            else:
                self.setBrush(QBrush(self.attributes.color, bs=Qt.Dense5Pattern))
        else:
            if self.color_sound >= 0:
                self.setBrush(QBrush(sound_color))
            else:
                self.setBrush(QBrush(self.attributes.color))
