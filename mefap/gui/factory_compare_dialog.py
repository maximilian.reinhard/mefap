from PySide2.QtCore import QCoreApplication
from math import pi
from typing import List

import matplotlib
from PySide2 import QtWidgets
from PySide2.QtWidgets import QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from mefap.factory.evaluation_data import EvaluationData

matplotlib.use('Qt5Agg')


class MplCanvas(FigureCanvasQTAgg):
    """
    This class is for creating a spider chart with the given EvaluationData
    """

    def __init__(self, data: List[EvaluationData], width, height, dpi, parent=None, index=None):
        self.old = (width, height, dpi)
        fig = Figure(figsize=(width, height), dpi=dpi)

        catalog = [QCoreApplication.translate("EvaluationDialog", "media_compatibility", None),
                   QCoreApplication.translate("EvaluationDialog", "media_availability", None),
                   QCoreApplication.translate("EvaluationDialog", "material_flow_length", None),
                   QCoreApplication.translate("EvaluationDialog", "no_overlapping", None),
                   QCoreApplication.translate("EvaluationDialog", "route_continuity", None),
                   QCoreApplication.translate("EvaluationDialog", "land_use_degree", None),
                   QCoreApplication.translate("EvaluationDialog", "lighting", None),
                   QCoreApplication.translate("EvaluationDialog", "quiet", None),
                   QCoreApplication.translate("EvaluationDialog", "vibration", None),
                   QCoreApplication.translate("EvaluationDialog", "temperature", None),
                   QCoreApplication.translate("EvaluationDialog", "cleanliness", None),
                   QCoreApplication.translate("EvaluationDialog", "direct_communication", None),
                   QCoreApplication.translate("EvaluationDialog", "formal_communication", None)]

        # number of variable
        categories = catalog
        N = len(catalog)

        # What will be the angle of each axis in the plot? (we divide the plot / number of variable)
        angles = [n / float(N) * 2 * pi for n in range(N)]
        angles += angles[:1]

        # Initialise the spider plot
        self.axes = fig.add_subplot(111, polar=True)

        # If you want the first axis to be on top:
        self.axes.set_theta_offset(pi / 2)
        self.axes.set_theta_direction(-1)

        # Draw one axe per variable + add labels labels yet
        self.axes.set_xticks(angles[:-1])
        self.axes.set_xticklabels(categories)

        # Draw ylabels
        self.axes.set_rlabel_position(0)
        self.axes.set_ylim(0, 1)
        self.axes.set_yticks([0.2, 0.4, 0.6, 0.8, 1])
        self.axes.set_yticklabels(["0.2", "0.4", "0.6", "0.8", "1"], color="grey", size=7)

        # ------- PART 2: Add plots
        name_list = []
        if data:
            for i, eval_data in enumerate(data):
                self.add_eval(eval_data, angles, i)
                name_list.append("Layout {}".format(i))

        # Add legend
        if not index:
            fig.legend(loc='lower left', labels=name_list)
        else:
            fig.legend(loc='lower left', labels=["Layout {}".format(index)])

        super().__init__(fig)

    def add_eval(self, eval_data: EvaluationData, angles, group) -> None:
        """
        Adds EvaluationData to the spider chart.
        :param eval_data: The EvaluationData whch gets added
        :param angles: Angle for each axis in the plot
        :param group: The data belongs to.
        :return: None
        """
        values = [eval_data.media_compatibility[1], eval_data.media_availability[1], eval_data.mf_distance[1],
                  eval_data.mf_overlapping[1], eval_data.mf_constancy[1], eval_data.area_utilization[1],
                  eval_data.illuminance[1], eval_data.noise[1], eval_data.vibration[1], eval_data.temperature[1],
                  eval_data.cleanliness[1], eval_data.direct_com[1], eval_data.formal_com[1]]
        values += values[:1]
        self.axes.plot(angles, values, linewidth=1, linestyle='solid',
                       label="group {}".format(group))
        self.axes.fill(angles, values, 'r', alpha=0.1)


class FactoryCompareDialog(QWidget):
    """
    This Dialog shows the spider chart from MplCanvas
    """

    def __init__(self, data: List[EvaluationData], *args, width=4, height=4, dpi=100, index=None,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)

        if not index:
            self.sc = MplCanvas(data, width, height, dpi)
        else:
            self.sc = MplCanvas(data, width, height, dpi, index=index)
        # self.sc = FigureCanvasQTAgg(Figure())
        # Create toolbar, passing canvas as first parament, parent (self, the MainWindow) as second.
        # self.toolbar = NavigationToolbar(self.sc, self)
        self.layout = QtWidgets.QHBoxLayout()
        # self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.sc)

        self.setLayout(self.layout)

    def update_canvas(self, data: List[EvaluationData]) -> None:
        """
        Removes old data from diagram and created a new Diagram from the given EvaluationData.
        :param data: List of EvaluationData.
        :return: None
        """
        old_size = self.sc.old
        self.layout.removeWidget(self.sc)
        self.layout.removeWidget(self.toolbar)
        del self.sc
        del self.toolbar
        self.sc = MplCanvas(data, old_size[0], old_size[1], old_size[2])
        # self.toolbar = NavigationToolbar(self.sc, self)
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.sc)
