import copy
import logging

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QButtonGroup, QWidget, QVBoxLayout, QRadioButton

from mefap.factory.factory_model import FactoryModel
from mefap.gui.compare_layout import CompareLayoutScene
from mefap.gui.compare_view import CompareView
from mefap.gui.factory_compare_dialog import FactoryCompareDialog
from mefap.gui.generated.CompareDialog import Ui_compare_dialog


class CompareDialog(QDialog):
    """
    This Dialog is displaying all saved variants from our layout, generated through the
    optimization. At the bottom a diagram displays the attributs of the layout.
    """

    def __init__(self, factory: FactoryModel):
        super().__init__()
        self.ui = Ui_compare_dialog()
        self.ui.setupUi(self)
        self.setWindowFlag(Qt.Window)
        self.button_group = QButtonGroup()
        self.button_group.setExclusive(True)
        self.factory = factory
        self.views = []
        row = 0
        column = 0
        allowed_layouts = self.factory.parameter['number_saved_variants']
        for i, (layout, facilities) in enumerate(
                zip(factory.layouts, factory.layouts_facility_data)):
            if allowed_layouts <= 0:
                break
            view = CompareView(self)
            facilities_list = copy.deepcopy(self.factory.facility_list)
            for facility in facilities:
                facilities_list[facility.index_num].curr_position = facility.curr_position
                facilities_list[
                    facility.index_num].curr_source_position = facility.curr_source_position
                facilities_list[facility.index_num].curr_sink_position = facility.curr_sink_position
                facilities_list[facility.index_num].curr_rotation = facility.curr_rotation
                facilities_list[facility.index_num].curr_mirror = facility.curr_mirror
            scene = CompareLayoutScene(layout, facilities_list, self.factory)
            scene.init_grid()
            scene.load_facilities()
            view.setScene(scene)
            self.views.append(view)
            widget = QWidget()
            vbox = QVBoxLayout()
            vbox.addWidget(view)
            r_btn = QRadioButton(self.tr("Layout {}: {:.3f}%").format(i, factory.layouts_evaluation_data[i].weight_sum * 100))
            vbox.addWidget(r_btn)
            self.button_group.addButton(r_btn, i)
            widget.setLayout(vbox)
            self.ui.gridLayout.addWidget(widget, row, column)
            if column == 1:
                column = 0
                row += 1
            else:
                column += 1
            allowed_layouts -= 1

        self.button_group.button(0).setChecked(True)

        self.diagram = FactoryCompareDialog(
            factory.layouts_evaluation_data[0:self.factory.parameter['number_saved_variants']],
            dpi=100)
        if column == 1:
            self.ui.gridLayout.addWidget(self.diagram, row + 1, 0, row + 2, 2)
            self.ui.gridLayout.setRowMinimumHeight(row + 1, 500)
        else:
            self.ui.gridLayout.addWidget(self.diagram, row, 0, row + 1, 2)
            self.ui.gridLayout.setRowMinimumHeight(row, 500)
        self.ui.buttonBox.accepted.connect(self.load_layout_to_main)

    def fit_view(self) -> None:
        """
        Fits the content of all factory views into the available space.
        :return: None
        """
        for view in self.views:
            view.fitInView(0, 0, self.factory.layout.shape[1] * self.factory.cell_zoom,
                           self.factory.layout.shape[0] * self.factory.cell_zoom,
                           Qt.KeepAspectRatio)

    def load_layout_to_main(self, reject=False) -> None:
        """
        If the Dialog is closed(accepted) the choosen layout will be loaded into our main window.
        :return: None
        """
        checked_id = self.button_group.checkedId()
        if checked_id >= 0:
            logging.info("Loading layout {} into MainView".format(checked_id))
            self.factory.layout = self.factory.layouts[checked_id]

            for facility in self.factory.layouts_facility_data[checked_id]:
                self.factory.facility_list[
                    facility.index_num].curr_position = facility.curr_position
                self.factory.facility_list[
                    facility.index_num].curr_source_position = facility.curr_source_position
                self.factory.facility_list[
                    facility.index_num].curr_sink_position = facility.curr_sink_position
                self.factory.facility_list[
                    facility.index_num].curr_rotation = facility.curr_rotation
                self.factory.facility_list[facility.index_num].curr_mirror = facility.curr_mirror

            self.factory.path_list = self.factory.layouts_path_data[checked_id]
            self.factory.quamfab = self.factory.layouts_evaluation_data[checked_id]
        if not reject:
            self.accept()

    def reject(self) -> None:
        self.load_layout_to_main(reject=True)
        super().reject()
