import copy
import logging
import math

import numpy as np
from PySide2 import QtGui
from PySide2.QtCore import Slot, Qt, QPoint
from PySide2.QtWidgets import QDialog, QTableWidgetItem, QDialogButtonBox, QMessageBox, QMenu, QInputDialog

from mefap.factory.factory_model import FactoryModel
from mefap.factory.path_elements import PathElements
from mefap.factory.util.light_level import LightLevel
from mefap.gui.facility_add_dialog import FactoryAddFacilityDialog
from mefap.gui.generated.TableDialog import Ui_table_dialog
from mefap.optimization.positionPath import try_path_planning_for_layout


class FactoryTable(QDialog):
    """
    This dialog contains all information about the factory which are loaded from the excel file.
    In different tabs are tables which contains the information.
    """

    def __init__(self, factory: FactoryModel, main_window, on_start=False) -> None:
        super().__init__()
        self.ui = Ui_table_dialog()
        self.ui.setupUi(self)

        self.main_view = main_window

        self.factory = copy.deepcopy(factory)
        self.factory_old = factory

        self.deleted_indices = []

        self.init_materialflow()
        self.init_communicationflow()
        self.init_facility()
        self.init_media_requirements()
        self.init_environmental_factors()

        self.on_start = on_start

        if on_start:
            self.ui.button_box.setStandardButtons(QDialogButtonBox.Ok)
        self.ui.button_box.setFocus()

        self.ui.facilitiy_table_widget.itemChanged.connect(self.item_changed_facility)
        self.ui.communicationflow_table_widget.itemChanged.connect(
            self.item_changed_communicationflow)
        self.ui.materialflow_table_widget.itemChanged.connect(self.item_changed_materialflow)
        self.ui.tableWidget_media.itemChanged.connect(self.item_changed_media)
        self.ui.tableWidget_enviroment.itemChanged.connect(self.item_changed_facility_chracterisics)

        self.ui.add_fac_pushButton.clicked.connect(self.open_add_facility)
        self.ui.rem_fac_pushButton.clicked.connect(self.delete_facility_button)
        self.ui.rem_fac_pushButton.setDisabled(True)

        self.ui.tab_widget.currentChanged.connect(self.set_delete_button_status)

        self.ui.facilitiy_table_widget.itemSelectionChanged.connect(self.facility_item_selection_changed)
        self.ui.facilitiy_table_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.facilitiy_table_widget.customContextMenuRequested.connect(self.delete_facility_context_menu)

        self.ui.tableWidget_media.itemSelectionChanged.connect(self.media_item_selection_changed)

        self.ui.add_media_pushButton.clicked.connect(self.add_media_dialog)
        self.ui.add_media_pushButton.setVisible(False)
        self.ui.delete_media_pushButton.clicked.connect(self.remove_media_dialog)
        self.ui.delete_media_pushButton.setVisible(False)

    def init_materialflow(self) -> None:
        """
        This method is loading the information about the materialflowmatrix from
        the factory into the matrialflow table.
        :return: None
        """
        self.ui.materialflow_table_widget.setRowCount(self.factory.mf_matrix.shape[0])
        self.ui.materialflow_table_widget.setColumnCount(self.factory.mf_matrix.shape[1])

        facility_names = [x.name for x in self.factory.facility_list]

        self.ui.materialflow_table_widget.setHorizontalHeaderLabels(facility_names)
        self.ui.materialflow_table_widget.setVerticalHeaderLabels(facility_names)

        for (x, y), value in np.ndenumerate(self.factory.mf_matrix):
            self.ui.materialflow_table_widget.setItem(x, y, QTableWidgetItem(str(value)))
            # logging.info(str(x) + " " + str(y) + " " + str(value))

    def init_communicationflow(self) -> None:
        """
        This method is loading the information about the communicationflowmatrix from
        the factory into the communicationflow table.
        :return: None
        """
        self.ui.communicationflow_table_widget.setRowCount(self.factory.cf_matrix.shape[0])
        self.ui.communicationflow_table_widget.setColumnCount(self.factory.cf_matrix.shape[1])

        facility_names = [x.name for x in self.factory.facility_list]

        self.ui.communicationflow_table_widget.setHorizontalHeaderLabels(facility_names)
        self.ui.communicationflow_table_widget.setVerticalHeaderLabels(facility_names)

        for (x, y), value in np.ndenumerate(self.factory.cf_matrix):
            self.ui.communicationflow_table_widget.setItem(x, y, QTableWidgetItem(str(value)))
            # logging.info(str(x) + " " + str(y) + " " + str(value))

    def init_facility(self) -> None:
        """
        This method is loading the information about the facilities from
        the factory into the facility table.
        :return: None
        """
        self.ui.facilitiy_table_widget.setRowCount(len(self.factory.facility_list))
        self.ui.facilitiy_table_widget.setColumnCount(10)
        self.ui.facilitiy_table_widget.setHorizontalHeaderLabels([self.tr("Name"),
                                                                  self.tr("Type"),
                                                                  self.tr("Department"),
                                                                  self.tr("Flaeche"),
                                                                  self.tr("Breite"),
                                                                  self.tr("Laenge"),
                                                                  self.tr("Hoehe"),
                                                                  self.tr("Bodentragkraft"),
                                                                  self.tr("Deckentraglast"),
                                                                  self.tr("Wandpositionierung")])
        for i, element in enumerate(self.factory.facility_list):
            self.ui.facilitiy_table_widget.setItem(i, 0, QTableWidgetItem(element.name))
            self.ui.facilitiy_table_widget.setItem(i, 1, QTableWidgetItem(element.facility_type))
            self.ui.facilitiy_table_widget.setItem(i, 2, QTableWidgetItem(str(element.department)))
            item = QTableWidgetItem(str(element.area))
            item.setFlags(item.flags() ^ Qt.ItemIsEditable)
            self.ui.facilitiy_table_widget.setItem(i, 3, item)
            self.ui.facilitiy_table_widget.setItem(i, 4, QTableWidgetItem(str(element.width1)))
            self.ui.facilitiy_table_widget.setItem(i, 5, QTableWidgetItem(str(element.width2)))
            self.ui.facilitiy_table_widget.setItem(i, 6, QTableWidgetItem(str(element.height)))
            self.ui.facilitiy_table_widget.setItem(i, 7, QTableWidgetItem(str(element.floorload)))
            self.ui.facilitiy_table_widget.setItem(i, 8, QTableWidgetItem(str(element.ceilingload)))
            self.ui.facilitiy_table_widget.setItem(i, 9, QTableWidgetItem(str(element.on_wall)))

    def init_media_requirements(self) -> None:
        """
        This method is loading the information about the MediaRequirementsImports from
        the factory into the facility table.
        :return: Mpme
        """
        self.ui.tableWidget_media.setRowCount(len(self.factory.facility_list))
        self.ui.tableWidget_media.setColumnCount(len(self.factory.media_requirements))

        facility_names = [x.name for x in self.factory.facility_list]
        self.ui.tableWidget_media.setVerticalHeaderLabels(facility_names)
        media_name = []
        for i, media in enumerate(self.factory.media_requirements):
            media_name.append(media.name)
            for j, value in enumerate(self.factory.media_requirements[i].requirements):
                self.ui.tableWidget_media.setItem(j, i, QTableWidgetItem(str(value)))
        self.ui.tableWidget_media.setHorizontalHeaderLabels(media_name)

    def init_environmental_factors(self) -> None:
        """
        This method is loading the FacilityCharacteristics about the facilities from
        the factory into the facility table.
        :return: None
        """
        self.ui.tableWidget_enviroment.setRowCount(len(self.factory.facility_list))
        self.ui.tableWidget_enviroment.setColumnCount(8)
        self.ui.tableWidget_enviroment.setHorizontalHeaderLabels([self.tr("Name"),
                                                                  self.tr("Ruhebedarf"),
                                                                  self.tr("Laermexposition"),
                                                                  self.tr("Lichtbedarf"),
                                                                  self.tr("Erschuetterungsanregung"),
                                                                  self.tr("Erschuetterungssensitivitaet"),
                                                                  self.tr("Temperatur"),
                                                                  self.tr("Sauberkeit")])
        facility_names = [x.name for x in self.factory.facility_list]
        for i, element in enumerate(self.factory.facility_characteristics):
            self.ui.tableWidget_enviroment.setItem(i, 0, QTableWidgetItem(facility_names[i]))
            self.ui.tableWidget_enviroment.setItem(i, 1, QTableWidgetItem(str(int(element.noise_sensitivity))))
            self.ui.tableWidget_enviroment.setItem(i, 2, QTableWidgetItem(str(int(element.noise_exposure))))
            self.ui.tableWidget_enviroment.setItem(i, 3, QTableWidgetItem(str(int(element.illuminance.value))))
            self.ui.tableWidget_enviroment.setItem(i, 4, QTableWidgetItem(str(int(element.vibration_excitation))))
            self.ui.tableWidget_enviroment.setItem(i, 5, QTableWidgetItem(str(int(element.vibration_sensitivity))))
            self.ui.tableWidget_enviroment.setItem(i, 6, QTableWidgetItem(str(int(element.temperature))))
            self.ui.tableWidget_enviroment.setItem(i, 7, QTableWidgetItem(str(int(element.cleanliness))))

    @Slot(QTableWidgetItem)
    def item_changed_facility(self, item: QTableWidgetItem) -> None:
        """
        If a item changed in the facility table, the corresponding value get changed in the factory.
        :param item: The table item which changed
        :return: None
        """
        if item.column() == 0:
            self.factory.facility_list[item.row()].name = item.text()
        elif item.column() == 1:
            if item.text() in [x.facility_type for x in self.factory.facility_list]:
                self.factory.facility_list[item.row()].facility_type = item.text()
            else:
                errordialog = QMessageBox(self)
                errordialog.setText(self.tr("Facility type unknown!"))
                errordialog.setInformativeText(self.tr("Choose an existing facility."))
                errordialog.setIcon(QMessageBox.Warning)
                errordialog.exec_()
        elif item.column() == 2:
            try:
                if int(round(float(item.text()))) in [x.department for x in self.factory.facility_list]:
                    self.factory.facility_list[item.row()].department = int(round(float(item.text())))
                else:
                    errordialog = QMessageBox(self)
                    errordialog.setText(self.tr("Department unknown!"))
                    errordialog.setInformativeText(self.tr("Choose an existing Department."))
                    errordialog.setIcon(QMessageBox.Warning)
                    errordialog.exec_()
            except:
                pass
            finally:
                item.setText(str(self.factory.facility_list[item.row()].department))
        elif item.column() == 3:
            pass  # No need to check, because coloumn 3 is not editable
        elif item.column() == 4:
            try:
                if float(item.text()) >= 0:
                    self.factory.facility_list[item.row()].width1 = float(item.text())
                    self.factory.facility_list[item.row()].cell_width1 = math.ceil(
                        float(item.text()) / self.factory.cell_size)
                else:
                    self.error_smaller_zero()
            except:
                pass
            finally:
                item.setText(str(self.factory.facility_list[item.row()].width1))
                self.factory.facility_list[item.row()].area = self.factory.facility_list[item.row()].width1 * \
                                                              self.factory.facility_list[item.row()].width2
                self.ui.facilitiy_table_widget.item(item.row(), 3).setText(
                    str(self.factory.facility_list[item.row()].area))
        elif item.column() == 5:
            try:
                if float(item.text()) >= 0:
                    self.factory.facility_list[item.row()].width2 = float(item.text())
                    self.factory.facility_list[item.row()].cell_width2 = math.ceil(
                        float(item.text()) / self.factory.cell_size)
                else:
                    self.error_smaller_zero()
            except:
                pass
            finally:
                item.setText(str(self.factory.facility_list[item.row()].width2))
                self.factory.facility_list[item.row()].area = self.factory.facility_list[item.row()].width1 * \
                                                              self.factory.facility_list[item.row()].width2
                self.ui.facilitiy_table_widget.item(item.row(), 3).setText(
                    str(self.factory.facility_list[item.row()].area))
        elif item.column() == 6:
            try:
                if float(item.text()) >= 0:
                    self.factory.facility_list[item.row()].height = float(item.text())
                else:
                    self.error_smaller_zero()
            except:
                item.setText(str(self.factory.facility_list[item.row()].height))
        elif item.column() == 7:
            try:
                if float(item.text()) >= 0:
                    self.factory.facility_list[item.row()].floorload = float(item.text())
                else:
                    self.error_smaller_zero()
            except:
                item.setText(str(self.factory.facility_list[item.row()].floorload))
        elif item.column() == 8:
            try:
                if float(item.text()) >= 0:
                    self.factory.facility_list[item.row()].ceilingload = float(item.text())
                else:
                    self.error_smaller_zero()
            except:
                item.setText(str(self.factory.facility_list[item.row()].ceilingload))
        elif item.column() == 9:
            if item.text() == "True" or item.text() == "true" or item.text() == "1":
                self.factory.facility_list[item.row()].on_wall = True
            elif item.text() == "False" or item.text() == "false" or item.text() == "0":
                self.factory.facility_list[item.row()].on_wall = False
            item.setText(str(self.factory.facility_list[item.row()].on_wall))

    @Slot(QTableWidgetItem)
    def item_changed_communicationflow(self, item: QTableWidgetItem) -> None:
        """
        If a item changed in the communicationflow table, the corresponding value get changed in the factory.
        :param item: The table item which changed.
        :return: None
        """
        try:
            if float(item.text()) >= 0:
                self.factory.cf_matrix[item.row()][item.column()] = item.text()
            else:
                self.error_smaller_zero()
        except:
            pass
        finally:
            item.setText(str(self.factory.cf_matrix[item.row()][item.column()]))

    @Slot(QTableWidgetItem)
    def item_changed_materialflow(self, item: QTableWidgetItem) -> None:
        """
        If a item changed in the materialflow table, the corresponding value get changed in the factory.
        :param item: The table item which changed.
        :return: None
        """
        try:
            if float(item.text()) >= 0:
                self.factory.mf_matrix[item.row()][item.column()] = item.text()
            else:
                self.error_smaller_zero()
        except:
            pass
        finally:
            item.setText(str(self.factory.mf_matrix[item.row()][item.column()]))

    @Slot(QTableWidgetItem)
    def item_changed_media(self, item: QTableWidgetItem) -> None:
        """
        If a item changed in the media requirement table, the corresponding value get changed in the factory.
        :param item: The table item which changed.
        :return: None
        """
        if item.text() == "True" or item.text() == "true" or item.text() == "1":
            self.factory.media_requirements[item.column()].requirements[item.row()] = True
        elif item.text() == "False" or item.text() == "false" or item.text() == "0":
            self.factory.media_requirements[item.column()].requirements[item.row()] = False
        item.setText(str(self.factory.media_requirements[item.column()].requirements[item.row()]))

    @Slot(QTableWidgetItem)
    def item_changed_facility_chracterisics(self, item: QTableWidgetItem) -> None:
        """
        If a item changed in the facility characteristics table, the corresponding value get changed in the factory.
        :param item: The table item which changed.
        :return: None
        """
        if item.column() == 0:
            self.factory.facility_list[item.row()].name = item.text()
        elif item.column() == 1:
            self.set_characteristics_int(item.row(), 0, 200, item, "noise_sensitivity")
        elif item.column() == 2:
            self.set_characteristics_int(item.row(), 0, 200, item, "noise_exposure")
        elif item.column() == 3:
            self.set_characteristics_light_level(item.row(), 0, 6, item)
        elif item.column() == 4:
            self.set_characteristics_int(item.row(), 2, 4, item, "vibration_excitation")
        elif item.column() == 5:
            self.set_characteristics_int(item.row(), 2, 4, item, "vibration_sensitivity")
        elif item.column() == 6:
            self.set_characteristics_int(item.row(), -1, 1, item, "temperature")
        elif item.column() == 7:
            self.set_characteristics_int(item.row(), -1, 1, item, "cleanliness")

    def set_characteristics_int(self, index, lower, upper, item, attribute) -> None:
        """
        Checks if the value from the item is in between the lower und upper bound. Otherwise an error dialog appears.
        :param index:
        :param lower: lower bound
        :param upper: upper bound
        :param item: table item
        :param attribute: attributes which get set
        :return: None
        """
        try:
            value = int(round(float(item.text())))
            if lower <= value <= upper:
                self.factory.facility_characteristics[index].__setattr__(attribute, value)
            else:
                error_dialog = QMessageBox(self)
                error_dialog.setText(self.tr("Value error!"))
                error_dialog.setInformativeText(self.tr("Value must be in range {}-{}!".format(lower, upper)))
                error_dialog.setIcon(QMessageBox.Warning)
                error_dialog.exec_()
        except:
            pass
        finally:
            item.setText(str(int(self.factory.facility_characteristics[index].__getattribute__(attribute))))

    def set_characteristics_light_level(self, index, lower, upper, item) -> None:
        """
        Sets LightLevel.
        :param index:
        :param lower: lower bound
        :param upper: upper bound
        :param item: table item
        :return: None
        """
        try:
            value = int(round(float(item.text())))
            if lower <= value <= upper:
                self.factory.facility_characteristics[index].illuminance = LightLevel(value)
            else:
                error_dialog = QMessageBox(self)
                error_dialog.setText(self.tr("Value error!"))
                error_dialog.setInformativeText(self.tr("Value must be in range {}-{}!".format(lower, upper)))
                error_dialog.setIcon(QMessageBox.Warning)
                error_dialog.exec_()
        except:
            pass
        finally:
            item.setText(str(int(self.factory.facility_characteristics[index].illuminance.value)))

    def accept(self) -> None:
        """
        On accept the values from this dialog will be written to the original FactoryModel.
        :return: None
        """
        self.factory_old.mf_matrix = self.factory.mf_matrix
        self.factory_old.cf_matrix = self.factory.cf_matrix
        self.factory_old.facility_list = self.factory.facility_list
        self.factory_old.media_requirements = self.factory.media_requirements
        self.factory_old.media_availability = self.factory.media_availability
        self.factory_old.facility_characteristics = self.factory.facility_characteristics
        self.factory_old.path_list = self.factory.path_list
        self.factory_old.layout = self.factory.layout
        self.factory_old.layouts = self.factory.layouts
        self.factory_old.layouts_facility_data = self.factory.layouts_facility_data
        self.factory_old.layouts_path_data = self.factory.layouts_path_data
        self.factory_old.area_types = self.factory.area_types
        self.factory_old.generate_colors()
        try_path_planning_for_layout(self.factory_old, True)
        super().accept()

    def error_smaller_zero(self) -> None:
        """
        Creates error dialog for entered values which are smaller then zero.
        :return: None
        """
        errordialog = QMessageBox(self)
        errordialog.setText(self.tr("Value error!"))
        errordialog.setInformativeText(self.tr("Value must be greater equals 0!"))
        errordialog.sejtIcon(QMessageBox.Warning)
        errordialog.exec_()

    def open_add_facility(self) -> None:
        """
        This method is opening the FactoryAddFacilityDialog where the user can add a facility to the factory.
        :return: None
        """
        add_dialog = FactoryAddFacilityDialog(self.factory)
        add_dialog.setModal(True)
        add_dialog.show()
        if add_dialog.exec():
            self.factory.facility_list.append(add_dialog.attribute)

            self.factory.facility_characteristics.append(add_dialog.characteristics)

            for req in self.factory.media_requirements:
                req.requirements.append(False)

            self.factory.cf_matrix = np.column_stack(
                (self.factory.cf_matrix, np.zeros(len(self.factory.facility_list) - 1)))
            self.factory.cf_matrix = np.row_stack(
                (self.factory.cf_matrix, np.zeros(len(self.factory.facility_list))))

            self.factory.mf_matrix = np.column_stack(
                (self.factory.mf_matrix, np.zeros(len(self.factory.facility_list) - 1)))
            self.factory.mf_matrix = np.row_stack((self.factory.mf_matrix, np.zeros(len(self.factory.facility_list))))

            self.factory.path_list.append(PathElements(add_dialog.attribute.index_num))
            for facility_path in self.factory.path_list:
                while len(facility_path.paths) < (len(self.factory.facility_list)):
                    facility_path.paths.append([])

            if not self.on_start:
                self.main_view.allocate_sink_source([add_dialog.attribute])

            self.refresh()

    @Slot(QPoint)
    def delete_facility_context_menu(self, point) -> None:
        """
        If the user right click a facility in the facility table, he becomes the option to delete the factory.
        :param point: Point clokced
        :return: None
        """
        top_menu = QMenu(self)

        menu = top_menu.addMenu("Menu")
        delete = menu.addAction(self.tr("Delete Facility"))

        action = menu.exec_(QtGui.QCursor.pos())

        if action == delete:
            self.delete_facility(self.ui.facilitiy_table_widget.itemAt(point).row())

    def delete_facility_button(self) -> None:
        """
        Deletes currently selected facility.
        Used by the delete faility button.
        :return: None
        """
        selected_items = self.ui.facilitiy_table_widget.selectedItems()
        rows = []
        if selected_items:
            for item in reversed(selected_items):
                rows.append(self.ui.facilitiy_table_widget.row(item))
            for row in rows:
                self.delete_facility(row)

    def delete_facility(self, row) -> None:
        """
        Delete Facility from row from factory
        :param row: row of facility which gets deleted
        :return: None
        """
        errordialog = QMessageBox(self)
        errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
        errordialog.setText(self.tr(
            "Are you sure you want to delete the {} facility?!".format(
                self.ui.facilitiy_table_widget.item(row, 0).text())))
        errordialog.setInformativeText(
            self.tr("After the facility has been deleted, the generated layouts are no longer optimal layouts!"))
        errordialog.setIcon(QMessageBox.Icon.Warning)
        errordialog.show()
        if errordialog.exec_() == QMessageBox.Cancel:
            return

        logging.info("Removing {}".format(self.ui.facilitiy_table_widget.item(row, 0).text()))
        self.factory.delete_facility_complete(self.ui.facilitiy_table_widget.item(row, 0).text())
        self.refresh()

    @Slot(int)
    def set_delete_button_status(self, index) -> None:
        """
        On tab changed this method ensures, that the delete facility button is only visible on den facility tab.
        :param index: Index of the current tab
        :return: None
        """
        if index != 2:
            self.ui.rem_fac_pushButton.setVisible(False)
        else:
            self.ui.rem_fac_pushButton.setVisible(True)
        if index == 3:
            self.ui.add_media_pushButton.setVisible(True)
            self.ui.delete_media_pushButton.setVisible(True)
            self.ui.delete_media_pushButton.setDisabled(True)
        else:
            self.ui.add_media_pushButton.setVisible(False)
            self.ui.delete_media_pushButton.setVisible(False)

    def facility_item_selection_changed(self) -> None:
        """
        If the item selection in the facility table changes. The delete facility button gets disabled.
        :return: None
        """
        if not self.ui.facilitiy_table_widget.selectedItems():
            self.ui.rem_fac_pushButton.setDisabled(True)
        else:
            self.ui.rem_fac_pushButton.setDisabled(False)

    def add_media_dialog(self) -> None:
        """
        Opens a dialog where the user can add a new media.
        :return: None
        """
        text, ok = QInputDialog.getText(self, self.tr("Add Media"), self.tr("Enter the media name"))
        if ok:
            if text not in [media.name for media in self.factory.media_availability]:
                self.factory.add_media(text)
            else:
                errordialog = QMessageBox(self)
                errordialog.setText(self.tr("This media already exists!"))
                errordialog.setInformativeText(self.tr("Choose a different name for this media."))
                errordialog.setIcon(QMessageBox.Icon.Warning)
                errordialog.show()
        self.refresh()
        self.ui.tab_widget.setCurrentIndex(3)

    def remove_media_dialog(self) -> None:
        """
        Removes the selected media from factory
        :return: None
        """
        selected_media = self.ui.tableWidget_media.selectedItems()

        column = []
        if selected_media:
            for item in selected_media:
                if item.column() not in column:
                    column.append(item.column())
        for col in column:
            header_item = self.ui.tableWidget_media.horizontalHeaderItem(col)
            errordialog = QMessageBox(self)
            errordialog.setStandardButtons(QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
            errordialog.setText(self.tr(
                "Are you sure you want to delete the {} media?".format(
                    header_item.text())))
            errordialog.setInformativeText(self.tr(
                "After the media has been deleted, the generated layouts are no longer optimal layouts!"))
            errordialog.setIcon(QMessageBox.Icon.Warning)
            errordialog.show()
            if errordialog.exec_() == QMessageBox.Cancel:
                continue
            self.factory.remove_media(header_item.text())
            logging.info("Removing {}".format(header_item.text()))
        self.refresh()

    def media_item_selection_changed(self) -> None:
        """
        If the item selection in the media table changes. The delete media button gets disabled.
        :return: None
        """
        if not self.ui.tableWidget_media.selectedItems():
            self.ui.delete_media_pushButton.setDisabled(True)
        else:
            self.ui.delete_media_pushButton.setDisabled(False)

    def refresh(self) -> None:
        """
        Refreshing all tabled
        :return:
        """
        self.ui.facilitiy_table_widget.clear()
        self.init_facility()
        self.ui.tableWidget_enviroment.clear()
        self.init_environmental_factors()
        self.ui.tableWidget_media.clear()
        self.init_media_requirements()
        self.ui.communicationflow_table_widget.clear()
        self.init_communicationflow()
        self.ui.materialflow_table_widget.clear()
        self.init_materialflow()
