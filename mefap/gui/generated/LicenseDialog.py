# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'LicenseDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_License_dialog(object):
    def setupUi(self, License_dialog):
        if not License_dialog.objectName():
            License_dialog.setObjectName(u"License_dialog")
        License_dialog.resize(714, 546)
        self.verticalLayout = QVBoxLayout(License_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.textEdit = QTextEdit(License_dialog)
        self.textEdit.setObjectName(u"textEdit")

        self.verticalLayout.addWidget(self.textEdit)


        self.retranslateUi(License_dialog)

        QMetaObject.connectSlotsByName(License_dialog)
    # setupUi

    def retranslateUi(self, License_dialog):
        License_dialog.setWindowTitle(QCoreApplication.translate("License_dialog", u"License", None))
    # retranslateUi

