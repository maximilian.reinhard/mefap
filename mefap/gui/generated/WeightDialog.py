# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'WeightDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_weight_dialog(object):
    def setupUi(self, weight_dialog):
        if not weight_dialog.objectName():
            weight_dialog.setObjectName(u"weight_dialog")
        weight_dialog.resize(394, 917)
        self.vertical_layout = QVBoxLayout(weight_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.label_title = QLabel(weight_dialog)
        self.label_title.setObjectName(u"label_title")
        self.label_title.setAlignment(Qt.AlignCenter)

        self.vertical_layout.addWidget(self.label_title)

        self.widget_mutability = QWidget(weight_dialog)
        self.widget_mutability.setObjectName(u"widget_mutability")
        self.horizontal_layout = QHBoxLayout(self.widget_mutability)
        self.horizontal_layout.setObjectName(u"horizontal_layout")
        self.line_mutability = QFrame(self.widget_mutability)
        self.line_mutability.setObjectName(u"line_mutability")
        self.line_mutability.setFrameShape(QFrame.VLine)
        self.line_mutability.setFrameShadow(QFrame.Sunken)

        self.horizontal_layout.addWidget(self.line_mutability)

        self.vertical_layout_3 = QVBoxLayout()
        self.vertical_layout_3.setObjectName(u"vertical_layout_3")
        self.label_2 = QLabel(self.widget_mutability)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.vertical_layout_3.addWidget(self.label_2)

        self.horizontal_layout_6 = QHBoxLayout()
        self.horizontal_layout_6.setObjectName(u"horizontal_layout_6")
        self.label_media_availability = QLabel(self.widget_mutability)
        self.label_media_availability.setObjectName(u"label_media_availability")

        self.horizontal_layout_6.addWidget(self.label_media_availability)

        self.doubleSpinBox_media_availability = QDoubleSpinBox(self.widget_mutability)
        self.doubleSpinBox_media_availability.setObjectName(u"doubleSpinBox_media_availability")
        self.doubleSpinBox_media_availability.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_media_availability.setMaximum(1.000000000000000)
        self.doubleSpinBox_media_availability.setSingleStep(0.010000000000000)

        self.horizontal_layout_6.addWidget(self.doubleSpinBox_media_availability)


        self.vertical_layout_3.addLayout(self.horizontal_layout_6)

        self.horizontal_layout_8 = QHBoxLayout()
        self.horizontal_layout_8.setObjectName(u"horizontal_layout_8")
        self.label_media_compatibility = QLabel(self.widget_mutability)
        self.label_media_compatibility.setObjectName(u"label_media_compatibility")

        self.horizontal_layout_8.addWidget(self.label_media_compatibility)

        self.doubleSpinBox_media_compatibility = QDoubleSpinBox(self.widget_mutability)
        self.doubleSpinBox_media_compatibility.setObjectName(u"doubleSpinBox_media_compatibility")
        self.doubleSpinBox_media_compatibility.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_media_compatibility.setMaximum(1.000000000000000)
        self.doubleSpinBox_media_compatibility.setSingleStep(0.010000000000000)

        self.horizontal_layout_8.addWidget(self.doubleSpinBox_media_compatibility)


        self.vertical_layout_3.addLayout(self.horizontal_layout_8)


        self.horizontal_layout.addLayout(self.vertical_layout_3)


        self.vertical_layout.addWidget(self.widget_mutability)

        self.widget_logistics = QWidget(weight_dialog)
        self.widget_logistics.setObjectName(u"widget_logistics")
        self.horizontal_layout_2 = QHBoxLayout(self.widget_logistics)
        self.horizontal_layout_2.setObjectName(u"horizontal_layout_2")
        self.line_logistics = QFrame(self.widget_logistics)
        self.line_logistics.setObjectName(u"line_logistics")
        self.line_logistics.setFrameShape(QFrame.VLine)
        self.line_logistics.setFrameShadow(QFrame.Sunken)

        self.horizontal_layout_2.addWidget(self.line_logistics)

        self.vertical_layout_7 = QVBoxLayout()
        self.vertical_layout_7.setObjectName(u"vertical_layout_7")
        self.label_3 = QLabel(self.widget_logistics)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.vertical_layout_7.addWidget(self.label_3)

        self.horizontal_layout_3 = QHBoxLayout()
        self.horizontal_layout_3.setObjectName(u"horizontal_layout_3")
        self.label_material_flow_length = QLabel(self.widget_logistics)
        self.label_material_flow_length.setObjectName(u"label_material_flow_length")

        self.horizontal_layout_3.addWidget(self.label_material_flow_length)

        self.doubleSpinBox_material_flow_length = QDoubleSpinBox(self.widget_logistics)
        self.doubleSpinBox_material_flow_length.setObjectName(u"doubleSpinBox_material_flow_length")
        self.doubleSpinBox_material_flow_length.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_material_flow_length.setMaximum(1.000000000000000)
        self.doubleSpinBox_material_flow_length.setSingleStep(0.010000000000000)

        self.horizontal_layout_3.addWidget(self.doubleSpinBox_material_flow_length)


        self.vertical_layout_7.addLayout(self.horizontal_layout_3)

        self.horizontal_layout_4 = QHBoxLayout()
        self.horizontal_layout_4.setObjectName(u"horizontal_layout_4")
        self.label_no_overlapping = QLabel(self.widget_logistics)
        self.label_no_overlapping.setObjectName(u"label_no_overlapping")

        self.horizontal_layout_4.addWidget(self.label_no_overlapping)

        self.doubleSpinBox_no_overlapping = QDoubleSpinBox(self.widget_logistics)
        self.doubleSpinBox_no_overlapping.setObjectName(u"doubleSpinBox_no_overlapping")
        self.doubleSpinBox_no_overlapping.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_no_overlapping.setMaximum(1.000000000000000)
        self.doubleSpinBox_no_overlapping.setSingleStep(0.010000000000000)

        self.horizontal_layout_4.addWidget(self.doubleSpinBox_no_overlapping)


        self.vertical_layout_7.addLayout(self.horizontal_layout_4)

        self.horizontal_layout_12 = QHBoxLayout()
        self.horizontal_layout_12.setObjectName(u"horizontal_layout_12")
        self.label_route_continuity = QLabel(self.widget_logistics)
        self.label_route_continuity.setObjectName(u"label_route_continuity")

        self.horizontal_layout_12.addWidget(self.label_route_continuity)

        self.doubleSpinBox_route_continuity = QDoubleSpinBox(self.widget_logistics)
        self.doubleSpinBox_route_continuity.setObjectName(u"doubleSpinBox_route_continuity")
        self.doubleSpinBox_route_continuity.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_route_continuity.setMaximum(1.000000000000000)
        self.doubleSpinBox_route_continuity.setSingleStep(0.010000000000000)

        self.horizontal_layout_12.addWidget(self.doubleSpinBox_route_continuity)


        self.vertical_layout_7.addLayout(self.horizontal_layout_12)

        self.horizontal_layout_13 = QHBoxLayout()
        self.horizontal_layout_13.setObjectName(u"horizontal_layout_13")
        self.label_land_use_degree = QLabel(self.widget_logistics)
        self.label_land_use_degree.setObjectName(u"label_land_use_degree")

        self.horizontal_layout_13.addWidget(self.label_land_use_degree)

        self.doubleSpinBox_land_use_degree = QDoubleSpinBox(self.widget_logistics)
        self.doubleSpinBox_land_use_degree.setObjectName(u"doubleSpinBox_land_use_degree")
        self.doubleSpinBox_land_use_degree.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_land_use_degree.setMaximum(1.000000000000000)
        self.doubleSpinBox_land_use_degree.setSingleStep(0.010000000000000)

        self.horizontal_layout_13.addWidget(self.doubleSpinBox_land_use_degree)


        self.vertical_layout_7.addLayout(self.horizontal_layout_13)


        self.horizontal_layout_2.addLayout(self.vertical_layout_7)


        self.vertical_layout.addWidget(self.widget_logistics)

        self.widget_enviromental_influences = QWidget(weight_dialog)
        self.widget_enviromental_influences.setObjectName(u"widget_enviromental_influences")
        self.horizontal_layout_15 = QHBoxLayout(self.widget_enviromental_influences)
        self.horizontal_layout_15.setObjectName(u"horizontal_layout_15")
        self.line_enviromental_effects = QFrame(self.widget_enviromental_influences)
        self.line_enviromental_effects.setObjectName(u"line_enviromental_effects")
        self.line_enviromental_effects.setFrameShape(QFrame.VLine)
        self.line_enviromental_effects.setFrameShadow(QFrame.Sunken)

        self.horizontal_layout_15.addWidget(self.line_enviromental_effects)

        self.vertical_layout_9 = QVBoxLayout()
        self.vertical_layout_9.setObjectName(u"vertical_layout_9")
        self.label_4 = QLabel(self.widget_enviromental_influences)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.vertical_layout_9.addWidget(self.label_4)

        self.horizontal_layout_17 = QHBoxLayout()
        self.horizontal_layout_17.setObjectName(u"horizontal_layout_17")
        self.label_lighting = QLabel(self.widget_enviromental_influences)
        self.label_lighting.setObjectName(u"label_lighting")

        self.horizontal_layout_17.addWidget(self.label_lighting)

        self.doubleSpinBox_lighting = QDoubleSpinBox(self.widget_enviromental_influences)
        self.doubleSpinBox_lighting.setObjectName(u"doubleSpinBox_lighting")
        self.doubleSpinBox_lighting.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_lighting.setMaximum(1.000000000000000)
        self.doubleSpinBox_lighting.setSingleStep(0.010000000000000)

        self.horizontal_layout_17.addWidget(self.doubleSpinBox_lighting)


        self.vertical_layout_9.addLayout(self.horizontal_layout_17)

        self.horizontal_layout_18 = QHBoxLayout()
        self.horizontal_layout_18.setObjectName(u"horizontal_layout_18")
        self.label_quiet = QLabel(self.widget_enviromental_influences)
        self.label_quiet.setObjectName(u"label_quiet")

        self.horizontal_layout_18.addWidget(self.label_quiet)

        self.doubleSpinBox_quiet = QDoubleSpinBox(self.widget_enviromental_influences)
        self.doubleSpinBox_quiet.setObjectName(u"doubleSpinBox_quiet")
        self.doubleSpinBox_quiet.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_quiet.setMaximum(1.000000000000000)
        self.doubleSpinBox_quiet.setSingleStep(0.010000000000000)

        self.horizontal_layout_18.addWidget(self.doubleSpinBox_quiet)


        self.vertical_layout_9.addLayout(self.horizontal_layout_18)

        self.horizontal_layout_19 = QHBoxLayout()
        self.horizontal_layout_19.setObjectName(u"horizontal_layout_19")
        self.label_vibration = QLabel(self.widget_enviromental_influences)
        self.label_vibration.setObjectName(u"label_vibration")

        self.horizontal_layout_19.addWidget(self.label_vibration)

        self.doubleSpinBox_vibration = QDoubleSpinBox(self.widget_enviromental_influences)
        self.doubleSpinBox_vibration.setObjectName(u"doubleSpinBox_vibration")
        self.doubleSpinBox_vibration.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_vibration.setMaximum(1.000000000000000)
        self.doubleSpinBox_vibration.setSingleStep(0.010000000000000)

        self.horizontal_layout_19.addWidget(self.doubleSpinBox_vibration)


        self.vertical_layout_9.addLayout(self.horizontal_layout_19)

        self.horizontal_layout_21 = QHBoxLayout()
        self.horizontal_layout_21.setObjectName(u"horizontal_layout_21")
        self.label_temperature = QLabel(self.widget_enviromental_influences)
        self.label_temperature.setObjectName(u"label_temperature")

        self.horizontal_layout_21.addWidget(self.label_temperature)

        self.doubleSpinBox_temperatur = QDoubleSpinBox(self.widget_enviromental_influences)
        self.doubleSpinBox_temperatur.setObjectName(u"doubleSpinBox_temperatur")
        self.doubleSpinBox_temperatur.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_temperatur.setMaximum(1.000000000000000)
        self.doubleSpinBox_temperatur.setSingleStep(0.010000000000000)

        self.horizontal_layout_21.addWidget(self.doubleSpinBox_temperatur)


        self.vertical_layout_9.addLayout(self.horizontal_layout_21)

        self.horizontal_layout_20 = QHBoxLayout()
        self.horizontal_layout_20.setObjectName(u"horizontal_layout_20")
        self.label_cleanliness = QLabel(self.widget_enviromental_influences)
        self.label_cleanliness.setObjectName(u"label_cleanliness")

        self.horizontal_layout_20.addWidget(self.label_cleanliness)

        self.doubleSpinBox_cleanliness = QDoubleSpinBox(self.widget_enviromental_influences)
        self.doubleSpinBox_cleanliness.setObjectName(u"doubleSpinBox_cleanliness")
        self.doubleSpinBox_cleanliness.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_cleanliness.setMaximum(1.000000000000000)
        self.doubleSpinBox_cleanliness.setSingleStep(0.010000000000000)

        self.horizontal_layout_20.addWidget(self.doubleSpinBox_cleanliness)


        self.vertical_layout_9.addLayout(self.horizontal_layout_20)


        self.horizontal_layout_15.addLayout(self.vertical_layout_9)


        self.vertical_layout.addWidget(self.widget_enviromental_influences)

        self.widget_communication = QWidget(weight_dialog)
        self.widget_communication.setObjectName(u"widget_communication")
        self.horizontal_layout_23 = QHBoxLayout(self.widget_communication)
        self.horizontal_layout_23.setObjectName(u"horizontal_layout_23")
        self.line_communication = QFrame(self.widget_communication)
        self.line_communication.setObjectName(u"line_communication")
        self.line_communication.setFrameShape(QFrame.VLine)
        self.line_communication.setFrameShadow(QFrame.Sunken)

        self.horizontal_layout_23.addWidget(self.line_communication)

        self.vertical_layout_11 = QVBoxLayout()
        self.vertical_layout_11.setObjectName(u"vertical_layout_11")
        self.label_5 = QLabel(self.widget_communication)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.vertical_layout_11.addWidget(self.label_5)

        self.horizontal_layout_24 = QHBoxLayout()
        self.horizontal_layout_24.setObjectName(u"horizontal_layout_24")
        self.label_direct_communication = QLabel(self.widget_communication)
        self.label_direct_communication.setObjectName(u"label_direct_communication")

        self.horizontal_layout_24.addWidget(self.label_direct_communication)

        self.doubleSpinBox_direct_communication = QDoubleSpinBox(self.widget_communication)
        self.doubleSpinBox_direct_communication.setObjectName(u"doubleSpinBox_direct_communication")
        self.doubleSpinBox_direct_communication.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_direct_communication.setMaximum(1.000000000000000)
        self.doubleSpinBox_direct_communication.setSingleStep(0.010000000000000)

        self.horizontal_layout_24.addWidget(self.doubleSpinBox_direct_communication)


        self.vertical_layout_11.addLayout(self.horizontal_layout_24)

        self.horizontal_layout_25 = QHBoxLayout()
        self.horizontal_layout_25.setObjectName(u"horizontal_layout_25")
        self.label_formal_communication = QLabel(self.widget_communication)
        self.label_formal_communication.setObjectName(u"label_formal_communication")

        self.horizontal_layout_25.addWidget(self.label_formal_communication)

        self.doubleSpinBox_formal_communication = QDoubleSpinBox(self.widget_communication)
        self.doubleSpinBox_formal_communication.setObjectName(u"doubleSpinBox_formal_communication")
        self.doubleSpinBox_formal_communication.setMaximumSize(QSize(50, 16777215))
        self.doubleSpinBox_formal_communication.setMaximum(1.000000000000000)
        self.doubleSpinBox_formal_communication.setSingleStep(0.010000000000000)

        self.horizontal_layout_25.addWidget(self.doubleSpinBox_formal_communication)


        self.vertical_layout_11.addLayout(self.horizontal_layout_25)


        self.horizontal_layout_23.addLayout(self.vertical_layout_11)


        self.vertical_layout.addWidget(self.widget_communication)

        self.widget_5 = QWidget(weight_dialog)
        self.widget_5.setObjectName(u"widget_5")
        self.vertical_layout_12 = QVBoxLayout(self.widget_5)
        self.vertical_layout_12.setObjectName(u"vertical_layout_12")
        self.horizontal_layout_28 = QHBoxLayout()
        self.horizontal_layout_28.setObjectName(u"horizontal_layout_28")
        self.label_30 = QLabel(self.widget_5)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontal_layout_28.addWidget(self.label_30)

        self.sum_label = QLabel(self.widget_5)
        self.sum_label.setObjectName(u"sum_label")
        self.sum_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontal_layout_28.addWidget(self.sum_label)


        self.vertical_layout_12.addLayout(self.horizontal_layout_28)

        self.pushButton_balance = QPushButton(self.widget_5)
        self.pushButton_balance.setObjectName(u"pushButton_balance")

        self.vertical_layout_12.addWidget(self.pushButton_balance)

        self.pushButton_comparison = QPushButton(self.widget_5)
        self.pushButton_comparison.setObjectName(u"pushButton_comparison")

        self.vertical_layout_12.addWidget(self.pushButton_comparison)

        self.horizontal_layout_27 = QHBoxLayout()
        self.horizontal_layout_27.setObjectName(u"horizontal_layout_27")
        self.pushButton_calculate = QPushButton(self.widget_5)
        self.pushButton_calculate.setObjectName(u"pushButton_calculate")

        self.horizontal_layout_27.addWidget(self.pushButton_calculate)

        self.pushButton_abort = QPushButton(self.widget_5)
        self.pushButton_abort.setObjectName(u"pushButton_abort")

        self.horizontal_layout_27.addWidget(self.pushButton_abort)


        self.vertical_layout_12.addLayout(self.horizontal_layout_27)


        self.vertical_layout.addWidget(self.widget_5)


        self.retranslateUi(weight_dialog)
        self.pushButton_abort.clicked.connect(weight_dialog.reject)

        self.pushButton_calculate.setDefault(True)


        QMetaObject.connectSlotsByName(weight_dialog)
    # setupUi

    def retranslateUi(self, weight_dialog):
        weight_dialog.setWindowTitle(QCoreApplication.translate("weight_dialog", u"Weighting", None))
        self.label_title.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p align=\"center\"><span style=\" font-size:24pt; font-weight:600;\">Set weighting</span></p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Versatility</span></p></body></html>", None))
        self.label_media_availability.setText(QCoreApplication.translate("weight_dialog", u"Media availability", None))
        self.label_media_compatibility.setText(QCoreApplication.translate("weight_dialog", u"Media compatibility", None))
        self.label_3.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Material flow and logistics</span></p></body></html>", None))
        self.label_material_flow_length.setText(QCoreApplication.translate("weight_dialog", u"Material flow length", None))
        self.label_no_overlapping.setText(QCoreApplication.translate("weight_dialog", u"No overlap", None))
        self.label_route_continuity.setText(QCoreApplication.translate("weight_dialog", u"Continuity", None))
        self.label_land_use_degree.setText(QCoreApplication.translate("weight_dialog", u"Land use", None))
        self.label_4.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Environmental influences</span></p></body></html>", None))
        self.label_lighting.setText(QCoreApplication.translate("weight_dialog", u"Lighting", None))
        self.label_quiet.setText(QCoreApplication.translate("weight_dialog", u"Quiet", None))
        self.label_vibration.setText(QCoreApplication.translate("weight_dialog", u"Vibration", None))
        self.label_temperature.setText(QCoreApplication.translate("weight_dialog", u"Temperature", None))
        self.label_cleanliness.setText(QCoreApplication.translate("weight_dialog", u"Cleanliness", None))
        self.label_5.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Communication</span></p></body></html>", None))
        self.label_direct_communication.setText(QCoreApplication.translate("weight_dialog", u"Direct communication", None))
        self.label_formal_communication.setText(QCoreApplication.translate("weight_dialog", u"Formal communication", None))
        self.label_30.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Total:</span></p></body></html>", None))
        self.sum_label.setText(QCoreApplication.translate("weight_dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">0</span></p></body></html>", None))
        self.pushButton_balance.setText(QCoreApplication.translate("weight_dialog", u"Balance of all criteria", None))
        self.pushButton_comparison.setText(QCoreApplication.translate("weight_dialog", u"Pairwise comparison", None))
        self.pushButton_calculate.setText(QCoreApplication.translate("weight_dialog", u"Evaluate", None))
        self.pushButton_abort.setText(QCoreApplication.translate("weight_dialog", u"Abbrechen", None))
    # retranslateUi

