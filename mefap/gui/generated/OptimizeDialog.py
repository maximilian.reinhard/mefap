# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'OptimizeDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_optimize_dialog(object):
    def setupUi(self, optimize_dialog):
        if not optimize_dialog.objectName():
            optimize_dialog.setObjectName(u"optimize_dialog")
        optimize_dialog.resize(1092, 623)
        self.vertical_layout = QVBoxLayout(optimize_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.scrollArea = QScrollArea(optimize_dialog)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setLineWidth(1)
        self.scrollArea.setWidgetResizable(True)
        self.scroll_area_widget_contents = QWidget()
        self.scroll_area_widget_contents.setObjectName(u"scroll_area_widget_contents")
        self.scroll_area_widget_contents.setGeometry(QRect(0, 0, 1068, 1849))
        self.horizontal_layout_11 = QHBoxLayout(self.scroll_area_widget_contents)
        self.horizontal_layout_11.setObjectName(u"horizontal_layout_11")
        self.widget_3 = QWidget(self.scroll_area_widget_contents)
        self.widget_3.setObjectName(u"widget_3")
        self.vertical_layout_4 = QVBoxLayout(self.widget_3)
        self.vertical_layout_4.setObjectName(u"vertical_layout_4")
        self.label_3 = QLabel(self.widget_3)
        self.label_3.setObjectName(u"label_3")

        self.vertical_layout_4.addWidget(self.label_3)

        self.comboBox_metaheuristic = QComboBox(self.widget_3)
        self.comboBox_metaheuristic.addItem("")
        self.comboBox_metaheuristic.addItem("")
        self.comboBox_metaheuristic.addItem("")
        self.comboBox_metaheuristic.addItem("")
        self.comboBox_metaheuristic.addItem("")
        self.comboBox_metaheuristic.setObjectName(u"comboBox_metaheuristic")

        self.vertical_layout_4.addWidget(self.comboBox_metaheuristic)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vertical_layout_4.addItem(self.verticalSpacer_2)

        self.widget_tabu_search = QWidget(self.widget_3)
        self.widget_tabu_search.setObjectName(u"widget_tabu_search")
        self.widget_tabu_search.setEnabled(True)
        self.vertical_layout_7 = QVBoxLayout(self.widget_tabu_search)
        self.vertical_layout_7.setObjectName(u"vertical_layout_7")
        self.widget_14 = QWidget(self.widget_tabu_search)
        self.widget_14.setObjectName(u"widget_14")
        self.horizontal_layout_8 = QHBoxLayout(self.widget_14)
        self.horizontal_layout_8.setObjectName(u"horizontal_layout_8")
        self.label_4 = QLabel(self.widget_14)
        self.label_4.setObjectName(u"label_4")

        self.horizontal_layout_8.addWidget(self.label_4)


        self.vertical_layout_7.addWidget(self.widget_14)

        self.widget_tabu_search_1 = QWidget(self.widget_tabu_search)
        self.widget_tabu_search_1.setObjectName(u"widget_tabu_search_1")
        self.widget_tabu_search_1.setEnabled(True)
        self.formLayout = QFormLayout(self.widget_tabu_search_1)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setLabelAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.label_7 = QLabel(self.widget_tabu_search_1)
        self.label_7.setObjectName(u"label_7")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_7)

        self.label_8 = QLabel(self.widget_tabu_search_1)
        self.label_8.setObjectName(u"label_8")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_8)

        self.spinBox_tabu_length = QSpinBox(self.widget_tabu_search_1)
        self.spinBox_tabu_length.setObjectName(u"spinBox_tabu_length")
        self.spinBox_tabu_length.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_tabu_length.setMaximum(9999)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.spinBox_tabu_length)

        self.spinBox_iteration = QSpinBox(self.widget_tabu_search_1)
        self.spinBox_iteration.setObjectName(u"spinBox_iteration")
        self.spinBox_iteration.setMaximum(9999)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.spinBox_iteration)


        self.vertical_layout_7.addWidget(self.widget_tabu_search_1)


        self.vertical_layout_4.addWidget(self.widget_tabu_search)

        self.widget_simulated_annealing = QWidget(self.widget_3)
        self.widget_simulated_annealing.setObjectName(u"widget_simulated_annealing")
        self.widget_simulated_annealing.setEnabled(True)
        self.widget_simulated_annealing.setMinimumSize(QSize(0, 267))
        self.verticalLayout_5 = QVBoxLayout(self.widget_simulated_annealing)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.widget_10 = QWidget(self.widget_simulated_annealing)
        self.widget_10.setObjectName(u"widget_10")
        self.horizontal_layout_7 = QHBoxLayout(self.widget_10)
        self.horizontal_layout_7.setObjectName(u"horizontal_layout_7")
        self.label_5 = QLabel(self.widget_10)
        self.label_5.setObjectName(u"label_5")

        self.horizontal_layout_7.addWidget(self.label_5)


        self.verticalLayout_5.addWidget(self.widget_10)

        self.widget_simulated_annealing_1 = QWidget(self.widget_simulated_annealing)
        self.widget_simulated_annealing_1.setObjectName(u"widget_simulated_annealing_1")
        self.widget_simulated_annealing_1.setEnabled(True)
        self.formLayout_2 = QFormLayout(self.widget_simulated_annealing_1)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label_10 = QLabel(self.widget_simulated_annealing_1)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setEnabled(True)

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_10)

        self.doubleSpinBox_starting_temperatur = QDoubleSpinBox(self.widget_simulated_annealing_1)
        self.doubleSpinBox_starting_temperatur.setObjectName(u"doubleSpinBox_starting_temperatur")
        self.doubleSpinBox_starting_temperatur.setMaximumSize(QSize(16777215, 16777215))
        self.doubleSpinBox_starting_temperatur.setMaximum(9999.000000000000000)

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.doubleSpinBox_starting_temperatur)

        self.label_11 = QLabel(self.widget_simulated_annealing_1)
        self.label_11.setObjectName(u"label_11")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_11)

        self.doubleSpinBox_cooling_rate = QDoubleSpinBox(self.widget_simulated_annealing_1)
        self.doubleSpinBox_cooling_rate.setObjectName(u"doubleSpinBox_cooling_rate")
        self.doubleSpinBox_cooling_rate.setMaximumSize(QSize(16777215, 16777215))
        self.doubleSpinBox_cooling_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_cooling_rate.setSingleStep(0.100000000000000)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.doubleSpinBox_cooling_rate)

        self.label_9 = QLabel(self.widget_simulated_annealing_1)
        self.label_9.setObjectName(u"label_9")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_9)

        self.spinBox_increment = QSpinBox(self.widget_simulated_annealing_1)
        self.spinBox_increment.setObjectName(u"spinBox_increment")
        self.spinBox_increment.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_increment.setMaximum(9999)

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.spinBox_increment)

        self.label_25 = QLabel(self.widget_simulated_annealing_1)
        self.label_25.setObjectName(u"label_25")

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.label_25)

        self.comboBox_cooling_type = QComboBox(self.widget_simulated_annealing_1)
        self.comboBox_cooling_type.addItem("")
        self.comboBox_cooling_type.addItem("")
        self.comboBox_cooling_type.addItem("")
        self.comboBox_cooling_type.setObjectName(u"comboBox_cooling_type")

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.comboBox_cooling_type)


        self.verticalLayout_5.addWidget(self.widget_simulated_annealing_1)


        self.vertical_layout_4.addWidget(self.widget_simulated_annealing)

        self.widget_genetic = QWidget(self.widget_3)
        self.widget_genetic.setObjectName(u"widget_genetic")
        self.verticalLayout_2 = QVBoxLayout(self.widget_genetic)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalWidget_genetic = QWidget(self.widget_genetic)
        self.horizontalWidget_genetic.setObjectName(u"horizontalWidget_genetic")
        self.horizontalLayout = QHBoxLayout(self.horizontalWidget_genetic)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_12 = QLabel(self.horizontalWidget_genetic)
        self.label_12.setObjectName(u"label_12")

        self.horizontalLayout.addWidget(self.label_12)


        self.verticalLayout_2.addWidget(self.horizontalWidget_genetic)

        self.horizontalWidget_population_size = QWidget(self.widget_genetic)
        self.horizontalWidget_population_size.setObjectName(u"horizontalWidget_population_size")
        self.horizontalWidget_population_size.setEnabled(True)
        self.formLayout_3 = QFormLayout(self.horizontalWidget_population_size)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.formLayout_3.setContentsMargins(12, 12, 12, 12)
        self.label_24 = QLabel(self.horizontalWidget_population_size)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setEnabled(True)

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole, self.label_24)

        self.spinBox_population_size = QSpinBox(self.horizontalWidget_population_size)
        self.spinBox_population_size.setObjectName(u"spinBox_population_size")
        self.spinBox_population_size.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_population_size.setMaximum(9999)

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole, self.spinBox_population_size)

        self.label_23 = QLabel(self.horizontalWidget_population_size)
        self.label_23.setObjectName(u"label_23")

        self.formLayout_3.setWidget(1, QFormLayout.LabelRole, self.label_23)

        self.label_21 = QLabel(self.horizontalWidget_population_size)
        self.label_21.setObjectName(u"label_21")

        self.formLayout_3.setWidget(2, QFormLayout.LabelRole, self.label_21)

        self.label_20 = QLabel(self.horizontalWidget_population_size)
        self.label_20.setObjectName(u"label_20")

        self.formLayout_3.setWidget(3, QFormLayout.LabelRole, self.label_20)

        self.spinBox_number_of_elite_parents = QSpinBox(self.horizontalWidget_population_size)
        self.spinBox_number_of_elite_parents.setObjectName(u"spinBox_number_of_elite_parents")
        self.spinBox_number_of_elite_parents.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_number_of_elite_parents.setMaximum(9999)

        self.formLayout_3.setWidget(3, QFormLayout.FieldRole, self.spinBox_number_of_elite_parents)

        self.label_19 = QLabel(self.horizontalWidget_population_size)
        self.label_19.setObjectName(u"label_19")

        self.formLayout_3.setWidget(4, QFormLayout.LabelRole, self.label_19)

        self.label_18 = QLabel(self.horizontalWidget_population_size)
        self.label_18.setObjectName(u"label_18")

        self.formLayout_3.setWidget(5, QFormLayout.LabelRole, self.label_18)

        self.spinBox_mutation_max_shift_step = QSpinBox(self.horizontalWidget_population_size)
        self.spinBox_mutation_max_shift_step.setObjectName(u"spinBox_mutation_max_shift_step")
        self.spinBox_mutation_max_shift_step.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_mutation_max_shift_step.setMaximum(9999)

        self.formLayout_3.setWidget(5, QFormLayout.FieldRole, self.spinBox_mutation_max_shift_step)

        self.label_13 = QLabel(self.horizontalWidget_population_size)
        self.label_13.setObjectName(u"label_13")

        self.formLayout_3.setWidget(6, QFormLayout.LabelRole, self.label_13)

        self.spinBox_mutation_max_exchange = QSpinBox(self.horizontalWidget_population_size)
        self.spinBox_mutation_max_exchange.setObjectName(u"spinBox_mutation_max_exchange")
        self.spinBox_mutation_max_exchange.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_mutation_max_exchange.setMaximum(9999)

        self.formLayout_3.setWidget(6, QFormLayout.FieldRole, self.spinBox_mutation_max_exchange)

        self.label_22 = QLabel(self.horizontalWidget_population_size)
        self.label_22.setObjectName(u"label_22")

        self.formLayout_3.setWidget(7, QFormLayout.LabelRole, self.label_22)

        self.spinBox_number_of_generations = QSpinBox(self.horizontalWidget_population_size)
        self.spinBox_number_of_generations.setObjectName(u"spinBox_number_of_generations")
        self.spinBox_number_of_generations.setMaximumSize(QSize(16777215, 16777215))
        self.spinBox_number_of_generations.setMaximum(9999)

        self.formLayout_3.setWidget(1, QFormLayout.FieldRole, self.spinBox_number_of_generations)

        self.comboBox_mutation_type = QComboBox(self.horizontalWidget_population_size)
        self.comboBox_mutation_type.addItem("")
        self.comboBox_mutation_type.addItem("")
        self.comboBox_mutation_type.addItem("")
        self.comboBox_mutation_type.setObjectName(u"comboBox_mutation_type")

        self.formLayout_3.setWidget(7, QFormLayout.FieldRole, self.comboBox_mutation_type)

        self.doubleSpinBox_crossover_rate = QDoubleSpinBox(self.horizontalWidget_population_size)
        self.doubleSpinBox_crossover_rate.setObjectName(u"doubleSpinBox_crossover_rate")
        self.doubleSpinBox_crossover_rate.setMaximumSize(QSize(16777215, 16777215))
        self.doubleSpinBox_crossover_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_crossover_rate.setSingleStep(0.100000000000000)

        self.formLayout_3.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_crossover_rate)

        self.doubleSpinBox_mutation_rate = QDoubleSpinBox(self.horizontalWidget_population_size)
        self.doubleSpinBox_mutation_rate.setObjectName(u"doubleSpinBox_mutation_rate")
        self.doubleSpinBox_mutation_rate.setMaximumSize(QSize(16777215, 16777215))
        self.doubleSpinBox_mutation_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_mutation_rate.setSingleStep(0.100000000000000)

        self.formLayout_3.setWidget(4, QFormLayout.FieldRole, self.doubleSpinBox_mutation_rate)


        self.verticalLayout_2.addWidget(self.horizontalWidget_population_size)


        self.vertical_layout_4.addWidget(self.widget_genetic)

        self.widget_ga_ts = QWidget(self.widget_3)
        self.widget_ga_ts.setObjectName(u"widget_ga_ts")
        self.verticalLayout_3 = QVBoxLayout(self.widget_ga_ts)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalWidget_2 = QWidget(self.widget_ga_ts)
        self.horizontalWidget_2.setObjectName(u"horizontalWidget_2")
        self.horizontalLayout_3 = QHBoxLayout(self.horizontalWidget_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_14 = QLabel(self.horizontalWidget_2)
        self.label_14.setObjectName(u"label_14")

        self.horizontalLayout_3.addWidget(self.label_14)


        self.verticalLayout_3.addWidget(self.horizontalWidget_2)

        self.formLayout_5 = QFormLayout()
        self.formLayout_5.setObjectName(u"formLayout_5")
        self.label_28 = QLabel(self.widget_ga_ts)
        self.label_28.setObjectName(u"label_28")

        self.formLayout_5.setWidget(0, QFormLayout.LabelRole, self.label_28)

        self.label_29 = QLabel(self.widget_ga_ts)
        self.label_29.setObjectName(u"label_29")

        self.formLayout_5.setWidget(1, QFormLayout.LabelRole, self.label_29)

        self.label_30 = QLabel(self.widget_ga_ts)
        self.label_30.setObjectName(u"label_30")

        self.formLayout_5.setWidget(2, QFormLayout.LabelRole, self.label_30)

        self.label_31 = QLabel(self.widget_ga_ts)
        self.label_31.setObjectName(u"label_31")

        self.formLayout_5.setWidget(3, QFormLayout.LabelRole, self.label_31)

        self.label_32 = QLabel(self.widget_ga_ts)
        self.label_32.setObjectName(u"label_32")

        self.formLayout_5.setWidget(4, QFormLayout.LabelRole, self.label_32)

        self.label_33 = QLabel(self.widget_ga_ts)
        self.label_33.setObjectName(u"label_33")

        self.formLayout_5.setWidget(5, QFormLayout.LabelRole, self.label_33)

        self.label_34 = QLabel(self.widget_ga_ts)
        self.label_34.setObjectName(u"label_34")

        self.formLayout_5.setWidget(6, QFormLayout.LabelRole, self.label_34)

        self.label_35 = QLabel(self.widget_ga_ts)
        self.label_35.setObjectName(u"label_35")

        self.formLayout_5.setWidget(7, QFormLayout.LabelRole, self.label_35)

        self.label_36 = QLabel(self.widget_ga_ts)
        self.label_36.setObjectName(u"label_36")

        self.formLayout_5.setWidget(8, QFormLayout.LabelRole, self.label_36)

        self.label_37 = QLabel(self.widget_ga_ts)
        self.label_37.setObjectName(u"label_37")

        self.formLayout_5.setWidget(9, QFormLayout.LabelRole, self.label_37)

        self.spinBox_ga_ts_population_size = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_population_size.setObjectName(u"spinBox_ga_ts_population_size")
        self.spinBox_ga_ts_population_size.setMaximum(9999)

        self.formLayout_5.setWidget(0, QFormLayout.FieldRole, self.spinBox_ga_ts_population_size)

        self.spinBox_ga_ts_number_of_generations = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_number_of_generations.setObjectName(u"spinBox_ga_ts_number_of_generations")
        self.spinBox_ga_ts_number_of_generations.setMaximum(9999)

        self.formLayout_5.setWidget(1, QFormLayout.FieldRole, self.spinBox_ga_ts_number_of_generations)

        self.spinBox_ga_ts_number_of_elite_parents = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_number_of_elite_parents.setObjectName(u"spinBox_ga_ts_number_of_elite_parents")
        self.spinBox_ga_ts_number_of_elite_parents.setMaximum(9999)

        self.formLayout_5.setWidget(3, QFormLayout.FieldRole, self.spinBox_ga_ts_number_of_elite_parents)

        self.spinBox_ga_ts_mutation_max_shift_step = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_mutation_max_shift_step.setObjectName(u"spinBox_ga_ts_mutation_max_shift_step")
        self.spinBox_ga_ts_mutation_max_shift_step.setMaximum(9999)

        self.formLayout_5.setWidget(5, QFormLayout.FieldRole, self.spinBox_ga_ts_mutation_max_shift_step)

        self.spinBox_ga_ts_mutations_max_exchange = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_mutations_max_exchange.setObjectName(u"spinBox_ga_ts_mutations_max_exchange")
        self.spinBox_ga_ts_mutations_max_exchange.setMaximum(9999)

        self.formLayout_5.setWidget(6, QFormLayout.FieldRole, self.spinBox_ga_ts_mutations_max_exchange)

        self.spinBox_ga_ts_length_of_tabu_list = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_length_of_tabu_list.setObjectName(u"spinBox_ga_ts_length_of_tabu_list")
        self.spinBox_ga_ts_length_of_tabu_list.setMaximum(9999)

        self.formLayout_5.setWidget(8, QFormLayout.FieldRole, self.spinBox_ga_ts_length_of_tabu_list)

        self.spinBox_ga_ts_iteration = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_iteration.setObjectName(u"spinBox_ga_ts_iteration")
        self.spinBox_ga_ts_iteration.setMaximum(9999)

        self.formLayout_5.setWidget(9, QFormLayout.FieldRole, self.spinBox_ga_ts_iteration)

        self.comboBox_ga_ts_mutation_type = QComboBox(self.widget_ga_ts)
        self.comboBox_ga_ts_mutation_type.addItem("")
        self.comboBox_ga_ts_mutation_type.addItem("")
        self.comboBox_ga_ts_mutation_type.addItem("")
        self.comboBox_ga_ts_mutation_type.setObjectName(u"comboBox_ga_ts_mutation_type")

        self.formLayout_5.setWidget(7, QFormLayout.FieldRole, self.comboBox_ga_ts_mutation_type)

        self.label_46 = QLabel(self.widget_ga_ts)
        self.label_46.setObjectName(u"label_46")

        self.formLayout_5.setWidget(10, QFormLayout.LabelRole, self.label_46)

        self.spinBox_ga_ts_number_take_over_variants = QSpinBox(self.widget_ga_ts)
        self.spinBox_ga_ts_number_take_over_variants.setObjectName(u"spinBox_ga_ts_number_take_over_variants")
        self.spinBox_ga_ts_number_take_over_variants.setMinimum(1)
        self.spinBox_ga_ts_number_take_over_variants.setMaximum(9999)

        self.formLayout_5.setWidget(10, QFormLayout.FieldRole, self.spinBox_ga_ts_number_take_over_variants)

        self.doubleSpinBox_ga_ts_crossover_rate = QDoubleSpinBox(self.widget_ga_ts)
        self.doubleSpinBox_ga_ts_crossover_rate.setObjectName(u"doubleSpinBox_ga_ts_crossover_rate")
        self.doubleSpinBox_ga_ts_crossover_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_ga_ts_crossover_rate.setSingleStep(0.100000000000000)

        self.formLayout_5.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_ga_ts_crossover_rate)

        self.doubleSpinBox_ga_ts_mutaiton_rate = QDoubleSpinBox(self.widget_ga_ts)
        self.doubleSpinBox_ga_ts_mutaiton_rate.setObjectName(u"doubleSpinBox_ga_ts_mutaiton_rate")
        self.doubleSpinBox_ga_ts_mutaiton_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_ga_ts_mutaiton_rate.setSingleStep(0.100000000000000)

        self.formLayout_5.setWidget(4, QFormLayout.FieldRole, self.doubleSpinBox_ga_ts_mutaiton_rate)


        self.verticalLayout_3.addLayout(self.formLayout_5)


        self.vertical_layout_4.addWidget(self.widget_ga_ts)

        self.widget_ga_sa = QWidget(self.widget_3)
        self.widget_ga_sa.setObjectName(u"widget_ga_sa")
        self.verticalLayout_4 = QVBoxLayout(self.widget_ga_sa)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.widget_6 = QWidget(self.widget_ga_sa)
        self.widget_6.setObjectName(u"widget_6")
        self.horizontalLayout_5 = QHBoxLayout(self.widget_6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_17 = QLabel(self.widget_6)
        self.label_17.setObjectName(u"label_17")

        self.horizontalLayout_5.addWidget(self.label_17)


        self.verticalLayout_4.addWidget(self.widget_6)

        self.formWidget = QWidget(self.widget_ga_sa)
        self.formWidget.setObjectName(u"formWidget")
        self.formLayout_6 = QFormLayout(self.formWidget)
        self.formLayout_6.setObjectName(u"formLayout_6")
        self.label_38 = QLabel(self.formWidget)
        self.label_38.setObjectName(u"label_38")

        self.formLayout_6.setWidget(0, QFormLayout.LabelRole, self.label_38)

        self.spinBox_ga_sa_population_size = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_population_size.setObjectName(u"spinBox_ga_sa_population_size")
        self.spinBox_ga_sa_population_size.setMaximum(9999)

        self.formLayout_6.setWidget(0, QFormLayout.FieldRole, self.spinBox_ga_sa_population_size)

        self.label_39 = QLabel(self.formWidget)
        self.label_39.setObjectName(u"label_39")

        self.formLayout_6.setWidget(1, QFormLayout.LabelRole, self.label_39)

        self.spinBox_ga_sa_number_of_generations = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_number_of_generations.setObjectName(u"spinBox_ga_sa_number_of_generations")
        self.spinBox_ga_sa_number_of_generations.setMaximum(9999)

        self.formLayout_6.setWidget(1, QFormLayout.FieldRole, self.spinBox_ga_sa_number_of_generations)

        self.label_40 = QLabel(self.formWidget)
        self.label_40.setObjectName(u"label_40")

        self.formLayout_6.setWidget(2, QFormLayout.LabelRole, self.label_40)

        self.label_41 = QLabel(self.formWidget)
        self.label_41.setObjectName(u"label_41")

        self.formLayout_6.setWidget(3, QFormLayout.LabelRole, self.label_41)

        self.spinBox_ga_sa_number_of_elite_parents = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_number_of_elite_parents.setObjectName(u"spinBox_ga_sa_number_of_elite_parents")
        self.spinBox_ga_sa_number_of_elite_parents.setMaximum(9999)

        self.formLayout_6.setWidget(3, QFormLayout.FieldRole, self.spinBox_ga_sa_number_of_elite_parents)

        self.label_42 = QLabel(self.formWidget)
        self.label_42.setObjectName(u"label_42")

        self.formLayout_6.setWidget(4, QFormLayout.LabelRole, self.label_42)

        self.label_43 = QLabel(self.formWidget)
        self.label_43.setObjectName(u"label_43")

        self.formLayout_6.setWidget(5, QFormLayout.LabelRole, self.label_43)

        self.spinBox_ga_sa_mutation_max_shift_step = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_mutation_max_shift_step.setObjectName(u"spinBox_ga_sa_mutation_max_shift_step")
        self.spinBox_ga_sa_mutation_max_shift_step.setMaximum(9999)

        self.formLayout_6.setWidget(5, QFormLayout.FieldRole, self.spinBox_ga_sa_mutation_max_shift_step)

        self.label_44 = QLabel(self.formWidget)
        self.label_44.setObjectName(u"label_44")

        self.formLayout_6.setWidget(6, QFormLayout.LabelRole, self.label_44)

        self.spinBox_ga_sa_mutation_max_exchange = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_mutation_max_exchange.setObjectName(u"spinBox_ga_sa_mutation_max_exchange")
        self.spinBox_ga_sa_mutation_max_exchange.setMaximum(9999)

        self.formLayout_6.setWidget(6, QFormLayout.FieldRole, self.spinBox_ga_sa_mutation_max_exchange)

        self.label_45 = QLabel(self.formWidget)
        self.label_45.setObjectName(u"label_45")

        self.formLayout_6.setWidget(7, QFormLayout.LabelRole, self.label_45)

        self.comboBox_ga_sa_mutation_type = QComboBox(self.formWidget)
        self.comboBox_ga_sa_mutation_type.addItem("")
        self.comboBox_ga_sa_mutation_type.addItem("")
        self.comboBox_ga_sa_mutation_type.addItem("")
        self.comboBox_ga_sa_mutation_type.setObjectName(u"comboBox_ga_sa_mutation_type")

        self.formLayout_6.setWidget(7, QFormLayout.FieldRole, self.comboBox_ga_sa_mutation_type)

        self.label_15 = QLabel(self.formWidget)
        self.label_15.setObjectName(u"label_15")

        self.formLayout_6.setWidget(8, QFormLayout.LabelRole, self.label_15)

        self.doubleSpinBox_ga_sa_starting_temperature = QDoubleSpinBox(self.formWidget)
        self.doubleSpinBox_ga_sa_starting_temperature.setObjectName(u"doubleSpinBox_ga_sa_starting_temperature")
        self.doubleSpinBox_ga_sa_starting_temperature.setMaximum(9999.000000000000000)

        self.formLayout_6.setWidget(8, QFormLayout.FieldRole, self.doubleSpinBox_ga_sa_starting_temperature)

        self.label_16 = QLabel(self.formWidget)
        self.label_16.setObjectName(u"label_16")

        self.formLayout_6.setWidget(9, QFormLayout.LabelRole, self.label_16)

        self.doubleSpinBox_ga_Sa_cooling_rate = QDoubleSpinBox(self.formWidget)
        self.doubleSpinBox_ga_Sa_cooling_rate.setObjectName(u"doubleSpinBox_ga_Sa_cooling_rate")
        self.doubleSpinBox_ga_Sa_cooling_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_ga_Sa_cooling_rate.setSingleStep(0.100000000000000)

        self.formLayout_6.setWidget(9, QFormLayout.FieldRole, self.doubleSpinBox_ga_Sa_cooling_rate)

        self.label_26 = QLabel(self.formWidget)
        self.label_26.setObjectName(u"label_26")

        self.formLayout_6.setWidget(10, QFormLayout.LabelRole, self.label_26)

        self.spinBox_ga_sa_increment = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_increment.setObjectName(u"spinBox_ga_sa_increment")
        self.spinBox_ga_sa_increment.setMaximum(9999)

        self.formLayout_6.setWidget(10, QFormLayout.FieldRole, self.spinBox_ga_sa_increment)

        self.label_27 = QLabel(self.formWidget)
        self.label_27.setObjectName(u"label_27")

        self.formLayout_6.setWidget(11, QFormLayout.LabelRole, self.label_27)

        self.comboBox_ga_sa_cooling_type = QComboBox(self.formWidget)
        self.comboBox_ga_sa_cooling_type.addItem("")
        self.comboBox_ga_sa_cooling_type.addItem("")
        self.comboBox_ga_sa_cooling_type.addItem("")
        self.comboBox_ga_sa_cooling_type.setObjectName(u"comboBox_ga_sa_cooling_type")

        self.formLayout_6.setWidget(11, QFormLayout.FieldRole, self.comboBox_ga_sa_cooling_type)

        self.label_47 = QLabel(self.formWidget)
        self.label_47.setObjectName(u"label_47")

        self.formLayout_6.setWidget(12, QFormLayout.LabelRole, self.label_47)

        self.spinBox_ga_sa_number_take_over_variants = QSpinBox(self.formWidget)
        self.spinBox_ga_sa_number_take_over_variants.setObjectName(u"spinBox_ga_sa_number_take_over_variants")
        self.spinBox_ga_sa_number_take_over_variants.setMinimum(1)
        self.spinBox_ga_sa_number_take_over_variants.setMaximum(9999)

        self.formLayout_6.setWidget(12, QFormLayout.FieldRole, self.spinBox_ga_sa_number_take_over_variants)

        self.doubleSpinBox_ga_sa_crossover_rate = QDoubleSpinBox(self.formWidget)
        self.doubleSpinBox_ga_sa_crossover_rate.setObjectName(u"doubleSpinBox_ga_sa_crossover_rate")
        self.doubleSpinBox_ga_sa_crossover_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_ga_sa_crossover_rate.setSingleStep(0.100000000000000)

        self.formLayout_6.setWidget(2, QFormLayout.FieldRole, self.doubleSpinBox_ga_sa_crossover_rate)

        self.doubleSpinBox_ga_sa_mutation_rate = QDoubleSpinBox(self.formWidget)
        self.doubleSpinBox_ga_sa_mutation_rate.setObjectName(u"doubleSpinBox_ga_sa_mutation_rate")
        self.doubleSpinBox_ga_sa_mutation_rate.setMaximum(1.000000000000000)
        self.doubleSpinBox_ga_sa_mutation_rate.setSingleStep(0.100000000000000)

        self.formLayout_6.setWidget(4, QFormLayout.FieldRole, self.doubleSpinBox_ga_sa_mutation_rate)


        self.verticalLayout_4.addWidget(self.formWidget)


        self.vertical_layout_4.addWidget(self.widget_ga_sa)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vertical_layout_4.addItem(self.verticalSpacer_3)


        self.horizontal_layout_11.addWidget(self.widget_3)

        self.line = QFrame(self.scroll_area_widget_contents)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontal_layout_11.addWidget(self.line)

        self.vertical_layout_9 = QVBoxLayout()
        self.vertical_layout_9.setObjectName(u"vertical_layout_9")
        self.widget_opening = QWidget(self.scroll_area_widget_contents)
        self.widget_opening.setObjectName(u"widget_opening")
        self.vertical_layout_2 = QVBoxLayout(self.widget_opening)
        self.vertical_layout_2.setObjectName(u"vertical_layout_2")
        self.label = QLabel(self.widget_opening)
        self.label.setObjectName(u"label")

        self.vertical_layout_2.addWidget(self.label)

        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.radioButton_opening_random = QRadioButton(self.widget_opening)
        self.radioButton_opening_random.setObjectName(u"radioButton_opening_random")
        self.radioButton_opening_random.setChecked(True)

        self.verticalLayout_6.addWidget(self.radioButton_opening_random)

        self.radioButton_opening_random_desc = QRadioButton(self.widget_opening)
        self.radioButton_opening_random_desc.setObjectName(u"radioButton_opening_random_desc")

        self.verticalLayout_6.addWidget(self.radioButton_opening_random_desc)

        self.radioButton_opening_schmigalla = QRadioButton(self.widget_opening)
        self.radioButton_opening_schmigalla.setObjectName(u"radioButton_opening_schmigalla")

        self.verticalLayout_6.addWidget(self.radioButton_opening_schmigalla)

        self.radioButton_opening_ist = QRadioButton(self.widget_opening)
        self.radioButton_opening_ist.setObjectName(u"radioButton_opening_ist")

        self.verticalLayout_6.addWidget(self.radioButton_opening_ist)


        self.vertical_layout_2.addLayout(self.verticalLayout_6)


        self.vertical_layout_9.addWidget(self.widget_opening)

        self.widget_nhs = QWidget(self.scroll_area_widget_contents)
        self.widget_nhs.setObjectName(u"widget_nhs")
        self.widget_nhs.setEnabled(True)
        self.verticalLayout = QVBoxLayout(self.widget_nhs)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(12, 12, 12, 12)
        self.label_6 = QLabel(self.widget_nhs)
        self.label_6.setObjectName(u"label_6")

        self.verticalLayout.addWidget(self.label_6)

        self.gridLayout_nhs = QGridLayout()
        self.gridLayout_nhs.setObjectName(u"gridLayout_nhs")
        self.checkBox_nhs_mlrs = QCheckBox(self.widget_nhs)
        self.checkBox_nhs_mlrs.setObjectName(u"checkBox_nhs_mlrs")
        self.checkBox_nhs_mlrs.setChecked(True)

        self.gridLayout_nhs.addWidget(self.checkBox_nhs_mlrs, 0, 0, 1, 1)

        self.spinBox_nhs_mlrs = QSpinBox(self.widget_nhs)
        self.spinBox_nhs_mlrs.setObjectName(u"spinBox_nhs_mlrs")
        self.spinBox_nhs_mlrs.setMaximumSize(QSize(70, 16777215))
        self.spinBox_nhs_mlrs.setMaximum(100)

        self.gridLayout_nhs.addWidget(self.spinBox_nhs_mlrs, 0, 1, 1, 1, Qt.AlignLeft)

        self.checkBox_nhs_fas = QCheckBox(self.widget_nhs)
        self.checkBox_nhs_fas.setObjectName(u"checkBox_nhs_fas")

        self.gridLayout_nhs.addWidget(self.checkBox_nhs_fas, 1, 0, 1, 1)

        self.spinBox_nhs_fas = QSpinBox(self.widget_nhs)
        self.spinBox_nhs_fas.setObjectName(u"spinBox_nhs_fas")
        self.spinBox_nhs_fas.setMaximumSize(QSize(70, 16777215))
        self.spinBox_nhs_fas.setMaximum(100)

        self.gridLayout_nhs.addWidget(self.spinBox_nhs_fas, 1, 1, 1, 1, Qt.AlignLeft)

        self.checkBox_nhs_mees = QCheckBox(self.widget_nhs)
        self.checkBox_nhs_mees.setObjectName(u"checkBox_nhs_mees")

        self.gridLayout_nhs.addWidget(self.checkBox_nhs_mees, 2, 0, 1, 1)

        self.spinBox_nhs_mees = QSpinBox(self.widget_nhs)
        self.spinBox_nhs_mees.setObjectName(u"spinBox_nhs_mees")
        self.spinBox_nhs_mees.setMaximumSize(QSize(70, 16777215))
        self.spinBox_nhs_mees.setMaximum(100)

        self.gridLayout_nhs.addWidget(self.spinBox_nhs_mees, 2, 1, 1, 1, Qt.AlignLeft)


        self.verticalLayout.addLayout(self.gridLayout_nhs)


        self.vertical_layout_9.addWidget(self.widget_nhs)

        self.widget_restriction = QWidget(self.scroll_area_widget_contents)
        self.widget_restriction.setObjectName(u"widget_restriction")
        self.vertical_layout_3 = QVBoxLayout(self.widget_restriction)
        self.vertical_layout_3.setObjectName(u"vertical_layout_3")
        self.label_2 = QLabel(self.widget_restriction)
        self.label_2.setObjectName(u"label_2")

        self.vertical_layout_3.addWidget(self.label_2)

        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.checkBox_mirror = QCheckBox(self.widget_restriction)
        self.checkBox_mirror.setObjectName(u"checkBox_mirror")
        self.checkBox_mirror.setChecked(True)

        self.verticalLayout_7.addWidget(self.checkBox_mirror)

        self.checkBox_rotation = QCheckBox(self.widget_restriction)
        self.checkBox_rotation.setObjectName(u"checkBox_rotation")
        self.checkBox_rotation.setChecked(True)

        self.verticalLayout_7.addWidget(self.checkBox_rotation)


        self.vertical_layout_3.addLayout(self.verticalLayout_7)

        self.label_48 = QLabel(self.widget_restriction)
        self.label_48.setObjectName(u"label_48")

        self.vertical_layout_3.addWidget(self.label_48)

        self.verticalLayout_8 = QVBoxLayout()
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.checkBox_path_planning = QCheckBox(self.widget_restriction)
        self.checkBox_path_planning.setObjectName(u"checkBox_path_planning")

        self.verticalLayout_8.addWidget(self.checkBox_path_planning)


        self.vertical_layout_3.addLayout(self.verticalLayout_8)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.gridLayout.setHorizontalSpacing(-1)
        self.gridLayout.setContentsMargins(0, -1, -1, -1)
        self.label_multicore = QLabel(self.widget_restriction)
        self.label_multicore.setObjectName(u"label_multicore")

        self.gridLayout.addWidget(self.label_multicore, 0, 0, 1, 1)

        self.spinBox_multicore = QSpinBox(self.widget_restriction)
        self.spinBox_multicore.setObjectName(u"spinBox_multicore")
        self.spinBox_multicore.setMaximumSize(QSize(70, 16777215))

        self.gridLayout.addWidget(self.spinBox_multicore, 0, 1, 1, 1)


        self.vertical_layout_3.addLayout(self.gridLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vertical_layout_3.addItem(self.verticalSpacer)


        self.vertical_layout_9.addWidget(self.widget_restriction)


        self.horizontal_layout_11.addLayout(self.vertical_layout_9)

        self.scrollArea.setWidget(self.scroll_area_widget_contents)

        self.vertical_layout.addWidget(self.scrollArea)

        self.buttonBox = QDialogButtonBox(optimize_dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok|QDialogButtonBox.Reset)

        self.vertical_layout.addWidget(self.buttonBox)


        self.retranslateUi(optimize_dialog)
        self.buttonBox.accepted.connect(optimize_dialog.accept)
        self.buttonBox.rejected.connect(optimize_dialog.reject)

        QMetaObject.connectSlotsByName(optimize_dialog)
    # setupUi

    def retranslateUi(self, optimize_dialog):
        optimize_dialog.setWindowTitle(QCoreApplication.translate("optimize_dialog", u"Optimization settings", None))
        self.label_3.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; text-decoration: underline;\">Metaheuristics\n"
"</span></p></body></html>", None))
        self.comboBox_metaheuristic.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Tabu Search (TS)", None))
        self.comboBox_metaheuristic.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Simulated Annealing (SA)", None))
        self.comboBox_metaheuristic.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Genetic Algorithm (GA)", None))
        self.comboBox_metaheuristic.setItemText(3, QCoreApplication.translate("optimize_dialog", u"GA_TS", None))
        self.comboBox_metaheuristic.setItemText(4, QCoreApplication.translate("optimize_dialog", u"GA_SA", None))

        self.label_4.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Tabu Search</span></p></body></html>", None))
        self.label_7.setText(QCoreApplication.translate("optimize_dialog", u"Length of tabu list", None))
        self.label_8.setText(QCoreApplication.translate("optimize_dialog", u"Iteration", None))
        self.label_5.setText(QCoreApplication.translate("optimize_dialog", u"\n"
"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Simulated Annealing</span></p></body></html>", None))
        self.label_10.setText(QCoreApplication.translate("optimize_dialog", u"Starting temperatur)", None))
        self.label_11.setText(QCoreApplication.translate("optimize_dialog", u"Cooling rate", None))
        self.label_9.setText(QCoreApplication.translate("optimize_dialog", u"Increment", None))
        self.label_25.setText(QCoreApplication.translate("optimize_dialog", u"Cooling type", None))
        self.comboBox_cooling_type.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Linear", None))
        self.comboBox_cooling_type.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Logarithm fast", None))
        self.comboBox_cooling_type.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Logarithm slow", None))

        self.label_12.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Genetic Algorithm</span></p></body></html>\n"
"", None))
        self.label_24.setText(QCoreApplication.translate("optimize_dialog", u"Population size", None))
        self.label_23.setText(QCoreApplication.translate("optimize_dialog", u"Number of generations", None))
        self.label_21.setText(QCoreApplication.translate("optimize_dialog", u"Crossover rate", None))
        self.label_20.setText(QCoreApplication.translate("optimize_dialog", u"Number of elite parents", None))
        self.label_19.setText(QCoreApplication.translate("optimize_dialog", u"Mutation rate", None))
        self.label_18.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max shift step", None))
        self.label_13.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max exchange", None))
        self.label_22.setText(QCoreApplication.translate("optimize_dialog", u"Mutation type", None))
        self.comboBox_mutation_type.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Shift mutation", None))
        self.comboBox_mutation_type.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Exchange mutation", None))
        self.comboBox_mutation_type.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Shift & Exchange mutation", None))

        self.label_14.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">GA_TS</span></p></body></html>\n"
"", None))
        self.label_28.setText(QCoreApplication.translate("optimize_dialog", u"Population size", None))
        self.label_29.setText(QCoreApplication.translate("optimize_dialog", u"Number of generations", None))
        self.label_30.setText(QCoreApplication.translate("optimize_dialog", u"Crossover rate", None))
        self.label_31.setText(QCoreApplication.translate("optimize_dialog", u"Number of elite parents", None))
        self.label_32.setText(QCoreApplication.translate("optimize_dialog", u"Mutation rate", None))
        self.label_33.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max shift step", None))
        self.label_34.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max exchange", None))
        self.label_35.setText(QCoreApplication.translate("optimize_dialog", u"Mutation type", None))
        self.label_36.setText(QCoreApplication.translate("optimize_dialog", u"Length of tabu list", None))
        self.label_37.setText(QCoreApplication.translate("optimize_dialog", u"Iteration", None))
        self.comboBox_ga_ts_mutation_type.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Shift mutation", None))
        self.comboBox_ga_ts_mutation_type.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Exchange mutation", None))
        self.comboBox_ga_ts_mutation_type.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Shift & Exchange mutation", None))

        self.label_46.setText(QCoreApplication.translate("optimize_dialog", u"Number take over variants", None))
        self.label_17.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">GA_SA</span></p></body></html>\n"
"", None))
        self.label_38.setText(QCoreApplication.translate("optimize_dialog", u"Population size", None))
        self.label_39.setText(QCoreApplication.translate("optimize_dialog", u"Number of generations", None))
        self.label_40.setText(QCoreApplication.translate("optimize_dialog", u"Crossover rate", None))
        self.label_41.setText(QCoreApplication.translate("optimize_dialog", u"Number of elite parents", None))
        self.label_42.setText(QCoreApplication.translate("optimize_dialog", u"Mutation rate", None))
        self.label_43.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max shift step", None))
        self.label_44.setText(QCoreApplication.translate("optimize_dialog", u"Mutation max exchange", None))
        self.label_45.setText(QCoreApplication.translate("optimize_dialog", u"Mutation type", None))
        self.comboBox_ga_sa_mutation_type.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Shift mutation", None))
        self.comboBox_ga_sa_mutation_type.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Exchange mutation", None))
        self.comboBox_ga_sa_mutation_type.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Shift & Exchange mutation", None))

        self.label_15.setText(QCoreApplication.translate("optimize_dialog", u"Starting Temperature", None))
        self.label_16.setText(QCoreApplication.translate("optimize_dialog", u"Cooling rate", None))
        self.label_26.setText(QCoreApplication.translate("optimize_dialog", u"Increment", None))
        self.label_27.setText(QCoreApplication.translate("optimize_dialog", u"Cooling Type", None))
        self.comboBox_ga_sa_cooling_type.setItemText(0, QCoreApplication.translate("optimize_dialog", u"Linear", None))
        self.comboBox_ga_sa_cooling_type.setItemText(1, QCoreApplication.translate("optimize_dialog", u"Logarithm fast", None))
        self.comboBox_ga_sa_cooling_type.setItemText(2, QCoreApplication.translate("optimize_dialog", u"Logarithm slow", None))

        self.label_47.setText(QCoreApplication.translate("optimize_dialog", u"Number take over variants", None))
        self.label.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; text-decoration: underline;\">Opening procedure</span></p></body></html>", None))
        self.radioButton_opening_random.setText(QCoreApplication.translate("optimize_dialog", u"Random positioning and selection", None))
        self.radioButton_opening_random_desc.setText(QCoreApplication.translate("optimize_dialog", u"Random positioning with factory objects sorted in descending order", None))
        self.radioButton_opening_schmigalla.setText(QCoreApplication.translate("optimize_dialog", u"Schmigalla", None))
        self.radioButton_opening_ist.setText(QCoreApplication.translate("optimize_dialog", u"Actual layout", None))
        self.label_6.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; text-decoration: underline;\">Neighborhood search type</span></p></body></html>", None))
        self.checkBox_nhs_mlrs.setText(QCoreApplication.translate("optimize_dialog", u"Local Reallocation Search", None))
        self.checkBox_nhs_fas.setText(QCoreApplication.translate("optimize_dialog", u"Free Area Search", None))
        self.checkBox_nhs_mees.setText(QCoreApplication.translate("optimize_dialog", u"Multiple Elements Exchange Search", None))
        self.label_2.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; text-decoration: underline;\">Optional restrictions</span></p></body></html>", None))
        self.checkBox_mirror.setText(QCoreApplication.translate("optimize_dialog", u"Rotatability", None))
        self.checkBox_rotation.setText(QCoreApplication.translate("optimize_dialog", u"Mirror", None))
        self.label_48.setText(QCoreApplication.translate("optimize_dialog", u"<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; text-decoration: underline;\">Other</span></p></body></html>", None))
        self.checkBox_path_planning.setText(QCoreApplication.translate("optimize_dialog", u"Pathplanning", None))
        self.label_multicore.setText(QCoreApplication.translate("optimize_dialog", u"Multicore", None))
    # retranslateUi

