# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactorySizeDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_factory_size_dialog(object):
    def setupUi(self, factory_size_dialog):
        if not factory_size_dialog.objectName():
            factory_size_dialog.setObjectName(u"factory_size_dialog")
        factory_size_dialog.resize(454, 313)
        self.vertical_layout = QVBoxLayout(factory_size_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.horizontal_layout = QHBoxLayout()
        self.horizontal_layout.setObjectName(u"horizontal_layout")
        self.label_cell_size = QLabel(factory_size_dialog)
        self.label_cell_size.setObjectName(u"label_cell_size")

        self.horizontal_layout.addWidget(self.label_cell_size)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontal_layout.addItem(self.verticalSpacer_2)

        self.doubleSpinBox_cell_size = QSpinBox(factory_size_dialog)
        self.doubleSpinBox_cell_size.setObjectName(u"doubleSpinBox_cell_size")
        self.doubleSpinBox_cell_size.setMinimum(1)
        self.doubleSpinBox_cell_size.setMaximum(999)
        self.doubleSpinBox_cell_size.setValue(5)

        self.horizontal_layout.addWidget(self.doubleSpinBox_cell_size)


        self.vertical_layout.addLayout(self.horizontal_layout)

        self.horizontal_layout_2 = QHBoxLayout()
        self.horizontal_layout_2.setObjectName(u"horizontal_layout_2")
        self.label_factory_width = QLabel(factory_size_dialog)
        self.label_factory_width.setObjectName(u"label_factory_width")
        self.label_factory_width.setMinimumSize(QSize(0, 0))

        self.horizontal_layout_2.addWidget(self.label_factory_width)

        self.doubleSpinBox_factory_width = QSpinBox(factory_size_dialog)
        self.doubleSpinBox_factory_width.setObjectName(u"doubleSpinBox_factory_width")
        self.doubleSpinBox_factory_width.setProperty("showGroupSeparator", True)
        self.doubleSpinBox_factory_width.setMaximum(99999)
        self.doubleSpinBox_factory_width.setValue(100)

        self.horizontal_layout_2.addWidget(self.doubleSpinBox_factory_width)

        self.vertical_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontal_layout_2.addItem(self.vertical_spacer)

        self.label_factory_height = QLabel(factory_size_dialog)
        self.label_factory_height.setObjectName(u"label_factory_height")

        self.horizontal_layout_2.addWidget(self.label_factory_height)

        self.doubleSpinBox_factory_height = QSpinBox(factory_size_dialog)
        self.doubleSpinBox_factory_height.setObjectName(u"doubleSpinBox_factory_height")
        self.doubleSpinBox_factory_height.setProperty("showGroupSeparator", True)
        self.doubleSpinBox_factory_height.setMaximum(99999)
        self.doubleSpinBox_factory_height.setValue(100)

        self.horizontal_layout_2.addWidget(self.doubleSpinBox_factory_height)


        self.vertical_layout.addLayout(self.horizontal_layout_2)

        self.warn_label = QLabel(factory_size_dialog)
        self.warn_label.setObjectName(u"warn_label")
        self.warn_label.setAlignment(Qt.AlignCenter)

        self.vertical_layout.addWidget(self.warn_label)

        self.button_box = QDialogButtonBox(factory_size_dialog)
        self.button_box.setObjectName(u"button_box")
        self.button_box.setOrientation(Qt.Horizontal)
        self.button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.vertical_layout.addWidget(self.button_box)


        self.retranslateUi(factory_size_dialog)
        self.button_box.accepted.connect(factory_size_dialog.accept)
        self.button_box.rejected.connect(factory_size_dialog.reject)

        QMetaObject.connectSlotsByName(factory_size_dialog)
    # setupUi

    def retranslateUi(self, factory_size_dialog):
        factory_size_dialog.setWindowTitle(QCoreApplication.translate("factory_size_dialog", u"Factory size", None))
        self.label_cell_size.setText(QCoreApplication.translate("factory_size_dialog", u"Edge length of the unit cells [m]", None))
        self.label_factory_width.setText(QCoreApplication.translate("factory_size_dialog", u"Factory width [m]", None))
        self.label_factory_height.setText(QCoreApplication.translate("factory_size_dialog", u"Factory height [m]", None))
        self.warn_label.setText(QCoreApplication.translate("factory_size_dialog", u"<html><head/><body><p><br/></p></body></html>", None))
    # retranslateUi

