# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FacilityDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_facility_dialog(object):
    def setupUi(self, facility_dialog):
        if not facility_dialog.objectName():
            facility_dialog.setObjectName(u"facility_dialog")
        facility_dialog.resize(674, 480)
        self.vertical_layout = QVBoxLayout(facility_dialog)
        self.vertical_layout.setObjectName(u"vertical_layout")
        self.label_actual_facility = QLabel(facility_dialog)
        self.label_actual_facility.setObjectName(u"label_actual_facility")
        self.label_actual_facility.setAlignment(Qt.AlignCenter)

        self.vertical_layout.addWidget(self.label_actual_facility)

        self.horizontal_layout_1 = QHBoxLayout()
        self.horizontal_layout_1.setObjectName(u"horizontal_layout_1")

        self.vertical_layout.addLayout(self.horizontal_layout_1)

        self.horizontal_layout_2 = QHBoxLayout()
        self.horizontal_layout_2.setObjectName(u"horizontal_layout_2")
        self.push_button_previous = QPushButton(facility_dialog)
        self.push_button_previous.setObjectName(u"push_button_previous")

        self.horizontal_layout_2.addWidget(self.push_button_previous)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontal_layout_2.addItem(self.horizontalSpacer)

        self.push_button_next = QPushButton(facility_dialog)
        self.push_button_next.setObjectName(u"push_button_next")

        self.horizontal_layout_2.addWidget(self.push_button_next)


        self.vertical_layout.addLayout(self.horizontal_layout_2)


        self.retranslateUi(facility_dialog)

        self.push_button_next.setDefault(True)


        QMetaObject.connectSlotsByName(facility_dialog)
    # setupUi

    def retranslateUi(self, facility_dialog):
        facility_dialog.setWindowTitle(QCoreApplication.translate("facility_dialog", u"Dialog", None))
        self.label_actual_facility.setText(QCoreApplication.translate("facility_dialog", u"Facility Name", None))
#if QT_CONFIG(tooltip)
        self.push_button_previous.setToolTip(QCoreApplication.translate("facility_dialog", u"Switch to the previous facility.", None))
#endif // QT_CONFIG(tooltip)
        self.push_button_previous.setText(QCoreApplication.translate("facility_dialog", u"<", None))
#if QT_CONFIG(tooltip)
        self.push_button_next.setToolTip(QCoreApplication.translate("facility_dialog", u"Switch to the next facility.", None))
#endif // QT_CONFIG(tooltip)
        self.push_button_next.setText(QCoreApplication.translate("facility_dialog", u">", None))
    # retranslateUi

