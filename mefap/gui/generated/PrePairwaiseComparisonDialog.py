# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'PrePairwaiseComparisonDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_PrePairwiseComparisonDialog(object):
    def setupUi(self, PrePairwiseComparisonDialog):
        if not PrePairwiseComparisonDialog.objectName():
            PrePairwiseComparisonDialog.setObjectName(u"PrePairwiseComparisonDialog")
        PrePairwiseComparisonDialog.resize(616, 487)
        self.verticalLayout_4 = QVBoxLayout(PrePairwiseComparisonDialog)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label = QLabel(PrePairwiseComparisonDialog)
        self.label.setObjectName(u"label")

        self.verticalLayout_4.addWidget(self.label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_2 = QLabel(PrePairwiseComparisonDialog)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_2.addWidget(self.label_2, 0, Qt.AlignHCenter)

        self.listWidget_left = QListWidget(PrePairwiseComparisonDialog)
        self.listWidget_left.setObjectName(u"listWidget_left")

        self.verticalLayout_2.addWidget(self.listWidget_left)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.pushButton_right = QPushButton(PrePairwiseComparisonDialog)
        self.pushButton_right.setObjectName(u"pushButton_right")

        self.verticalLayout.addWidget(self.pushButton_right)

        self.pushButton_left = QPushButton(PrePairwiseComparisonDialog)
        self.pushButton_left.setObjectName(u"pushButton_left")

        self.verticalLayout.addWidget(self.pushButton_left)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_3 = QLabel(PrePairwiseComparisonDialog)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_3.addWidget(self.label_3, 0, Qt.AlignHCenter)

        self.listWidget_right = QListWidget(PrePairwiseComparisonDialog)
        self.listWidget_right.setObjectName(u"listWidget_right")

        self.verticalLayout_3.addWidget(self.listWidget_right)


        self.horizontalLayout.addLayout(self.verticalLayout_3)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.buttonBox = QDialogButtonBox(PrePairwiseComparisonDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_4.addWidget(self.buttonBox)


        self.retranslateUi(PrePairwiseComparisonDialog)
        self.buttonBox.accepted.connect(PrePairwiseComparisonDialog.accept)
        self.buttonBox.rejected.connect(PrePairwiseComparisonDialog.reject)

        QMetaObject.connectSlotsByName(PrePairwiseComparisonDialog)
    # setupUi

    def retranslateUi(self, PrePairwiseComparisonDialog):
        PrePairwiseComparisonDialog.setWindowTitle(QCoreApplication.translate("PrePairwiseComparisonDialog", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("PrePairwiseComparisonDialog", u"<html><head/><body><h1>Choose your weighting criteria</h1></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("PrePairwiseComparisonDialog", u"Criteria", None))
        self.pushButton_right.setText(QCoreApplication.translate("PrePairwiseComparisonDialog", u">", None))
        self.pushButton_left.setText(QCoreApplication.translate("PrePairwiseComparisonDialog", u"<", None))
        self.label_3.setText(QCoreApplication.translate("PrePairwiseComparisonDialog", u"Non criteria", None))
    # retranslateUi

