from PySide2.QtCore import Qt
from PySide2.QtWidgets import QGraphicsTextItem

from mefap.gui.cell import Cell
from mefap.gui.util import colors
from mefap.gui.util.sinksource import SINK_SOURCE, SOURCE, SINK


class FacilityCell(Cell):
    """
    This Objects is representing a part of a facility. The size of this facility part depends
    on the cell size defined inside the FactoryModel
    """

    def __init__(self, x_c, y_c, *args, **kwargs):
        Cell.__init__(self, x_c, y_c, *args, **kwargs)
        self.text: QGraphicsTextItem = None

    def set_sink(self) -> None:
        """
        The method will be called if this FacilityCell should represent the sink of the represented
        facility. It adds an "S" on top of this cell.
        :return: None
        """
        if self.text:
            if self.text.toPlainText() == SOURCE:
                self.scene().removeItem(self.text)
                self.text = QGraphicsTextItem(SINK_SOURCE, self)
            elif self.text.toPlainText() == SINK:
                return
            else:
                self.scene().removeItem(self.text)
                self.text = None
        else:
            self.text = QGraphicsTextItem(SINK, self)
        self.set_text()

    def unset_sink(self) -> None:
        """
        If this cell should not represent a sink position any more, it will remove the sink mark.
        :return: None
        """
        if self.text:
            if self.text.toPlainText() == SINK:
                self.scene().removeItem(self.text)
                self.text = None
            elif self.text.toPlainText() == SINK_SOURCE:
                self.scene().removeItem(self.text)
                self.text = QGraphicsTextItem(SOURCE, self)
                self.set_text()

    def set_source(self) -> None:
        """
        The method will be called if this FacilityCell should represent the source of the
        represented facility. It adds an "Q" on top of this cell.
        :return: None
        """
        if self.text:
            if self.text.toPlainText() == SINK:
                self.scene().removeItem(self.text)
                self.text = QGraphicsTextItem(SINK_SOURCE, self)
            elif self.text.toPlainText() == SOURCE:
                return
            else:
                self.scene().removeItem(self.text)
                self.text = None
        else:
            self.text = QGraphicsTextItem(SOURCE, self)
        self.set_text()

    def unset_source(self) -> None:
        """
        If this cell should not represent a source position any more, it will remove the sink mark.
        :return: None
        """
        if self.text:
            if self.text.toPlainText() == SOURCE:
                self.scene().removeItem(self.text)
                self.text = None
            elif self.text.toPlainText() == SINK_SOURCE:
                self.scene().removeItem(self.text)
                self.text = QGraphicsTextItem(SINK, self)
                self.set_text()

    def set_text(self) -> None:
        """
        This methods is setting the layout of the letter with is representing sink or source.
        :return: None
        """
        self.text.setDefaultTextColor(colors.BLACK)
        self.text.setPos(self.boundingRect().center())
        self.text.moveBy(-self.text.boundingRect().width() / 2 + self.pen().width()/2,
                         -self.text.boundingRect().height() / 2 + self.pen().width()/2)
        self.scale_text()
        self.text.setTextInteractionFlags(Qt.NoTextInteraction)

    def remove_text(self) -> None:
        """
        This methods removes the "S" or "Q" from this cell. Called if this cell is either sink or
        source.
        :return: None
        """
        self.scene().removeItem(self.text)

    def scale_text(self) -> None:
        """
        Scales the naming text object of this facility depending on the size of the facility object.
        :return: None
        """
        if self.text:
            # old_br = self.text.boundingRect()
            self.text.setScale(self.boundingRect().width() / self.text.boundingRect().width())
            # if old_br.height() * self.text.scale() > self.boundingRect().height():
            #     self.text.setScale(
            #        (old_br.height() * self.text.scale()) / self.boundingRect().height())
