import logging

from PySide2.QtWidgets import QDialog

from mefap.gui.generated.FactoryChangeSizeDialog import Ui_FactoryChangeSizeDialog


class FactoryChangeSizeDialog(QDialog):
    """
    Dialog which is starting after StartDialog,
    where the user can adjust the size and cell size of the factory.
    """

    def __init__(self, factory) -> None:
        super().__init__()
        self.ui = Ui_FactoryChangeSizeDialog()
        self.ui.setupUi(self)
        self.factory = factory

        # Gives Factory inital size from gui
        logging.info("Initialising change factory size")
        self.ui.spinBox_factory_width.setValue(self.factory.rows * self.factory.cell_size)
        self.ui.spinBox_factory_length.setValue(self.factory.columns * self.factory.cell_size)

        self.ui.label_warn.setStyleSheet("font-weight: bold; color: red")

        # Further initial of spinboxes
        self.adjust_spinbox_steps()
        self.ui.spinBox_factory_length.valueChanged.connect(self.change_length)
        self.ui.spinBox_factory_width.valueChanged.connect(self.change_width)

    def change_width(self):
        """
        Checks if with is a valid value.
        :return: None
        """
        if self.ui.spinBox_factory_width.value() % self.factory.cell_size != 0:
            self.ui.label_warn.setText(self.tr("Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!"))
        elif self.ui.spinBox_factory_width.value() % self.factory.cell_size == 0:
            self.ui.label_warn.setText("")

    def change_length(self):
        """
        Checks if length is a valid value.
        :return: None
        """
        if self.ui.spinBox_factory_length.value() % self.factory.cell_size != 0:
            self.ui.label_warn.setText(self.tr("Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!"))
        elif self.ui.spinBox_factory_length.value() % self.factory.cell_size == 0:
            self.ui.label_warn.setText("")

    def adjust_spinbox_steps(self) -> None:
        """
        This method adjusts the steps of the spinboxes, when the cell size was changed.
        :return: None
        """
        self.ui.spinBox_factory_width.setSingleStep(self.factory.cell_size)
        self.ui.spinBox_factory_length.setSingleStep(self.factory.cell_size)
