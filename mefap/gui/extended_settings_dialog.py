from PySide2.QtWidgets import QDialog, QButtonGroup

from mefap.factory.factory_model import FactoryModel
from mefap.factory.util.light_level import LightLevel
from mefap.gui.generated.FactoryExtendedSettings import Ui_FactoryExtendedSettings


class ExtendedSettingsDialog(QDialog):
    """
    In this dialag the user can edit extended settings of the factory.
    """

    def __init__(self, factory: FactoryModel):
        super().__init__()
        self.ui = Ui_FactoryExtendedSettings()
        self.ui.setupUi(self)
        self.factory = factory

        self.ui.doubleSpinBox_empty_cell_costs.setValue(self.factory.parameter['empty_cell_costs'])
        self.ui.doubleSpinBox_local_road_costs.setValue(self.factory.parameter['local_road_costs'])
        self.ui.doubleSpinBox_fixed_road_costs.setValue(self.factory.parameter['fixed_road_costs'])
        self.ui.doubleSpinBox_media_avail_factor.setValue(self.factory.parameter['media_avail_factor'])
        self.ui.comboBox_road_illuminance.setCurrentIndex(self.factory.parameter['road_illuminance'])
        self.ui.spinBox_sound_pressure_reduction.setValue(self.factory.parameter['sound_pressure_reduction'])
        self.ui.spinBox_sound_propagation_conditions.setValue(self.factory.parameter['sound_propagation_conditions'])
        self.ui.spinBox_termination.setValue(self.factory.parameter['termination_criterion'])
        self.ui.spinBox_lrs_search_steps.setValue(self.factory.parameter['lrs_search_steps'])
        self.ui.spinBox_lrs_step_size.setValue(self.factory.optimization.lrs_step_size)
        self.ui.spinBox_lrs_step_size.setMaximum(max(self.factory.rows, self.factory.columns))
        self.ui.spinBox_fas_order.setValue(self.factory.optimization.fas_order)
        self.ui.doubleSpinBox_mees_area_difference.setValue(self.factory.optimization.mees_area_difference)
        self.ui.spinBox_mees_facility_range_from.setValue(self.factory.optimization.mees_facility_range[0])
        self.ui.spinBox_mees_facility_range_from.setMaximum(len(self.factory.facility_list))
        self.ui.spinBox_mees_facility_range_to.setValue(self.factory.optimization.mees_facility_range[1])
        self.ui.spinBox_mees_facility_range_to.setMaximum(len(self.factory.facility_list))
        self.ui.spinBox_mees_step_size.setValue(self.factory.optimization.mees_step_size)
        self.ui.spinBox_mees_step_size.setMaximum(max(self.factory.rows, self.factory.columns))
        self.ui.checkBox_noise_calc_wall_corner.setChecked(self.factory.parameter['noise_calc_wall_corner'])

        self.ui.spinBox_number_of_experiments.setValue(self.factory.parameter['number_of_experiments'])

        self.ui.spinBox_noise_encapsulation.setValue(self.factory.parameter['noise_encapsulation'])
        self.ui.spinBox_noise_barrier_isolation.setValue(self.factory.parameter['noise_barrier_isolation'])
        self.ui.spinBox_noise_factory_wall_isolation.setValue(self.factory.parameter['noise_factory_wall_isolation'])

        self.ui.doubleSpinBox_noise_absorption_coefficient.setValue(self.factory.parameter["noise_absorption_coefficient"])
        self.ui.doubleSpinBox_noise_absorption_floor.setValue(self.factory.parameter["noise_absorption_floor"])
        self.ui.doubleSpinBox_noise_absorption_ceil.setValue(self.factory.parameter["noise_absorption_ceil"])
        self.ui.doubleSpinBox_noise_absorption_wall.setValue(self.factory.parameter["noise_absorption_wall"])

        self.button_group = QButtonGroup()
        self.button_group.setExclusive(True)
        self.button_group.addButton(self.ui.radioButton_none, 0)
        self.button_group.addButton(self.ui.radioButton_noise_semi_diffuse, 1)
        self.button_group.addButton(self.ui.radioButton_noise_flat_room, 2)
        if self.factory.parameter["noise_semi_diffuse"]:
            self.button_group.button(1).setChecked(True)
        elif self.factory.parameter["noise_flat_room"]:
            self.button_group.button(2).setChecked(True)
        else:
            self.button_group.button(0).setChecked(True)

        if self.factory.parameter["color_facilities_by_sound"]:
            self.ui.checkBox_color_facilities_by_noise_exposure.setChecked(True)
        else:
            self.ui.checkBox_color_facilities_by_noise_exposure.setChecked(False)

    def accept(self) -> None:
        """
        On accept write the values back to factory.
        :return: None
        """
        self.factory.parameter['empty_cell_costs'] = self.ui.doubleSpinBox_empty_cell_costs.value()
        self.factory.parameter['local_road_costs'] = self.ui.doubleSpinBox_local_road_costs.value()
        self.factory.parameter['fixed_road_costs'] = self.ui.doubleSpinBox_fixed_road_costs.value()
        self.factory.parameter['media_avail_factor'] = self.ui.doubleSpinBox_media_avail_factor.value()
        self.factory.parameter['road_illuminance'] = self.ui.comboBox_road_illuminance.currentIndex()
        self.factory.parameter['sound_pressure_reduction'] = self.ui.spinBox_sound_pressure_reduction.value()
        self.factory.parameter['sound_propagation_conditions'] = self.ui.spinBox_sound_propagation_conditions.value()
        self.factory.parameter['lrs_search_steps'] = self.ui.spinBox_lrs_search_steps.value()
        self.factory.parameter['termination_criterion'] = self.ui.spinBox_termination.value()
        self.factory.optimization.lrs_step_size = self.ui.spinBox_lrs_step_size.value()
        self.factory.optimization.fas_order = self.ui.spinBox_fas_order.value()
        self.factory.optimization.mees_area_difference = self.ui.doubleSpinBox_mees_area_difference.value()
        if self.ui.spinBox_mees_facility_range_from.value() <= self.ui.spinBox_mees_facility_range_to.value():
            self.factory.optimization.mees_facility_range = (
            self.ui.spinBox_mees_facility_range_from.value(), self.ui.spinBox_mees_facility_range_to.value())
        else:
            self.factory.optimization.mees_facility_range = (
            self.ui.spinBox_mees_facility_range_to.value(), self.ui.spinBox_mees_facility_range_from.value())
        self.factory.optimization.mees_step_size = self.ui.spinBox_mees_step_size.value()
        self.factory.parameter['noise_calc_wall_corner'] = self.ui.checkBox_noise_calc_wall_corner.isChecked()

        self.factory.parameter['number_of_experiments'] = self.ui.spinBox_number_of_experiments.value()

        self.factory.parameter['noise_encapsulation'] = self.ui.spinBox_noise_encapsulation.value()
        self.factory.parameter['noise_barrier_isolation'] = self.ui.spinBox_noise_barrier_isolation.value()
        self.factory.parameter['noise_factory_wall_isolation'] = self.ui.spinBox_noise_factory_wall_isolation.value()

        self.factory.parameter["noise_absorption_coefficient"] = self.ui.doubleSpinBox_noise_absorption_coefficient.value()
        self.factory.parameter["noise_absorption_floor"] = self.ui.doubleSpinBox_noise_absorption_floor.value()
        self.factory.parameter["noise_absorption_ceil"] = self.ui.doubleSpinBox_noise_absorption_ceil.value()
        self.factory.parameter["noise_absorption_wall"] = self.ui.doubleSpinBox_noise_absorption_wall.value()

        checked_id = self.button_group.checkedId()
        if checked_id == 1:
            self.factory.parameter["noise_semi_diffuse"] = True
            self.factory.parameter["noise_flat_room"] = False
        elif checked_id == 2:
            self.factory.parameter["noise_semi_diffuse"] = False
            self.factory.parameter["noise_flat_room"] = True
        else:
            self.factory.parameter["noise_semi_diffuse"] = False
            self.factory.parameter["noise_flat_room"] = False

        self.factory.parameter["color_facilities_by_sound"] = self.ui.checkBox_color_facilities_by_noise_exposure.isChecked()

        super().accept()
