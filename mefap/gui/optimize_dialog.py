import functools
import logging
import os

from PySide2.QtWidgets import QDialog, QDialogButtonBox, QMessageBox

from mefap.factory.reset_layout import reset_layout
from mefap.factory.optimization import Optimization
from mefap.factory.factory_model import FactoryModel
from mefap.factory.util.metaheuristik_type import MetaheuristicType
from mefap.factory.util.mutation_type import MutationType
from mefap.factory.util.neighborhood_search_type import NeighborhoodSearchType
from mefap.factory.util.opening_type import OpeningType
from mefap.factory.util.sa_cooling_type import SimulatedAnnealingCoolingType
from mefap.gui.generated.OptimizeDialog import Ui_optimize_dialog
from mefap.gui.util.util_methods import set_spinbox, set_spinbox_triple


class OptimizeDialog(QDialog):
    """Dialog which shows user options for optimizing"""

    def __init__(self, factory: FactoryModel, calc=False) -> None:
        super().__init__()
        self.ui = Ui_optimize_dialog()
        self.ui.setupUi(self)
        self.factory = factory

        self.LOCK = True

        if calc:
            for button in self.ui.buttonBox.buttons():
                if button.text() == "OK":
                    button.setText("Calculate")
                    break
        self.calc = calc

        self.ui.comboBox_metaheuristic.currentIndexChanged.connect(self.on_changed_metaheuristic)
        self.ui.buttonBox.button(QDialogButtonBox.Reset).clicked.connect(self.reset)

        if self.factory.optimization.metaheuristic_type == MetaheuristicType.TABU_SEARCH:
            self.ui.comboBox_metaheuristic.setCurrentIndex(0)
            self.activate_ts()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.SIMULATED_ANNEALING:
            self.ui.comboBox_metaheuristic.setCurrentIndex(1)
            self.activate_sa()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GENETIC_ALGORITHM:
            self.ui.comboBox_metaheuristic.setCurrentIndex(2)
            self.activate_ga()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_SA:
            self.ui.comboBox_metaheuristic.setCurrentIndex(3)
            self.activate_ga_sa()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_TS:
            self.ui.comboBox_metaheuristic.setCurrentIndex(4)
            self.activate_ga_ts()

        self.init_tabu_search()
        self.init_simulated_annealing()
        self.init_genetic()
        self.init_ga_sa()
        self.init_ga_ts()

        self.init_path_planning()
        self.init_multicore()
        self.init_opening()
        self.init_optional_restrictions()
        self.init_nhs()
        self.limit_spinboxes()

    def init_multicore(self) -> None:
        """
        Init the multicore options with values from factory.
        :return: None
        """
        self.ui.label_multicore.setText(
            self.ui.label_multicore.text() + self.tr(" (Available cores: ") + str(os.cpu_count()) + ")")
        self.ui.spinBox_multicore.setMaximum(os.cpu_count())
        self.ui.spinBox_multicore.setMinimum(1)
        if self.factory.parameter['multicore'] > os.cpu_count():
            self.ui.spinBox_multicore.setValue(os.cpu_count() - 1)
        else:
            self.ui.spinBox_multicore.setValue(self.factory.parameter['multicore'])
        self.ui.spinBox_multicore.valueChanged.connect(self.update_cores)

    def init_path_planning(self) -> None:
        """
        Init the path planning options with values from factory.
        :return: None
        """
        if self.factory.parameter['no_path_planning']:
            self.ui.checkBox_path_planning.setChecked(False)
        else:
            self.ui.checkBox_path_planning.setChecked(True)
        self.ui.checkBox_path_planning.stateChanged.connect(self.update_path_planning)

    def init_genetic(self) -> None:
        """
        Init the genetic algorithm options with values from factory.
        :return: None
        """
        self.ui.spinBox_population_size.setValue(self.factory.optimization.population_size)
        self.ui.spinBox_population_size.valueChanged.connect(self.update_population_size)
        self.ui.spinBox_number_of_elite_parents.setValue(
            self.factory.optimization.number_of_elite_parents)
        self.ui.spinBox_number_of_elite_parents.valueChanged.connect(
            self.update_number_of_elite_parents)
        self.ui.spinBox_number_of_generations.setValue(
            self.factory.optimization.number_of_generations)
        self.ui.spinBox_number_of_generations.valueChanged.connect(
            self.update_number_of_generations)
        self.ui.doubleSpinBox_crossover_rate.setValue(self.factory.optimization.crossover_rate)
        self.ui.doubleSpinBox_crossover_rate.valueChanged.connect(self.update_crossover_rate)
        self.ui.spinBox_mutation_max_exchange.setValue(
            self.factory.optimization.mutation_max_exchange)
        self.ui.spinBox_mutation_max_exchange.valueChanged.connect(
            self.update_mutation_max_exchange)
        if MutationType.SHIFT_MUTATION in self.factory.optimization.mutation_type and MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_mutation_type.setCurrentIndex(2)
        elif MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_mutation_type.setCurrentIndex(1)
        else:
            self.ui.comboBox_mutation_type.setCurrentIndex(0)
        self.ui.comboBox_mutation_type.currentTextChanged.connect(self.update_mutation_type)
        self.ui.doubleSpinBox_mutation_rate.setValue(self.factory.optimization.mutation_rate)
        self.ui.doubleSpinBox_mutation_rate.valueChanged.connect(self.update_mutation_rate)
        self.ui.spinBox_mutation_max_shift_step.setValue(
            self.factory.optimization.mutation_max_shift_step)
        self.ui.spinBox_mutation_max_shift_step.valueChanged.connect(
            self.update_mutation_max_shift_step)

    def init_ga_ts(self) -> None:
        """
        Init the ga_ts algorithm options with values from factory.
        :return: None
        """
        self.ui.spinBox_ga_ts_number_take_over_variants.setValue(
            self.factory.optimization.number_take_over_variants)
        self.ui.spinBox_ga_ts_number_take_over_variants.valueChanged.connect(
            self.update_number_take_over_variants)
        self.ui.spinBox_ga_ts_length_of_tabu_list.setValue(self.factory.optimization.tabu_length)
        self.ui.spinBox_ga_ts_length_of_tabu_list.valueChanged.connect(self.update_tabu_length)
        self.ui.spinBox_ga_ts_iteration.setValue(self.factory.optimization.tabu_iteration)
        self.ui.spinBox_ga_ts_iteration.valueChanged.connect(self.update_tabu_iteration)
        self.ui.spinBox_ga_ts_population_size.setValue(self.factory.optimization.population_size)
        self.ui.spinBox_ga_ts_population_size.valueChanged.connect(self.update_population_size)
        self.ui.spinBox_ga_ts_number_of_generations.setValue(
            self.factory.optimization.number_of_generations)
        self.ui.spinBox_ga_ts_number_of_generations.valueChanged.connect(
            self.update_number_of_generations)
        self.ui.doubleSpinBox_ga_ts_crossover_rate.setValue(self.factory.optimization.crossover_rate)
        self.ui.doubleSpinBox_ga_ts_crossover_rate.valueChanged.connect(self.update_crossover_rate)
        self.ui.spinBox_ga_ts_number_of_elite_parents.setValue(
            self.factory.optimization.number_of_elite_parents)
        self.ui.spinBox_ga_ts_number_of_elite_parents.valueChanged.connect(
            self.update_number_of_elite_parents)
        self.ui.doubleSpinBox_ga_ts_mutaiton_rate.setValue(self.factory.optimization.mutation_rate)
        self.ui.doubleSpinBox_ga_ts_mutaiton_rate.valueChanged.connect(self.update_mutation_rate)
        self.ui.spinBox_ga_ts_mutation_max_shift_step.setValue(
            self.factory.optimization.mutation_max_shift_step)
        self.ui.spinBox_ga_ts_mutation_max_shift_step.valueChanged.connect(
            self.update_mutation_max_shift_step)
        self.ui.spinBox_ga_ts_mutations_max_exchange.setValue(
            self.factory.optimization.mutation_max_exchange)
        self.ui.spinBox_ga_ts_mutations_max_exchange.valueChanged.connect(
            self.update_mutation_max_exchange)
        if MutationType.SHIFT_MUTATION in self.factory.optimization.mutation_type and MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_ga_ts_mutation_type.setCurrentIndex(2)
        elif MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_ga_ts_mutation_type.setCurrentIndex(1)
        else:
            self.ui.comboBox_ga_ts_mutation_type.setCurrentIndex(0)
        self.ui.comboBox_ga_ts_mutation_type.currentIndexChanged.connect(self.update_mutation_type)

    def init_ga_sa(self) -> None:
        """
        Init the ga_sa algorithm options with values from factory.
        :return: None
        """
        self.ui.spinBox_ga_sa_number_take_over_variants.setValue(
            self.factory.optimization.number_take_over_variants)
        self.ui.spinBox_ga_sa_number_take_over_variants.valueChanged.connect(
            self.update_number_take_over_variants)
        self.ui.spinBox_ga_sa_population_size.setValue(self.factory.optimization.population_size)
        self.ui.spinBox_ga_sa_population_size.valueChanged.connect(self.update_population_size)
        self.ui.spinBox_ga_sa_number_of_generations.setValue(
            self.factory.optimization.number_of_generations)
        self.ui.spinBox_ga_sa_number_of_generations.valueChanged.connect(
            self.update_number_of_generations)
        self.ui.doubleSpinBox_ga_sa_crossover_rate.setValue(self.factory.optimization.crossover_rate)
        self.ui.doubleSpinBox_ga_sa_crossover_rate.valueChanged.connect(self.update_crossover_rate)
        self.ui.spinBox_ga_sa_number_of_elite_parents.setValue(
            self.factory.optimization.number_of_elite_parents)
        self.ui.spinBox_ga_sa_number_of_elite_parents.valueChanged.connect(
            self.update_number_of_elite_parents)
        self.ui.doubleSpinBox_ga_sa_mutation_rate.setValue(self.factory.optimization.mutation_rate)
        self.ui.doubleSpinBox_ga_sa_mutation_rate.valueChanged.connect(self.update_mutation_rate)
        self.ui.spinBox_ga_sa_mutation_max_shift_step.setValue(
            self.factory.optimization.mutation_max_shift_step)
        self.ui.spinBox_ga_sa_mutation_max_shift_step.valueChanged.connect(
            self.update_mutation_max_shift_step)
        self.ui.spinBox_ga_sa_mutation_max_exchange.setValue(
            self.factory.optimization.mutation_max_exchange)
        self.ui.spinBox_ga_sa_mutation_max_exchange.valueChanged.connect(
            self.update_mutation_max_exchange)
        if MutationType.SHIFT_MUTATION in self.factory.optimization.mutation_type and MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_ga_sa_mutation_type.setCurrentIndex(2)
        elif MutationType.EXCHANGE_MUTATION in self.factory.optimization.mutation_type:
            self.ui.comboBox_ga_sa_mutation_type.setCurrentIndex(1)
        else:
            self.ui.comboBox_ga_sa_mutation_type.setCurrentIndex(0)
        self.ui.comboBox_ga_sa_mutation_type.currentIndexChanged.connect(self.update_mutation_type)
        self.ui.doubleSpinBox_ga_sa_starting_temperature.setValue(
            self.factory.optimization.starting_temperature)
        self.ui.doubleSpinBox_ga_sa_starting_temperature.valueChanged.connect(
            self.update_starting_temperatur)
        self.ui.doubleSpinBox_ga_Sa_cooling_rate.setValue(self.factory.optimization.cooling_rate)
        self.ui.doubleSpinBox_ga_Sa_cooling_rate.valueChanged.connect(self.update_cooling_rate)
        self.ui.spinBox_ga_sa_increment.setValue(self.factory.optimization.increment)
        self.ui.spinBox_ga_sa_increment.valueChanged.connect(self.update_increment)
        if self.factory.optimization.cooling_type == SimulatedAnnealingCoolingType.LOGARITHM_FAST:
            self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(1)
        elif self.factory.optimization.cooling_type == SimulatedAnnealingCoolingType.LOGARITHM_SLOW:
            self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(2)
        else:
            self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(0)
        self.ui.comboBox_ga_sa_cooling_type.currentIndexChanged.connect(self.update_cooling_type)

    def init_tabu_search(self) -> None:
        """
        Init the tabu search algorithm options with values from factory.
        :return: None
        """
        self.ui.spinBox_tabu_length.setValue(self.factory.optimization.tabu_length)
        self.ui.spinBox_tabu_length.valueChanged.connect(self.update_tabu_length)
        self.ui.spinBox_iteration.setValue(self.factory.optimization.tabu_iteration)
        self.ui.spinBox_iteration.valueChanged.connect(self.update_tabu_iteration)

    def init_simulated_annealing(self) -> None:
        """
        Init the simulated annealing algorithm options with values from factory.
        :return: None
        """
        self.ui.doubleSpinBox_starting_temperatur.setValue(
            self.factory.optimization.starting_temperature)
        self.ui.doubleSpinBox_starting_temperatur.valueChanged.connect(
            self.update_starting_temperatur)
        self.ui.doubleSpinBox_cooling_rate.setValue(self.factory.optimization.cooling_rate)
        self.ui.doubleSpinBox_cooling_rate.valueChanged.connect(self.update_cooling_rate)
        self.ui.spinBox_increment.setValue(self.factory.optimization.increment)
        self.ui.spinBox_increment.valueChanged.connect(self.update_increment)

        if self.factory.optimization.cooling_type == SimulatedAnnealingCoolingType.LOGARITHM_FAST:
            self.ui.comboBox_cooling_type.setCurrentIndex(1)
        elif self.factory.optimization.cooling_type == SimulatedAnnealingCoolingType.LOGARITHM_SLOW:
            self.ui.comboBox_cooling_type.setCurrentIndex(2)
        else:
            self.ui.comboBox_cooling_type.setCurrentIndex(0)
        self.ui.comboBox_cooling_type.currentIndexChanged.connect(self.update_cooling_type)

    def init_optional_restrictions(self) -> None:
        """
        Init the optional restriction options with values from factory.
        :return: None
        """
        if self.factory.optimization.mirror:
            self.ui.checkBox_mirror.setChecked(True)
        else:
            self.ui.checkBox_mirror.setChecked(False)
        self.ui.checkBox_mirror.stateChanged.connect(self.update_mirror)
        if self.factory.optimization.rotation:
            self.ui.checkBox_rotation.setChecked(True)
        else:
            self.ui.checkBox_rotation.setChecked(False)
        self.ui.checkBox_rotation.stateChanged.connect(self.update_rotation)

    def init_opening(self) -> None:
        """
        Init the opening types options with values from factory.
        :return: None
        """
        if self.factory.optimization.opening == OpeningType.SCHMIGALLA:
            self.ui.radioButton_opening_schmigalla.setChecked(True)
        elif self.factory.optimization.opening == OpeningType.RANDOM_BY_SIZE:
            self.ui.radioButton_opening_random_desc.setChecked(True)
        elif self.factory.optimization.opening == OpeningType.JUMP_OVER:
            self.ui.radioButton_opening_ist.setChecked(True)
        else:
            self.ui.radioButton_opening_random.setChecked(True)
        self.ui.radioButton_opening_random.toggled.connect(self.activate_random_opening)
        self.ui.radioButton_opening_schmigalla.toggled.connect(self.activate_schmigalla_opening)
        self.ui.radioButton_opening_random_desc.toggled.connect(
            self.activate_random_by_size_opening)
        self.ui.radioButton_opening_ist.toggled.connect(self.activate_is_layout)

    def init_nhs(self) -> None:
        """
        Init the neighborhood search type options with values from factory.
        :return: None
        """
        if NeighborhoodSearchType.LocalReallocationSearch in self.factory.optimization.nhs_methods:
            self.ui.checkBox_nhs_mlrs.setChecked(True)
        if NeighborhoodSearchType.FreeAreaSearch in self.factory.optimization.nhs_methods:
            self.ui.checkBox_nhs_fas.setChecked(True)
        if NeighborhoodSearchType.MultipleElementsExchangeSearch in self.factory.optimization.nhs_methods:
            self.ui.checkBox_nhs_mees.setChecked(True)
        self.ui.checkBox_nhs_mlrs.toggled.connect(self.update_nhs_mlrs)
        self.ui.checkBox_nhs_fas.toggled.connect(self.update_nhs_fas)
        self.ui.checkBox_nhs_mees.toggled.connect(self.update_nhs_mees)
        self.ui.spinBox_nhs_mlrs.valueChanged.connect(self.chang_dist_lrs)
        self.ui.spinBox_nhs_mees.valueChanged.connect(self.change_dist_mees)
        self.ui.spinBox_nhs_fas.valueChanged.connect(self.change_dist_fas)
        self.ui.checkBox_nhs_mlrs.stateChanged.connect(self.change_check_lrs)
        self.ui.checkBox_nhs_fas.stateChanged.connect(self.change_check_fas)
        self.ui.checkBox_nhs_mees.stateChanged.connect(self.chancge_check_mees)

    def on_changed_metaheuristic(self) -> None:
        """
        If the metaheuristic selector is changed, also so metaheuristic in the factory get changed.
        :return: None
        """
        if self.ui.comboBox_metaheuristic.currentIndex() == 0:
            self.activate_ts()
            self.factory.optimization.metaheuristic_type = MetaheuristicType.TABU_SEARCH
        elif self.ui.comboBox_metaheuristic.currentIndex() == 1:
            self.activate_sa()
            self.factory.optimization.metaheuristic_type = MetaheuristicType.SIMULATED_ANNEALING
        elif self.ui.comboBox_metaheuristic.currentIndex() == 2:
            self.activate_ga()
            self.factory.optimization.metaheuristic_type = MetaheuristicType.GENETIC_ALGORITHM
        elif self.ui.comboBox_metaheuristic.currentIndex() == 3:
            self.activate_ga_ts()
            self.factory.optimization.metaheuristic_type = MetaheuristicType.GA_TS
        elif self.ui.comboBox_metaheuristic.currentIndex() == 4:
            self.activate_ga_sa()
            self.factory.optimization.metaheuristic_type = MetaheuristicType.GA_SA

    def activate_ts(self) -> None:
        """
        Activate the visibility of tabu search and deactivate all other.
        :return: None
        """
        self.ui.widget_tabu_search.setVisible(True)
        self.ui.widget_simulated_annealing.setVisible(False)
        self.ui.widget_genetic.setVisible(False)
        self.ui.widget_ga_sa.setVisible(False)
        self.ui.widget_ga_ts.setVisible(False)
        self.show_nhs_ts()

    def activate_sa(self) -> None:
        """
        Activate the visibility of simulated annealing and deactivate all other.
        :return: None
        """
        self.ui.widget_tabu_search.setVisible(False)
        self.ui.widget_simulated_annealing.setVisible(True)
        self.ui.widget_genetic.setVisible(False)
        self.ui.widget_ga_sa.setVisible(False)
        self.ui.widget_ga_ts.setVisible(False)
        self.show_nhs_sa()

    def activate_ga(self) -> None:
        """
        Activate the visibility of genetic algorithm and deactivate all other.
        :return: None
        """
        self.ui.widget_tabu_search.setVisible(False)
        self.ui.widget_simulated_annealing.setVisible(False)
        self.ui.widget_genetic.setVisible(True)
        self.ui.widget_ga_sa.setVisible(False)
        self.ui.widget_ga_ts.setVisible(False)
        self.show_nhs_ga()

    def activate_ga_ts(self) -> None:
        """
        Activate the visibility of ga_ts and deactivate all other.
        :return: None
        """
        self.ui.widget_tabu_search.setVisible(False)
        self.ui.widget_simulated_annealing.setVisible(False)
        self.ui.widget_genetic.setVisible(False)
        self.ui.widget_ga_sa.setVisible(False)
        self.ui.widget_ga_ts.setVisible(True)
        self.show_nhs_ts()

    def activate_ga_sa(self) -> None:
        """
        Activate the visibility of ga_sa and deactivate all other.
        :return: None
        """
        self.ui.widget_tabu_search.setVisible(False)
        self.ui.widget_simulated_annealing.setVisible(False)
        self.ui.widget_genetic.setVisible(False)
        self.ui.widget_ga_sa.setVisible(True)
        self.ui.widget_ga_ts.setVisible(False)
        self.show_nhs_sa()

    def show_nhs_ga(self) -> None:
        """
        Shows the right neighborhood search type for the genetic algorithm.
        :return: None
        """
        self.ui.spinBox_nhs_fas.setEnabled(False)
        self.ui.checkBox_nhs_fas.setEnabled(False)
        self.ui.spinBox_nhs_mees.setEnabled(False)
        self.ui.checkBox_nhs_mees.setEnabled(False)
        self.ui.spinBox_nhs_mlrs.setEnabled(False)
        self.ui.checkBox_nhs_mlrs.setEnabled(False)
        if NeighborhoodSearchType.FreeAreaSearch in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.FreeAreaSearch)
        if NeighborhoodSearchType.MultipleElementsExchangeSearch in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.MultipleElementsExchangeSearch)
        if NeighborhoodSearchType.LocalReallocationSearch in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.LocalReallocationSearch)
        self.factory.optimization.set_distribution_zero()
        self.update_nhs_distribution_from_model()

    def show_nhs_ts(self) -> None:
        """
        Shows the right neighborhood search type for the tabu search.
        :return: None
        """
        if self.ui.checkBox_nhs_fas.isChecked():
            self.ui.spinBox_nhs_fas.setEnabled(True)
        self.ui.checkBox_nhs_fas.setEnabled(True)
        self.ui.spinBox_nhs_mees.setEnabled(False)
        self.ui.checkBox_nhs_mees.setEnabled(False)
        if self.ui.checkBox_nhs_mlrs.isChecked():
            self.ui.spinBox_nhs_mlrs.setEnabled(True)
        self.ui.checkBox_nhs_mlrs.setEnabled(True)
        if NeighborhoodSearchType.MultipleElementsExchangeSearch in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.MultipleElementsExchangeSearch)
        if self.ui.checkBox_nhs_fas.isChecked() and NeighborhoodSearchType.FreeAreaSearch not in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.FreeAreaSearch)
        if self.ui.checkBox_nhs_mlrs.isChecked() and NeighborhoodSearchType.LocalReallocationSearch not in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.LocalReallocationSearch)
        self.factory.optimization.set_distribution_tabu()
        self.update_nhs_distribution_from_model()

    def show_nhs_sa(self) -> None:
        """
        Shows the right neighborhood search type for the simulated annealing.
        :return: None
        """
        if self.ui.checkBox_nhs_fas.isChecked():
            self.ui.spinBox_nhs_fas.setEnabled(True)
        self.ui.checkBox_nhs_fas.setEnabled(True)
        if self.ui.checkBox_nhs_mees.isChecked():
            self.ui.spinBox_nhs_mees.setEnabled(True)
        self.ui.checkBox_nhs_mees.setEnabled(True)
        if self.ui.checkBox_nhs_mlrs.isChecked():
            self.ui.spinBox_nhs_mlrs.setEnabled(True)
        self.ui.checkBox_nhs_mlrs.setEnabled(True)
        if self.ui.checkBox_nhs_fas.isChecked() and NeighborhoodSearchType.FreeAreaSearch not in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.FreeAreaSearch)
        if self.ui.checkBox_nhs_mlrs.isChecked() and NeighborhoodSearchType.LocalReallocationSearch not in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.LocalReallocationSearch)
        if self.ui.checkBox_nhs_mees.isChecked() and NeighborhoodSearchType.MultipleElementsExchangeSearch not in self.factory.optimization.nhs_methods:
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.MultipleElementsExchangeSearch)
        self.factory.optimization.set_distribution_simulated_annealing()
        self.update_nhs_distribution_from_model()

    def update_nhs_distribution_from_model(self) -> None:
        """
        Set the neighborhood search type values from the factory.
        :return: None
        """
        self.ui.spinBox_nhs_mlrs.setValue(self.factory.optimization.lrs_distribution)
        self.ui.spinBox_nhs_mees.setValue(self.factory.optimization.mees_distribution)
        self.ui.spinBox_nhs_fas.setValue(self.factory.optimization.fas_distribution)

    def update_tabu_length(self) -> None:
        """
        This method is updating the tabu_length inside the Optimization object of the FactoryModel.
        And it syncs all tabu_length spinboxes.
        :return: None
        """
        value = set_spinbox(self.ui.spinBox_tabu_length, self.ui.spinBox_ga_ts_length_of_tabu_list,
                            self.factory.optimization.tabu_length)
        self.factory.optimization.tabu_length = value

    def update_tabu_iteration(self) -> None:
        """
        This method is updating the tabu_iteration inside the Optimization object
        of the FactoryModel. And it syncs all tabu_iteration spinboxes.
        :return: None
        """
        value = set_spinbox(self.ui.spinBox_iteration, self.ui.spinBox_ga_ts_iteration,
                            self.factory.optimization.tabu_iteration)
        self.factory.optimization.tabu_iteration = value

    def update_starting_temperatur(self) -> None:
        """
        This method is updating the starting_temperature inside the Optimization object
        of the FactoryModel. And it syncs all starting temperatur spinboxes.
        :return: None
        """
        value = set_spinbox(self.ui.doubleSpinBox_starting_temperatur,
                            self.ui.doubleSpinBox_ga_sa_starting_temperature,
                            self.factory.optimization.starting_temperature)
        self.factory.optimization.starting_temperature = value

    def update_cooling_rate(self) -> None:
        """
        This method is updating the cooling_rate inside the Optimization object of the FactoryModel.
        And it syncs all cooling rate spinboxes.
        :return: None
        """
        value = set_spinbox(self.ui.doubleSpinBox_cooling_rate,
                            self.ui.doubleSpinBox_ga_Sa_cooling_rate,
                            self.factory.optimization.cooling_rate)
        self.factory.optimization.cooling_rate = value

    def update_cooling_type(self) -> None:
        """
        This method is updating the cooling_type inside the Optimization object of the FactoryModel.
        And it syncs all cooling_type comboboxes.
        :return: None
        """
        if self.ui.comboBox_cooling_type.currentIndex() != self.factory.optimization.cooling_type.value:
            if self.ui.comboBox_cooling_type.currentIndex() == 0:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LINEAR
                self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(0)
            elif self.ui.comboBox_cooling_type.currentIndex() == 1:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_FAST
                self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(1)
            else:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_SLOW
                self.ui.comboBox_ga_sa_cooling_type.setCurrentIndex(2)
        elif self.ui.comboBox_ga_sa_cooling_type.currentIndex() != self.factory.optimization.cooling_type.value:
            if self.ui.comboBox_ga_sa_cooling_type.currentIndex() == 0:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LINEAR
                self.ui.comboBox_cooling_type.setCurrentIndex(0)
            elif self.ui.comboBox_ga_sa_cooling_type.currentIndex() == 1:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_FAST
                self.ui.comboBox_cooling_type.setCurrentIndex(1)
            else:
                self.factory.optimization.cooling_type = SimulatedAnnealingCoolingType.LOGARITHM_SLOW
                self.ui.comboBox_cooling_type.setCurrentIndex(2)

    def update_increment(self) -> None:
        """
        This method is updating the increment inside the Optimization object of the FactoryModel.
        And it syncs all increment spinboxes.
        :return: None
        """
        value = set_spinbox(self.ui.spinBox_increment, self.ui.spinBox_ga_sa_increment,
                            self.factory.optimization.increment)
        self.factory.optimization.increment = value

    def update_population_size(self) -> None:
        """
        This method is updating the population_size inside the Optimization object of the
        FactoryModel. And it syncs all population_size spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.spinBox_population_size,
                                   self.ui.spinBox_ga_sa_population_size,
                                   self.ui.spinBox_ga_ts_population_size,
                                   self.factory.optimization.population_size)
        self.factory.optimization.population_size = value

    def update_number_of_generations(self) -> None:
        """
        This method is updating the number_of_generations inside the Optimization object of the
        FactoryModel. And it syncs all number_of_generations spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.spinBox_number_of_generations,
                                   self.ui.spinBox_ga_ts_number_of_generations,
                                   self.ui.spinBox_ga_sa_number_of_generations,
                                   self.factory.optimization.number_of_generations)
        self.factory.optimization.number_of_generations = value

    def update_crossover_rate(self) -> None:
        """
        This method is updating the crossover_rate inside the Optimization object of the
        FactoryModel. And it syncs all crossover_rate spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.doubleSpinBox_crossover_rate,
                                   self.ui.doubleSpinBox_ga_ts_crossover_rate,
                                   self.ui.doubleSpinBox_ga_sa_crossover_rate,
                                   self.factory.optimization.crossover_rate)
        self.factory.optimization.crossover_rate = value

    def update_number_of_elite_parents(self) -> None:
        """
        This method is updating the number_of_elite_parents inside the Optimization object of the
        FactoryModel. And it syncs all number_of_elite_parents spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.spinBox_number_of_elite_parents,
                                   self.ui.spinBox_ga_ts_number_of_elite_parents,
                                   self.ui.spinBox_ga_sa_number_of_elite_parents,
                                   self.factory.optimization.number_of_elite_parents)
        self.factory.optimization.number_of_elite_parents = value

    def update_mutation_rate(self) -> None:
        """
        This method is updating the mutation_rate inside the Optimization object of the
        FactoryModel. And it syncs all mutation_rate spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.doubleSpinBox_mutation_rate,
                                   self.ui.doubleSpinBox_ga_ts_mutaiton_rate,
                                   self.ui.doubleSpinBox_ga_sa_mutation_rate,
                                   self.factory.optimization.mutation_rate)
        self.factory.optimization.mutation_rate = value

    def update_mutation_max_shift_step(self) -> None:
        """
        This method is updating the mutation_max_shift_step inside the Optimization object of the
        FactoryModel. And it syncs all mutation_max_shift_step spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.spinBox_mutation_max_shift_step,
                                   self.ui.spinBox_ga_sa_mutation_max_shift_step,
                                   self.ui.spinBox_ga_ts_mutation_max_shift_step,
                                   self.factory.optimization.mutation_max_shift_step)
        self.factory.optimization.mutation_max_shift_step = value

    def update_mutation_max_exchange(self) -> None:
        """
        This method is updating the mutation_max_exchange inside the Optimization object of the
        FactoryModel. And it syncs all mutation_max_exchange spinboxes.
        :return: None
        """
        value = set_spinbox_triple(self.ui.spinBox_mutation_max_exchange,
                                   self.ui.spinBox_ga_ts_mutations_max_exchange,
                                   self.ui.spinBox_ga_sa_mutation_max_exchange,
                                   self.factory.optimization.mutation_max_exchange)
        self.factory.optimization.mutation_max_exchange = value

    def update_mutation_type(self) -> None:
        """
        This method is updating the mutation_type inside the Optimization object of the
        FactoryModel. And it syncs all mutation_type comboboxes.
        :return: None
        """
        if self.ui.comboBox_mutation_type.currentIndex() != self.ui.comboBox_ga_ts_mutation_type.currentIndex() and self.ui.comboBox_mutation_type.currentIndex() != self.ui.comboBox_ga_sa_mutation_type.currentIndex() and self.LOCK:
            self.LOCK = False
            if self.ui.comboBox_mutation_type.currentIndex() == 0:
                self.factory.optimization.mutation_type = [MutationType.SHIFT_MUTATION]
            elif self.ui.comboBox_mutation_type.currentIndex() == 1:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION]
            else:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION,
                                                           MutationType.SHIFT_MUTATION]
            self.ui.comboBox_ga_ts_mutation_type.setCurrentIndex(
                self.ui.comboBox_mutation_type.currentIndex())
            self.ui.comboBox_ga_sa_mutation_type.setCurrentIndex(
                self.ui.comboBox_mutation_type.currentIndex())
            self.LOCK = True
        elif self.ui.comboBox_ga_ts_mutation_type.currentIndex() != self.ui.comboBox_mutation_type.currentIndex() and self.ui.comboBox_ga_ts_mutation_type.currentIndex() != self.ui.comboBox_ga_sa_mutation_type.currentIndex() and self.LOCK:
            self.LOCK = False
            if self.ui.comboBox_ga_ts_mutation_type.currentIndex() == 0:
                self.factory.optimization.mutation_type = [MutationType.SHIFT_MUTATION]
            elif self.ui.comboBox_ga_ts_mutation_type.currentIndex() == 1:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION]
            else:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION,
                                                           MutationType.SHIFT_MUTATION]
            self.ui.comboBox_ga_sa_mutation_type.setCurrentIndex(
                self.ui.comboBox_ga_ts_mutation_type.currentIndex())
            self.ui.comboBox_mutation_type.setCurrentIndex(
                self.ui.comboBox_ga_ts_mutation_type.currentIndex())
            self.LOCK = True
        elif self.ui.comboBox_ga_sa_mutation_type.currentIndex() != self.ui.comboBox_mutation_type.currentIndex() and self.ui.comboBox_ga_sa_mutation_type.currentIndex() != self.ui.comboBox_ga_ts_mutation_type.currentIndex() and self.LOCK:
            self.LOCK = False
            if self.ui.comboBox_ga_sa_mutation_type.currentIndex() == 0:
                self.factory.optimization.mutation_type = [MutationType.SHIFT_MUTATION]
            elif self.ui.comboBox_ga_sa_mutation_type.currentIndex() == 1:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION]
            else:
                self.factory.optimization.mutation_type = [MutationType.EXCHANGE_MUTATION,
                                                           MutationType.SHIFT_MUTATION]
            self.ui.comboBox_ga_ts_mutation_type.setCurrentIndex(
                self.ui.comboBox_ga_sa_mutation_type.currentIndex())
            self.ui.comboBox_mutation_type.setCurrentIndex(
                self.ui.comboBox_ga_sa_mutation_type.currentIndex())
            self.LOCK = True

    def update_mirror(self) -> None:
        """
        This method is updating the mirror inside the Optimization object of the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_mirror.isChecked():
            self.factory.optimization.mirror = True
        else:
            self.factory.optimization.mirror = False

    def update_rotation(self) -> None:
        """
        This method is updating the rotation inside the Optimization object of the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_rotation.isChecked():
            self.factory.optimization.rotation = True
        else:
            self.factory.optimization.rotation = False

    def activate_random_opening(self) -> None:
        """
        This method is updating the opening inside the Optimization object
        of the FactoryModel to random.
        :return: None
        """
        self.factory.optimization.opening = OpeningType.RANDOM

    def activate_random_by_size_opening(self) -> None:
        """
        This method is updating the opening inside the Optimization object
        of the FactoryModel to random by size.
        :return: None
        """
        self.factory.optimization.opening = OpeningType.RANDOM_BY_SIZE

    def activate_is_layout(self) -> None:
        """
        This method is updating the opening inside the Optimization object
        of the FactoryModel to jump over.
        OpeningType.JUMP_OVER only gets activated if all facilities are placed.
        :return: None
        """
        if self.ui.radioButton_opening_ist.isChecked():
            if self.factory.all_facilities_placed():
                self.factory.optimization.opening = OpeningType.JUMP_OVER
            else:
                if self.factory.optimization.opening == OpeningType.RANDOM:
                    self.ui.radioButton_opening_random.setChecked(True)
                    self.activate_random_opening()
                elif self.factory.optimization.opening == OpeningType.RANDOM_BY_SIZE:
                    self.ui.radioButton_opening_random_desc.setChecked(True)
                    self.activate_random_by_size_opening()
                elif self.factory.optimization.opening == OpeningType.SCHMIGALLA:
                    self.ui.radioButton_opening_schmigalla.setChecked(True)
                    self.activate_schmigalla_opening()

                errordialog = QMessageBox(self)
                errordialog.setText(self.tr("All facilities must be placed for this opening type!"))
                errordialog.setIcon(QMessageBox.Warning)
                errordialog.exec()

    def activate_schmigalla_opening(self) -> None:
        """
        This method is updating the opening inside the Optimization object
        of the FactoryModel to schmigalla.
        :return: None
        """
        self.factory.optimization.opening = OpeningType.SCHMIGALLA

    def update_nhs_mlrs(self) -> None:
        """
        This method is updating the NeighborhoodSearchType.LocalReallocationSearch inside the Optimization object of
        the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_nhs_mlrs.isChecked():
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.LocalReallocationSearch)
        else:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.LocalReallocationSearch)

    def update_nhs_fas(self) -> None:
        """
        This method is updating the NeighborhoodSearchType.FreeAreaSearch inside the Optimization object of
        the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_nhs_fas.isChecked():
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.FreeAreaSearch)
        else:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.FreeAreaSearch)

    def update_nhs_mees(self) -> None:
        """
        This method is updating the NeighborhoodSearchType.MultipleElementsExchangeSearch inside the Optimization
        object of the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_nhs_mees.isChecked():
            self.factory.optimization.nhs_methods.append(NeighborhoodSearchType.MultipleElementsExchangeSearch)
        else:
            self.factory.optimization.nhs_methods.remove(NeighborhoodSearchType.MultipleElementsExchangeSearch)

    def update_number_take_over_variants(self) -> None:
        """
        This method is updating the number_take_over_variants inside the Optimization object of the FactoryModel.
        :return: None
        """
        value = set_spinbox(self.ui.spinBox_ga_sa_number_take_over_variants,
                            self.ui.spinBox_ga_ts_number_take_over_variants,
                            self.factory.optimization.number_take_over_variants,
                            limit=self.factory.optimization.population_size)
        self.factory.optimization.number_take_over_variants = value

    def chang_dist_lrs(self) -> None:
        """
        This method is updating the lrs_distribution inside the Optimization object of the FactoryModel.
        :return: None
        """
        self.factory.optimization.lrs_distribution = self.ui.spinBox_nhs_mlrs.value()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.TABU_SEARCH or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_TS:
            if self.ui.checkBox_nhs_fas.isChecked():
                self.factory.optimization.fas_distribution = 100 - self.factory.optimization.lrs_distribution
            else:
                self.factory.optimization.lrs_distribution = 100
                self.factory.optimization.fas_distribution = 0
        elif self.factory.optimization.metaheuristic_type == MetaheuristicType.SIMULATED_ANNEALING or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_SA:
            if not self.factory.optimization.is100():
                if self.ui.checkBox_nhs_fas.isChecked() and self.ui.checkBox_nhs_mees.isChecked():
                    part = 100 - self.factory.optimization.lrs_distribution
                    self.factory.optimization.fas_distribution = round((part / 2) + 0.2)
                    self.factory.optimization.mees_distribution = round((part / 2) - 0.2)
                elif self.ui.checkBox_nhs_fas.isChecked():
                    self.factory.optimization.fas_distribution = 100 - self.factory.optimization.lrs_distribution
                    self.factory.optimization.mees_distribution = 0
                elif self.ui.checkBox_nhs_mees.isChecked():
                    self.factory.optimization.mees_distribution = 100 - self.factory.optimization.lrs_distribution
                    self.factory.optimization.fas_distribution_distribution = 0
                else:
                    self.factory.optimization.fas_distribution = 0
                    self.factory.optimization.mees_distribution = 0
                    self.factory.optimization.lrs_distribution = 100
        else:
            pass

        self.update_nhs_distribution_from_model()

    def change_dist_fas(self) -> None:
        """
        This method is updating the fas_distribution inside the Optimization object of the FactoryModel.
        :return: None
        """
        self.factory.optimization.fas_distribution = self.ui.spinBox_nhs_fas.value()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.TABU_SEARCH or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_TS:
            if self.ui.checkBox_nhs_mlrs.isChecked():
                self.factory.optimization.lrs_distribution = 100 - self.factory.optimization.fas_distribution
            else:
                self.factory.optimization.lrs_distribution = 100
                self.factory.optimization.fas_distribution = 0
        elif self.factory.optimization.metaheuristic_type == MetaheuristicType.SIMULATED_ANNEALING or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_SA:
            if not self.factory.optimization.is100():
                if self.ui.checkBox_nhs_mees.isChecked() and self.ui.checkBox_nhs_mlrs.isChecked():
                    part = 100 - self.factory.optimization.fas_distribution
                    self.factory.optimization.lrs_distribution = round((part / 2) + 0.2)
                    self.factory.optimization.mees_distribution = round((part / 2) - 0.2)
                elif self.ui.checkBox_nhs_mees.isChecked():
                    self.factory.optimization.mees_distribution = 100 - self.factory.optimization.fas_distribution
                    self.factory.optimization.lrs_distribution = 0
                elif self.ui.checkBox_nhs_mlrs.isChecked():
                    self.factory.optimization.lrs_distribution = 100 - self.factory.optimization.fas_distribution
                    self.factory.optimization.mees_distribution = 0
                else:
                    self.factory.optimization.fas_distribution = 100
                    self.factory.optimization.mees_distribution = 0
                    self.factory.optimization.lrs_distribution = 0
        else:
            pass
        self.update_nhs_distribution_from_model()

    def change_dist_mees(self) -> None:
        """
        This method is updating the mees_distribution inside the Optimization object of the FactoryModel.
        :return: None
        """
        self.factory.optimization.mees_distribution = self.ui.spinBox_nhs_mees.value()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.TABU_SEARCH or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_TS:
            pass
        elif self.factory.optimization.metaheuristic_type == MetaheuristicType.SIMULATED_ANNEALING or self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_SA:
            if not self.factory.optimization.is100():
                if self.ui.checkBox_nhs_fas.isChecked() and self.ui.checkBox_nhs_mlrs.isChecked():
                    part = 100 - self.factory.optimization.mees_distribution
                    self.factory.optimization.lrs_distribution = round((part / 2) + 0.2)
                    self.factory.optimization.fas_distribution = round((part / 2) - 0.2)
                elif self.ui.checkBox_nhs_fas.isChecked():
                    self.factory.optimization.fas_distribution = 100 - self.factory.optimization.mees_distribution
                    self.factory.optimization.lrs_distribution = 0
                elif self.ui.checkBox_nhs_mlrs.isChecked():
                    self.factory.optimization.lrs_distribution = 100 - self.factory.optimization.mees_distribution
                    self.factory.optimization.fas_distribution_distribution = 0
                else:
                    self.factory.optimization.fas_distribution = 0
                    self.factory.optimization.mees_distribution = 100
                    self.factory.optimization.lrs_distribution = 0
        else:
            pass
        self.update_nhs_distribution_from_model()

    def change_check_lrs(self) -> None:
        """
        This method is enabling/disabling the mlrs neighborhood search type.
        :return: None
        """
        if self.ui.checkBox_nhs_mlrs.isChecked():
            self.ui.spinBox_nhs_mlrs.setEnabled(True)
        else:
            self.ui.spinBox_nhs_mlrs.setEnabled(False)
            self.ui.spinBox_nhs_mlrs.setValue(0)

    def change_check_fas(self) -> None:
        """
        This method is enabling/disabling the fas neighborhood search type.
        :return: None
        """
        if self.ui.checkBox_nhs_fas.isChecked():
            self.ui.spinBox_nhs_fas.setEnabled(True)
        else:
            self.ui.spinBox_nhs_fas.setEnabled(False)
            self.ui.spinBox_nhs_fas.setValue(0)

    def chancge_check_mees(self) -> None:
        """
        This method is enabling/disabling the mees neighborhood search type.
        :return: None
        """
        if self.ui.checkBox_nhs_mees.isChecked():
            self.ui.spinBox_nhs_mees.setEnabled(True)
        else:
            self.ui.spinBox_nhs_mees.setEnabled(False)
            self.ui.spinBox_nhs_mees.setValue(0)

    def update_path_planning(self) -> None:
        """
        This method is updating the no_path_planning inside the Optimization object of the FactoryModel.
        :return: None
        """
        if self.ui.checkBox_path_planning.isChecked():
            self.factory.parameter['no_path_planning'] = False
        else:
            self.factory.parameter['no_path_planning'] = True

    def update_cores(self) -> None:
        """
        This method is updating the multicore inside the Optimization object of the FactoryModel.
        :return: None
        """
        self.factory.parameter['multicore'] = self.ui.spinBox_multicore.value()

    def disconnect_all(self):
        """
        except multicore
        :return:
        """
        self.ui.checkBox_path_planning.stateChanged.disconnect()

        self.ui.spinBox_population_size.valueChanged.disconnect()
        self.ui.spinBox_number_of_elite_parents.valueChanged.disconnect()
        self.ui.spinBox_number_of_generations.valueChanged.disconnect()
        self.ui.doubleSpinBox_crossover_rate.valueChanged.disconnect()
        self.ui.spinBox_mutation_max_exchange.valueChanged.disconnect()
        self.ui.comboBox_mutation_type.currentTextChanged.disconnect()
        self.ui.doubleSpinBox_mutation_rate.valueChanged.disconnect()
        self.ui.spinBox_mutation_max_shift_step.valueChanged.disconnect()

        self.ui.spinBox_ga_ts_number_take_over_variants.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_length_of_tabu_list.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_iteration.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_population_size.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_number_of_generations.valueChanged.disconnect()
        self.ui.doubleSpinBox_ga_ts_crossover_rate.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_number_of_elite_parents.valueChanged.disconnect()
        self.ui.doubleSpinBox_ga_ts_mutaiton_rate.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_mutation_max_shift_step.valueChanged.disconnect()
        self.ui.spinBox_ga_ts_mutations_max_exchange.valueChanged.disconnect()
        self.ui.comboBox_ga_ts_mutation_type.currentIndexChanged.disconnect()

        self.ui.spinBox_ga_sa_number_take_over_variants.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_population_size.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_number_of_generations.valueChanged.disconnect()
        self.ui.doubleSpinBox_ga_sa_crossover_rate.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_number_of_elite_parents.valueChanged.disconnect()
        self.ui.doubleSpinBox_ga_sa_mutation_rate.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_mutation_max_shift_step.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_mutation_max_exchange.valueChanged.disconnect()
        self.ui.comboBox_ga_sa_mutation_type.currentIndexChanged.disconnect()
        self.ui.doubleSpinBox_ga_sa_starting_temperature.valueChanged.disconnect()
        self.ui.doubleSpinBox_ga_Sa_cooling_rate.valueChanged.disconnect()
        self.ui.spinBox_ga_sa_increment.valueChanged.disconnect()
        self.ui.comboBox_ga_sa_cooling_type.currentIndexChanged.disconnect()

        self.ui.spinBox_tabu_length.valueChanged.disconnect()
        self.ui.spinBox_iteration.valueChanged.disconnect()

        self.ui.doubleSpinBox_starting_temperatur.valueChanged.disconnect()
        self.ui.doubleSpinBox_cooling_rate.valueChanged.disconnect()
        self.ui.spinBox_increment.valueChanged.disconnect()

        self.ui.comboBox_cooling_type.currentIndexChanged.disconnect()

        self.ui.checkBox_rotation.stateChanged.disconnect()

        self.ui.radioButton_opening_random.toggled.disconnect()
        self.ui.radioButton_opening_schmigalla.toggled.disconnect()
        self.ui.radioButton_opening_random_desc.toggled.disconnect()
        self.ui.radioButton_opening_ist.toggled.disconnect()

        self.ui.checkBox_nhs_mlrs.toggled.disconnect()
        self.ui.checkBox_nhs_fas.toggled.disconnect()
        self.ui.checkBox_nhs_mees.toggled.disconnect()
        self.ui.spinBox_nhs_mlrs.valueChanged.disconnect()
        self.ui.spinBox_nhs_mees.valueChanged.disconnect()
        self.ui.spinBox_nhs_fas.valueChanged.disconnect()
        self.ui.checkBox_nhs_mlrs.stateChanged.disconnect()
        self.ui.checkBox_nhs_fas.stateChanged.disconnect()
        self.ui.checkBox_nhs_mees.stateChanged.disconnect()

    def reset(self) -> None:
        logging.info("Reset Optimization parameter")
        self.factory.optimization = Optimization()
        self.factory.optimization.set_default(len(self.factory.facility_list))

        self.disconnect_all()

        self.init_tabu_search()
        self.init_simulated_annealing()
        self.init_genetic()
        self.init_ga_sa()
        self.init_ga_ts()

        self.init_path_planning()
        self.init_opening()
        self.init_optional_restrictions()
        self.init_nhs()

        if self.factory.optimization.metaheuristic_type == MetaheuristicType.TABU_SEARCH:
            self.ui.comboBox_metaheuristic.setCurrentIndex(0)
            self.activate_ts()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.SIMULATED_ANNEALING:
            self.ui.comboBox_metaheuristic.setCurrentIndex(1)
            self.activate_sa()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GENETIC_ALGORITHM:
            self.ui.comboBox_metaheuristic.setCurrentIndex(2)
            self.activate_ga()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_SA:
            self.ui.comboBox_metaheuristic.setCurrentIndex(3)
            self.activate_ga_sa()
        if self.factory.optimization.metaheuristic_type == MetaheuristicType.GA_TS:
            self.ui.comboBox_metaheuristic.setCurrentIndex(4)
            self.activate_ga_ts()

    def limit_spinboxes(self) -> None:
        """
        This methods is limiting the spinboxes where the value depends on the number of facilities.
        :return: None
        """
        num_facilities = len(self.factory.facility_list)
        self.ui.spinBox_number_of_elite_parents.setMaximum(num_facilities)
        self.ui.spinBox_ga_sa_number_of_elite_parents.setMaximum(num_facilities)
        self.ui.spinBox_ga_ts_number_of_elite_parents.setMaximum(num_facilities)
        self.ui.spinBox_mutation_max_shift_step.setMaximum(num_facilities)
        self.ui.spinBox_ga_sa_mutation_max_shift_step.setMaximum(num_facilities)
        self.ui.spinBox_ga_ts_mutation_max_shift_step.setMaximum(num_facilities)
        self.ui.spinBox_mutation_max_exchange.setMaximum(num_facilities)
        self.ui.spinBox_ga_sa_mutation_max_exchange.setMaximum(num_facilities)
        self.ui.spinBox_ga_ts_mutations_max_exchange.setMaximum(num_facilities)

    def accept(self) -> None:
        """
        If the user already has optimized his factory or placed fixed facilities, this dialog ensure, he knows what
        he is doing.
        :return: None
        """
        if self.calc and self.factory.quamfab.weight_sum and self.factory.optimization.opening != OpeningType.JUMP_OVER:
            errordialog = QMessageBox(self)
            errordialog.setText(self.tr(
                "You have already optimized the factory. Any further optimization would lose all of your progress."))
            errordialog.setInformativeText(self.tr("Choose 'Ist-Layout' to improve your current layout!"))
            errordialog.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            errordialog.setDefaultButton(QMessageBox.Cancel)
            errordialog.setIcon(QMessageBox.Warning)
            if errordialog.exec() == QMessageBox.Cancel:
                return
            else:
                reset_layout(self.factory)
        if not self.calc:
           self.factory.parameter['no_path_planning'] = False
        super().accept()

