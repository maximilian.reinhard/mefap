import logging

from PySide2.QtWidgets import QDialog, QListWidgetItem

from mefap.factory.factory_model import FactoryModel
from mefap.gui.generated.FactoryAllLayouts import Ui_Factory_all_layouts
from mefap.gui.layout_detail_dialog import LayoutDetailDialog


class FactoryAllLayoutsDialog(QDialog):
    """
    In this dialog the user can see all the layouts the optimisation hat created.
    In comparison at the CompareDialog the user can only see the best layouts.
    """

    def __init__(self, factory: FactoryModel) -> None:
        super().__init__()
        self.ui = Ui_Factory_all_layouts()
        self.ui.setupUi(self)

        self.factory = factory

        for index, evaluation in enumerate(self.factory.layouts_evaluation_data):
            self.ui.listWidget.addItem(QListWidgetItem("Layout {}: {}".format(index, evaluation.weight_sum)))

    def accept(self) -> None:
        """
        On accepting the dialog, a LayoutDetailDialog opens to show more detailed information about
        the chosen layout.
        :return: None
        """
        super().accept()
        index = int(self.ui.listWidget.currentItem().text().split()[1][0])
        logging.info("Opening Layout Detail View")
        layout_detail_dialog = LayoutDetailDialog(self.factory, self.factory.layouts[index],
                                                  self.factory.layouts_facility_data[index],
                                                  self.factory.layouts_path_data[index],
                                                  self.factory.layouts_evaluation_data[index], index)
        layout_detail_dialog.show()
        layout_detail_dialog.fit_view()
        layout_detail_dialog.exec()
