import logging

from PySide2.QtWidgets import QDialog

from mefap.gui.generated.FactorySizeDialog import Ui_factory_size_dialog


class FactorySizeDialog(QDialog):
    """
    Dialog which is starting after StartDialog,
    where the user can adjust the size and cell size of the factory.
    """

    def __init__(self, factory) -> None:
        super().__init__()
        self.ui = Ui_factory_size_dialog()
        self.ui.setupUi(self)
        self.factory = factory

        # Gives Factory inital size from gui
        logging.info("Initialising factory size")
        self.ui.doubleSpinBox_factory_width.setValue(self.factory.rows)
        self.ui.doubleSpinBox_factory_height.setValue(self.factory.columns)
        self.ui.doubleSpinBox_cell_size.setValue(self.factory.cell_size)

        # Further initial of spinboxes
        self.adjust_spinbox_steps()

        self.ui.warn_label.setStyleSheet("font-weight: bold; color: red")

        # On change change size of factory
        self.ui.doubleSpinBox_factory_width.valueChanged.connect(self.change_factory_width)
        self.ui.doubleSpinBox_factory_height.valueChanged.connect(self.change_factory_height)
        self.ui.doubleSpinBox_cell_size.valueChanged.connect(self.change_factory_cell_size)

    def change_factory_width(self) -> None:
        """
        On change of rows SpinBox change value of factory rows in FactoryModel.
        :return: None
        """
        logging.info("Factory rows changed")
        self.factory.rows = self.ui.doubleSpinBox_factory_width.value()
        if self.factory.rows % self.factory.cell_size != 0:
            self.ui.warn_label.setText(self.tr("Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!"))
        elif self.factory.columns % self.factory.cell_size == 0:
            self.ui.warn_label.setText("")

    def change_factory_height(self) -> None:
        """
        On change of columns SpinBox change value of factory columns in FactoryModel.
        :return: None
        """
        logging.info("Factory columns changed")
        self.factory.columns = self.ui.doubleSpinBox_factory_height.value()
        if self.factory.columns % self.factory.cell_size != 0:
            self.ui.warn_label.setText(self.tr("Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!"))
        elif self.factory.rows % self.factory.cell_size == 0:
            self.ui.warn_label.setText("")

    def change_factory_cell_size(self) -> None:
        """
        On change of cellsize SpinBox change value of factory cellsize in FactoryModel
        :return: On change of cellsize SpinBox change value of factory cellsize in FactoryModel
        """
        logging.info("Factory cellsize changed")
        self.factory.cell_size = self.ui.doubleSpinBox_cell_size.value()
        self.adjust_spinbox_steps()
        self.change_factory_height()
        self.change_factory_width()

    def adjust_spinbox_steps(self) -> None:
        """
        This method adjusts the steps of the spinboxes, when the cell size was changed.
        :return: None
        """
        self.ui.doubleSpinBox_factory_width.setSingleStep(self.factory.cell_size)
        self.ui.doubleSpinBox_factory_height.setSingleStep(self.factory.cell_size)
