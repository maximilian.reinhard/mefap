import functools
import logging
from typing import List

from PySide2.QtCore import QPoint, Qt, QPointF
from PySide2.QtWidgets import QGraphicsScene, QMenu, QAction, QGraphicsSceneMouseEvent

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.gui.facility_cell import FacilityCell


class FacilityScene(QGraphicsScene):
    """
    The Facility inherits the QGraphicsScene and will be shown by the Facility View.
    It will manage all objects of the facility representation.
    The Scene will be used for setting source and sink of facility objects.
    """

    def __init__(self, facility: FacilityAttributes, cell_size: int, cell_zoom: int, *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.facility: FacilityAttributes = facility
        self.cell_size = cell_size
        self.cell_zoom = cell_zoom
        self.cells: List[FacilityCell] = []
        self.sink: FacilityCell = None
        self.source: FacilityCell = None

    def init_facility_grid(self) -> None:
        """
        Initialises grid.
        :return: None
        """
        logging.info("Initialising grid")
        self.clear()
        self.cells = self.generate_facility_cells()
        for cell in self.cells:
            self.addItem(cell)
            if self.is_sink(cell.x_cord, cell.y_cord):
                cell.set_sink()
                self.sink = cell
            if self.is_source(cell.x_cord, cell.y_cord):
                cell.set_source()
                self.source = cell

    def generate_facility_cells(self) -> List[FacilityCell]:
        """
        Generated empty cells for the grid.
        :return: A List of FacilityCells.
        """
        logging.info("Generating Cells")
        cells = []
        for x_cord in range(self.facility.cell_width1):
            for y_cord in range(self.facility.cell_width2):
                cell = FacilityCell(x_cord, y_cord, x_cord * self.cell_zoom,
                                    y_cord * self.cell_zoom,
                                    self.cell_zoom,
                                    self.cell_zoom)
                cells.append(cell)
        return cells

    def mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        """
        On left click shows context menu for setting sink and source to the position under the mouse.
        :param event:
        :return: None
        """
        if event.button() == Qt.LeftButton:
            self.show_sink_source_context_menu(event.screenPos(), event.scenePos())
        super().mouseReleaseEvent(event)

    def show_sink_source_context_menu(self, screen_pos: QPoint, scene_pos: QPointF) -> None:
        """
        This method will generate a context menu, where the user can choose sink or source.
        The menu only appears if the underlying object is a FacilityCell
        :param screen_pos: Position on the screen where the menu should appear.
        :param scene_pos: Position of the scene where the FacilityCell has to be.
        :return: None
        """
        clicked_items: List[FacilityCell] = [x for x in self.items(scene_pos) if
                                             isinstance(x, FacilityCell)]
        if len(clicked_items) == 1:
            menu = QMenu(self.tr("SinkSourceMenu"), self.parent())
            action1 = QAction(self.tr("Quelle"))
            action1.triggered.connect(functools.partial(self.change_source, clicked_items[0]))
            action2 = QAction(self.tr("Senke"))
            action2.triggered.connect(functools.partial(self.change_sink, clicked_items[0]))
            menu.addAction(action1)
            menu.addAction(action2)
            menu.exec_(screen_pos)

    def change_sink(self, cell: FacilityCell) -> None:
        """
        Changes given FacilityCell to actual sink of this FacilityScene
        :param cell: new sink FacilityCell
        :return: None
        """
        if self.sink:
            self.sink.unset_sink()
        self.sink = cell
        self.facility.sink_position = [cell.y_cord, cell.x_cord]
        self.sink.set_sink()

    def change_source(self, cell: FacilityCell) -> None:
        """
        Changes given FacilityCell to actual source of this FacilityScene
        :param cell: new source FacilityCell
        :return: None
        """
        if self.source:
            self.source.unset_source()
        self.source = cell
        self.facility.source_position = [cell.y_cord, cell.x_cord]
        self.source.set_source()

    def is_sink(self, x_pos: int, y_pos: int) -> bool:
        """
        Decide is on the given coordinate(grid) a sink position
        :param x_pos: x coordinate on grid
        :param y_pos: y coordinate on grid
        :return: Is sink or not
        """
        if self.facility.sink_position:
            if self.facility.sink_position[1] == x_pos and self.facility.sink_position[0] == y_pos:
                return True
        return False

    def is_source(self, x_pos: int, y_pos: int) -> bool:
        """
        Decide is on the given coordinate(grid) a source position
        :param x_pos: x coordinate on grid
        :param y_pos: y coordinate on grid
        :return: Is source or not
        """
        if self.facility.source_position:
            if self.facility.source_position[1] == x_pos and self.facility.source_position[0] == y_pos:
                return True
        return False
