import socket

from mefap.factory.factory_model import FactoryModel
from mefap.optimization.metaheuristics import simulated_annealing
from mefap.optimization.metaheuristics import tabu_search
from mefap.optimization.metaheuristics import genetic_algorithm
from mefap.xlparser.xl_set_parameter import parse_xl_parameter
from mefap.factory.optimization import set_optimization_npp
from mefap.optimization.initialization import post_npp_init
from mefap.factory.database import Database
from mefap.optimization.ga_sub_functions import encode_population
from mefap.factory.reset_layout import reset_layout
from mefap.optimization.npp_optimization import npp_optimization
from mefap.evaluation.noise_propagation.test_noise_prop import test_noise
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition


# generate new Layout
factory = FactoryModel()

# load factory & parameter
pc_name = socket.gethostname()

# load parameter
if pc_name == 'LO-Quamfap' or pc_name == "Aurich":  # laptop: 4 cores
    factory.load_factory("D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_Testdaten.json")
    # factory.load_factory("C:\\Users\\aurich\\Desktop\\MeFaP_Testdaten.json")
    parse_xl_parameter(
        "D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_SWD_Parameter.xlsx", factory,
        pc_name, 0)
elif pc_name == "Franz-Leopold":
    factory.load_factory("C:\\Users\\Paul\\Desktop\\MeFaP_Testdaten.json")
    """
    parse_xl_parameter(
        "D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_SWD_Parameter.xlsx", factory,
        pc_name, 0)
    """
else:
    factory.load_factory(
        "\\\\sonne\projekte\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_SWD_L20.json")
    parse_xl_parameter(
        "\\\\sonne\projekte\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_SWD_Parameter.xlsx",
        factory, pc_name, 0)

factory.parameter.update({'no_path_planning': False})
factory.parameter.update({'termination_criterion': 20})
factory.optimization.number_take_over_variants = 1
factory.parameter.update({'noise_flat_room': False})

# start optimization
"""
test_noise(factory)
"""
# calc noise_room_conditions if necessary
if factory.parameter["noise_semi_diffuse"]:
    calc_noise_room_condition(factory)
# calc noise_propagation_matrix for all cells
factory.noise_propagation_matrix.calc_noise_propagation_conditions(factory)
npp_optimization(factory)
#"""

# end
breakpoint()
