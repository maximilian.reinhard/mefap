import logging
import random
import math
import numpy as np
import copy
import sys
import os

# import methods
from mefap.factory.database import Database
from mefap.optimization.initialization import random_layout_generation_by_facility_size
from mefap.optimization.initialization import random_layout_generation
from mefap.optimization.initialization import schmigalla_layout_generation
from mefap.optimization.neighborhood_search import free_area_search
from mefap.optimization.neighborhood_search import local_reallocation_search
from mefap.optimization.neighborhood_search import multiple_elements_exchange_search
from mefap.optimization.layout_compression import compress_layout
from mefap.optimization.layout_compression import compress_paths
from mefap.factory.reset_layout import reset_layout_to_variant
from mefap.factory.reset_layout import reset_layout
from mefap.evaluation.evaluate_layout import evaluate_layout
from mefap.evaluation.evaluate_layout import evaluate_layout_all_objectives
from mefap.evaluation.eval_environmental import re_calc_noise_matrix
from mefap.optimization.ga_sub_functions import encode_population
from mefap.optimization.ga_sub_functions import encode_individual
from mefap.optimization.ga_sub_functions import decode_individual
from mefap.optimization.ga_sub_functions import mutation_shift_facilities
from mefap.optimization.ga_sub_functions import mutation_exchange_facilities
from mefap.optimization.ga_sub_functions import crossover_biased_key
from mefap.optimization.ga_sub_functions import crossover_elite_shift
from mefap.optimization.ga_sub_functions import crossover_two_point
from mefap.optimization.ga_sub_functions import fitness_selection
from mefap.optimization.ga_sub_functions import stochastic_universal_sampling
from mefap.optimization.ga_sub_functions import get_elite_parents
from mefap.factory.util.opening_type import OpeningType


def genetic_algorithm(factory):
    """
    :param factory: children storage: factory.layouts, factory.layouts_facility_data, ...
    :return:
    """

    # control parameter
    pop_size = factory.optimization.population_size
    generations = factory.optimization.number_of_generations
    crossover_rate = factory.optimization.crossover_rate  # goncalves: 0.1 - 0.25
    crossover_num_elite_parents = factory.optimization.number_of_elite_parents
    mutation_rate = factory.optimization.mutation_rate
    mutation_max_exchange_fac = factory.optimization.mutation_max_exchange
    mutation_max_shift_step = factory.optimization.mutation_max_shift_step

    # init local_database
    database = Database()
    local_database = Database()

    # init population
    if not factory.optimization.opening.value == 3:

        # help variables
        break_counter = 0
        init_chromosome = []
        for index in range(len(factory.facility_list)):
            init_chromosome.append((index, (0, 0, 0, 0), 0))

        # init generation
        individual = 0
        break_helper = 0
        while individual < pop_size - break_helper:
            """
            # generate layout
            if round(individual+break_helper) < round(pop_size / 20):
                while not schmigalla_layout_generation(factory):
                    break_counter += 1
                    if break_counter > factory.parameter["termination_criterion"]:
                        print("Process {}: Error in layout initialisation: schmigalla".format(os.getpid()))
                        # individual += 1
                        break
                else:
                    break_counter = 0
                    individual += 1

            elif round(individual + break_helper) < round(pop_size / 10):
                while not random_layout_generation_by_facility_size(factory):
                    break_counter += 1
                    if break_counter > factory.parameter["termination_criterion"]:
                        print("Process {}: Error in layout initialisation: random positioning by facility size".format(os.getpid()))
                        # individual += 1
                        break
                else:
                    break_counter = 0
                    individual += 1

            else:
            """
            random.shuffle(init_chromosome)
            while not decode_individual(factory, init_chromosome):
                break_counter += 1
                reset_layout(factory)
                random.shuffle(init_chromosome)
                if break_counter > factory.parameter["termination_criterion"]:  # pop_size * 10:
                    print("Process {}: Error in layout initialisation: random positioning by evolution".format(os.getpid()))
            else:
                break_counter = 0
                individual += 1

            if break_counter > factory.parameter["termination_criterion"]:
                break_counter = 0
                break_helper += 1
                continue

            # eval initial layout
            evaluate_layout(factory, database)
            # save layout in children_list
            database.save_in_database(factory)
            # reset layout
            reset_layout(factory)
            # user feedback
            print("Process " + str(os.getpid()) + ": " + str(individual) + " out of " +
                  str(pop_size) + " layouts were initialised")

        # init successful
        if individual > 0:
            print("Process {}: Initialisation of generation was successful".format(os.getpid()))
        else:
            print("Process {}: Aborting layout initialisation".format(os.getpid()))
            raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))

    # post npp init
    else:
        database.transfer_all_variants_between_databases(factory)
        factory.delete_all_from_layouts_data()

    # decode population
    ga_list_parents = encode_population(database)

    # !!! start generation process !!!
    for generation in range(generations):

        # clear children
        local_database.clear_database()

        # overwrite children_list
        ga_list_children = copy.deepcopy(ga_list_parents)


        # CROSSOVER
        if random.random() > crossover_rate:
            # get elite parents
            elite_parents = get_elite_parents(database, crossover_num_elite_parents)
            # crossover_biased_key(ga_list_children, crossover_rate, elite_parents)
            crossover_elite_shift(ga_list_children, crossover_rate, mutation_max_exchange_fac, elite_parents)
        else:
            elite_parents = []
            crossover_two_point(ga_list_children, crossover_num_elite_parents)

        # MUTATION
        # rand_method = random.choices(factory.optimization.nhs_methods, nhs_weights)
        rand_method = random.choice(factory.optimization.mutation_type)
        if rand_method.value == 0:
            mutation_shift_facilities(ga_list_children, mutation_rate, mutation_max_shift_step, elite_parents)
        elif rand_method.value == 1:
            mutation_exchange_facilities(ga_list_children, mutation_rate, mutation_max_exchange_fac, elite_parents)

        # ENCODE children
        individual = 0
        while individual < len(ga_list_children):
            # decode child -> construct layout
            if decode_individual(factory, ga_list_children[individual]):
                # eval child
                evaluate_layout(factory, database, local_database)
                # save layout in children_list
                local_database.save_in_database(factory)  # factory.add_layout_data()
                # set individual
                individual += 1
            else:
                # delete not feasible individual
                ga_list_children.pop(individual)

            # reset layout
            reset_layout(factory)

        feasible_children = len(ga_list_children)

        # DECODE children
        ga_list_children = encode_population(local_database)  # factory

        # SELECTION
        """
        best_objective = fitness_selection(database, local_database, ga_list_parents, ga_list_children, pop_size)
        """
        best_objective = stochastic_universal_sampling(database, local_database, ga_list_parents, ga_list_children,
                                                       pop_size)
        #"""

        # print something
        print("Process " + str(os.getpid()) + ": " + "generation = " + str(
            generation + 1) + " / " + str(generations) + " | best objective value = " + "{0:.3f}".format(best_objective) +
              " | feasible children = " + str(feasible_children))

    # END OF EVOLUTION

    # GET BEST LAYOUT
    # iterate over population
    best_variants = []
    for index in range(len(database.layouts)):
        best_variants.append((index, database.layouts_evaluation_data[index].weight_sum))
    # sort based in weight_sum
    best_variants.sort(key=lambda tup: tup[1], reverse=True)

    # END or METAHEURISTIC
    if factory.optimization.metaheuristic_type.name == "GENETIC_ALGORITHM":
        # open best layout
        reset_layout_to_variant(factory, database, best_variants[0][0])
        # save date and escape
        evaluate_layout_all_objectives(factory)
        # re_calc_noise_matrix(factory)
        factory.transfer_all_variants_to_factory(database)
        # save only best x variants
        if not factory.parameter["no_path_planning"]:
            factory.cut_layout_data(factory.parameter["number_saved_variants"])

        print("Process " + str(os.getpid()) + ": " + "best_layout: " + str(best_variants[0][0]) +
              " | weight_sum = " + "{0:.5f}".format(factory.quamfab.weight_sum))

    elif factory.optimization.metaheuristic_type.name == "GA_TS" or \
            factory.optimization.metaheuristic_type.name == "GA_SA":
        # TABU SEARCH or SIMULATED ANNEALING based on best layout of GA_LC

        # cut down to elite_parents
        best_variants = best_variants[0:factory.optimization.number_take_over_variants]

        # clear local_database
        local_database.clear_database()

        # reset opening type
        factory.optimization.opening: OpeningType = OpeningType(3)

        # do it for all elite_parents
        for variant in best_variants:
            # open elite layout
            reset_layout_to_variant(factory, database, variant[0])
            # evaluate_layout_all_objectives(factory, database) todo?

            # start metaheuristic
            if factory.optimization.metaheuristic_type.name == "GA_TS":
                tabu_search(factory, from_GA=True)
            else:
                simulated_annealing(factory, from_GA=True)

            # store layout in local_db
            local_database.save_in_database(factory)

        # GET BEST LAYOUT
        # iterate over population
        best_variants = []
        for index in range(len(local_database.layouts)):
            best_variants.append((index, local_database.layouts_evaluation_data[index].weight_sum))
        # sort based in weight_sum
        best_variants.sort(key=lambda tup: tup[1], reverse=True)
        # open best layout
        reset_layout_to_variant(factory, local_database, best_variants[0][0])
        # save date and escape
        evaluate_layout_all_objectives(factory)
        # re_calc_noise_matrix(factory)
        factory.transfer_all_variants_to_factory(local_database)
        # save only best x variants
        if not factory.parameter["no_path_planning"]:
            factory.cut_layout_data(factory.parameter["number_saved_variants"])

        print("Process " + str(os.getpid()) + ": " + "best_layout: " + str(best_variants[0][0]) +
              " | weight_sum = " + "{0:.5f}".format(factory.quamfab.weight_sum))

    else:
        print("Process {}: Failure in: GA ending".format(os.getgid()))

    if not factory.parameter["no_path_planning"]:
        pass


def tabu_search(factory, **kwargs):
    """
    This function is an implementation of the metaheuristic tabu search.

    :param factory: see FactoryModel
    :return: no return -> everything is stored in factory object
    """
    # control parameter of tabu search
    length_tabu_list = factory.optimization.tabu_length  # 10
    tabu_list = [-1] * length_tabu_list
    num_iterations = factory.optimization.tabu_iteration  # 1000

    # init nhs_weights
    nhs_weights = []
    if factory.optimization.lrs_distribution > 0:
        nhs_weights.append(factory.optimization.lrs_distribution)
    if factory.optimization.fas_distribution > 0:
        nhs_weights.append(factory.optimization.fas_distribution)
    if factory.optimization.mees_distribution > 0:
        nhs_weights.append(factory.optimization.mees_distribution)

    # init local database
    database = Database()  # best known
    local_database = Database()  # current
    nhs_database = Database()

    # generated initial layout
    best_layout_index = 0
    break_counter = 0
    # choose init method
    if factory.optimization.opening.value == 2:
        while not schmigalla_layout_generation(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    elif factory.optimization.opening.value == 1:
        while not random_layout_generation_by_facility_size(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    elif factory.optimization.opening.value == 0:
        while not random_layout_generation(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    else:
        print("Process {}: Layout initialisation was skipped".format(os.getpid()))

    # eval initial layout
    evaluate_layout_all_objectives(factory, database)

    # save layout
    database.save_in_database(factory)
    local_database.save_in_database(factory)

    # init successful
    print("Process {}: Layout initialisation was successful".format(os.getpid()))

    # start Tabu Search
    for iteration in range(num_iterations):
        # generate all neighbors
        # rand_method = random.choice(factory.optimization.nhs_methods)  # set random value for NHS-selection
        # choose nhs_method by random with weights
        rand_method = random.choices(factory.optimization.nhs_methods, nhs_weights)
        for facility in factory.facility_list:

            if not facility.fixed_position:
                breaker = False
                # select and process NHS
                if rand_method[0].value == 0:  # factory.parameter["NHS_allocation"]:
                    if not local_reallocation_search(factory, True, facility.index_num):
                        breaker = True
                elif rand_method[0].value == 1:  # factory.parameter["NHS_allocation"]:
                    if not free_area_search(factory, True, True, facility.index_num):
                        breaker = True
                elif rand_method[0].value == 2:  # factory.parameter["NHS_allocation"]:
                    number_of_facilities = random.randint(factory.optimization.mees_facility_range[0],
                                                          factory.optimization.mees_facility_range[1])
                    # todo: is this useful with tabu search
                    if not multiple_elements_exchange_search(factory, True, number_of_facilities, facility.index_num):
                        breaker = True

                if not breaker:
                    # evaluate neighbor layout
                    evaluate_layout(factory, database, local_database, nhs_database)

                    # local save data
                    nhs_database.save_in_database(factory)  # factory.add_layout_data()

                    # reset layout
                    reset_layout_to_variant(factory, local_database,
                                            len(local_database.layouts) - 1)  # last_layout_index

                else:
                    # local save data
                    nhs_database.save_in_database(factory)  # factory.add_layout_data()
                    # overwrite quamfab_data
                    for objective_name, objective_value in vars(
                            nhs_database.layouts_evaluation_data[facility.index_num]).items():
                        if objective_name == "weight_sum":
                            setattr(nhs_database.layouts_evaluation_data[facility.index_num], objective_name, 0)
                        else:
                            setattr(nhs_database.layouts_evaluation_data[facility.index_num], objective_name, [-1, 0])
                    # nhs_database.layouts_evaluation_data[facility.index_num].weight_sum = 0  # factory.

                    # reset layout
                    reset_layout_to_variant(factory, local_database,
                                            len(local_database.layouts) - 1)  # last_layout_index

            else:
                # local save data
                nhs_database.save_in_database(factory)  # factory.add_layout_data()
                # overwrite quamfab_data
                for objective_name, objective_value in vars(
                        nhs_database.layouts_evaluation_data[facility.index_num]).items():
                    if objective_name == "weight_sum":
                        setattr(nhs_database.layouts_evaluation_data[facility.index_num], objective_name, 0)
                    else:
                        setattr(nhs_database.layouts_evaluation_data[facility.index_num], objective_name, [-1, 0])
                # nhs_database.layouts_evaluation_data[facility.index_num].weight_sum = 0  # factory.
                # reset layout
                reset_layout_to_variant(factory, local_database, len(local_database.layouts) - 1)  # last_layout_index

            """
            print("Process " + str(os.getpid()) + ": " + "facility = " + str(facility.index_num) + " | weight_sum = " +
                  "{0:.4f}".format(nhs_database.layouts_evaluation_data[facility.index_num].weight_sum))  # factory.
            """

        # generate neighbor_list
        neighbor_list = np.zeros((len(nhs_database.layouts_evaluation_data), 2), dtype=float)
        for neighbor in range(len(neighbor_list)):
            neighbor_list[neighbor, 0] = int(neighbor)
            neighbor_list[neighbor, 1] = nhs_database.layouts_evaluation_data[neighbor].weight_sum

        # sort by best neighbor
        neighbor_list = neighbor_list[neighbor_list[:, 1].argsort()[::-1]]

        # calc objective_value_best_layout
        objective_value_best_layout = 0
        for old_variant in database.layouts_evaluation_data:
            if old_variant.weight_sum > objective_value_best_layout:
                objective_value_best_layout = old_variant.weight_sum

        # check tabu list and aspiration criteria
        for neighbor in neighbor_list:
            if neighbor[1] > objective_value_best_layout or int(
                    neighbor[0]) not in tabu_list:  # todo: wert nicht sinnvoll übertragen
                objective_value_new_layout = neighbor[1]  # todo: mehrere Nachbarn bei MEES

                # reset layout
                reset_layout_to_variant(factory, nhs_database, int(neighbor[0]))

                # check with best layout
                if objective_value_new_layout > objective_value_best_layout:
                    # eval all objectives
                    evaluate_layout_all_objectives(factory, database)
                    # save in database
                    database.save_in_database(factory)
                    # store best layout index and
                    best_layout_index = len(database.layouts_evaluation_data) - 1
                    # print
                    print("Process " + str(os.getpid()) + ": " +
                          "Iteration = " + str(iteration) +
                          " | Layout with best objective function value generated = " +
                          "{0:.3f}".format(objective_value_new_layout))
                    # + " | neighbor = " + str(int(neighbor[0]))
                    # +" !! best variant:  " + str(best_layout_index) + "  !!")

                # save in local_database
                local_database.clear_database()
                local_database.save_in_database(factory)

                # update tabu list
                tabu_list.append(int(neighbor[0]))  # neighbor is two times in tabu list
                tabu_list.pop(0)
                break

        # clear neighbor lists
        nhs_database.clear_database()

        print("Process " + str(os.getpid()) + ": " + str(iteration + 1) + " of " + str(num_iterations) +
              " tabu search iterations completed")

    # END
    # open best layout
    reset_layout_to_variant(factory, database, best_layout_index)
    evaluate_layout_all_objectives(factory)

    # re_calc_noise_matrix(factory)
    if "from_GA" not in kwargs:
        factory.transfer_all_variants_to_factory(database)

    # save only best x variants
    if not factory.parameter["no_path_planning"]:
        factory.cut_layout_data(factory.parameter["number_saved_variants"])


def simulated_annealing(factory, **kwargs):
    # init control parameter # todo: take from GUI
    temperature_zero = factory.optimization.starting_temperature  # 30
    temperature = temperature_zero
    cooling_rate = factory.optimization.cooling_rate  # 0.9
    step_size = factory.optimization.increment  # 50

    # init nhs
    nhs_weights = []
    if factory.optimization.lrs_distribution > 0:
        nhs_weights.append(factory.optimization.lrs_distribution)
    if factory.optimization.fas_distribution > 0:
        nhs_weights.append(factory.optimization.fas_distribution)
    if factory.optimization.mees_distribution > 0:
        nhs_weights.append(factory.optimization.mees_distribution)

    # init database
    database = Database()  # store best variants
    local_database = Database()  # store all variants

    # help variables
    time_count = 0
    best_layout_index = 0
    last_layout_index = 0

    # generated initial layout
    break_counter = 0
    # choose init method
    if factory.optimization.opening.value == 2:
        while not schmigalla_layout_generation(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    elif factory.optimization.opening.value == 1:
        while not random_layout_generation_by_facility_size(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    elif factory.optimization.opening.value == 0:
        while not random_layout_generation(factory):
            break_counter += 1
            if break_counter > factory.parameter["termination_criterion"]:
                print("Process {}: Aborting layout initialisation".format(os.getpid()))
                raise Exception("Process {}: Error in layout initialisation".format(os.getpid()))
    else:
        print("Process {}: Layout initialisation was skipped".format(os.getpid()))

    # eval initial layout
    evaluate_layout_all_objectives(factory, database)
    # save layout
    database.save_in_database(factory)
    local_database.save_in_database(factory)

    # init successful
    print("Process {}: Layout initialisation was successful".format(os.getpid()))

    # start simulated annealing
    while temperature > 0.01:
        for step in range(0, step_size, 1):

            # neighborhood search
            breaker = True
            while breaker:
                breaker = False
                break_counter = 0
                # choose nhs_method by random with weights
                # rand_method = random.choice(factory.optimization.nhs_methods)
                rand_method = random.choices(factory.optimization.nhs_methods, nhs_weights)
                # select and process nhs
                if rand_method[0].value == 0:  # factory.parameter["NHS_allocation"]:
                    while not local_reallocation_search(factory, reconstruct_path=True):
                        break_counter += 1
                        if break_counter > len(factory.facility_list) * 3:
                            breaker = True
                            break
                elif rand_method[0].value == 1:  # factory.parameter["NHS_allocation"]:
                    while not free_area_search(factory, reconstruct_path=True, area_order=True):
                        break_counter += 1
                        if break_counter > len(factory.facility_list) * 3:
                            breaker = True
                            break
                elif rand_method[0].value == 2:  # factory.parameter["NHS_allocation"]:
                    number_of_facilities = random.randint(factory.optimization.mees_facility_range[0],
                                                          factory.optimization.mees_facility_range[1])
                    while not multiple_elements_exchange_search(factory, reconstruct_path=True,
                                                                number_of_facilities=number_of_facilities):
                        break_counter += 1
                        if break_counter > len(factory.facility_list) * 3:
                            breaker = True
                            break

            # evaluate neighbor layout
            evaluate_layout(factory, database)
            objective_value_new_layout = factory.quamfab.weight_sum

            # calc objective_value_best_layout (database)
            objective_value_best_layout = 0
            for old_variant in database.layouts_evaluation_data:
                if old_variant.weight_sum > objective_value_best_layout:
                    objective_value_best_layout = old_variant.weight_sum

            # get objective_value_last_layout (local_database)
            objective_value_last_layout = local_database.layouts_evaluation_data[last_layout_index].weight_sum

            if objective_value_new_layout > objective_value_last_layout:  # set >= instead of >
                # check if better then current best
                if objective_value_new_layout >= objective_value_best_layout:  # set >= instead of >
                    # eval all objectives
                    evaluate_layout_all_objectives(factory, database)
                    # save in database and store best layout index
                    database.save_in_database(factory)
                    best_layout_index = len(database.layouts_evaluation_data) - 1
                    # save in local_database and store new last_layout_index
                    local_database.clear_database()
                    local_database.save_in_database(factory)
                    last_layout_index = len(local_database.layouts_evaluation_data) - 1
                    # print
                    print("Process " + str(os.getpid()) + ": " +
                          "Layout with best objective function value generated = " +
                          "{0:.2f}".format(objective_value_new_layout) +
                          " | current temperature = " + "{0:.3f}".format(temperature))
                    # + " !! best variant: " + str(best_layout_index) + "  !!")
                    # " | variant = " + str(last_layout_index) +

                else:
                    # update layout_data_saver
                    local_database.clear_database()
                    local_database.save_in_database(factory)
                    last_layout_index = len(local_database.layouts_evaluation_data) - 1

            else:
                # overwrite Layout if boltzmann
                # generate RandomValue between 0 - 1
                random_value = random.random()
                # calc boltzmann value
                obj_percent_last = objective_value_last_layout * 100
                obj_percent_new = objective_value_new_layout * 100
                boltzmann = math.exp(-((obj_percent_last - obj_percent_new) / temperature))
                if random_value < boltzmann:
                    # update layout_data_saver
                    local_database.clear_database()
                    local_database.save_in_database(factory)
                    last_layout_index = len(local_database.layouts_evaluation_data) - 1

                else:
                    # reset last_layout
                    reset_layout_to_variant(factory, local_database, last_layout_index)

        # compress layout based on GA_LC functions
        # chromosome = encode_individual(factory)
        # reset_layout(factory)
        # decode_individual(factory, chromosome)

        # update time count
        time_count += 1
        # update temperature
        if factory.optimization.cooling_type.value == 0:
            temperature = temperature_zero - (cooling_rate * time_count)  # linear
        elif factory.optimization.cooling_type.value == 1:
            temperature = temperature_zero * pow(cooling_rate, time_count)  # log fast
        else:
            temperature = temperature_zero / math.log((time_count + 1))  # log slow

        print("Process " + str(os.getpid()) + ": Temperature was reduced to " + "{0:.3f}".format(temperature) + " °C ".format(os.getpid()))

    # open best layout
    reset_layout_to_variant(factory, database, best_layout_index)
    evaluate_layout_all_objectives(factory, database)

    # re_calc_noise_matrix(factory)
    if "from_GA" not in kwargs:
        factory.transfer_all_variants_to_factory(database)

    # save only best x variants
    if not factory.parameter["no_path_planning"]:
        factory.cut_layout_data(factory.parameter["number_saved_variants"])
