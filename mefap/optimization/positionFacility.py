import os

import numpy as np
import math

from mefap.optimization.deleteFacility import delete_position_facility
from mefap.optimization.positionPath import try_position_road_around
from mefap.optimization.positionPath import try_position_road_on_source_sink_side
from mefap.optimization.positionPath import try_path_planning_for_facility
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.optimization.deletePath import delete_paths_from_layout
from mefap.optimization.deletePath import delete_all_paths
from mefap.optimization.restrictions import check_restrictions
from mefap.optimization.functional_areas import check_functional_areas
from mefap.optimization.positionSourceAndSink import do_position_source_sink
from mefap.optimization.positionSourceAndSink import calculate_position_source_sink


def position_facility_next_to(factory, origin_facility, curr_facility, direction, rotation, mirror, write_path):
    """
    Positioniert eine facility neben einer anderen
    ToDo: Erweiterung fur curr_width > origin_width
    """
    # set spreading direction based on rotation
    curr_facility_width_y, curr_facility_width_x = calc_width_on_rotation(curr_facility, rotation)

    # set start cell based on rotation of origin_facility or curr_facility AND spreading dircetion
    # set searching steps based on width-difference of origin_facility and curr_facility
    if direction == 'right':  # rechts
        direction_shifter = 1
        if origin_facility.curr_rotation == 0 or origin_facility.curr_rotation == 2:
            y_start = origin_facility.curr_position[0]
            x_start = origin_facility.curr_position[1] + origin_facility.cell_width1
            max_search_steps = max(origin_facility.cell_width2 - curr_facility_width_y, 1)
        elif origin_facility.curr_rotation == 1 or origin_facility.curr_rotation == 3:
            y_start = origin_facility.curr_position[0]
            x_start = origin_facility.curr_position[1] + origin_facility.cell_width2
            max_search_steps = max(origin_facility.cell_width1 - curr_facility_width_y, 1)
    elif direction == 'left':  # links
        direction_shifter = -1
        if rotation == 0 or rotation == 2:
            y_start = origin_facility.curr_position[0]
            x_start = origin_facility.curr_position[1] - curr_facility.cell_width1
            max_search_steps = max(origin_facility.cell_width2 - curr_facility_width_y, 1)
        elif rotation == 1 or rotation == 3:
            y_start = origin_facility.curr_position[0]
            x_start = origin_facility.curr_position[1] - curr_facility.cell_width2
            max_search_steps = max(origin_facility.cell_width1 - curr_facility_width_y, 1)
    elif direction == 'up':  # oben
        direction_shifter = -1
        if rotation == 0 or rotation == 2:
            y_start = origin_facility.curr_position[0] - curr_facility.cell_width2
            x_start = origin_facility.curr_position[1]
            max_search_steps = max(origin_facility.cell_width1 - curr_facility_width_x, 1)
        elif rotation == 1 or rotation == 3:
            y_start = origin_facility.curr_position[0] - curr_facility.cell_width1
            x_start = origin_facility.curr_position[1]
            max_search_steps = max(origin_facility.cell_width2 - curr_facility_width_x, 1)
    elif direction == 'down':  # unten
        direction_shifter = 1
        if origin_facility.curr_rotation == 0 or origin_facility.curr_rotation == 2:
            y_start = origin_facility.curr_position[0] + origin_facility.cell_width2
            x_start = origin_facility.curr_position[1]
            max_search_steps = max(origin_facility.cell_width1 - curr_facility_width_x, 1)
        elif origin_facility.curr_rotation == 1 or origin_facility.curr_rotation == 3:
            y_start = origin_facility.curr_position[0] + origin_facility.cell_width1
            x_start = origin_facility.curr_position[1]
            max_search_steps = max(origin_facility.cell_width2 - curr_facility_width_x, 1)

    # try positioning of curr_facility
    search_range = 0
    if max_search_steps > 1:
        debug = 1
    while search_range < 2:
        for search_step in range(0, max_search_steps, 1):
            if direction == "right" or direction == "left":
                if curr_facility.curr_position == [None, None]:
                    try:
                        position_facility_on_cell(factory, curr_facility, x_start + (search_range * direction_shifter),
                                                  y_start + search_step, rotation, mirror, write_path)
                        return True
                    except:
                        pass
                else:
                    try:
                        re_position_facility_on_cell(factory, curr_facility,
                                                     x_start + (search_range * direction_shifter),
                                                     y_start + search_step, rotation, mirror, write_path)
                        return True
                    except:
                        pass

            elif direction == "up" or direction == "down":
                if curr_facility.curr_position == [None, None]:
                    try:
                        position_facility_on_cell(factory, curr_facility, x_start + search_step,
                                                  y_start + (search_range * direction_shifter), rotation, mirror,
                                                  write_path)
                        return True
                    except:
                        pass
                else:
                    try:
                        re_position_facility_on_cell(factory, curr_facility, x_start + search_step,
                                                     y_start + (search_range * direction_shifter), rotation, mirror,
                                                     write_path)
                        return True
                    except:
                        pass

        search_range += 1
    return False


def position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation, mirror, write_path, *args):
    """ old version, but used """
    try:
        try_position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation)
        check_restrictions(factory, curr_facility, x_start, y_start, rotation)
        check_functional_areas(factory, curr_facility, x_start, y_start, rotation)
    except Exception as e:
        raise e  # return False

    # if try_position_road_around(factory, curr_facility, x_start, y_start, rotation):
    # used for GA
    if args:
        if not try_position_road_on_source_sink_side(factory, curr_facility, x_start, y_start, rotation, mirror,
                                                     write_path=False):
            raise Exception("try")  # return False
    do_position_facility_on_cell(curr_facility, factory.layout, x_start, y_start, rotation, mirror)

    try:
        try_path_planning_for_facility(factory, curr_facility, x_start, y_start,
                                       rotation, mirror, write_path)
        return True
    except Exception as e:
        delete_position_facility(factory, curr_facility)
        raise e  # return False


def position_facility_all_paths(factory, curr_facility, x_start, y_start, rotation, mirror, write_path):
    try:
        try_position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation)
        check_restrictions(factory, curr_facility, x_start, y_start, rotation)
        check_functional_areas(factory, curr_facility, x_start, y_start, rotation)
    except Exception as e:
        raise e  # return False

    do_position_facility_on_cell(curr_facility, factory.layout, x_start, y_start, rotation, mirror)

    try:
        try_path_planning_for_layout(factory, write_path)
        return True
    except Exception as e:
        delete_position_facility(factory, curr_facility)
        delete_all_paths(factory)
        raise e  # return False


def re_position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation, mirror, write_path):
    """ old version, but used """
    # delete curr_facility from current position
    x_start_saver = curr_facility.curr_position[1]
    y_start_saver = curr_facility.curr_position[0]
    rotation_saver = curr_facility.curr_rotation
    mirror_saver = curr_facility.curr_mirror
    delete_position_facility(factory, curr_facility)
    delete_paths_from_layout(factory, curr_facility)
    # try to reallocate curr_facility
    try:
        position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation, mirror, write_path)
        return True
    except Exception as e:
        try:
            position_facility_on_cell(factory, curr_facility, x_start_saver, y_start_saver,rotation_saver, mirror_saver,
                                      write_path)
            raise e  # return False
        except Exception as e:
            print ("Process {}: Error in: re_position_facility_on_cell".format(os.getpid()))
            raise e  # return False


def re_position_facility_all_paths(factory, curr_facility, x_start, y_start, rotation, mirror, write_path):
    # delete curr_facility from current position
    x_start_saver = curr_facility.curr_position[1]
    y_start_saver = curr_facility.curr_position[0]
    rotation_saver = curr_facility.curr_rotation
    mirror_saver = curr_facility.curr_mirror
    delete_position_facility(factory, curr_facility)
    delete_paths_from_layout(factory, curr_facility)
    # try to reallocate curr_facility
    try:
        position_facility_all_paths(factory, curr_facility, x_start, y_start, rotation, mirror, write_path)
        return True
    except Exception as e:
        try:
            position_facility_on_cell(factory, curr_facility, x_start_saver, y_start_saver,rotation_saver,
                                      mirror_saver, write_path=False)

        except Exception as e:
            print("Process {}: Error in: re_position_facility_all_paths".format(os.getpid()))
            raise e  # return False
        raise e  # return False


def try_position_facility_on_cell(factory, curr_facility, x_start, y_start, rotation):
    try_position = 0
    y_facility_width, x_facility_width = calc_width_on_rotation(curr_facility, rotation)
    # check if wall_facility is on wall
    if curr_facility.on_wall:
        y_end = y_start + y_facility_width - 1
        x_end = x_start + x_facility_width - 1
        if y_start == 0 or y_start == factory.layout.shape[0] - 1 or \
                x_start == 0 or x_start == factory.layout.shape[1] - 1 or \
                y_end == 0 or y_end == factory.layout.shape[0] - 1 or \
                x_end == 0 or x_end == factory.layout.shape[1] - 1:
            # y_start == factory.layout.shape[1] - 1
            # x_start == factory.layout.shape[0] - 1
            # y_end == factory.layout.shape[1] - 1
            # x_end == factory.layout.shape[0] - 1
            pass
        else:
            raise Exception("Fabrikobjekt muss an einer Außenwand positioniert werden. \nFacility must be positioned "
                            "on an exterior wall.")

    # check, if each facility-element could be positioned
    for y_index in range(0, y_facility_width, 1):
        for x_index in range(0, x_facility_width, 1):
            # check layout borders
            if factory.layout.shape[1] - 1 >= x_start + x_index >= 0 and \
                    factory.layout.shape[0] - 1 >= y_start + y_index >= 0:
                # check if cells are free
                if factory.layout[y_start + y_index, x_start + x_index] == factory.parameter["empty_cell_value"]:
                    try_position += 1
                else:
                    raise Exception("Zellen sind nicht frei. \nCells aren't free.")
            else:
                raise Exception("Fabrikobjekt können nicht außerhalb der Grenzen positioniert werden. \nFacility can "
                                "not be positioned outside the boundaries.")
    if try_position == curr_facility.cell_num:
        return True


# Methode zur positionieren einer Facility im Layout
def do_position_facility_on_cell(curr_facility, curr_layout, x_start, y_start, rotation, mirror):
    y_facility_width, x_facility_width = calc_width_on_rotation(curr_facility, rotation)
    for y_index in range(0, y_facility_width, 1):
        for x_index in range(0, x_facility_width, 1):
            curr_layout[y_start + y_index, x_start + x_index] = curr_facility.index_num
    curr_facility.curr_position = [y_start, x_start]
    curr_facility.curr_rotation = rotation
    curr_facility.curr_mirror = mirror
    do_position_source_sink(curr_facility, rotation, mirror)
    curr_layout[
        curr_facility.curr_source_position[0], curr_facility.curr_source_position[1]] = curr_facility.index_num  # + 0.1
    curr_layout[
        curr_facility.curr_sink_position[0], curr_facility.curr_sink_position[1]] = curr_facility.index_num  # + 0.2


def calc_width_on_rotation(facility, rotation):
    if rotation == 0 or rotation == 2:
        facility_width_x = facility.cell_width1
        facility_width_y = facility.cell_width2
    elif rotation == 1 or rotation == 3:
        facility_width_x = facility.cell_width2
        facility_width_y = facility.cell_width1
    return facility_width_y, facility_width_x
