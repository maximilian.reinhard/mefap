import socket
import multiprocessing as mp
import os
import math
import time
import traceback

from datetime import datetime

from mefap.factory.factory_model import FactoryModel
from mefap.xlparser.xl_set_parameter import parse_xl_parameter
from mefap.xlparser.xl_result_writer import write_xl_results
from mefap.factory.database import Database
from mefap.factory.reset_layout import reset_layout
from mefap.factory.reset_layout import reset_layout_to_variant
from mefap.evaluation.evaluate_layout import update_objective_value
from mefap.optimization.npp_optimization import npp_optimization
from mefap.evaluation.noise_propagation.noise_propagation_conditions import NoisePropagationMatrix
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition


def load_factory_data():
    # generate new Layout
    factory = FactoryModel()

    # load factory & parameter
    pc_name_process = socket.gethostname()

    """
    # home-desktop
    # factory.load_factory("D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_Testdaten.json")
    file = open('D:/LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/file_path.txt', 'r')
    """
    # Remote-PCs
    # open file which includes path to factory modell
    file = open('//192.168.50.13/projekte/LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/file_path.txt',
                'r')
    #"""
    file_lines = file.readlines()

    # remote-pc selection
    if pc_name_process == "LO-CAD01":
        file_path = file_lines[0]
    elif pc_name_process == "LO-CAD02":
        file_path = file_lines[1]
    elif pc_name_process == "lo-cad03":
        file_path = file_lines[2]
    elif pc_name_process == "lo-cad04":
        file_path = file_lines[3]
    elif pc_name_process == "POOL01":
        file_path = file_lines[4]
    elif pc_name_process == "POOL02":
        file_path = file_lines[5]
    elif pc_name_process == "POOL03":
        file_path = file_lines[6]
    elif pc_name_process == "POOL04":
        file_path = file_lines[7]
    elif pc_name_process == "POOL05":
        file_path = file_lines[8]
    elif pc_name_process == "POOL06":
        file_path = file_lines[9]
    elif pc_name_process == "POOL07":
        file_path = file_lines[10]
    elif pc_name_process == "POOL08":
        file_path = file_lines[11]
    else:
        file_path = file_lines[12]

    # close file
    file.close()

    # cut line break
    if file_path[-1] == "\n":
        file_path = file_path[:-1]

    # open factory model and read parameter
    factory.load_factory(file_path)

    """
    parse_xl_parameter(
        "D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_SWD_Parameter.xlsx",
        factory, pc_name_process, 0)
    """
    parse_xl_parameter(
        "//192.168.50.13/projekte/LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/MeFaP_SWD_Parameter.xlsx",
        factory, pc_name_process, 0)
    #"""

    return factory


def mp_optimization(task):
    # print("Process {}: starts".format(os.getpid()))

    factory = load_factory_data()

    # print("Process " + str(os.getpid()) + ": Init noise propagation.")
    # factory.parameter.update({'noise_factory_wall_isolation': 15})
    # calc noise_room_conditions if necessary
    if factory.parameter["noise_semi_diffuse"]:
        calc_noise_room_condition(factory)
    # calc noise_propagation_matrix for all cells
    # factory.noise_propagation_matrix.calc_noise_propagation_conditions(factory)

    # set parameter
    factory.parameter.update({'no_path_planning': False})  # todo: insert on right position
    factory.parameter.update({'time_stamp': time.time()})

    # overwrite weightings
    """
    overwrite_weighting(factory, task)
    """
    overwrite_parameter(factory, task)
    #"""

    # START OPTIMIZATION
    try:
        npp_optimization(factory)
    except:
        print("Failure in Process " + str(os.getpid()) + ":")
        print(traceback.print_exc())
        reset_layout(factory)
        factory.quamfab_min.weight_sum = 0

    print("Process " + str(os.getpid()) + ": finished with weight_sum = " + str(factory.quamfab.weight_sum))
    # factory.transfer_all_variants_to_factory(database)

    # update time stamp
    # factory.parameter.update({'time_stamp': datetime.now() - factory.parameter["time_stamp"]})
    factory.parameter.update({'time_stamp': ((time.time() - factory.parameter["time_stamp"]) // 60)})
    return task, factory


def overwrite_weighting(factory, task):

    column = math.floor(task / factory.parameter["number_of_experiments"])

    for weighting_name_ist, weighting_value_ist in vars(factory.weighting).items():
        for weighting_name_soll, weighting_value_soll in vars(factory.weight_list).items():
            if weighting_name_ist == weighting_name_soll:
                setattr(factory.weighting, weighting_name_ist, weighting_value_soll[column])
                break


def overwrite_parameter(factory, task):

    column = math.floor(task / factory.parameter["number_of_experiments"])

    # delattr(factory.doe_list, "lrs_step_size")

    for parameter_name_ist, parameter_value_ist in vars(factory.optimization).items():
        for parameter_name_soll, parameter_value_soll in vars(factory.doe_list).items():
            if parameter_name_ist == parameter_name_soll:
                setattr(factory.optimization, parameter_name_ist, parameter_value_soll[column])
                break


def save_results_in_factory_object(factory, result_list):
    # init database
    database = Database()

    # set meta_factory to not npp
    factory.parameter.update({'no_path_planning': False})

    # combine results in on object
    for factory_index in range(len(result_list)):

        for facility in result_list[factory_index][1].facility_list:
            if facility.curr_position == [None, None]:
                # breakpoint()
                break
        else:
            # store best layout of process in db
            database.save_in_database(result_list[factory_index][1])

    # reset to result_factory
    reset_layout_to_variant(factory, database, 0)
    # eval layout
    # evaluate_layout_all_objectives(factory, database)
    update_objective_value(factory, database)

    # get best layout
    best_variants = []
    # iterate over database
    for index in range(len(database.layouts_evaluation_data)):
        best_variants.append((index, database.layouts_evaluation_data[index].weight_sum))
    # sort based in weight_sum
    best_variants.sort(key=lambda tup: tup[1], reverse=True)
    # reset to best layout
    reset_layout_to_variant(factory, database, best_variants[0][0])
    # transfer db to factory
    factory.transfer_all_variants_to_factory(database)

    # save result_list as "project"
    pc_name_save = socket.gethostname()

    # get time
    now = datetime.now()
    date_time = now.strftime("%Y%m%d")

    """
    # home-desktop
    file_path = "D:\LO_AiF(BVL)_MeFaP_2017\Projektbearbeitung\AP6_Experimente\MeFaP_Experiment_" + \
                pc_name_save + "_" + date_time + ".json"
    """
    # Remote-pc_name_saves
    file_path = "//192.168.50.13/projekte/LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/MeFaP_Experiment_" + \
                pc_name_save + "_" + date_time + ".json"
    #"""

    factory.save_factory(file_path)


if __name__ == '__main__':

    # load factory data
    meta_factory = load_factory_data()

    # define outputs of processes
    output = mp.Queue()
    # define maximum parallel workers
    workers = mp.cpu_count() - math.ceil(mp.cpu_count() * 0.05)  # mp.Semaphore(mp.cpu_count() - 2)

    # define number of experiments
    """
    tasks = range(0, meta_factory.parameter["number_of_experiments"] * len(meta_factory.weight_list.quiet))
    """
    tasks = range(0, meta_factory.parameter["number_of_experiments"] * len(meta_factory.doe_list.population_size))
    #"""

    # use pool
    pool = mp.Pool(workers)

    results = pool.map(func=mp_optimization, iterable=tasks)

    pool.close()
    pool.join()

    # write results into xl-file
    write_xl_results(results)

    # save results in one object
    save_results_in_factory_object(meta_factory, results)

    # END
    # breakpoint()
