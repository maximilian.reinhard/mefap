import random
import copy

from scipy.spatial import distance
from mefap.optimization.positionFacility import position_facility_on_cell
from mefap.optimization.positionSourceAndSink import calculate_position_source_sink
from mefap.optimization.neighborhood_search import free_area_search
from mefap.factory.reset_layout import reset_layout
from mefap.optimization.searchFreeCell import free_area_test
from mefap.optimization.positionPath import try_position_road_on_source_sink_side


def encode_population(database):
    # iterate over population
    population_list = []
    for individual in range(len(database.layouts)):
        individual_list = []

        # iterate over facilities
        for index in range(len(database.layouts_facility_data[0])):
            # calc euclidean distance
            euclidean_distance = distance.euclidean([0, 0],
                                                    database.layouts_facility_data[individual][index].curr_position)
            # set individuals
            individual_list.append((index, (database.layouts_facility_data[individual][index].curr_position[0],
                                            database.layouts_facility_data[individual][index].curr_position[1],
                                            database.layouts_facility_data[individual][index].curr_rotation,
                                            database.layouts_facility_data[individual][index].curr_mirror),
                                    euclidean_distance))

        # sort facilities based on distance
        individual_list.sort(key=lambda tup: tup[2])

        # save chromosome in population_list
        population_list.append(individual_list)

    return population_list


def encode_individual(factory):
    """

    :param factory:
    :return:
    """
    individual_list = []
    # iterate over facilities
    for index in range(len(factory.facility_list)):
        # calc euclidean distance
        euclidean_distance = distance.euclidean([0, 0], factory.facility_list[index].curr_position)
        # set individuals
        individual_list.append((index, (factory.facility_list[index].curr_position[0],
                                        factory.facility_list[index].curr_position[1],
                                        factory.facility_list[index].curr_rotation,
                                        factory.facility_list[index].curr_mirror),
                                euclidean_distance))

    # sort facilities based on distance
    individual_list.sort(key=lambda tup: tup[2])

    return individual_list


def mutation_shift_facilities(children_list, mutation_rate, max_shift_step, elite_parents):
    # shift facilities in chromosome
    index = 0
    for chromosome in children_list:
        if index not in elite_parents:
            # elite_parents must mutate
            # check if mutates
            if random.random() <= mutation_rate:
                # chromosome does not mutate
                # set counter
                index += 1
                continue

        # select facility
        rand_facility = random.randint(0, len(chromosome) - 1)
        # calc mutation
        rand_mutation_step = random.randint(-1 * max_shift_step, max_shift_step)
        # delete facility from current position in chromosome and get tuple
        facility_tuple = chromosome.pop(rand_facility)
        # set facility at new position in chromosome
        if rand_facility + rand_mutation_step < 0:
            # set facility to origin
            facility_tuple = (facility_tuple[0], (0, 0, facility_tuple[1][2], facility_tuple[1][3]), facility_tuple[2])
            chromosome.insert(0, facility_tuple)
        elif rand_facility + rand_mutation_step > len(chromosome) - 1:
            # do not set -> should be ok?
            chromosome.append(facility_tuple)
        else:
            # set next to the one before in chromosome
            facility_tuple = (facility_tuple[0], (chromosome[rand_facility + rand_mutation_step - 1][1][0],
                                                  chromosome[rand_facility + rand_mutation_step - 1][1][1],
                                                  facility_tuple[1][2], facility_tuple[1][3]),
                              facility_tuple[2])
            chromosome.insert(rand_facility + rand_mutation_step, facility_tuple)
        # set counter
        index += 1


def mutation_exchange_facilities(children_list, mutation_rate, max_exchange_facilities, elite_parents):
    index = 0
    for chromosome in children_list:
        if index not in elite_parents:
            # elite_parents must mutate
            # check if it mutates
            if random.random() <= mutation_rate:
                # chromosome does not mutate
                index += 1
                continue

        # select facilities
        facilities_position = []
        facilities_tuple = []
        for iteration in range(max_exchange_facilities):
            rand_facility = random.randint(0, len(chromosome) - 1)
            while rand_facility in facilities_position:
                rand_facility = random.randint(0, len(chromosome) - 1)
            facilities_position.append(rand_facility)
        # sort list
        facilities_position.sort()
        # get facility tuples
        for index in facilities_position:
            # get tuple
            facilities_tuple.append(chromosome[index])

        # set facilities at new position in chromosome
        while facilities_position:
            # delete facility from current position in chromosome
            old_facility_tuple = chromosome.pop(facilities_position[-1])
            # get new facility for the position
            new_facility = random.randint(0, len(facilities_tuple) - 1)
            # position facility in chromosome
            new_facility_tuple = (facilities_tuple[new_facility][0], (old_facility_tuple[1][0],
                                                                      old_facility_tuple[1][1],
                                                                      facilities_tuple[new_facility][1][2],
                                                                      facilities_tuple[new_facility][1][3]),
                                  facilities_tuple[new_facility][2])
            chromosome.insert(facilities_position[-1], new_facility_tuple)  # facilities_tuple[new_facility])
            # delete from facilities_tuple
            facilities_tuple.pop(new_facility)
            facilities_position.pop(-1)
        # set counter
        index += 1


def crossover_two_point(children_list, crossover_distance):
    temp_children_list = list(range(len(children_list)))
    while temp_children_list:
        chromosome_1 = children_list[temp_children_list.pop(random.randint(0, len(temp_children_list) - 1))]
        if temp_children_list:
            chromosome_2 = children_list[temp_children_list.pop(random.randint(0, len(temp_children_list) - 1))]
        else:
            continue

        chromo_index_1 = random.randint(0, len(chromosome_1) - 1)
        chromo_index_2 = random.randint(chromo_index_1, chromo_index_1 + crossover_distance)
        # prevent outside of chromosome
        while chromo_index_2 > len(chromosome_2) - 1:
            chromo_index_2 = random.randint(chromo_index_1, chromo_index_1 + crossover_distance)

        chromo_list = [chromosome_1, chromosome_2]

        fac_tuples_1 = []
        for chromo_index in range(chromo_index_1, chromo_index_2 + 1, 1):
            fac_tuples_1.append(chromosome_1[chromo_index])
        fac_tuples_2 = []
        for chromo_index in range(chromo_index_1, chromo_index_2 + 1, 1):
            fac_tuples_2.append(chromosome_2[chromo_index])

        for chromo in chromo_list:
            if chromo == chromo_list[0]:
                fac_tuples = fac_tuples_2
            else:
                fac_tuples = fac_tuples_1

            for fac_tuple in fac_tuples:
                # delete fac_tuple-gen from chromosome (could be down before, than for without change on chromosome)
                for gen in chromo:
                    if gen[0] == fac_tuple[0]:
                        chromo.pop(chromo.index(gen))
                        break

            fac_tuple = fac_tuples[0]
            # insert fac_tuples in chromosome
            for gen in chromo:
                if gen[1][0] >= fac_tuple[1][0] and gen[2] >= fac_tuple[2]:  # gen[1][1] >= fac_tuple[1][1]:
                    for fac_tuple in fac_tuples:
                        chromo.insert(chromo.index(gen), fac_tuple)
                    break
            else:
                for gen in chromo:
                    if gen[2] >= fac_tuple[2]:
                        for fac_tuple in fac_tuples:
                            chromo.insert(chromo.index(gen), fac_tuple)
                        break
                else:
                    for fac_tuple in fac_tuples:
                        chromo.append(fac_tuple)

            if len(chromo) < 20:
                breakpoint()


def crossover_biased_key(children_list, crossover_rate, elite_parents):
    # do the crossover
    individual_count = 0
    for chromo_index in range(len(children_list)):
        if chromo_index not in elite_parents:
            # get elite parent for child
            elite_parent = random.choice(elite_parents)

            # iterate over facilities in individual(chromosome)
            for fac_index in range(len(children_list[chromo_index])):
                if random.random() <= crossover_rate:
                    # CROSSOVER
                    # find index in elite_parent
                    fac_index_ep = [tup[0] for tup in children_list[elite_parent]].index(
                        children_list[chromo_index][fac_index][0])
                    # protect list consistence
                    if fac_index_ep > fac_index:
                        fac_index_ep -= 1
                    facility_tuple = children_list[chromo_index].pop(fac_index)
                    children_list[chromo_index].insert(fac_index_ep, facility_tuple)


def crossover_elite_shift(children_list, crossover_rate, max_exchange_facilities, elite_parents):
    """
    new crossover
    :param children_list:
    :param crossover_rate:
    :param max_exchange_facilities:
    :param elite_parents:
    :return:
    """

    for chromo_index in range(len(children_list)):
        # check if chromosome is elite
        if chromo_index not in elite_parents:
            # check crossover probability
            if random.random() <= 1:  # crossover_rate:

                # get elite parent for child
                elite_parent = random.choice(elite_parents)
                crossover_list = []

                # randomly choose facilities in elite_parent(chromosome) -> 2 point crossover
                # for fac_index_ep in range(len(children_list[elite_parent])):
                fac_index_ep = random.randint(0, len(children_list[elite_parent]) - 1)

                # todo and not children_list[elite_parent][fac_index_ep].fixed_position:
                # generate crossover_list
                for help_counter in range(max_exchange_facilities):
                    if help_counter + fac_index_ep <= len(children_list[elite_parent]) - 1:
                        # todo: if not children_list[elite_parent][fac_index_ep + help_counter].fixed_position:
                        # todo: only add if distance to first is smaller than X (cells)?
                        crossover_list.append(fac_index_ep + help_counter)
                    else:
                        break

                if crossover_list:
                    # delete facilities from individual
                    for fac_index_ep in crossover_list:
                        # find fac_index_individual
                        fac_index_indi = [tup[0] for tup in children_list[chromo_index]].index(
                            children_list[elite_parent][fac_index_ep][0])
                        # todo: protect list consistence ????
                        # delete
                        children_list[chromo_index].pop(fac_index_indi)

                    # find insert point
                    distance_list = []
                    for fac_index_indi in range(len(children_list[chromo_index])):
                        euclidean_distance = distance.euclidean([children_list[chromo_index][fac_index_indi][1][0],
                                                                 children_list[chromo_index][fac_index_indi][1][1]],
                                                                [children_list[elite_parent][crossover_list[0]][1][0],
                                                                 children_list[elite_parent][crossover_list[0]][1][1]])
                        distance_list.append((fac_index_indi, euclidean_distance))
                    # sort list
                    distance_list.sort(key=lambda tup: tup[1])
                    chromo_insert_index = distance_list[0][0]

                    # insert ep_facilities
                    for fac_index_ep in crossover_list:
                        children_list[chromo_index].insert(chromo_insert_index, children_list[elite_parent][fac_index_ep])
                        chromo_insert_index += 1


def get_elite_parents(database, num_elite_parents):
    # get objective_values of parents to find elite parents
    parents_objective_values = []
    for index in range(len(database.layouts)):
        parents_objective_values.append((index, database.layouts_evaluation_data[index].weight_sum))

    # sort parents based on objective
    parents_objective_values.sort(key=lambda tup: tup[1], reverse=True)
    elite_parents = [parent_tuple[0] for parent_tuple in parents_objective_values]
    num_elite_parents = round(len(database.layouts) / 4)
    elite_parents = elite_parents[0:num_elite_parents]
    return elite_parents


def fitness_selection(database, local_database, parents_list, children_list, pop_size):
    # iterate over population
    objectives_parents_and_children = []
    for index in range(len(database.layouts)):
        objectives_parents_and_children.append(("p", index, database.layouts_evaluation_data[index].weight_sum))
    for index in range(len(local_database.layouts)):  # factory.layouts
        objectives_parents_and_children.append(("c", index,
                                                local_database.layouts_evaluation_data[index].weight_sum))  # factor

    # sort based in weight_sum
    objectives_parents_and_children.sort(key=lambda tup: tup[2], reverse=True)

    # store parents and children that should be deleted
    delete_parents_list = []
    delete_children_list = []
    for index in range(pop_size, len(objectives_parents_and_children), 1):
        if objectives_parents_and_children[index][0] == "p":
            delete_parents_list.append(objectives_parents_and_children[index][1])
        else:
            delete_children_list.append(objectives_parents_and_children[index][1])

    # sort lists in reverse
    delete_parents_list.sort(reverse=True)
    delete_children_list.sort(reverse=True)

    # delete unfit parents and children
    for index in delete_parents_list:
        parents_list.pop(index)
        database.delete_variant_from_database(index)
    for index in delete_children_list:
        children_list.pop(index)
        local_database.delete_variant_from_database(index)  # factory.delete_variant_from_layouts_data(index)

    # extend parents with children
    parents_list.extend(children_list)
    for index in range(len(local_database.layouts)):  # len(factory.layouts)
        database.transfer_variant_between_databases(local_database, index)
        # database.save_variant_in_database(factory, index)
    # return best_objective
    return objectives_parents_and_children[0][2]


def stochastic_universal_sampling(database, local_database, parents_list, children_list, pop_size):
    # todo: if useful: tutorialspoint.com/genetic_algorithms/genetic_algorithms_parent_selection.htm
    # https://en.wikipedia.org/wiki/Stochastic_universal_sampling
    # iterate over population
    total_fitness = 0
    objectives_parents_and_children = []
    for index in range(len(database.layouts)):
        objectives_parents_and_children.append(("p", index, database.layouts_evaluation_data[index].weight_sum))
        total_fitness += database.layouts_evaluation_data[index].weight_sum
    for index in range(len(local_database.layouts)):  # factory.layouts
        objectives_parents_and_children.append(("c", index,
                                                local_database.layouts_evaluation_data[index].weight_sum))  # factor
        total_fitness += local_database.layouts_evaluation_data[index].weight_sum

    # calc distance between pointers
    distance_between_pointers = total_fitness / pop_size
    # define start point
    start = random.uniform(0, distance_between_pointers)
    # calc all pointers based on start_point
    pointers = []
    for entry in range(pop_size):
        pointers.append(start + (entry * distance_between_pointers))

    # sort p & c layouts based in weight_sum
    objectives_parents_and_children.sort(key=lambda tup: tup[2], reverse=True)

    # selection of layouts
    keep = []
    for entry in pointers:
        i = 0
        fitness_sum = objectives_parents_and_children[i][2]
        while fitness_sum < entry:
            i += 1
            fitness_sum += objectives_parents_and_children[i][2]
        keep.append(objectives_parents_and_children[i])

    if objectives_parents_and_children[0] not in keep:
        # add best layout to keep, if not in keep before
        keep.insert(0, objectives_parents_and_children[0])
        # remove worst layout
        keep.pop()

    # identify duplictes, delete from keep and store in duplist
    newlist = []  # empty list to hold unique elements from the list
    duplist = []  # empty list to hold the duplicate elements from the list
    for i in keep:
        if i not in newlist:
            newlist.append(i)
        else:
            duplist.append(i)
    keep = newlist

    # save duplicates
    for entry in duplist:
        if entry[0] == "p":
            parents_list.append(copy.deepcopy(parents_list[entry[1]]))
            database.duplicate_variant(entry[1])
        else:
            children_list.append(copy.deepcopy(children_list[entry[1]]))
            local_database.duplicate_variant(entry[1])

    # store parents and children that should be deleted
    delete_parents_list = []
    delete_children_list = []
    for entry in objectives_parents_and_children:
        if entry not in keep:
            if entry[0] == "p":
                delete_parents_list.append(entry[1])
            else:
                delete_children_list.append(entry[1])

    # sort lists in reverse
    delete_parents_list.sort(reverse=True)
    delete_children_list.sort(reverse=True)

    # delete unfit parents and children
    for index in delete_parents_list:
        parents_list.pop(index)
        database.delete_variant_from_database(index)
    for index in delete_children_list:
        children_list.pop(index)
        local_database.delete_variant_from_database(index)  # factory.delete_variant_from_layouts_data(index)

    # extend parents with children
    parents_list.extend(children_list)
    for index in range(len(local_database.layouts)):  # len(factory.layouts)
        database.transfer_variant_between_databases(local_database, index)
        # database.save_variant_in_database(factory, index)
    # return best_objective
    return objectives_parents_and_children[0][2]


def decode_individual(factory, individual):
    """
    :param individual:
    :param factory:
    :return:
    """

    # ensure that all facilities have a connection to the first
    for index in range(factory.mf_matrix.shape[0]):
        factory.mf_matrix[individual[0][0], index] += 1
        factory.mf_matrix[index, individual[0][0]] += 1

    first_flag = True

    # iterate over all facilities
    for facility_chromosome in individual:
        # step over fixed facilities
        if not factory.facility_list[facility_chromosome[0]].fixed_position:

            # set reference_point
            reference_point = False
            # try to locate at "old" position
            if not locate_facility(factory, facility_chromosome, reference_point):
                # try location based on pareto_point method
                # set reference_point
                reference_point = True
                # get pareto points to origin [0, 0]
                pareto_point_list = find_free_pareto_points(factory.layout,
                                                            factory.facility_list[facility_chromosome[0]].cell_width1,
                                                            factory.facility_list[facility_chromosome[0]].cell_width2,
                                                            factory.facility_list[facility_chromosome[0]].on_wall)
                # remove duplicated entries
                pareto_point_list = list(dict.fromkeys(pareto_point_list))

                # if list is empty try free_area_search
                if not pareto_point_list:
                    if free_area_search(factory, False, False, facility_chromosome[0],
                                        old_rotation=facility_chromosome[1][2]):
                        continue
                    else:

                        # facility could not be allocated -> reset mf_matrix
                        for index in range(factory.mf_matrix.shape[0]):
                            factory.mf_matrix[individual[0][0], index] -= 1
                            factory.mf_matrix[index, individual[0][0]] -= 1
                        return False

                # calc pareto_point distance to origin
                for index in range(len(pareto_point_list)):
                    # calc distance to origin
                    euclidean_dis_org = distance.euclidean([0, 0],
                                                           [pareto_point_list[index][0], pareto_point_list[index][1]])
                    # calc distance to old_position
                    euclidean_dis_old_pos = distance.euclidean([facility_chromosome[1][0], facility_chromosome[1][1]],
                                                               [pareto_point_list[index][0], pareto_point_list[index][1]])
                    # sum up distances
                    euclidean_distance = euclidean_dis_org + euclidean_dis_old_pos
                    pareto_point_list[index] = (pareto_point_list[index][0], pareto_point_list[index][1], euclidean_distance)

                # sort pareto points based on distance
                pareto_point_list.sort(key=lambda tup: tup[2])

                # locate facility
                # todo: path planning, if source next to sink ...
                if not locate_facility(factory, facility_chromosome, reference_point, pareto_point_list):

                    # try to position with free area search
                    if not free_area_search(factory, False, False, facility_chromosome[0],
                                            old_rotation=facility_chromosome[1][2]):

                        # facility could not be allocated -> reset mf_matrix
                        for index in range(factory.mf_matrix.shape[0]):
                            factory.mf_matrix[individual[0][0], index] -= 1
                            factory.mf_matrix[index, individual[0][0]] -= 1
                        return False

            if first_flag and not factory.parameter["no_path_planning"]:
                try_position_road_on_source_sink_side(factory, factory.facility_list[facility_chromosome[0]],
                                                      factory.facility_list[facility_chromosome[0]].curr_position[1],
                                                      factory.facility_list[facility_chromosome[0]].curr_position[0],
                                                      factory.facility_list[facility_chromosome[0]].curr_rotation,
                                                      factory.facility_list[facility_chromosome[0]].curr_mirror,
                                                      write_path=True)
                # path_around_first(factory.layout, factory.facility_list[facility_chromosome[0]], factory.parameter)
                first_flag = False
    else:
        # reset mf_matrix
        for index in range(factory.mf_matrix.shape[0]):
            factory.mf_matrix[individual[0][0], index] -= 1
            factory.mf_matrix[index, individual[0][0]] -= 1

    return True


def find_free_pareto_points(curr_layout, facility_width1, facility_width2, on_wall):
    free_pareto_points = []
    facility_max_width = max(facility_width1, facility_width2)
    facility_min_width = min(facility_width1, facility_width2)
    y_layout_shape, x_layout_shape = curr_layout.shape
    # y_max = True: means, that area is big enough, facility wit max_width can be position in y_direction
    iteration_flag = True

    if on_wall:
        # search in y_walls
        for y_index in range(y_layout_shape - facility_min_width + 1):
            for x_index in (0, x_layout_shape - facility_min_width, x_layout_shape - facility_max_width):
                if curr_layout[y_index, x_index] == -1:
                    y_min, x_min, y_max, x_max = free_area_test(curr_layout, y_index, x_index, y_layout_shape,
                                                                x_layout_shape, facility_max_width,
                                                                facility_min_width)
                    # test if curr_point = pareto_point
                    if (x_max and y_max) or (x_max and y_min) or (x_min and y_max):
                        free_pareto_points.append((y_index, x_index))

        # search on x_walls
        for y_index in (0, y_layout_shape - facility_min_width, y_layout_shape - facility_max_width):
            for x_index in range(x_layout_shape - facility_min_width + 1):
                if curr_layout[y_index, x_index] == -1:
                    y_min, x_min, y_max, x_max = free_area_test(curr_layout, y_index, x_index, y_layout_shape,
                                                                x_layout_shape, facility_max_width,
                                                                facility_min_width)
                    # test if curr_point = pareto_point
                    if (x_max and y_max) or (x_max and y_min) or (x_min and y_max):
                        free_pareto_points.append((y_index, x_index))

    else:
        for y_index in range(y_layout_shape - facility_min_width + 1):
            for x_index in range(x_layout_shape - facility_min_width + 1):
                if curr_layout[y_index, x_index] == -1:

                    y_min, x_min, y_max, x_max = free_area_test(curr_layout, y_index, x_index, y_layout_shape,
                                                                x_layout_shape, facility_max_width,
                                                                facility_min_width)

                    # test if curr_point = pareto_point
                    if (x_max and y_max) or (x_max and y_min) or (x_min and y_max):
                        free_pareto_points.append((y_index, x_index))

                    # noch ne runde?
                    if iteration_flag:
                        iteration_flag = False
                        continue
                    else:
                        iteration_flag = True
                        break

    return free_pareto_points


def locate_facility(factory, facility_tuple, reference_point, *pareto_point_list):
    # get facility
    facility = factory.facility_list[facility_tuple[0]]
    facility_rotation = facility_tuple[1][2]
    facility_mirror = facility_tuple[1][3]

    # set random positioning parameter
    if factory.optimization.rotation:
        rotation_list = [0, 1, 2, 3]
        random.shuffle(rotation_list)
        facility_rotation_index = rotation_list.index(facility_rotation)
        rotation_list.pop(facility_rotation_index)
        rotation_list.insert(0, facility_rotation)
    else:
        rotation_list = [0]

    if factory.optimization.mirror:
        mirror_list = [0, 1]
        if facility_mirror == 1:
            mirror_list.sort(reverse=True)
    else:
        mirror_list = [0]

    if not reference_point:
        # get "old" position
        facility_y_pos = facility_tuple[1][0]
        facility_x_pos = facility_tuple[1][1]
        # calc max allowed distance from old position
        for distance_to_curr_pos in range(0, factory.optimization.lrs_step_size +
                                             round(0.5 * factory.optimization.lrs_step_size), 1):

            for y_start in range(-1 * distance_to_curr_pos + facility_y_pos, distance_to_curr_pos + facility_y_pos, 1):
                for x_start in range(-1 * distance_to_curr_pos + facility_x_pos, distance_to_curr_pos + facility_x_pos,
                                     1):
                    for mirror in mirror_list:
                        for rotation in rotation_list:

                            # write_path = True | *args = True
                            if not factory.parameter["no_path_planning"]:

                                try:
                                    position_facility_on_cell(factory, facility, x_start, y_start, rotation, mirror,
                                                              True, True)
                                    return True
                                except:
                                    pass
                            else:
                                # if no_path_planning -> *args = "False"
                                try:
                                    position_facility_on_cell(factory, facility, x_start, y_start,rotation, mirror,
                                                              True)
                                    return True
                                except:
                                    pass

    else:
        # try position facility
        for pareto_point in pareto_point_list[0]:
            for mirror in mirror_list:
                for rotation in rotation_list:
                    if pareto_point[0] == 0 and pareto_point[1] == 0 and \
                            facility.cell_width1 >= 2 and facility.cell_width2 >= 2:
                        source_pos, sink_pos = calculate_position_source_sink(facility, pareto_point[1], pareto_point[0],
                                                                              rotation, mirror)
                        if source_pos == [0, 0] or sink_pos == [0, 0]:
                            continue

                    # write_path = True | *args = True
                    if not factory.parameter["no_path_planning"]:
                        try:
                            position_facility_on_cell(factory, facility, pareto_point[1], pareto_point[0],
                                                      rotation, mirror, True, True)
                            return True
                        except:
                            pass
                    else:
                        # if no_path_planning -> *args = "False"
                        try:
                            position_facility_on_cell(factory, facility, pareto_point[1], pareto_point[0],
                                                      rotation, mirror, True)
                            return True
                        except:
                            pass

    # todo: here was a breakpoint
    return False
