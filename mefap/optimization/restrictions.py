

def check_restrictions(factory, curr_facility, x_start, y_start, rotation):

    # get rotation of facility
    if rotation == 0 or rotation == 2:
        y_facility_width = curr_facility.cell_width2
        x_facility_width = curr_facility.cell_width1
    elif rotation == 1 or rotation == 3:
        y_facility_width = curr_facility.cell_width1
        x_facility_width = curr_facility.cell_width2

    # get noise area id
    # noise_area_id = factory.noise_area.get_noise_walls_of_position(x_start, y_start)
    noise_area_id = factory.noise_area.get_noise_area_id(x_start, y_start)

    for y_index in range(0, y_facility_width, 1):
        for x_index in range(0, x_facility_width, 1):
            if factory.restrictions[0].matrix[y_start + y_index, x_start + x_index] < curr_facility.floorload:
                raise Exception("Bodentraglast wird von Fabrikobjekt überschritten \nFloor load limit is exceeded by "
                                "facility")
            if factory.restrictions[1].matrix[y_start + y_index, x_start + x_index] < curr_facility.ceilingload:
                raise Exception("Deckentraglast wird von Fabrikobjekt überschritten \nCeiling load limit is exceeded "
                                "by facility")
            if factory.restrictions[2].matrix[y_start + y_index, x_start + x_index] < curr_facility.height:
                raise Exception("Fabrikobjekt überschreitet Deckenhöhe \nFacility exceeds ceiling height")
            if factory.noise_area.noise_area_matrix[y_start + y_index, x_start + x_index] != noise_area_id:
                raise Exception("Fabrikobjekt liegt über einer Schallschutzwand")
    return True
