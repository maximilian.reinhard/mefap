<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AddFacilityDialog</name>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="14"/>
        <source>MeFaP - Add Facility</source>
        <translation>MeFaP - Add Facility</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot;
       font-size:24pt; font-weight:600;&quot;&gt;Add Facility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
      </source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot;font-size:24pt; font-weight:600;&quot;&gt;Add Facility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="32"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Allgemein&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;General&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="41"/>
        <source>Bereichsname</source>
        <translation>Facility name</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="58"/>
        <source>Abteilung</source>
        <translation>Department</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="77"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Abmessungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Measurement&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="141"/>
        <source>min. Breite [m]</source>
        <translation>min. width [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Traglasten&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Load capacity&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="199"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Eigenschaften&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Properties&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="211"/>
        <source>Ruhebedarf [dB]</source>
        <translation>max. noise immission</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="239"/>
        <source>Lichtbedarf [lx]</source>
        <translation>Light requirement [lx]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="397"/>
        <source>Niedrig</source>
        <translation>Low</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="402"/>
        <source>Mittel</source>
        <translation>Middle</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="407"/>
        <source>Hoch</source>
        <translation>High</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="337"/>
        <source>Temperatur</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="345"/>
        <source>Hitzeemfindlich</source>
        <translation>Sensitive to heat</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="376"/>
        <source>Neutral</source>
        <translation>Neutral</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="355"/>
        <source>Hitzeemittierend</source>
        <translation>Heat emitting</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="363"/>
        <source>Sauberkeit</source>
        <translation>Cleanliness</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="371"/>
        <source>Schmutzemfindlich</source>
        <translation>Dirt sensitive</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="381"/>
        <source>Schmutzemittierend</source>
        <translation>Dirt emitting</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="415"/>
        <source>Mitarbeiteranzahl</source>
        <translation>Number of employees</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="431"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Weitere
       Restriktionen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
      </source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Further restrictions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="48"/>
        <source>Flaechenart</source>
        <translation>Functional area type</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="124"/>
        <source>Hoehe [m]</source>
        <translation>Height [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="148"/>
        <source>min. Laenge [m]</source>
        <translation>min. length [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="169"/>
        <source>Erforderliche Deckentraglast [kg/m2]</source>
        <translation>Required ceiling load capacity [kg / m²]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="183"/>
        <source>Gewicht / erf. Bodentraglast [kg/m2]</source>
        <translation>Weight / required floor load [kg / m²]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="225"/>
        <source>Laermexposition [dB]</source>
        <translation>Noise emission [dB]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="247"/>
        <source>&lt; 100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="252"/>
        <source>100 - 200</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="257"/>
        <source>200 - 300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="262"/>
        <source>300 - 500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="267"/>
        <source>500 - 750</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="272"/>
        <source>750 - 1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="277"/>
        <source>1000 - 1500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="285"/>
        <source>Erschuetterungsanregung</source>
        <translation>Vibration emission</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="311"/>
        <source>Erschuetterungssensitivitaet</source>
        <translation>Vibration sensitivity</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="389"/>
        <source>Mobilitaet</source>
        <translation>Mobility</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="445"/>
        <source>Aussenwandpositioinierung</source>
        <translation>External wall positioning</translation>
    </message>
</context>
<context>
    <name>CollapsibleDialog</name>
    <message>
        <location filename="collapsible_dialog.py" line="130"/>
        <source>Availible Facilities</source>
        <translation>Available facilities</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="131"/>
        <source>Restrictive area&apos;s</source>
        <translation>Restrictive areas</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="217"/>
        <source>Diagram</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="61"/>
        <source>Movable facility</source>
        <translation>Movable facility</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="67"/>
        <source>Fixed facility</source>
        <translation>Fixed facility</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="129"/>
        <source>Legend</source>
        <translation>Legend</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="278"/>
        <source>Noise Legend</source>
        <translation>Noise Legend</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="85"/>
        <source>Variable transport route</source>
        <translation>Variable transport route</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="91"/>
        <source>Fixed transport route</source>
        <translation>Fixed transport route</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="224"/>
        <source>A network diagram with all evaluation criteria is displayed here.</source>
        <translation>A network diagram with all evaluation criteria is displayed here.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="117"/>
        <source>All restrictive areas of use and their associated color are displayed here.</source>
        <translation>All restrictive areas of use and their associated color are displayed here.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="120"/>
        <source>Here is a legend showing what which cell means.</source>
        <translation>Here is a legend showing what which cell means.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="122"/>
        <source>This shows which facilities have already been placed.</source>
        <translation>This shows which facilities have already been placed.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="279"/>
        <source>A legend is shown here which indicates which color means which volume.</source>
        <translation>A legend is shown here which indicates which color means which volume.</translation>
    </message>
</context>
<context>
    <name>CompareDialog</name>
    <message>
        <location filename="compare_dialog.py" line="53"/>
        <source>Layout {}: {:.3f}%</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EvaluationDialog</name>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Kriterien</source>
        <translation>objectives</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Gewichtung[%]</source>
        <translation>weighting[%]</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Ergebniswerte</source>
        <translation>objective values</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="34"/>
        <source>Bewertung</source>
        <translation>Rating</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="34"/>
        <source>Bestwerte</source>
        <translation>best objective values</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="164"/>
        <source>Weighted Sum</source>
        <translation>weight sum</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="27"/>
        <source>material_flow_length</source>
        <translation>Material flow length</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="28"/>
        <source>no_overlapping</source>
        <translation>No overlapping</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="29"/>
        <source>route_continuity</source>
        <translation>Route continuity</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="30"/>
        <source>land_use_degree</source>
        <translation>Land use degree</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="26"/>
        <source>media_availability</source>
        <translation>Media availability</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="25"/>
        <source>media_compatibility</source>
        <translation>Media compatibility</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="31"/>
        <source>lighting</source>
        <translation>Illuminance</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="32"/>
        <source>quiet</source>
        <translation>Quiet</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="33"/>
        <source>vibration</source>
        <translation>Vibration</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="34"/>
        <source>temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="35"/>
        <source>cleanliness</source>
        <translation>Cleanliness</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="36"/>
        <source>direct_communication</source>
        <translation>Direct communication</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="40"/>
        <source>formal_communication</source>
        <translation>Formal communication</translation>
    </message>
</context>
<context>
    <name>FacilityDialog</name>
    <message>
        <location filename="facility_dialog.py" line="30"/>
        <source>Allokation Quelle und Senke der Facilites</source>
        <translation>Allocation pick-up and drop-off points of the facilities</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="87"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="95"/>
        <source>Nicht alle Quelle und Senken Positionen wurden definiert</source>
        <translation>Not all pick-up and drop-off points have been defined</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="111"/>
        <source>Quelle und Senken Position wurden noch nicht definiert</source>
        <translation>pick-up and drop-off pointsn have not yet been defined</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="128"/>
        <source>Allokation der Quelle und Senke wird verlassen</source>
        <translation>Allocation of pick-up and drop-off points is left</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="97"/>
        <source>Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden.</source>
        <translation>All sources and sinks must be defined before further processing.</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="113"/>
        <source>Vor der weiteren Bearbeitung muessen Quelle und Senke definiert werden.</source>
        <translation>Before further processing, the source and sink must be defined.</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="131"/>
        <source>Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden.
Alle Aenderungen gehen verloren!</source>
        <translation>All sources and sinks must be defined before further processing.
All changes will be lost!</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="44"/>
        <source>With the left mouse button you can assign either a source, a sink or both at the same time to the unit cells.</source>
        <translation>With the left mouse button you can assign either a source, a sink or both at the same time to the unit cells.</translation>
    </message>
</context>
<context>
    <name>FacilityScene</name>
    <message>
        <location filename="facility_scene.py" line="83"/>
        <source>SinkSourceMenu</source>
        <translation>pick-up and drop-off points menu</translation>
    </message>
    <message>
        <location filename="facility_scene.py" line="84"/>
        <source>Quelle</source>
        <translation>pick-up point</translation>
    </message>
    <message>
        <location filename="facility_scene.py" line="86"/>
        <source>Senke</source>
        <translation>drop-off point</translation>
    </message>
</context>
<context>
    <name>FactoryAddFacilityDialog</name>
    <message>
        <location filename="facility_add_dialog.py" line="107"/>
        <source>Add Item</source>
        <translation>Add Fabrikobjekt</translation>
    </message>
    <message>
        <location filename="facility_add_dialog.py" line="114"/>
        <source>Add area type</source>
        <translation>Add area type</translation>
    </message>
    <message>
        <location filename="facility_add_dialog.py" line="114"/>
        <source>Enter a name for the new area type.</source>
        <translation>Enter a name for the new area type.</translation>
    </message>
</context>
<context>
    <name>FactoryCell</name>
    <message>
        <location filename="factory_cell.py" line="199"/>
        <source>Name: {}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FactoryChangeSizeDialog</name>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="14"/>
        <source>Change facory size</source>
        <translation>Change factory size</translation>
    </message>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="22"/>
        <source>Factory width [m]</source>
        <translation>Factory width [m]</translation>
    </message>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="52"/>
        <source>Factory length [m]</source>
        <translation>Factory length [m]</translation>
    </message>
    <message>
        <location filename="factory_change_size_dialog.py" line="48"/>
        <source>Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!</source>
        <translation>The size of the factory must be divisible by the cell size!</translation>
    </message>
</context>
<context>
    <name>FactoryExtendedSettings</name>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="30"/>
        <source>A*-Algorithmus</source>
        <translation>A*-Algorithm</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="103"/>
        <source>Beleuchtung</source>
        <translation>Illuminance</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="162"/>
        <source>Ruhe</source>
        <translation>Quiet / Noise</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="168"/>
        <source>sound pressure reduction</source>
        <translation>Sound pressure reduction</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="455"/>
        <source>Parameter der Optimierung</source>
        <translation>Optimisation parameter</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="461"/>
        <source>lrs search steps</source>
        <translation>LRS: Iterations</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="475"/>
        <source>lrs step size</source>
        <translation>LRS: Step size (moving distance)</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="495"/>
        <source>mees facility range from</source>
        <translation>MEES: min. number of facilities</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="518"/>
        <source>mees facility range to</source>
        <translation>MEES: max. number of facilities</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="538"/>
        <source>mees area difference</source>
        <translation>MEES: Area ratio (difference)</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="561"/>
        <source>fas order</source>
        <translation>OAS: Position selection</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="581"/>
        <source>mees step size</source>
        <translation>MEES: Step size (moving distance)</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="14"/>
        <source>Extended Settings</source>
        <translation>Extended Settings</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="36"/>
        <source>Empty cell costs</source>
        <translation>Utilization costs - empty cell</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="43"/>
        <source>Local road costs</source>
        <translation>Route usage costs - variable path</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="50"/>
        <source>Fixed road costs</source>
        <translation>Route usage costs - fixed path</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="85"/>
        <source>media availability factor</source>
        <translation>Media availability factor</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="109"/>
        <source>Road illuminance</source>
        <translation>minimal path lighting</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="175"/>
        <source>Sound propagation conditions</source>
        <translation>Sound propagation condition</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="208"/>
        <source>Sound reflection on walls</source>
        <translation>Sound reflection on walls</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="601"/>
        <source>termination criterion</source>
        <translation>termination criterion</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="79"/>
        <source>Medienverfuegbarkeit</source>
        <translation>Media availability</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="123"/>
        <source>Kein Licht [&lt; 100lx]</source>
        <translation>No light [≤100lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="128"/>
        <source>Sehr wenig Licht [100 - 200 lx]</source>
        <translation>Very low light [&gt;100 - ≤200 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="133"/>
        <source>Wenig Licht 200 - 300lx]</source>
        <translation>Low light [&gt;200 - ≤300lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="138"/>
        <source>Normales Licht [300 - 500 lx]</source>
        <translation>Normal light [&gt;300 - ≤500 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="143"/>
        <source>Helles Licht [500 - 750 lx]</source>
        <translation>Bright light [&gt;500 - ≤750 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="148"/>
        <source>Sehr helles Licht [750 - 1000 lx]</source>
        <translation>Very bright light [&gt;750 - ≤1000 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="153"/>
        <source>maximales Licht [1000 - 1500 lx]</source>
        <translation>Maximum bright light [&gt;1000 - ≤1500 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="222"/>
        <source>&lt;h2&gt;Sound propagation&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Sound propagation&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="229"/>
        <source>Semi diffuse</source>
        <translation>Semi diffuse</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="236"/>
        <source>Flat room</source>
        <translation>Flat room</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="243"/>
        <source>&lt;h2&gt;Sound reduction&lt;/h2&gt;

</source>
        <translation>&lt;h2&gt;Sound reduction&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="252"/>
        <source>Encapsulation</source>
        <translation>Encapsulation</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="259"/>
        <source>Barrier isolation</source>
        <translation>Barrier isolation</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="266"/>
        <source>Factory wall isolation</source>
        <translation>Factory wall isolation</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="273"/>
        <source>&lt;h2&gt;Sound absorption&lt;/h2&gt;
</source>
        <translation>&lt;h2&gt;Sound absorption&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="281"/>
        <source>Absorption coefficient</source>
        <translation>Absorption coefficient</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="288"/>
        <source>Absorption floor</source>
        <translation>Absorption floor</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="295"/>
        <source>Absorption ceil</source>
        <translation>Absorption ceil</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="302"/>
        <source>Absorption wall</source>
        <translation>Absorption wall</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="624"/>
        <source>Number of experiments</source>
        <translation>Number of experiments</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="426"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="440"/>
        <source>Color facilties by noise exposure</source>
        <translation>Color facilties by noise exposure</translation>
    </message>
</context>
<context>
    <name>FactoryFacility</name>
    <message>
        <location filename="factory_facility.py" line="111"/>
        <source>Mirror</source>
        <translation>Mirror</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="101"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="108"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="114"/>
        <source>Un-/Lock position</source>
        <translation>Un-/Lock position</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="514"/>
        <source>Placement error</source>
        <translation>Placement error</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="358"/>
        <source>No Placing outside the grid allowed!</source>
        <translation>No Placing outside the grid allowed!</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="102"/>
        <source>Rotate 90</source>
        <translation>Rotate 90°</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="105"/>
        <source>Rotate 180</source>
        <translation>Rotate 180°</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="542"/>
        <source>Name: {}
Position: {}
Rotation: {}
Spiegelung: {}
Fixiert: {}</source>
        <translation>Name: {}
Position: {}
Rotation: {} °
Reflection: {}
Fixed: {}</translation>
    </message>
</context>
<context>
    <name>FactoryLayoutScene</name>
    <message>
        <location filename="factory_scene_layout.py" line="184"/>
        <source>&lt;h1&gt;Ansicht: </source>
        <translation>Ansicht: </translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="114"/>
        <source>&lt;h1&gt;Ansicht: Cell Info&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Zellen Info&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="128"/>
        <source>&lt;h1&gt;Ansicht: Standard&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;View: Standard&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="142"/>
        <source>&lt;h1&gt;Ansicht: Licht&lt;/h2&gt;</source>
        <translation>&lt;h1&gt;View: Lights&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="219"/>
        <source>&lt;h1&gt;Ansicht: Restrictive Area&apos;s&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;View: Restrictive areas&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="407"/>
        <source>Some areas {} are not large enough to accommodate all of the related facilities!</source>
        <translation>Some areas {} are not large enough to accommodate all of the related facilities!</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="420"/>
        <source>Some facilities {} are placed outside their area of use and will be deleted!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="233"/>
        <source>&lt;h1&gt;Ansicht: Noise matrix&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; View: Noise matrix &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="330"/>
        <source>&lt;h1&gt;Ansicht: Material flow (out)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; View: Material flow (outgoing) &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="357"/>
        <source>&lt;h1&gt;Ansicht: Communication flow (out)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; View: communication flow (outgoing) &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="340"/>
        <source>&lt;h1&gt;Ansicht: Material flow (in)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; View: Material flow (incoming) &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="367"/>
        <source>&lt;h1&gt;Ansicht: Communication flow (in)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; View: communication flow (incoming) &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="404"/>
        <source>Value error!</source>
        <translation>Value error!</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="418"/>
        <source>Some facilities will be deleted!</source>
        <translation>Some facilities will be deleted!</translation>
    </message>
</context>
<context>
    <name>FactorySetSizeDialog</name>
    <message>
        <location filename="factory_set_size_dialog.py" line="35"/>
        <source>With the right mouse button cells can be marked which will later be hidden.

White = in use
Black = not in use</source>
        <translation>With the right mouse button cells can be marked which will later be hidden.

White = in use
Black = not in use</translation>
    </message>
</context>
<context>
    <name>FactorySizeDialog</name>
    <message>
        <location filename="factory_size_dialog.py" line="56"/>
        <source>Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!</source>
        <translation>The size of the factory must be divisible by the cell size!</translation>
    </message>
</context>
<context>
    <name>FactoryTable</name>
    <message>
        <location filename="factory_table.py" line="166"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="117"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="118"/>
        <source>Department</source>
        <translation>Department</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="120"/>
        <source>Breite</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="123"/>
        <source>Bodentragkraft</source>
        <translation>Floor load capacity</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="124"/>
        <source>Deckentraglast</source>
        <translation>Ceiling load</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="125"/>
        <source>Wandpositionierung</source>
        <translation>Outer wall positioning</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="167"/>
        <source>Ruhebedarf</source>
        <translation>Noise sensitivity</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="169"/>
        <source>Lichtbedarf</source>
        <translation>Illuminance</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="172"/>
        <source>Temperatur</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="173"/>
        <source>Sauberkeit</source>
        <translation>Cleanliness</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="199"/>
        <source>Facility type unknown!</source>
        <translation>Facility type unknown!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="209"/>
        <source>Department unknown!</source>
        <translation>Department unknown!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="394"/>
        <source>Value must be in range {}-{}!</source>
        <translation>Value must be in range {}-{}!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="430"/>
        <source>Value must be greater equals 0!</source>
        <translation>Value must be greater equals 0!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="479"/>
        <source>Delete Facility</source>
        <translation>Delete Facility</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="512"/>
        <source>After the facility has been deleted, the generated layouts are no longer optimal layouts!</source>
        <translation>After the factory object has been deleted, the layouts created are no longer optimal!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="200"/>
        <source>Choose an existing facility.</source>
        <translation>Choose an existing facility.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="210"/>
        <source>Choose an existing Department.</source>
        <translation>Choose an existing Department.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="429"/>
        <source>Value error!</source>
        <translation>Value error!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="509"/>
        <source>Are you sure you want to delete the {} facility?!</source>
        <translation>Are you sure you want to delete the {} facility?!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="556"/>
        <source>Add Media</source>
        <translation>Add Media</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="556"/>
        <source>Enter the media name</source>
        <translation>Enter the media name</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="562"/>
        <source>This media already exists!</source>
        <translation>This media already exists!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="563"/>
        <source>Choose a different name for this media.</source>
        <translation>Choose a different name for this media.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="586"/>
        <source>Are you sure you want to delete the {} media?</source>
        <translation>Are you sure you want to delete the {} media?</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="589"/>
        <source>After the media has been deleted, the generated layouts are no longer optimal layouts!</source>
        <translation>After the media has been deleted, the generated layouts are no longer optimal layouts!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="119"/>
        <source>Flaeche</source>
        <translation>Area [m²]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="121"/>
        <source>Laenge</source>
        <translation>Length [m]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="122"/>
        <source>Hoehe</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="168"/>
        <source>Laermexposition</source>
        <translation>Noise emission [dB]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="170"/>
        <source>Erschuetterungsanregung</source>
        <translation>Vibration emission</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="171"/>
        <source>Erschuetterungssensitivitaet</source>
        <translation>Vibration sensitivity</translation>
    </message>
</context>
<context>
    <name>FactoryView</name>
    <message>
        <location filename="factory_view.py" line="94"/>
        <source>FacilityMenu</source>
        <translation>Facility menu</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="111"/>
        <source> vorhanden</source>
        <translation> available</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="112"/>
        <source>Kein </source>
        <translation>no </translation>
    </message>
    <message>
        <location filename="factory_view.py" line="126"/>
        <source>LightMenu</source>
        <translation>Illuminance Menu</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="205"/>
        <source>Geben Sie einen Wert ein</source>
        <translation>Enter a value</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="244"/>
        <source>Fixed Road</source>
        <translation>Fixed Road</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="252"/>
        <source>Remove Fixed Road</source>
        <translation>Remove Fixed Road</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="193"/>
        <source>Toggle In/Out</source>
        <translation>Toggle In/Out</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="228"/>
        <source>Value error!</source>
        <translation>Value error!</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="230"/>
        <source>Not all cells could be updated because the objects placed on them need a larger {} value than the entered value.</source>
        <translation>Not all cells could be updated because the objects placed on them need a larger {} value than the entered value.</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="304"/>
        <source>Facility can&apos;t be placed here!</source>
        <translation>The facility can&apos;t be placed here!</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="127"/>
        <source>Kein Licht [&lt; 100lx]</source>
        <translation>No light [≤100lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="128"/>
        <source>Sehr wenig Licht [100 - 200 lx]</source>
        <translation>Very low light [&gt;100 - ≤200 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="129"/>
        <source>Wenig Licht [200 - 300lx]</source>
        <translation>Low light [&gt;200 - ≤300lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="130"/>
        <source>Normales Licht [300 - 500 lx]</source>
        <translation>Normal light [&gt;300 - ≤500 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="131"/>
        <source>Helles Licht [500 - 750 lx]</source>
        <translation>Bright light [&gt;500 - ≤750 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="132"/>
        <source>Sehr helles Licht [750 - 1000 lx]</source>
        <translation>Very bright light [&gt;750 - ≤1000 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="133"/>
        <source>maximales Licht [1000 - 1500 lx]</source>
        <translation>Maximum bright light [&gt;1000 - ≤1500 lx]</translation>
    </message>
</context>
<context>
    <name>Factory_all_layouts</name>
    <message>
        <location filename="FactoryAllLayouts.ui" line="14"/>
        <source>All Layouts</source>
        <translation>All Layouts</translation>
    </message>
</context>
<context>
    <name>LayoutDetailDialog</name>
    <message>
        <location filename="LayoutDetailDialog.ui" line="14"/>
        <source>Layout Details</source>
        <translation>Layout Details</translation>
    </message>
</context>
<context>
    <name>License_dialog</name>
    <message>
        <location filename="LicenseDialog.ui" line="14"/>
        <source>License</source>
        <translation>License</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="main_view.py" line="662"/>
        <source>All facilities must be placed before evaluation!</source>
        <translation>All facilities must be placed before evaluation!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="221"/>
        <source>Do you want to load data from an Excel sheet?</source>
        <translation>Do you want to load data from an Excel sheet?</translation>
    </message>
    <message>
        <location filename="main_view.py" line="351"/>
        <source>The new factory size is smaller than the previous one!</source>
        <translation>The new factory size is smaller than the previous one!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="353"/>
        <source>Some facilities may get deleted.
All previously optimised layouts will get deleted!</source>
        <translation>Some facilities may get deleted.
All previously optimized layouts will get deleted!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="367"/>
        <source>The new factory size differs from the the previous one!</source>
        <translation>The new factory size differs from the previous one!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="368"/>
        <source>All previously optimised layouts will get deleted!</source>
        <translation>All previously optimized layouts will get deleted!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="542"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="main_view.py" line="543"/>
        <source>Facility {} already exist.</source>
        <translation>Facility {} already exist.</translation>
    </message>
    <message>
        <location filename="main_view.py" line="545"/>
        <source>Change the name of {} or it won&apos;t get imported!
New Name:</source>
        <translation>Change the name of {} or it won&apos;t get imported!
New Name:</translation>
    </message>
    <message>
        <location filename="main_view.py" line="632"/>
        <source>An error occured during optimization!</source>
        <translation>An error occurred during optimization!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="661"/>
        <source>Not enough information!</source>
        <translation>Not enough information!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="805"/>
        <source>Do you really want to reset the current layout?</source>
        <translation>Möchten Sie das aktuelle Layout wirklich zurücksetzen?</translation>
    </message>
    <message>
        <location filename="main_view.py" line="806"/>
        <source>All facilties except the fixed ones, will be deleted!</source>
        <translation>All facilities except the fixed ones will be deleted!</translation>
    </message>
</context>
<context>
    <name>OptimizeDialog</name>
    <message>
        <location filename="optimize_dialog.py" line="75"/>
        <source> (Available cores: </source>
        <translation> (Available cores: </translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="740"/>
        <source>All facilities must be placed for this opening type!</source>
        <translation>All facilities must be placed for this opening type!</translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="1066"/>
        <source>You have already optimized the factory. Any further optimization would lose all of your progress.</source>
        <translation>You have already optimized the factory. Any further optimization would lose all of your progress.</translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="1067"/>
        <source>Choose &apos;Ist-Layout&apos; to improve your current layout!</source>
        <translation>Choose &apos;Ist-Layout&apos; to improve your current layout!</translation>
    </message>
</context>
<context>
    <name>PrePairwiseComparisonDialog</name>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Choose your weighting criteria&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Choose your target criteria&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="31"/>
        <source>Criteria</source>
        <translation>objective</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="58"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="65"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="89"/>
        <source>Non criteria</source>
        <translation>non objective</translation>
    </message>
</context>
<context>
    <name>StartDialog</name>
    <message>
        <location filename="start_dialog.py" line="28"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="start_dialog.py" line="29"/>
        <source>English</source>
        <translation>English</translation>
    </message>
</context>
<context>
    <name>WeightDialog</name>
    <message>
        <location filename="weight_dialog.py" line="244"/>
        <source>Weighting error!</source>
        <translation>Weighting error!</translation>
    </message>
    <message>
        <location filename="weight_dialog.py" line="246"/>
        <source>The sum of the weights must be 1!
 Actual sum: {}</source>
        <translation>The sum of the weights must be 100!
 Actual sum: {}</translation>
    </message>
</context>
<context>
    <name>compare_dialog</name>
    <message>
        <location filename="CompareDialog.ui" line="14"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="CompareDialog.ui" line="20"/>
        <source>&lt;h1&gt;Results&lt;/h1&gt;
</source>
        <translation>&lt;h1&gt;Results&lt;/h1&gt;
</translation>
    </message>
</context>
<context>
    <name>evaluation_table_dialog</name>
    <message>
        <location filename="EvaluationTableDialog.ui" line="14"/>
        <source>Evaluation</source>
        <translation>Evaluation</translation>
    </message>
</context>
<context>
    <name>facility_dialog</name>
    <message>
        <location filename="FacilityDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="20"/>
        <source>Facility Name</source>
        <translation>Facility Name</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="38"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="61"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="35"/>
        <source>Switch to the previous facility.</source>
        <translation>Switch to the previous facility.</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="58"/>
        <source>Switch to the next facility.</source>
        <translation>Switch to the next facility.</translation>
    </message>
</context>
<context>
    <name>factory_size_dialog</name>
    <message>
        <location filename="FactorySizeDialog.ui" line="14"/>
        <source>Factory size</source>
        <translation>Factory size</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="22"/>
        <source>Edge length of the unit cells [m]</source>
        <translation>Edge length of the unit cells [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="65"/>
        <source>Factory width [m]</source>
        <translation>Factory width [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="98"/>
        <source>Factory height [m]</source>
        <translation>Factory length [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="120"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>help_dialog</name>
    <message>
        <location filename="HelpDialog.ui" line="14"/>
        <source>Guide</source>
        <translation type="obsolete">Guide</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <location filename="MeFaPMainView.ui" line="14"/>
        <source>MeFaP</source>
        <translation>MeFaP</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;View: Standard&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;View: Standard&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="113"/>
        <source>Evaluate</source>
        <translation>Evaluate</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="123"/>
        <source>Optimize</source>
        <translation>Optimize</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="145"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="158"/>
        <source>Factory data</source>
        <translation>Factory data</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="171"/>
        <source>Views</source>
        <translation>Views</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="189"/>
        <source>Parameter</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="198"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="220"/>
        <source>Control parameters</source>
        <translation>Control parameters</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="228"/>
        <source>New layout</source>
        <translation>New Layout</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="239"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="247"/>
        <source>Save layout as</source>
        <translation>Save layout as</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="255"/>
        <source>Load layout ...</source>
        <translation>Load layout ...</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="266"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="274"/>
        <source>Machine park</source>
        <translation>Facility list</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="339"/>
        <source>Material flow</source>
        <translation>Material flow</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="331"/>
        <source>Communication flow</source>
        <translation>Communication flow</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="295"/>
        <source>Cell properties</source>
        <translation>Sink and Source</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="300"/>
        <source>Lighting</source>
        <translation>Lighting</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="305"/>
        <source>Elevation profile</source>
        <translation>Ceiling profile</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="310"/>
        <source>Ceiling load</source>
        <translation>Ceiling load</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="315"/>
        <source>Weighting</source>
        <translation>Weighting</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="320"/>
        <source>Save layout</source>
        <translation>Save layout</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="344"/>
        <source>Floor load</source>
        <translation>Floor load</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="349"/>
        <source>Restrictive Area&apos;s</source>
        <translation>Restrictive Area&apos;s</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="354"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="362"/>
        <source>Layout comparison</source>
        <translation>Layout comparison</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="370"/>
        <source>Extended Settings</source>
        <translation>Extended Settings</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="378"/>
        <source>Media requirement</source>
        <translation>Media requirement</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="205"/>
        <source>Erweitert</source>
        <translation>Extended</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="234"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="261"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="269"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="280"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="323"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="357"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="365"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="373"/>
        <source>Ctrl+,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="383"/>
        <source>Environmental factors</source>
        <translation>Environmental factors</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="388"/>
        <source>All Layouts</source>
        <translation>All Layouts</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="33"/>
        <source>Hi, I am a Logo.</source>
        <translation>Hi, I am a Logo.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="49"/>
        <source>This shows which aspects of the layout are currently being displayed to you.</source>
        <translation>This shows which aspects of the layout are currently being displayed to you.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="88"/>
        <source>Recalculates all routes between the facilities.</source>
        <translation>Recalculates all routes between the facilities.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="91"/>
        <source>Recalculate paths</source>
        <translation>Recalculate paths</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="98"/>
        <source>Resets the current layout.</source>
        <translation>Resets the current layout.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="101"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="110"/>
        <source>When all the facilities have been placed, an evaluation of the current layout can be carried out here.</source>
        <translation>When all the facilities have been placed, an evaluation of the current layout can be carried out here.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="120"/>
        <source>The optimization of the current layout can be started here.</source>
        <translation>The optimization of the current layout can be started here.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="393"/>
        <source>Noise</source>
        <translation>Noise</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="398"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="403"/>
        <source>Factory size</source>
        <translation>Factory size</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="408"/>
        <source>Excluded areas</source>
        <translation>Excluded areas</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="413"/>
        <source>Import excel</source>
        <translation>Import excel</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="223"/>
        <source>Settings on the control parameters can be made here.</source>
        <translation>Settings on the control parameters can be made here.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="231"/>
        <source>A new factory can be created here.</source>
        <translation>A new factory can be created here.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="242"/>
        <source>Here you switch to the standard view of the factory.</source>
        <translation>Here you switch to the standard view of the factory.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="250"/>
        <source>Save factory</source>
        <translation>Save factory</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="258"/>
        <source>Load fayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="277"/>
        <source>Here you can see and adjust all information about the facilities.</source>
        <translation>Here you can see and adjust all information about the facilities.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="418"/>
        <source>Anleitung</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="423"/>
        <source>License</source>
        <translation>License</translation>
    </message>
</context>
<context>
    <name>optimize_dialog</name>
    <message>
        <location filename="OptimizeDialog.ui" line="14"/>
        <source>Optimization settings</source>
        <translation>Optimization settings</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Metaheuristics
&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Metaheuristics
&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="53"/>
        <source>Tabu Search (TS)</source>
        <translation>Tabu Search (TS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="58"/>
        <source>Simulated Annealing (SA)</source>
        <translation>Simulated Annealing (SA)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="63"/>
        <source>Genetic Algorithm (GA)</source>
        <translation>Genetic Algorithm (GA)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="68"/>
        <source>GA_TS</source>
        <translation>GA &amp; TS</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="73"/>
        <source>GA_SA</source>
        <translation>GA &amp; SA</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Tabu Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Tabu Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="583"/>
        <source>Length of tabu list</source>
        <translation>Tabu list length</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="590"/>
        <source>Iteration</source>
        <translation>Iterations</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="177"/>
        <source>
&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Simulated Annealing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>
&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Simulated Annealing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="197"/>
        <source>Starting temperatur)</source>
        <translation>Starting temperature</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="851"/>
        <source>Cooling rate</source>
        <translation>Cooling rate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="868"/>
        <source>Increment</source>
        <translation>Increment</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="260"/>
        <source>Cooling type</source>
        <translation>Cooling type</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="890"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="895"/>
        <source>Logarithm fast</source>
        <translation>Logarithm fast</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="900"/>
        <source>Logarithm slow</source>
        <translation>Logarithm slow</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="298"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Genetic Algorithm&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Genetic Algorithm&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="727"/>
        <source>Population size</source>
        <translation>Population size</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="741"/>
        <source>Number of generations</source>
        <translation>Generations</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="755"/>
        <source>Crossover rate</source>
        <translation>Crossover rate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="762"/>
        <source>Number of elite parents</source>
        <translation>Number of elite parents</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="776"/>
        <source>Mutation rate</source>
        <translation>Mutation rate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="783"/>
        <source>Mutation max shift step</source>
        <translation>Mutation: max. shift step</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="797"/>
        <source>Mutation max exchange</source>
        <translation>Mutation:  max. number exchanged facilities</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="811"/>
        <source>Mutation type</source>
        <translation>Mutation type</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="819"/>
        <source>Shift mutation</source>
        <translation>Mutation: Shift</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="824"/>
        <source>Exchange mutation</source>
        <translation>Mutation: Exchange</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="829"/>
        <source>Shift &amp; Exchange mutation</source>
        <translation>Mutation: Shift &amp; Exchange</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="514"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_TS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_TS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="908"/>
        <source>Number take over variants</source>
        <translation>Number take over variants</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="713"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_SA&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_SA&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="837"/>
        <source>Starting Temperature</source>
        <translation>Starting temperature</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="882"/>
        <source>Cooling Type</source>
        <translation>Cooling Type</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="979"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Opening procedure&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Opening procedure&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="988"/>
        <source>Random positioning and selection</source>
        <translation>Random positioning and selection</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="998"/>
        <source>Random positioning with factory objects sorted in descending order</source>
        <translation>Random positioning with facilities sorted in descending order by area</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1005"/>
        <source>Schmigalla</source>
        <translation>Schmigalla</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1012"/>
        <source>Actual layout</source>
        <translation>Current layout</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1042"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Neighborhood search type&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Neighborhood search type&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1051"/>
        <source>Local Reallocation Search</source>
        <translation>Local Reallocation Search (LRS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1074"/>
        <source>Free Area Search</source>
        <translation>Open Area Search (OAS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1094"/>
        <source>Multiple Elements Exchange Search</source>
        <translation>Multiple Elements Exchange Search (MEES)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Optional restrictions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Optional restrictions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1131"/>
        <source>Rotatability</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1141"/>
        <source>Mirror</source>
        <translation>Mirror</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Other&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Other&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1162"/>
        <source>Pathplanning</source>
        <translation>Path planning</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1182"/>
        <source>Multicore</source>
        <translation>Multicore</translation>
    </message>
</context>
<context>
    <name>pairwise_comparison_dialog</name>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="14"/>
        <source>Pairwise Comparison</source>
        <translation>Pairwise Comparison</translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Which one is more important?&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Which one is more important?&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="64"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="78"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>set_size_dialog</name>
    <message>
        <location filename="FactorySetSize.ui" line="14"/>
        <source>Set size</source>
        <translation>Set size</translation>
    </message>
</context>
<context>
    <name>start_dialog</name>
    <message>
        <location filename="StartDialog.ui" line="26"/>
        <source>MeFaP - Start</source>
        <translation>MeFaP - Start</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="35"/>
        <source>New factory layout</source>
        <translation>New factory layout</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="55"/>
        <source>Welcome to MeFaP</source>
        <translation>Welcome to MeFaP</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="68"/>
        <source>Load factory layout</source>
        <translation>Load factory layout</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="32"/>
        <source>A new factory can be created here</source>
        <translation>A new factory can be created here</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="65"/>
        <source>If you already created a factory you can reload it here.</source>
        <translation>If you already created a factory you can reload it here.</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="137"/>
        <source>Here you can change between multiple languages.</source>
        <translation>Here you can change between multiple languages.</translation>
    </message>
</context>
<context>
    <name>table_dialog</name>
    <message>
        <location filename="TableDialog.ui" line="14"/>
        <source>Factory information</source>
        <translation>Factory information</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="24"/>
        <source>Material flow</source>
        <translation>Material flow</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="34"/>
        <source>Communication flow</source>
        <translation>Communication flow</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="44"/>
        <source>Machine park</source>
        <translation>Facility informations</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="54"/>
        <source>Medien</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="64"/>
        <source>Umgebungsfaktoren</source>
        <translation>Environmental factors</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="88"/>
        <source>Add Facility</source>
        <translation>Add Facility</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="101"/>
        <source>Delete Facility</source>
        <translation>Delete Facility</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="111"/>
        <source>Add Media</source>
        <translation>Add Media</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="121"/>
        <source>Delete Media</source>
        <translation>Delete Media</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="85"/>
        <source>A new facility can be added here.</source>
        <translation>A new facility can be added here.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="98"/>
        <source>All marked facilities can be deleted here.</source>
        <translation>All marked facilities can be deleted here.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="108"/>
        <source>A new medium can be added to the factory and named here.</source>
        <translation>A new medium can be added to the factory and named here.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="118"/>
        <source>All marked media can be deleted here.</source>
        <translation>All marked media can be deleted here.</translation>
    </message>
</context>
<context>
    <name>weight_dialog</name>
    <message>
        <location filename="WeightDialog.ui" line="14"/>
        <source>Weighting</source>
        <translation>Weighting</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Set weighting&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Set weighting&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Versatility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Versatility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="54"/>
        <source>Media availability</source>
        <translation>Media availability</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="81"/>
        <source>Media compatibility</source>
        <translation>Media compatibility</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="123"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Material flow and logistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Material flow and logistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="135"/>
        <source>Material flow length</source>
        <translation>Material flow length</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="162"/>
        <source>No overlap</source>
        <translation>No overlap</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="189"/>
        <source>Continuity</source>
        <translation>Continuity</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="216"/>
        <source>Land use</source>
        <translation>Land use</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="258"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Environmental influences&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Environmental influences&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="270"/>
        <source>Lighting</source>
        <translation>Lighting</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="297"/>
        <source>Quiet</source>
        <translation>Quiet</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="324"/>
        <source>Vibration</source>
        <translation>Vibration</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="351"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="378"/>
        <source>Cleanliness</source>
        <translation>Cleanliness</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="420"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Communication&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Communication&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="432"/>
        <source>Direct communication</source>
        <translation>Direkte Kommunikation</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="459"/>
        <source>Formal communication</source>
        <translation>Formal communication</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="494"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Total:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Total:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="504"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="516"/>
        <source>Balance of all criteria</source>
        <translation>Balance of all criteria</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="523"/>
        <source>Pairwise comparison</source>
        <translation>Pairwise comparison</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="532"/>
        <source>Evaluate</source>
        <translation>Evaluate</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="542"/>
        <source>Abbrechen</source>
        <translation>Abort</translation>
    </message>
</context>
</TS>
