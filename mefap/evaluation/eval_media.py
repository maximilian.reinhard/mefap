from mefap.evaluation.pre_calculation import pre_calc_check_rotation


def eval_media_compatibility(factory):
    """evaluate equipment compatibility"""
    medium_compatibility_count = 0
    for medium in factory.media_availability:  # range(0, len(factory.media_availability), 1):
        for row in medium.availability_layout:  # factory.media_compatibility[medium].availability_layout:
            for cell_value in row:
                if cell_value:
                    medium_compatibility_count += 1

    media_compatibility_rating = medium_compatibility_count / (len(factory.media_availability) * factory.layout.size)

    # write data
    factory.quamfab.media_compatibility[0] = medium_compatibility_count
    factory.quamfab.media_compatibility[1] = media_compatibility_rating


def eval_media_availability(factory):
    """ eval equipment availability """
    medium_avail_count = 0
    # get medium requirement
    for medium in range(0, len(factory.media_requirements), 1):
        # get facility with medium requirement
        for facility in range(0, len(factory.media_requirements[medium].requirements), 1):
            if factory.media_requirements[medium].requirements[facility]:
                # check position of facility based on rotation
                y_facility_width, x_facility_width = pre_calc_check_rotation(factory, facility)
                # reset counter
                medium_avail_facility_count = 0
                # search over layout/facility
                for y_index in range(0, y_facility_width, 1):
                    for x_index in range(0, x_facility_width, 1):
                        if factory.media_availability[medium].availability_layout[
                                y_index + factory.facility_list[facility].curr_position[0],
                                x_index + factory.facility_list[facility].curr_position[1]]:
                            medium_avail_facility_count += 1
                
                if medium_avail_facility_count > 0:
                    if medium_avail_facility_count / factory.facility_list[facility].cell_num >= \
                            factory.parameter["media_avail_factor"]:
                        medium_avail_count += 1

    # write data
    factory.quamfab.media_availability[0] = medium_avail_count
    factory.quamfab.media_availability[1] = None  # media_availability_rating
