import math
import numpy as np
from scipy.spatial import distance

from mefap.factory.factory_model import FactoryModel
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_free_field
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_semi_diffus
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_flat_room

def calc_noise_propagation_wall(factory: FactoryModel, noise_area):
    """

    """
    layout = np.zeros(shape=(factory.columns, factory.rows))

    # get corners of wall
    corner_list = factory.noise_area.get_noise_wall_corners_by_index(noise_area)
    # define walls
    walls = [["N", (corner_list[0], corner_list[1])], ["E", (corner_list[1], corner_list[2])],
             ["S", (corner_list[3], corner_list[2])], ["W", (corner_list[0], corner_list[3])]]

    # check if noise_wall on factory_wall
    for wall in walls:
        if wall[1][0][0] == wall[1][1][0]:
            # x-direction
            centre = [wall[1][0][0], wall[1][0][1] + 1]
        else:
            # y-direction
            centre = [wall[1][0][0] + 1, wall[1][0][1]]

        if centre[0] + 1 > factory.layout.shape[0] - 1 or \
                centre[0] - 1 < 0 or \
                centre[1] + 1 > factory.layout.shape[1] - 1 or \
                centre[1] - 1 < 0:
            wall.append("delete")

    # delete noise_walls that overlap with factory_walls
    for wall in range(len(walls) - 1, -1, -1):
        if len(walls[wall]) > 2:
            walls.pop(wall)

    # get centre of wall
    for wall in walls:
        wall.extend(get_centre_of_wall(wall[1][0], wall[1][1]))

    # get area_amount (m²) of wall
    for wall in walls:
        height = factory.restrictions[2].matrix[wall[1][0]]
        wall.append((wall[3] * factory.cell_size) * height)

    # calc L_I (mean noise level before wall (inside & outside))
    for wall in walls:
        noise_sum_inside = 0
        noise_sum_outside = 0
        amount = 0

        if wall[0] == "N" or wall[0] == "S":
            if wall[0] == "N":
                outside_add = -1  # north
            else:
                outside_add = 1  # south

            for x in range(wall[1][0][1], wall[1][1][1], 1):
                noise_sum_inside += pow(10, (factory.noise_matrix[wall[1][0][0], x] / 10))
                noise_sum_outside += pow(10, (factory.noise_matrix[wall[1][0][0] + outside_add, x] / 10))
                amount += 1

        else:
            if wall[0] == "W":
                outside_add = -1  # west
            else:
                outside_add = 1  # east

            for y in range(wall[1][0][0], wall[1][1][0], 1):
                noise_sum_inside += pow(10, (factory.noise_matrix[y, wall[1][0][1]] / 10))
                noise_sum_outside += pow(10, (factory.noise_matrix[y, wall[1][0][1] + outside_add] / 10))
                amount += 1

        # calc L_mean
        wall.append(10 * math.log10(noise_sum_inside / amount))
        wall.append(10 * math.log10(noise_sum_outside / amount))

    # iterate over all cells in layout
    for immission_cell_y in range(0, layout.shape[0], 1):
        for immission_cell_x in range(0, layout.shape[1], 1):

            corner_walls = []

            # inside
            if factory.noise_area.get_noise_area_id(immission_cell_x, immission_cell_y) == noise_area:
                # iterate over walls, cause inside all walls are relevant
                L_sum = 0
                for wall in walls:
                    if wall[0] == "N":
                        centre = [wall[1][0][0] - 1, immission_cell_x]
                    elif wall[0] == "E":
                        centre = centre = [immission_cell_y, wall[1][0][1] + 1]
                    elif wall[0] == "S":
                        centre = [wall[1][0][0] + 1, immission_cell_x]
                    elif wall[0] == "W":
                        centre = [immission_cell_y, wall[1][0][1] - 1]
                    else:
                        breakpoint()

                    L = calc_resulting_noise_level(factory, wall, centre, corner_walls, immission_cell_y,
                                                   immission_cell_x, inside=True)

                    L_sum += pow(10, (L / 10))
                else:
                    L = (10 * math.log10(L_sum))

            # outside
            else:
                # check angle / position of noise_walls and immission_cell to identify relevant wall
                for wall in walls:
                    if wall[1][0][0] <= immission_cell_y <= wall[1][1][0] and immission_cell_x < corner_list[0][1] and \
                            wall[0] == "W":
                        # west
                        centre = [immission_cell_y, wall[1][0][1]]
                        break
                    elif wall[1][0][0] <= immission_cell_y <= wall[1][1][0] and immission_cell_x > corner_list[1][1] and \
                            wall[0] == "E":
                        # east
                        centre = centre = [immission_cell_y, wall[1][0][1]]
                        break
                    elif wall[1][0][1] <= immission_cell_x <= wall[1][1][1] and immission_cell_y < corner_list[0][0] and \
                            wall[0] == "N":
                        # north
                        centre = [wall[1][0][0], immission_cell_x]
                        break
                    elif wall[1][0][1] <= immission_cell_x <= wall[1][1][1] and immission_cell_y > corner_list[3][0] and \
                            wall[0] == "S":
                        # south
                        centre = [wall[1][0][0], immission_cell_x]
                        break
                else:
                    # if immission_cell is in range of two noise_walls
                    for corner in corner_list:
                        if immission_cell_y < corner[0] and immission_cell_x < corner[1] and corner == corner_list[0]:
                            # north / west
                            centre = corner
                            for wall in walls:
                                if wall[0] == "N" or wall[0] == "W":
                                    corner_walls.append(wall)
                            # y_direction = 1, x_direction = 1
                            corner_walls.extend([1, 1])
                            break
                        elif immission_cell_y < corner[0] and immission_cell_x > corner[1] and corner == corner_list[1]:
                            # north / east
                            centre = corner
                            for wall in walls:
                                if wall[0] == "N" or wall[0] == "E":
                                    corner_walls.append(wall)
                            # y_direction = 1, x_direction = -1
                            corner_walls.extend([1, -1])
                            break
                        elif immission_cell_y > corner[0] and immission_cell_x < corner[1] and corner == corner_list[3]:
                            # south / west
                            centre = corner
                            for wall in walls:
                                if wall[0] == "S" or wall[0] == "W":
                                    corner_walls.append(wall)
                            # y_direction = -1, x_direction = 1
                            corner_walls.extend([-1, 1])
                            break
                        elif immission_cell_y > corner[0] and immission_cell_x > corner[1] and corner == corner_list[2]:
                            # south / east
                            centre = corner
                            for wall in walls:
                                if wall[0] == "S" or wall[0] == "E":
                                    corner_walls.append(wall)
                            # y_direction = -1, x_direction = -1
                            corner_walls.extend([-1, -1])
                            break
                    else:
                        breakpoint()

                L = calc_resulting_noise_level(factory, wall, centre, corner_walls,
                                               immission_cell_y, immission_cell_x, inside=False)

            layout[immission_cell_y, immission_cell_x] = L

    return layout


def calc_resulting_noise_level(factory, wall, centre, corner_walls, immission_cell_y, immission_cell_x, inside):
    """

    """

    # calc euclidean distance between wall and immission_cell
    euclidean_distance = distance.euclidean([centre[0], centre[1]],
                                            [immission_cell_y,
                                             immission_cell_x]) * factory.cell_size

    # calc n (amount of virtual sound sources)
    if not corner_walls:
        n = (2 * math.sqrt(wall[4])) / euclidean_distance
        if n > 10:
            n = 10

        if inside:
            L_I = wall[6]
        else:
            L_I = wall[5]

        # calc L_wt
        L_wt = L_I - factory.parameter["noise_factory_wall_isolation"] + 10 * math.log10(wall[4] / 1) + -3
    else:
        n = (2 * math.sqrt(corner_walls[0][4] + corner_walls[1][4])) / euclidean_distance
        if n > 10:
            n = 10

        # calc L_wt for corner walls , based on mean of both walls
        L_I = 10 * math.log10((pow(10, (corner_walls[0][5] / 10)) + pow(10, (corner_walls[1][5] / 10))) / 2)

        L_wt = L_I - factory.parameter["noise_factory_wall_isolation"] + 10 * math.log10(
            (corner_walls[0][4] + corner_walls[1][4]) / 1) + -3

    # calc L_w
    L_w = L_wt - 10 * math.log10(n)

    # set position of noise source
    if n <= 1:
        if factory.parameter["noise_semi_diffuse"]:
            L = calc_noise_propagation_semi_diffus(factory.parameter, L_w, euclidean_distance,
                                                   int(factory.noise_area.get_noise_area_id(immission_cell_x,
                                                                                            immission_cell_y)))
        elif factory.parameter["noise_flat_room"]:
            L = calc_noise_propagation_flat_room(factory.parameter, L_w, euclidean_distance,
                                                 factory.restrictions[2].matrix[immission_cell_y, immission_cell_x])
        else:
            L = calc_noise_propagation_free_field(factory.parameter, L_w, euclidean_distance)

        if L < 0:
            L = 0

    else:
        # dived wall in geometric samples
        n_rounded = math.ceil(n)
        L_w_cells = []

        if not corner_walls:
            # all sound sources on one wall
            if wall[0] == "E" or wall[0] == "W":
                for n_count in range(0, n_rounded, 1):
                    if centre[0] <= wall[2][0]:
                        L_w_cells.append([wall[1][0][0] + math.floor((wall[3] / n_rounded) * n_count), centre[1]])
                    else:
                        L_w_cells.append([wall[1][1][0] - math.floor((wall[3] / n_rounded) * n_count), centre[1]])
            elif wall[0] == "N" or wall[0] == "S":
                for n_count in range(0, n_rounded, 1):
                    if centre[1] <= wall[2][1]:
                        L_w_cells.append([centre[0], wall[1][0][1] + math.floor((wall[3] / n_rounded) * n_count)])
                    else:
                        L_w_cells.append([centre[0], wall[1][1][1] - math.floor((wall[3] / n_rounded) * n_count)])
        else:
            # sound sources on two walls
            helper = 0
            y_direction = corner_walls[2]
            x_direction = corner_walls[3]

            for n_count in range(0, math.ceil(n_rounded / 2), 1):
                if helper == n_rounded:
                    break
                for wall in corner_walls:
                    if helper == n_rounded:
                        break
                    if type(wall) == int:
                        continue

                    # choose correct wall
                    if wall[0] == "E" or wall[0] == "W":
                        L_w_cells.append(
                            [centre[0] + (math.floor((wall[3] / (n_rounded / 2)) * n_count)) * y_direction,
                             centre[1]])
                        helper += 1
                    elif wall[0] == "N" or wall[0] == "S":
                        L_w_cells.append([centre[0],
                                          centre[1] + (
                                              math.floor((wall[3] / (n_rounded / 2)) * n_count)) * x_direction])
                        helper += 1

        # calc L for all L_w
        for wall_cell in L_w_cells:
            euclidean_distance = distance.euclidean([wall_cell[0], wall_cell[1]],
                                                    [immission_cell_y,
                                                     immission_cell_x]) * factory.cell_size

            if factory.parameter["noise_semi_diffuse"]:
                L = calc_noise_propagation_semi_diffus(factory.parameter, L_w, euclidean_distance,
                                                       int(factory.noise_area.get_noise_area_id(immission_cell_x,
                                                                                                immission_cell_y)))
            elif factory.parameter["noise_flat_room"]:
                L = calc_noise_propagation_flat_room(factory.parameter, L_w, euclidean_distance,
                                                     factory.restrictions[2].matrix[immission_cell_y, immission_cell_x])
            else:
                L = calc_noise_propagation_free_field(factory.parameter, L_w, euclidean_distance)

            if L < 0:
                L = 0

            wall_cell.append(L)

        # add up L
        L = 0
        for wall_cell in L_w_cells:
            L = L + pow(10, (wall_cell[2] / 10))
        else:
            L = (10 * math.log10(L))

    return L


def get_centre_of_wall(cell_one, cell_two):
    """
    find centre between two points / cells
    """
    if cell_one[0] == cell_two[0]:
        # check if two greater then second
        if cell_one[1] < cell_two[1]:
            diff = cell_two[1] - cell_one[1] + 1
            centre = [cell_one[0], cell_one[1] + round(diff / 2)]
        else:
            diff = cell_one[1] - cell_two[1] + 1
            centre = [cell_one[0], cell_two[1] + round(diff / 2)]
    elif cell_one[1] == cell_two[1]:
        # check if two greater then second
        if cell_one[0] < cell_two[0]:
            diff = cell_two[0] - cell_one[0] + 1
            centre = [cell_one[0] + round(diff / 2), cell_one[1]]
        else:
            diff = cell_one[0] - cell_two[0] + 1
            centre = [cell_two[0] + round(diff / 2), cell_one[1]]
    else:
        # cells must be on direction (x or y)
        breakpoint()

    return centre, diff