import openpyxl
from mefap.factory.util.metaheuristik_type import MetaheuristicType
from mefap.factory.util.opening_type import OpeningType
from mefap.factory.util.neighborhood_search_type import NeighborhoodSearchType
from mefap.factory.util.sa_cooling_type import SimulatedAnnealingCoolingType
from mefap.factory.util.mutation_type import MutationType
from mefap.factory.weighting import Weighting
from mefap.factory.optimization_doe import Optimization_DOE


def parse_xl_parameter(file_path, factory, pc_name, column_index):
    # get xl_sheet
    sheet_opt_para, sheet_opt_weights, sheet_opt_doepara = open_excel_file_parameter(file_path, pc_name)

    #################################################################################
    # read optimizaion parameter ##
    # set start cell
    xl_table_start_row = 3
    xl_table_start_column = 2
    # get last row
    xl_table_end_row = sheet_opt_para.max_row + 1  # 26
    for i in range(xl_table_start_row, xl_table_end_row, 1):
        if sheet_opt_para.cell(i, 1).value is None:
            xl_table_end_row = i + 1
            break

    # get last column
    xl_table_end_col = sheet_opt_para.max_column + 1
    for i in range(2, xl_table_end_col, 1):
        if sheet_opt_para.cell(1, i).value is None:
            xl_table_end_col = i + 1
            break

    # get right column
    # column_index += 1
    for column in range(xl_table_start_column, xl_table_end_col, 1):
        column_name = sheet_opt_para.cell(1, column).value
        if column_name == pc_name:
            column_index = column
            break

    # write parameter into factory.optimization
    for index_row in range(xl_table_start_row, xl_table_end_row, 1):

        # get parameter name
        para_name = sheet_opt_para.cell(index_row, 1).value

        if para_name == "Weights":
            xl_table_start_row = index_row + 1
            break
        elif para_name == "Metaheuristic_Parameter" or para_name == "Neighborhood_Search_Parameter":
            # jump over headings
            continue

        # get parameter value
        para_value = sheet_opt_para.cell(index_row, column_index).value
        """
        # convert to integer if possible
        if not isinstance(para_value, str):
            if para_value.is_integer():
                para_value = int(para_value)
        """
        # set parameter
        if para_name == "metaheuristic_type":
            factory.optimization.metaheuristic_type: MetaheuristicType = MetaheuristicType(para_value)
        elif para_name == "opening":
            factory.optimization.opening: OpeningType = OpeningType(para_value)
        elif para_name == "cooling_type":
            factory.optimization.cooling_type: SimulatedAnnealingCoolingType = SimulatedAnnealingCoolingType(para_value)
        elif para_name == "nhs_methods":
            if isinstance(para_value, int):
                nhs_tuple = (para_value,)
            else:
                nhs_tuple = tuple(map(int, para_value.split(';')))
            factory.optimization.nhs_methods = []
            if 0 in nhs_tuple:
                factory.optimization.nhs_methods.append(NeighborhoodSearchType.LocalReallocationSearch)
            if 1 in nhs_tuple:
                factory.optimization.nhs_methods.append(NeighborhoodSearchType.FreeAreaSearch)
            if 2 in nhs_tuple:
                factory.optimization.nhs_methods.append(NeighborhoodSearchType.MultipleElementsExchangeSearch)
        elif para_name == "mutation_type":
            if isinstance(para_value, int):
                mutation_tuple = (para_value,)
            else:
                mutation_tuple = tuple(map(int, para_value.split(';')))
            factory.optimization.mutation_type = []
            if 0 in mutation_tuple:
                factory.optimization.mutation_type.append(MutationType.SHIFT_MUTATION)
            if 1 in mutation_tuple:
                factory.optimization.mutation_type.append(MutationType.EXCHANGE_MUTATION)
        elif para_name == "mees_facility_range":
            factory.optimization.mees_facility_range = tuple(map(int, para_value.split(';')))
        elif para_name == "rotation" or para_name == "mirror":
            if para_value == 0:
                setattr(factory.optimization, para_name, False)
            else:
                setattr(factory.optimization, para_name, True)
        elif para_name == "number_of_experiments":
            factory.parameter.update({'number_of_experiments': para_value})
        else:
            setattr(factory.optimization, para_name, para_value)

    # write weights into factory.weighting
    for index_row in range(xl_table_start_row, xl_table_end_row, 1):
        # get parameter
        para_name = sheet_opt_para.cell(index_row, 1).value
        para_value = sheet_opt_para.cell(index_row, column_index).value
        # set parameter
        setattr(factory.weighting, para_name, para_value)

    #################################################################################
    # read weights ##
    xl_table_start_row = 2
    xl_table_start_column = 2
    # get last row
    xl_table_end_row = sheet_opt_weights.max_row + 1  # 26
    for i in range(xl_table_start_row, xl_table_end_row, 1):
        if sheet_opt_weights.cell(i, 1).value is None:
            xl_table_end_row = i + 1
            break

    # get last column
    xl_table_end_col = sheet_opt_weights.max_column + 1
    for i in range(2, xl_table_end_col, 1):
        if sheet_opt_weights.cell(2, i).value is None:
            xl_table_end_col = i + 1
            break

    # add attribut to factory
    factory.weight_list = Weighting()

    for index_row in range(xl_table_start_row, xl_table_end_row, 1):
        # get parameter
        para_name = sheet_opt_weights.cell(index_row, 1).value
        para_values = []

        # iterate over columns
        for index_column in range(xl_table_start_column, xl_table_end_col, 1):
            if not sheet_opt_weights.cell(index_row, index_column).value is None:
                para_values.append(sheet_opt_weights.cell(index_row, index_column).value)
            else:
                break

        # set parameter
        setattr(factory.weight_list, para_name, para_values)

    #################################################################################
    # read doe parameter ##
    xl_table_start_row = 2
    xl_table_start_column = 2
    # get last row
    xl_table_end_row = sheet_opt_doepara.max_row + 1  # 26
    for i in range(xl_table_start_row, xl_table_end_row, 1):
        if sheet_opt_doepara.cell(i, 1).value is None:
            xl_table_end_row = i + 1
            break

    # get last column
    xl_table_end_col = sheet_opt_doepara.max_column + 1
    for i in range(2, xl_table_end_col, 1):
        if sheet_opt_doepara.cell(2, i).value is None:
            xl_table_end_col = i + 1
            break

    # add attribut to factory
    factory.doe_list = Optimization_DOE()

    for index_row in range(xl_table_start_row, xl_table_end_row, 1):
        # get parameter
        para_name = sheet_opt_doepara.cell(index_row, 1).value
        para_values = []

        # iterate over columns
        for index_column in range(xl_table_start_column, xl_table_end_col, 1):
            if not sheet_opt_doepara.cell(index_row, index_column).value is None:
                para_values.append(sheet_opt_doepara.cell(index_row, index_column).value)
            else:
                break

        # set parameter
        setattr(factory.doe_list, para_name, para_values)


def open_excel_file_parameter(file_path, pc_name):
    # open workbook
    workbook = openpyxl.load_workbook(file_path)
    # open worksheets
    sheet_names = workbook.get_sheet_names()
    sheet_opt_para = None
    sheet_opt_weights = None
    sheet_opt_doepara = None
    for sheet_name in sheet_names:
        # if sheet_name == pc_name:
        if sheet_name == "Remote":
            sheet_opt_para = workbook.get_sheet_by_name(sheet_name)
            # break
        elif sheet_name == "Weights":
            sheet_opt_weights = workbook.get_sheet_by_name(sheet_name)
        elif sheet_name == "ControlParameter":
            sheet_opt_doepara = workbook.get_sheet_by_name(sheet_name)
    # else:
        # print("Failure in: read optimization parameter from excel")
    return sheet_opt_para, sheet_opt_weights, sheet_opt_doepara
