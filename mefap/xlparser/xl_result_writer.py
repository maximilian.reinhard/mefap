import openpyxl
import socket
from datetime import datetime


def write_xl_results(results):
    ### transform result in correct form
    # get metaheuristic
    metaheuristic = results[0][1].optimization.metaheuristic_type.value

    # do the transformation
    result_list = structure_results(results, metaheuristic)

    ### do excel stuff
    # get time
    now = datetime.now()
    date_time = now.strftime("%Y%m%d")
    # get pc name
    pc_name = socket.gethostname()

    # build file name
    file_name = "//192.168.50.13/projekte/LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/MeFaP_SWD_Experiment_" + \
                pc_name + "_" + date_time + ".xlsx"

    """
    # home-desktop
    file_name = "D://LO_AiF(BVL)_MeFaP_2017/Projektbearbeitung/AP6_Experimente/MeFaP_SWD_Experiment_" + \
                pc_name + "_" + date_time + ".xlsx"

    # other pc's ?
        file_path = __file__
        file_path = file_path[:-35]
        file_name = file_path + "\\MeFaP_SWD_Experiment_" + pc_name + "_" + date_time + ".xlsx"
    """

    # open workbook
    workbook = openpyxl.Workbook()
    # open sheet
    sheet = workbook.active
    # rename sheet
    sheet_name = "metaheuristic" + str(metaheuristic)
    sheet.title = sheet_name
    # sheet.cell(row=1, column=1).value = 1
    # init row
    row = 1
    # row = result
    column = 1

    for content in result_list:
        for item in content:
            sheet.cell(row=row, column=column).value = item  # (result[0], column, item)
            column += 1
        else:
            row += 1
            column = 1

    # save workbook
    workbook.save(file_name)


def structure_results(results, metaheuristic):
    counter = 1
    contents = []
    experiment_counter = 1
    break_counter = 0
    aggregations = []
    aggregation_list = []
    aggregation_min_list = []

    for result in results:

        number_of_experiments = result[1].parameter["number_of_experiments"]

        # if npp was not successful, jump over entry
        for facility in result[1].facility_list:
            if facility.curr_position == [None, None]:
                break_counter += 1
                break
        else:
            # get parameter names of used metaheuristic
            optimization_list = []
            if metaheuristic == 0:
                opt_attr = ["tabu_length", "tabu_iteration", "opening", "nhs_methods",
                            "lrs_step_size", "mees_facility_range", "mees_step_size", "mees_area_difference",
                            "fas_order",
                            "rotation", "mirror", "lrs_step_size"]
            elif metaheuristic == 1:
                opt_attr = ["starting_temperature", "cooling_rate", "increment", "cooling_type", "opening",
                            "nhs_methods",
                            "lrs_step_size", "mees_facility_range", "mees_step_size", "mees_area_difference",
                            "fas_order",
                            "rotation", "mirror", "lrs_step_size"]
            elif metaheuristic == 4:
                opt_attr = ["population_size", "number_of_generations", "crossover_rate", "number_of_elite_parents",
                            "mutation_type", "mutation_rate", "mutation_max_shift_step", "mutation_max_exchange",
                            "rotation", "mirror", "lrs_step_size"]
            elif metaheuristic == 5:
                opt_attr = ["population_size", "number_of_generations", "crossover_rate", "number_of_elite_parents",
                            "mutation_type", "mutation_rate", "mutation_max_shift_step", "mutation_max_exchange",
                            "tabu_length", "tabu_iteration", "opening", "nhs_methods",
                            "lrs_step_size", "mees_facility_range", "mees_step_size", "mees_area_difference",
                            "fas_order",
                            "rotation", "mirror", "lrs_step_size"]
            elif metaheuristic == 6:
                opt_attr = ["population_size", "number_of_generations", "crossover_rate", "number_of_elite_parents",
                            "mutation_type", "mutation_rate", "mutation_max_shift_step", "mutation_max_exchange",
                            "starting_temperature", "cooling_rate", "increment", "cooling_type", "opening",
                            "nhs_methods",
                            "lrs_step_size", "mees_facility_range", "mees_step_size", "mees_area_difference",
                            "fas_order",
                            "rotation", "mirror", "lrs_step_size"]

            while opt_attr:
                attr_name = opt_attr[0]
                for attribute_name, attribute_value in vars(result[1].optimization).items():
                    if attribute_name == attr_name:

                        if attribute_name == "opening" or attribute_name == "cooling_type":
                            optimization_list.append(attribute_value.value)
                        elif attribute_name == "nhs_methods" or \
                                attribute_name == "mutation_type":
                            entry_tuple = ()
                            for entry in attribute_value:
                                entry_tuple += (entry.value,)  # (str(entry.value),)
                            optimization_list.append(str(entry_tuple))
                        elif attribute_name == "mees_facility_range":
                            optimization_list.append(str(attribute_value))
                        elif attribute_name == "rotation" or attribute_name == "mirror":
                            if attribute_value:
                                optimization_list.append(1)
                            else:
                                optimization_list.append(0)
                        else:
                            optimization_list.append(attribute_value)

                        # delete name from list
                        opt_attr.pop(0)

            # get weightings
            weighting_list = []
            weighting_attr = ["material_flow_length", "no_overlapping", "route_continuity", "land_use_degree",
                              "media_availability", "media_compatibility",
                              "lighting", "quiet", "vibration", "temperature", "cleanliness",
                              "direct_communication", "formal_communication"]
            while weighting_attr:
                weight_name = weighting_attr[0]
                for weighting_name, weighting_value in vars(result[1].weighting).items():
                    if weighting_name == weight_name:
                        weighting_list.append(weighting_value)
                        # delete name from list
                        weighting_attr.pop(0)

            # quamfab
            objective_list = []
            aggregation_help_list = []
            objective_attr = ["weight_sum",
                              "mf_distance", "mf_overlapping", "mf_constancy", "area_utilization",
                              "media_availability", "media_compatibility",
                              "illuminance", "noise", "vibration", "temperature", "cleanliness",
                              "direct_com", "formal_com"]
            while objective_attr:
                obj_name = objective_attr[0]
                for objective_name, objective_value in vars(result[1].quamfab).items():
                    if objective_name == obj_name:
                        if objective_name == "weight_sum":
                            objective_list.append(objective_value)
                            aggregation_help_list.append(objective_value)
                        else:
                            objective_list.append(objective_value[0])
                            # objective_list.append(objective_value[1])
                            aggregation_help_list.append(objective_value[0])

                        # delete name from list
                        objective_attr.pop(0)

            # quamfab_min
            objective_min_list = []
            aggregation_min_help_list = []
            objective_attr = ["mf_distance", "mf_overlapping", "mf_constancy", "area_utilization",
                              "media_availability", "media_compatibility",
                              "illuminance", "noise", "vibration", "temperature", "cleanliness",
                              "direct_com", "formal_com"]
            objective_attr_out = ["area_utilization", "media_compatibility", "illuminance", "noise"]
            while objective_attr:
                obj_name = objective_attr[0]
                for objective_name, objective_value in vars(result[1].quamfab_min).items():
                    if objective_name == obj_name:
                        if objective_name == "weight_sum":
                            objective_min_list.append(objective_value)
                            aggregation_min_help_list.append(objective_value)
                        elif objective_name in objective_attr_out:
                            objective_min_list.append(objective_value[0])
                            # objective_min_list.append(objective_value[1])
                            aggregation_min_help_list.append(objective_value[0])
                        else:
                            objective_min_list.append(objective_value[0])
                            aggregation_min_help_list.append(objective_value[0])

                        # add value to aggregation list

                        # delete name from list
                        objective_attr.pop(0)

            # add time stamp to excel output
            objective_min_list.append(result[1].parameter["time_stamp"])
            aggregation_min_help_list.append(result[1].parameter["time_stamp"])

            content = []
            content.extend(optimization_list)
            content.extend(weighting_list)
            content.extend(objective_list)
            content.extend(objective_min_list)
            # add some experiment parameters
            content.extend([number_of_experiments, counter])

            contents.append(content)
            aggregation_list.append(aggregation_help_list)
            aggregation_min_list.append(aggregation_min_help_list)

        if counter / number_of_experiments == experiment_counter:

            objective_list = []
            objective_min_list = []

            for objective_id in range(0, len(aggregation_help_list), 1):
                obj_sum = 0
                for item in aggregation_list:
                    obj_sum = obj_sum + item[objective_id]
                else:
                    obj_sum = obj_sum / (number_of_experiments - break_counter)
                    objective_list.append(obj_sum)

            for objective_id in range(0, len(aggregation_min_help_list), 1):
                obj_min_sum = 0
                for item in aggregation_min_list:
                    obj_min_sum = obj_min_sum + item[objective_id]
                else:
                    obj_min_sum = obj_min_sum / (number_of_experiments - break_counter)
                    objective_min_list.append(obj_min_sum)

            aggregation = []
            aggregation.extend(optimization_list)
            aggregation.extend(weighting_list)
            aggregation.extend(objective_list)
            aggregation.extend(objective_min_list)
            aggregation.extend([number_of_experiments, experiment_counter])

            aggregations.append(aggregation)

            # delete list contens
            aggregation_list = []
            aggregation_min_list = []
            experiment_counter = experiment_counter + 1
            break_counter = 0

        # increment counter
        counter = counter + 1

    result_list = []
    for aggregation in aggregations:
        result_list.append(aggregation)

    # add emtpy row
    result_list.append([])

    for content in contents:
        result_list.append(content)

    return result_list
